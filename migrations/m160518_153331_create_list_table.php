<?php

use yii\db\Migration;

/**
 * Handles the creation for table `catalog`.
 */
class m160518_153331_create_catalog_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('catalog', [
            'id'    => $this->primaryKey(),
            'title'  => $this->string(100),
            'price' => $this->integer(),
            'description' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('catalog');
    }
}
