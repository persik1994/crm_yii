<?php

use yii\db\Migration;

/**
 * Handles the creation for table `master`.
 */
class m160520_133829_create_master_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('master', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'telephone' => $this->string(15),
            'email' => $this->string(100),
            'address' => $this->string(200)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('master');
    }
}
