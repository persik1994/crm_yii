<?php

use yii\db\Migration;

/**
 * Handles the creation for table `order`.
 */
class m160520_154433_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'catalog_id' => $this->integer(),
            'name' => $this->string(100),
            'telephone' => $this->string(15),
            'email' => $this->string(100),
            'address' => $this->string(150),
            'message' => $this->string(300)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order');
    }
}
