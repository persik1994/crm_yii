<?php

use yii\db\Migration;

/**
 * Handles the creation for table `about_table`.
 */
class m160711_123733_create_about_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `about` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `type` INT NOT NULL,
            `title` VARCHAR (255) NOT NULL,
            `description` TEXT NOT NULL,
            PRIMARY KEY(`id`)
        )");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('about');
    }
}
