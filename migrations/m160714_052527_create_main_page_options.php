<?php

use yii\db\Migration;

/**
 * Handles the creation for table `main_page_options`.
 */
class m160714_052527_create_main_page_options extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute('CREATE TABLE IF NOT EXISTS `main_page_option` (
          `id` INT AUTO_INCREMENT,
          `main_animation_description` TEXT,
          `second_animation_description` TEXT,
          `stock1_description` TEXT,
          `stock2_description` TEXT,
          `stock3_description` TEXT,
          `stock4_description` TEXT,
          `stock1_image` VARCHAR (255),
          `stock2_image` VARCHAR (255),
          `stock3_image` VARCHAR (255),
          `stock4_image1` VARCHAR (255),
          `stock4_image2` VARCHAR (255),
          `stock4_image3` VARCHAR (255),
          `stock4_image4` VARCHAR (255),
          `stock4_image5` VARCHAR (255),
          PRIMARY KEY(`id`)
        )');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('main_page_option');
    }
}
