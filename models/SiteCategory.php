<?php

namespace app\models;

use Yii;
use app\models\SiteLang;
use app\models\SiteTranslate;

/**
 * This is the model class for table "site_category".
 *
 * @property integer $category_id
 * @property integer $list_id
 * @property integer $sort
 * @property integer $type
 */
class SiteCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_id', 'type'], 'required'],
            [['list_id', 'sort', 'type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'list_id' => 'List ID',
            'sort' => 'Sort',
            'type' => 'Type',
        ];
    }

    static function findCategory($category_id, $language = 1){
        $category = SiteCategory::findOne(['category_id'=>(int)$category_id]);
        if(isset($category)){
            $list = SiteList::findOne(['list_id'=>$category->list_id, 'status'=>1]);
            if(isset($list)){
                $translate = SiteTranslate::findOne(['list_id'=>$list->list_id, 'lang_id'=>(int)$language]);
                if(isset($translate)){
                    return array('translate'=>$translate,'category'=>$category,'list'=>$list);
                }
            }
        }
        return array();
    }
}
