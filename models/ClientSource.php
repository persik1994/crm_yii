<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "client_source".
 *
 * @property integer $source_id
 * @property string $name
 * @property integer $status
 */
class ClientSource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_source';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_id','name','status'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['name', 'status'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'source_id' => 'Source ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }
}
