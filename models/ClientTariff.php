<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "client_tariff".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $type_cargo_id
 * @property integer $tariff_id
 * @property double $number
 */
class ClientTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','client_id','type_cargo_id','tariff_id','number'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['client_id', 'type_cargo_id', 'tariff_id', 'number'], 'required'],
            [['client_id', 'type_cargo_id', 'tariff_id'], 'integer'],
            [['number'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'type_cargo_id' => 'Type Cargo ID',
            'tariff_id' => 'Tariff ID',
            'number' => 'Number',
        ];
    }
}
