<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "manager_plan".
 *
 * @property integer $plan_id
 * @property integer $manager_id
 * @property integer $type
 * @property string $count
 */
class ManagerPlan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manager_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_id','manager_id','type','count'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['manager_id', 'type', 'count'], 'required'],
            [['manager_id', 'type'], 'integer'],
            [['count'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => 'Plan ID',
            'manager_id' => 'Manager ID',
            'type' => 'Type',
            'count' => 'Count',
        ];
    }
}
