<?php

namespace app\models;

use Yii;
use app\models\Client;
use app\models\Cargo;
use app\models\Project;
use app\models\BuyingList;
use app\models\Shipment;
use app\models\Payments;
use app\models\ClientTariff;
use app\models\ShipmentCalculation;
use app\models\CurrencyCash;

/**
 * This is the model class for table "logs".
 *
 * @property integer $log_id
 * @property integer $manager_id
 * @property integer $post_id
 * @property integer $type
 * @property integer $action
 * @property integer $status
 * @property integer $access
 * @property integer $date
 * @property string $before
 * @property string $after
 */
class Logs extends \yii\db\ActiveRecord
{
    public static $types = array([
        0=>'None',
        1=>'Client',
        2=>'Cargo',
        3=>'Project',
        4=>'Buying',
        5=>'Shipment',
        6=>'Payment',
        7=>'Project Pricing',
        8=>'Shipment Calculation',
        9=>'Exchanges', //CurrencyCash
    ]);

    public static $actions = array([
        0=>'None',
        1=>'Create',
        2=>'Update',
        3=>'Delete',
        4=>'View'
    ]);

    public static $statuses = array([
        0=>'Normal',
        1=>'Alarm',
        2=>'Delete'
    ]);

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manager_id', 'post_id', 'type', 'action', 'status', 'access', 'date'], 'integer'],
            [['date'], 'required'],
            [['before', 'after'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'log_id' => 'Log ID',
            'manager_id' => 'Manager ID',
            'post_id' => 'Post ID',
            'type' => 'Type',
            'action' => 'Action',
            'status' => 'Status',
            'access' => 'Access',
            'date' => 'Date',
            'before' => 'Before',
            'after' => 'After',
        ];
    }

    public static function Create($post_id = 0, $model = array(), $type = 0, $action = 0){
        $before = '';
        $after = '';
        /////////////////////////
        $log = new Logs;
        $log->manager_id    = Yii::$app->user->identity->id;
        $log->post_id       = (int)$post_id;
        $log->type          = (int)$type;
        $log->action        = (int)$action;
        $log->date          = time();
        /////////////////////////
        if($action == 1){
            foreach ($model as $key => $value) {
                $after .= $key.'==='.$value.'; ';
            }
            $log->before = $before;
            $log->after  = $after;
        }
        else if($action == 2 || $action == 3){
            switch ($type) {
                case 1:
                    $client = Client::findOne(['client_id'=>(int)$post_id]);
                    if(isset($client)){
                        foreach ($client as $key => $value) {
                            $before .= $key.'==='.$value.'; ';
                        }
                    }
                break;

                case 2:
                    $cargo = Cargo::findOne(['cargo_id'=>(int)$post_id]);
                    if(isset($cargo)){
                        foreach ($cargo as $key => $value) {
                            $before .= $key.'==='.$value.'; ';
                        }
                    }
                break;

                case 3:
                    $project = Project::findOne(['project_id'=>(int)$post_id]);
                    if(isset($project)){
                        foreach ($project as $key => $value) {
                            $before .= $key.'==='.$value.'; ';
                        }
                    }
                break;

                case 4:
                    $buying = BuyingList::findOne(['buying_id'=>(int)$post_id]);
                    if(isset($buying)){
                        foreach ($buying as $key => $value) {
                            $before .= $key.'==='.$value.'; ';
                        }
                    }
                break;

                case 5:
                    $shipment = Shipment::findOne(['shipment_id'=>(int)$post_id]);
                    if(isset($shipment)){
                        foreach ($shipment as $key => $value) {
                            $before .= $key.'==='.$value.'; ';
                        }
                    }
                break;

                case 6:
                    $payment = Payments::findOne(['payment_id'=>(int)$post_id]);
                    if(isset($payment)){
                        foreach ($payment as $key => $value) {
                            $before .= $key.'==='.$value.'; ';
                        }
                    }
                break;

                case 7:
                    $tariff = ClientTariff::findOne(['id'=>(int)$post_id]);
                    if(isset($tariff)){
                        foreach ($tariff as $key => $value) {
                            $before .= $key.'==='.$value.'; ';
                        }
                    }
                break;

                case 8:
                    $calculation = ShipmentCalculation::findOne(['calculation_id'=>(int)$post_id]);
                    if(isset($calculation)){
                        foreach ($calculation as $key => $value) {
                            $before .= $key.'==='.$value.'; ';
                        }
                    }
                break;

                case 9:
                    $exchanges = CurrencyCash::findOne(['сurrency_id'=>(int)$post_id]);
                    if(isset($exchanges)){
                        foreach ($exchanges as $key => $value) {
                            $before .= $key.'==='.$value.'; ';
                        }
                    }
                break;
            }
            foreach ($model as $key => $value) {
                $after .= $key.'==='.$value.'; ';
            }
            $log->before = $before;
            $log->after  = $after;
        }

        $log->save();
    }
}