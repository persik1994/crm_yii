<?php

namespace app\models;

use Yii;
use app\models\ShipmentType;
use app\models\Payments;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "project".
 *
 * @property integer $project_id
 * @property integer $manager_id
 * @property integer $client_id
 * @property string $name
 * @property integer $date_create
 * @property integer $date_close
 * @property integer $project_type
 * @property integer $delivery_type
 * @property string $comment
 * @property string $total
 * @property string $profit
 * @property string $payed
 * @property string $must_pay
 * @property string $chargers
 * @property string $plan_bonuse
 * @property string $cargo_cost
 * @property string $insurance
 * @property string $insurance_sum
 * @property integer $status
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id','manager_id','client_id','name','date_create','date_close','project_type', 'delivery_type', 'comment', 'total', 'profit','payed','must_pay', 'chargers', 'plan_bonuse', 'cargo_cost', 'insurance', 'insurance_sum','status','buying_id'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['client_id', 'project_type', 'total', 'profit', 'status'], 'required'],
            [['client_id', 'manager_id', 'date_create', 'date_close', 'project_type', 'delivery_type', 'status','buying_id'], 'integer'],
            [['comment'], 'string'],
            [['total', 'profit','payed','must_pay', 'chargers', 'plan_bonuse', 'cargo_cost', 'insurance', 'insurance_sum'], 'number'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_id' => 'Project ID',
            'manager_id' => 'Manager',
            'client_id' => 'Client ID',
            'name' => 'Project Name',
            'date_create' => 'Created',
            'date_close' => 'Closed',
            'project_type' => 'Type',
            'delivery_type' => 'Delivery',
            'comment' => 'Comments',
            'total' => 'Total',
            'profit' => 'Profit',
            'payed' => 'Payed',
            'must_pay' => 'Must Pay',
            'chargers' => 'Chargers',
            'plan_bonuse' => 'Plan Bonuse',
            'cargo_cost' => 'Cargo Cost',
            'insurance' => 'Insurance',
            'insurance_sum' => 'Insurance Sum',
            'status' => 'Status',
            'buying_id' => 'Buying ID',
        ];
    }

    static function types(){
        return array(1 => 'Delivery', 2 => 'Money Transfer', 3 => 'Buying', 4 => 'Customs clearance');
    }

    public function type($id){
        switch ($id) {
            case 1:
                return Project::types()[1];
                break;
            case 2:
                return Project::types()[2];
                break;
            case 3:
                return Project::types()[3];
                break;
            case 4:
                return Project::types()[4];
                break;
            default:
                return 'No type';
                break;
        }
    }

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_id']);
    }

    public function getDevivery()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_id']);
    }

    public function getShipmentType()
    {
        return $this->hasOne(ShipmentType::className(), ['type_id' => 'delivery_type']);
    }

    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    public function getBuying()
    {
        return $this->hasOne(BuyingList::className(), ['buying_id' => 'buying_id']);
    }

    static function projectDropMenu($name, $code, $type = 0, $placeholder = ''){
        $input = '';
        $btns_sort = '';
        $dropdown = '<div class="dropdown-divider"></div>
                      <a class="dropdown-item dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$placeholder.'</a>
                      <div class="dropdown-menu dropdown-menu-table dropdown-menu-table-scroll" aria-labelledby="navbarDropdownMenuLink">';
        switch ($type) {
            case 'text':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'date':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" id="form-choose-date" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'date2':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" id="form-choose-date-2" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'select':
                switch ($code) {
                    case 'manager_id':
                        $managers = Manager::find()->all();
                        if(isset($managers)){
                            $input .= $dropdown;
                            foreach ($managers as $manager) {
                                $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="'.$manager->manager_id.'">'.$manager->username.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'project_type':
                        $types = Project::types();
                        if(isset($types)){
                            $input .= $dropdown;
                            foreach ($types as $type_key => $type_name) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$type_key.'">'.$type_name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'delivery_type':
                        $types_delivery = ShipmentType::find()->all();
                        if(isset($types_delivery)){
                            $input .= $dropdown;
                            foreach ($types_delivery as $type_delivery) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$type_delivery->type_id.'">'.$type_delivery->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                }
                break;
        }
        if($code != 'comment'){
            $btns_sort = '<a class="dropdown-item" href="?sort='.$code.'">Sort by Asc</a><a class="dropdown-item" href="?sort=-'.$code.'">Sort by Desc</a>';
        }

        return '<div class="dropdown dropdown-table">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      '.$name.'
                    </a>
                    <div class="dropdown-menu dropdown-menu-table" aria-labelledby="dropdownMenuLink">
                      '.$btns_sort.'
                      '.$input.'
                    </div>
                </div>';
    }

    static function SearchSession(){
        $session = Yii::$app->session;

        if(Yii::$app->request->get('project_id')){
            $session['project_project_id'] = (int)str_replace('PAY-', '', Yii::$app->request->get('project_id'));
        }
        if(Yii::$app->request->get('manager_id')){
            $session['project_manager_id'] = Yii::$app->request->get('manager_id');
        }
        if(Yii::$app->request->get('client_id')){
            $session['project_client_id'] = (int)str_replace('88-', '', Yii::$app->request->get('client_id'));
        }
        if(Yii::$app->request->get('date_create')){
            $session['project_date_create'] = (int)strtotime(Yii::$app->request->get('date_create'));
        }
        if(Yii::$app->request->get('date_close')){
            $session['project_date_close'] = (int)strtotime(Yii::$app->request->get('date_close'));
        }
        if(Yii::$app->request->get('project_type')){
            $session['project_project_type'] = Yii::$app->request->get('project_type');
        }
        if(Yii::$app->request->get('delivery_type')){
            $session['project_delivery_type'] = Yii::$app->request->get('delivery_type');
        }
        if(Yii::$app->request->get('name')){
            $session['project_name'] = Yii::$app->request->get('name');
        }
        if(Yii::$app->request->get('comment')){
            $session['project_comment'] = Yii::$app->request->get('comment');
        }
        if(isset($_GET['total'])){
            $session['project_total'] = (int)$_GET['total'];
        }
        if(isset($_GET['profit'])){
            $session['project_profit'] = (int)$_GET['profit'];
        }
        if(isset($_GET['payed'])){
            $session['project_payed'] = (int)$_GET['payed'];
        }
        if(isset($_GET['must_pay'])){
            $session['project_must_pay'] = (int)$_GET['must_pay'];
        }
        if(isset($_GET['chargers'])){
            $session['project_chargers'] = (int)$_GET['chargers'];
        }
        if(isset($_GET['plan_bonuse'])){
            $session['project_plan_bonuse'] = (int)$_GET['plan_bonuse'];
        }
        if(Yii::$app->request->get('buying_id')){
            $session['project_buying_id'] = (int)str_replace('BL-', '', Yii::$app->request->get('buying_id'));
        }
        ////////////////////////
        Project::SearchQuery();
    }

    static function SearchQuery($type = 0){
        $session = Yii::$app->session;

        ///////////
        if(Yii::$app->user->identity->role >= 10){
            $query = Project::find()->where(['status'=>[0,1]]);
        }
        else if(Yii::$app->user->identity->id == 34){ //Andrei_N
            $query = Project::find()->where(['status'=>[0,1],'manager_id'=>[34,25,27,28,30,31,26,29,32]]);
        }
        else{
            $query = Project::find()->where(['status'=>[0,1],'manager_id'=>Yii::$app->user->identity->id]);
        }
        /////////
        if($type == 1){
            $query->andWhere('total <= payed');
        }
        else if($type == 2){
            $query->andWhere('total > payed');
        }
        /////////
        if($session->has('project_total')){ 
            $query->andFilterWhere(['=', 'total', $session['project_total']]);
        }
        if($session->has('project_profit')){ 
            $query->andFilterWhere(['=', 'profit', $session['project_profit']]);
        }
        if($session->has('project_payed')){ 
            $query->andFilterWhere(['=', 'payed', $session['project_payed']]);
        }
        if($session->has('project_must_pay')){ 
            $query->andFilterWhere(['=', 'must_pay', $session['project_must_pay']]);
        }
        if($session->has('project_chargers')){ 
            $query->andFilterWhere(['=', 'chargers', $session['project_chargers']]);
        }
        if($session->has('project_plan_bonuse')){ 
            $query->andFilterWhere(['=', 'plan_bonuse', $session['project_plan_bonuse']]);
        }
        if($session->has('project_project_id')){ 
            $query->andFilterWhere(['=', 'project_id', $session['project_project_id']]);
        }
        if($session->has('project_manager_id')){ 
            $query->andFilterWhere(['=', 'manager_id', $session['project_manager_id']]);
        }
        if($session->has('project_client_id')){
            $query->andFilterWhere(['=', 'client_id', $session['project_client_id']]);
        }
        if($session->has('project_project_type')){
            $query->andFilterWhere(['=', 'project_type', $session['project_project_type']]);
        }
        if($session->has('project_delivery_type')){
            $query->andFilterWhere(['=', 'delivery_type', $session['project_delivery_type']]);
        }
        if($session->has('project_name')){
            $query->andFilterWhere(['like', 'name', $session['project_name']]);
        }
        if($session->has('project_comment')){
            $query->andFilterWhere(['like', 'comment', $session['project_comment']]);
        }
        if($session->has('project_buying_id')){
            $query->andFilterWhere(['=', 'buying_id', $session['project_buying_id']]);
        }
        //////date////
        if($session->has('project_date_create')){
            $query->andFilterWhere(['>=', 'date_create', (int)strtotime(date('d.m.Y 00:00',$session['project_date_create']))]);
            $query->andFilterWhere(['<=', 'date_create', (int)strtotime(date('d.m.Y 23:59',$session['project_date_create']))]);
        }
        //////date////
        if($session->has('project_date_close')){
            $query->andFilterWhere(['>=', 'date_close', (int)strtotime(date('d.m.Y 00:00',$session['project_date_close']))]);
            $query->andFilterWhere(['<=', 'date_close', (int)strtotime(date('d.m.Y 23:59',$session['project_date_close']))]);
        }
        ////////////////////////
        return $query;
    }

    static function SearchShowBtn(){
        $session = Yii::$app->session;
        $btnAll = Project::SearchSortBtn();
        foreach ($session as $type => $value){
            switch ($type) {
                case 'project_project_id':
                    $btnAll .= Project::SearchHtmlBtn($type, 'PJ-'.$value);
                    break;
                
                case 'project_manager_id':
                    $manager = Manager::findOne(['manager_id'=>$value]);
                    $btnAll .= Project::SearchHtmlBtn($type, $manager->username);
                    break;

                case 'project_client_id':
                    $btnAll .= Project::SearchHtmlBtn($type, '88-'.$value);
                    break;
                
                case 'project_date_create':
                    $btnAll .= Project::SearchHtmlBtn($type, date('d.M.Y H:i',$value));
                    break;
                
                case 'project_date_close':
                    $btnAll .= Project::SearchHtmlBtn($type, date('d.M.Y H:i',$value));
                    break;

                case 'project_project_type': ///
                    $btnAll .= Project::SearchHtmlBtn($type, Project::type($value));
                    break;

                case 'project_delivery_type':
                    $delivery_type = ShipmentType::findOne(['type_id'=>$value]);
                    $btnAll .= Project::SearchHtmlBtn($type, $delivery_type->name);
                    break;
                
                case 'project_name':
                    $btnAll .= Project::SearchHtmlBtn($type, 'Name - '.$value);
                    break;
                
                case 'project_comment':
                    $btnAll .= Project::SearchHtmlBtn($type, 'Comment - '.$value);
                    break;
                
                case 'project_total':
                    $btnAll .= Project::SearchHtmlBtn($type, 'Total - '.$value);
                    break;
                
                case 'project_profit':
                    $btnAll .= Project::SearchHtmlBtn($type, 'Profit - '.$value);
                    break;
                
                case 'project_payed':
                    $btnAll .= Project::SearchHtmlBtn($type, 'Payed - '.$value);
                    break;
                
                case 'project_must_pay':
                    $btnAll .= Project::SearchHtmlBtn($type, 'Must pay - '.$value);
                    break;
                
                case 'project_chargers':
                    $btnAll .= Project::SearchHtmlBtn($type, 'Chargers - '.$value);
                    break;
                
                case 'project_plan_bonuse':
                    $btnAll .= Project::SearchHtmlBtn($type, 'Plan bonuse - '.$value);
                    break;

                case 'project_buying_id':
                    $btnAll .= Project::SearchHtmlBtn($type, 'BL-'.$value);
                    break;
            }
        }
        if(!empty($btnAll)){
            $btnAll .= '<p class="btn-search-filter-show btn-search-filter-delete">Remove all filters <a href="/projects/searchdeleteall"><i class="fas fa-times"></i></a></p>';
        }
        return $btnAll;
    }

    static function DeleteAllSession(){
        $session = Yii::$app->session;
        foreach ($session as $type => $value){
            if(stristr($type, 'project_') !== FALSE) {
                $session->remove($type);
            }
        }
    }

    static function SearchSortBtn(){
        $row = '';
        if(Yii::$app->request->get('sort')){
            if(stristr(Yii::$app->request->get('sort'), '-') === FALSE) {
                $sort = 'ASC';
                if(isset(Project::attributeLabels()[Yii::$app->request->get('sort')])){
                    $row = Project::attributeLabels()[Yii::$app->request->get('sort')];
                }
            }
            else{
                $sort = 'DESC';
                if(isset(Project::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))])){
                    $row = Project::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))];
                }
            }
            return '<p class="btn-search-filter-show">Sort '.$sort.' '.$row.'<a href="index?sort="><i class="fas fa-times"></i></a></p>';
        }
        return '';
    }

    static function SearchHtmlBtn($type, $name){
        return '<p class="btn-search-filter-show">'.$name.'<a href="/projects/searchdelete?type='.$type.'&sort='.Yii::$app->request->get('sort').'"><i class="fas fa-times"></i></a></p>';
    }

    static function queryCargersBackground($project_id = 0){
        $project = Project::findOne(['project_id'=>(int)$project_id]);
        if(isset($project)){
            if($project->chargers == 0){
                $project->chargers = $project->total - $project->profit;
            }
            if(($project->chargers > 0 && $project->client_id > 0)){
                $paymentsClientProjectOutcome = Payments::find()->where(['payment_type'=>2,'out_source_id'=>4,'project_id'=>$project->project_id])->all();
                $paid = 0;
                foreach ($paymentsClientProjectOutcome as $payment) {
                    $paid += $payment->cash_usd;
                }
                $mathProfit = $paid - $project->chargers;
                if($project->profit != $mathProfit && $project->total != $paid){
                    return 'background-color: #ff8787;'.$project->total.';'.$paid.';';
                }
            }
        }
    }

    static function MathTotalPricing($project_id = 0){
        ////////общий вес и обьем
        /////kg
        $kg_general_all = 0;
        $kg_electro_all = 0;
        $kg_brand_all = 0;
        /////m3
        $m3_general_all = 0;
        $m3_electro_all = 0;
        $m3_brand_all = 0;

        ////////общая цена по типам
        /////kg
        $price_kg_general_all = 0;
        $price_kg_electro_all = 0;
        $price_kg_brand_all = 0;
        /////m3
        $price_m3_general_all = 0;
        $price_m3_electro_all = 0;
        $price_m3_brand_all = 0;
        ////////
        $project = Project::findOne(['project_id'=>$project_id]);
        if(isset($project) && $project->project_type == 1 && $project->project_id > 1197){ //применять только для проектов из типом delivery и только новым 

            $cargos = Cargo::find()->where(['project_id'=>$project->project_id])->all(); //ищем все карго прикрепленные к проекту
            if(isset($cargos)){
                foreach ($cargos as $cargo) {
                    if($cargo->client_id == $project->client_id){ //прорка соответствие клиента к проекту
                        if($cargo->shipment_type_id == 5 || $cargo->shipment_type_id == 9){ //если карго Railway тип доставки
                            switch ($cargo->type_id) { //прогон по типам груза електронига, главный или бренд в кубе
                                case 1:
                                    $m3_general_all += $cargo->volume;
                                    break;
                                case 2:
                                    $m3_electro_all += $cargo->volume;
                                    break;
                                case 3:
                                    $m3_brand_all += $cargo->volume;
                                    break;
                            }
                            //////проверка есть ли цена на данный тип
                            $price = ClientTariff::find()->where(['project_id'=>$project->project_id, 'client_id'=>$project->client_id, 'tariff_id'=>3,'type_cargo_id'=>$cargo->type_id])->count();
                            if($price == 0){ //если нет, тогда создать автоматически
                                Project::NewPrice($project->client_id, $project->project_id, $cargo->type_id, 3);
                            }
                            //////////
                        }
                        else{ //если любой другой тип доставки
                            switch ($cargo->type_id) { //прогон по типам груза електронига, главный или бренд КГ
                                case 1:
                                    $kg_general_all += $cargo->weight;
                                    break;
                                case 2:
                                    $kg_electro_all += $cargo->weight;
                                    break;
                                case 3:
                                    $kg_brand_all += $cargo->weight;
                                    break;
                            }
                            //////проверка есть ли цена на данный тип
                            $price = ClientTariff::find()->where(['project_id'=>$project->project_id, 'client_id'=>$project->client_id, 'tariff_id'=>1,'type_cargo_id'=>$cargo->type_id])->count();
                            if($price == 0){ //если нет, тогда создать автоматически
                                Project::NewPrice($project->client_id, $project->project_id, $cargo->type_id, 1);
                            }
                            //////////
                        }
                    }
                }
            }

            ////считаю общую стоимость pricing в проекте, по типу KG
            $prices_kg = ClientTariff::find()->where(['project_id'=>$project->project_id, 'client_id'=>$project->client_id, 'tariff_id'=>1])->all();
            if(isset($prices_kg)){
                foreach ($prices_kg as $price_kg) {
                    switch ($price_kg->type_cargo_id) { //прогон по типам груза електронига, главный или бренд в КГ
                        case 1:
                            $price_kg_general_all += $price_kg->number;
                            break;
                        case 2:
                            $price_kg_electro_all += $price_kg->number;
                            break;
                        case 3:
                            $price_kg_brand_all += $price_kg->number;
                            break;
                    }
                }
            }

            ////считаю общую стоимость pricing в проекте, по типу m3
            $prices_m3 = ClientTariff::find()->where(['project_id'=>$project->project_id, 'client_id'=>$project->client_id, 'tariff_id'=>3])->all();
            if(isset($prices_m3)){
                foreach ($prices_m3 as $price_m3) {
                    switch ($price_m3->type_cargo_id) { //прогон по типам груза електронига, главный или бренд в m3
                        case 1:
                            $price_m3_general_all += $price_m3->number;
                            break;
                        case 2:
                            $price_m3_electro_all += $price_m3->number;
                            break;
                        case 3:
                            $price_m3_brand_all += $price_m3->number;
                            break;
                    }
                }
            }

            ///сумирую все в тотал
            $total_kg = ($kg_general_all * $price_kg_general_all) + ($kg_electro_all * $price_kg_electro_all) + ($kg_brand_all * $price_kg_brand_all);
            $total_m3 = ($m3_general_all * $price_m3_general_all) + ($m3_electro_all * $price_m3_electro_all) + ($m3_brand_all * $price_m3_brand_all);

            //замена total в project
            $project->total = ($total_kg + $total_m3) + $project->insurance_sum;
            $project->update(false);
        }
    }

    static function NewPrice($client_id, $project_id, $type_id, $tariff_id){
        $new_price                 = new ClientTariff;
        $new_price->client_id      = $client_id;
        $new_price->project_id     = $project_id;
        $new_price->type_cargo_id  = $type_id;
        $new_price->tariff_id      = $tariff_id;
        $new_price->number         = 0;
        $new_price->save();
    }
}
