<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "cargo_type".
 *
 * @property integer $type_id
 * @property string $name
 * @property string $flag
 * @property double $price
 * @property integer $status
 */
class CargoType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cargo_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id','name','flag','price','status'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            [['name', 'flag', 'price', 'status'], 'required'],
            [['price'], 'number'],
            [['status'], 'integer'],
            [['name', 'flag'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'name' => 'Name',
            'flag' => 'Flag',
            'price' => 'Price',
            'status' => 'Status',
        ];
    }
}
