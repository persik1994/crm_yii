<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "client_messenger".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $messenger_id
 * @property string $number
 */
class ClientMessenger extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_messenger';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','client_id','messenger_id','number'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['client_id', 'messenger_id', 'number'], 'required'],
            [['client_id', 'messenger_id'], 'integer'],
            [['number'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'messenger_id' => 'Messenger ID',
            'number' => 'Number',
        ];
    }
}
