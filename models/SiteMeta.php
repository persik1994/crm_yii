<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_meta".
 *
 * @property integer $meta_id
 * @property integer $type
 * @property integer $post_id
 * @property integer $lang_id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $meta_url
 */
class SiteMeta extends \yii\db\ActiveRecord
{
    public $types = array(
        1=>'home',
        2=>'about',
        3=>'services',
        4=>'blogs',
        5=>'service',
        6=>'blog'
    );
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'lang_id'], 'required'],
            [['type', 'post_id', 'lang_id'], 'integer'],
            [['meta_title', 'meta_description', 'meta_keywords', 'meta_url'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'meta_id' => 'ID',
            'type' => 'Тип',
            'post_id' => 'ID поста',
            'lang_id' => 'Язык',
            'meta_title' => 'Title',
            'meta_description' => 'Description',
            'meta_keywords' => 'Keywords',
            'meta_url' => 'URL',
        ];
    }
}
