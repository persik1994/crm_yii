<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency_history".
 *
 * @property integer $currency_history_id
 * @property integer $currency_id
 * @property integer $manager_id
 * @property integer $date
 * @property string $currency
 */
class CurrencyHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency_id', 'manager_id', 'date', 'currency'], 'required'],
            [['currency_id', 'manager_id', 'date'], 'integer'],
            [['currency'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_history_id' => 'Currency History ID',
            'currency_id' => 'Currency ID',
            'manager_id' => 'Manager ID',
            'date' => 'Date',
            'currency' => 'Currency',
        ];
    }

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_id']);
    }

    public function getExchange()
    {
        return $this->hasOne(CurrencyCash::className(), ['сurrency_id' => 'currency_id']);
    }
}
