<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Manager;
use yii\helpers\HtmlPurifier;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Login extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    public function rules()
    {
        return [
            [['username','password','rememberMe'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword']
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()){ //если нет ошибок в валидации
            $user = $this->getUser(); //получаем пользователя для сравнения пароля
            if(!$user || !$user->validatePassword(HtmlPurifier::process($this->password))){
                $this->addError($attribute, 'login or password incorrect!');
            }
        }
    }

    public function getUser()
    {
        return Manager::findOne(['username'=>HtmlPurifier::process($this->username)]);//получение пользователя по email
    }


}
