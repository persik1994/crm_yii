<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_list".
 *
 * @property integer $list_id
 * @property integer $status
 */
class SiteList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'list_id' => 'List ID',
            'status' => 'Status',
        ];
    }
}
