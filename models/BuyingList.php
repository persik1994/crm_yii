<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "buying_list".
 *
 * @property integer $buying_id
 * @property integer $date_create
 * @property integer $client_id
 * @property integer $manager_id
 * @property integer $manager_buy_id
 * @property integer $project_id
 * @property integer $status_id
 * @property string $total
 * @property string $comment
 * @property string $file
 * @property integer $date_file
 * @property string $file_two
 * @property integer $date_file_two
 * @property integer $status
 */
class BuyingList extends \yii\db\ActiveRecord
{
    public static function name_sesion() {
        return 'manager_';
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buying_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_create', 'client_id', 'manager_id', 'manager_buy_id', 'project_id', 'status_id','status'], 'integer'],
            [['total'], 'number'],
            [['comment', 'file', 'file_two'], 'required'],
            [['name', 'comment', 'file', 'file_two', 'description_file', 'description_file_two'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'buying_id' => 'Buying code',
            'date_create' => 'Date Create',
            'client_id' => 'Client',
            'manager_id' => 'Manager',
            'manager_buy_id' => 'Manager Buying',
            'project_id' => 'Project',
            'status_id' => 'Status',
            'total' => 'Total',
            'name' => 'Buying Name',
            'comment' => 'Comment',
            'file' => 'File',
            'date_file' => 'File Manager',
            'description_file' => 'Description File',
            'file_two' => 'File Two',
            'date_file_two' => 'File Client',
            'description_file_two' => 'Description File Two',
            'status' => 'Status',
        ];
    }

    static function statuses(){
        return array(1 => 'Checking', 2 => 'Recheck', 3 => 'Buiyng', 4 => 'Received', 5 => 'Checked', 6 => 'Bought');
    }

    public function status($id){
        switch ($id) {
            case 1:
                return BuyingList::statuses()[1];
                break;
            case 2:
                return BuyingList::statuses()[2];
                break;
            case 3:
                return BuyingList::statuses()[3];
                break;
            case 4:
                return BuyingList::statuses()[4];
                break;
            case 5:
                return BuyingList::statuses()[5];
                break;
            case 6:
                return BuyingList::statuses()[6];
                break;
            default:
                return 'No type';
                break;
        }
    }

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_id']);
    }

    public function getManagerbuying()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_buy_id']);
    }

    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    static function projectDropMenu($name, $code, $type = 0, $placeholder = ''){
        $input = '';
        $btns_sort = '';
        $dropdown = '<div class="dropdown-divider"></div>
                      <a class="dropdown-item dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$placeholder.'</a>
                      <div class="dropdown-menu dropdown-menu-table dropdown-menu-table-scroll" aria-labelledby="navbarDropdownMenuLink">';
        switch ($type) {
            case 'text':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'date':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" id="form-choose-date" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'date2':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" id="form-choose-date-2" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'select':
                switch ($code) {
                    case 'manager_id':
                        $managers = Manager::find()->all();
                        if(isset($managers)){
                            $input .= $dropdown;
                            foreach ($managers as $manager) {
                                $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="'.$manager->manager_id.'">'.$manager->username.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'manager_buy_id':
                        $managers = Manager::find()->all();
                        if(isset($managers)){
                            $input .= $dropdown;
                            foreach ($managers as $manager) {
                                $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="'.$manager->manager_id.'">'.$manager->username.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'status_id':
                        $statuses = BuyingList::statuses();
                        if(isset($statuses)){
                            $input .= $dropdown;
                            foreach ($statuses as $status_key => $status_name) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$status_key.'">'.$status_name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                }
                break;
        }
        if($code != 'comment'){
            $btns_sort = '<a class="dropdown-item" href="?sort='.$code.'">Sort by Asc</a><a class="dropdown-item" href="?sort=-'.$code.'">Sort by Desc</a>';
        }

        return '<div class="dropdown dropdown-table">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      '.$name.'
                    </a>
                    <div class="dropdown-menu dropdown-menu-table" aria-labelledby="dropdownMenuLink">
                      '.$btns_sort.'
                      '.$input.'
                    </div>
                </div>';
    }

    static function SearchSession(){
        $session = Yii::$app->session;

        if(Yii::$app->request->get('buying_id')){
            $session[BuyingList::name_sesion().'buying_id'] = (int)str_replace('BL-', '', Yii::$app->request->get('buying_id'));
        }
        if(Yii::$app->request->get('date_create')){
            $session[BuyingList::name_sesion().'date_create'] = (int)strtotime(Yii::$app->request->get('date_create'));
        }
        if(Yii::$app->request->get('client_id')){
            $session[BuyingList::name_sesion().'client_id'] = (int)str_replace('88-', '', Yii::$app->request->get('client_id'));
        }
        if(Yii::$app->request->get('manager_id')){
            $session[BuyingList::name_sesion().'manager_id'] = (int)Yii::$app->request->get('manager_id');
        }
        if(Yii::$app->request->get('manager_buy_id')){
            $session[BuyingList::name_sesion().'manager_buy_id'] = (int)Yii::$app->request->get('manager_buy_id');
        }
        if(Yii::$app->request->get('project_id')){
            $session[BuyingList::name_sesion().'project_id'] = (int)str_replace('PJ-', '', Yii::$app->request->get('project_id'));
        }
        if(Yii::$app->request->get('status_id')){
            $session[BuyingList::name_sesion().'status_id'] = (int)Yii::$app->request->get('status_id');
        }
        if(Yii::$app->request->get('total')){
            $session[BuyingList::name_sesion().'total'] = (int)Yii::$app->request->get('total');
        }
        if(Yii::$app->request->get('name')){
            $session[BuyingList::name_sesion().'name'] = (int)Yii::$app->request->get('name');
        }
        if(Yii::$app->request->get('date_file')){
            $session[BuyingList::name_sesion().'date_file'] = (int)Yii::$app->request->get('date_file');
        }
        if(Yii::$app->request->get('date_file_two')){
            $session[BuyingList::name_sesion().'date_file_two'] = (int)Yii::$app->request->get('date_file_two');
        }
        if(Yii::$app->request->get('comment')){
            $session[BuyingList::name_sesion().'comment'] = Yii::$app->request->get('comment');
        }
        ////////////////////////
        BuyingList::SearchQuery();
    }

    static function SearchQuery(){
        $session = Yii::$app->session;

        ///////////
        if(Yii::$app->user->identity->role >= 10){
            $query = BuyingList::find()->where(['status'=>[0,1]]);
        }
        else if(Yii::$app->user->identity->id == 34){ //Andrei_N
            $query = BuyingList::find()->where(['status'=>[0,1]]);
            $query->andFilterWhere(['or', //добавити OR
                ['manager_id'=>[34,25,27,28,30,31,26,29,32]],
                ['manager_buy_id'=>[34,25,27,28,30,31,26,29,32]]
            ]); 
        }
        else{
            $query = BuyingList::find()->where(['status'=>[0,1]]);
            $query->andFilterWhere(['or',
                ['=','manager_id',Yii::$app->user->identity->id],
                ['=','manager_buy_id',Yii::$app->user->identity->id]
            ]);
        }
        /////////

        if($session->has(BuyingList::name_sesion().'buying_id')){ 
            $query->andFilterWhere(['=', 'buying_id', $session[BuyingList::name_sesion().'buying_id']]);
        }
        if($session->has(BuyingList::name_sesion().'client_id')){ 
            $query->andFilterWhere(['=', 'client_id', $session[BuyingList::name_sesion().'client_id']]);
        }
        if($session->has(BuyingList::name_sesion().'manager_id')){ 
            $query->andFilterWhere(['=', 'manager_id', $session[BuyingList::name_sesion().'manager_id']]);
        }
        if($session->has(BuyingList::name_sesion().'manager_buy_id')){ 
            $query->andFilterWhere(['=', 'manager_buy_id', $session[BuyingList::name_sesion().'manager_buy_id']]);
        }
        if($session->has(BuyingList::name_sesion().'project_id')){ 
            $query->andFilterWhere(['=', 'project_id', $session[BuyingList::name_sesion().'project_id']]);
        }
        if($session->has(BuyingList::name_sesion().'status_id')){ 
            $query->andFilterWhere(['=', 'status_id', $session[BuyingList::name_sesion().'status_id']]);
        }
        if($session->has(BuyingList::name_sesion().'total')){ 
            $query->andFilterWhere(['=', 'total', $session[BuyingList::name_sesion().'total']]);
        }
        if($session->has(BuyingList::name_sesion().'name')){
            $query->andFilterWhere(['like', 'name', $session[BuyingList::name_sesion().'name']]);
        }
        if($session->has(BuyingList::name_sesion().'comment')){
            $query->andFilterWhere(['like', 'comment', $session[BuyingList::name_sesion().'comment']]);
        }
        //////date////
        if($session->has(BuyingList::name_sesion().'date_create')){
            $query->andFilterWhere(['>=', 'date_create', (int)strtotime(date('d.m.Y 00:00',$session[BuyingList::name_sesion().'date_create']))]);
            $query->andFilterWhere(['<=', 'date_create', (int)strtotime(date('d.m.Y 23:59',$session[BuyingList::name_sesion().'date_create']))]);
        }
        //////date////
        if($session->has(BuyingList::name_sesion().'date_file')){
            $query->andFilterWhere(['>=', 'date_file', (int)strtotime(date('d.m.Y 00:00',$session[BuyingList::name_sesion().'date_file']))]);
            $query->andFilterWhere(['<=', 'date_file', (int)strtotime(date('d.m.Y 23:59',$session[BuyingList::name_sesion().'date_file']))]);
        }
        //////date////
        if($session->has(BuyingList::name_sesion().'date_file_two')){
            $query->andFilterWhere(['>=', 'date_file_two', (int)strtotime(date('d.m.Y 00:00',$session[BuyingList::name_sesion().'date_file_two']))]);
            $query->andFilterWhere(['<=', 'date_file_two', (int)strtotime(date('d.m.Y 23:59',$session[BuyingList::name_sesion().'date_file_two']))]);
        }
        ////////////////////////
        return $query;
    }

    static function SearchShowBtn(){
        $session = Yii::$app->session;
        $btnAll = BuyingList::SearchSortBtn();
        foreach ($session as $type => $value){
            switch ($type) {
                case BuyingList::name_sesion().'buying_id':
                    $btnAll .= BuyingList::SearchHtmlBtn($type, 'BL-'.$value);
                    break;

                case BuyingList::name_sesion().'client_id':
                    $btnAll .= BuyingList::SearchHtmlBtn($type, '88-'.$value);
                    break;
                
                case BuyingList::name_sesion().'manager_id':
                    $manager = Manager::findOne(['manager_id'=>$value]);
                    $btnAll .= BuyingList::SearchHtmlBtn($type, $manager->username);
                    break;
                
                case BuyingList::name_sesion().'manager_buy_id':
                    $manager = Manager::findOne(['manager_id'=>$value]);
                    $btnAll .= BuyingList::SearchHtmlBtn($type, $manager->username);
                    break;

                case BuyingList::name_sesion().'project_id':
                    $btnAll .= BuyingList::SearchHtmlBtn($type, 'PJ-'.$value);
                    break;

                case BuyingList::name_sesion().'status_id': ///
                    $btnAll .= BuyingList::SearchHtmlBtn($type, BuyingList::status($value));
                    break;
                
                case BuyingList::name_sesion().'total':
                    $btnAll .= BuyingList::SearchHtmlBtn($type, 'Total - '.$value);
                    break;
                
                case BuyingList::name_sesion().'name':
                    $btnAll .= BuyingList::SearchHtmlBtn($type, 'Buying name - '.$value);
                    break;
                
                case BuyingList::name_sesion().'comment':
                    $btnAll .= BuyingList::SearchHtmlBtn($type, 'Comment - '.$value);
                    break;
                
                case BuyingList::name_sesion().'date_create':
                    $btnAll .= BuyingList::SearchHtmlBtn($type, date('d.M.Y',$value));
                    break;
                
                case BuyingList::name_sesion().'date_file':
                    $btnAll .= BuyingList::SearchHtmlBtn($type, 'File Manager - '.date('d.M.Y',$value));
                    break;
                
                case BuyingList::name_sesion().'date_file_two':
                    $btnAll .= BuyingList::SearchHtmlBtn($type, 'File Client - '.date('d.M.Y',$value));
                    break;
            }
        }
        if(!empty($btnAll)){
            $btnAll .= '<p class="btn-search-filter-show btn-search-filter-delete">Remove all filters <a href="/buying/searchdeleteall"><i class="fas fa-times"></i></a></p>';
        }
        return $btnAll;
    }

    static function DeleteAllSession(){
        $session = Yii::$app->session;
        foreach ($session as $type => $value){
            if(stristr($type, BuyingList::name_sesion()) !== FALSE) {
                $session->remove($type);
            }
        }
    }

    static function SearchSortBtn(){
        $row = '';
        if(Yii::$app->request->get('sort')){
            if(stristr(Yii::$app->request->get('sort'), '-') === FALSE) {
                $sort = 'ASC';
                if(isset(BuyingList::attributeLabels()[Yii::$app->request->get('sort')])){
                    $row = BuyingList::attributeLabels()[Yii::$app->request->get('sort')];
                }
            }
            else{
                $sort = 'DESC';
                if(isset(BuyingList::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))])){
                    $row = BuyingList::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))];
                }
            }
            return '<p class="btn-search-filter-show">Sort '.$sort.' '.$row.'<a href="index?sort="><i class="fas fa-times"></i></a></p>';
        }
        return '';
    }

    static function SearchHtmlBtn($type, $name){
        return '<p class="btn-search-filter-show">'.$name.'<a href="/buying/searchdelete?type='.$type.'&sort='.Yii::$app->request->get('sort').'"><i class="fas fa-times"></i></a></p>';
    }
}
