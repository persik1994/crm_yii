<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification_option".
 *
 * @property integer $manager_id
 * @property integer $telegram
 * @property integer $telegram_token
 * @property integer $email
 * @property integer $sms
 */
class NotificationOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manager_id'], 'required'],
            [['manager_id', 'telegram', 'telegram_token', 'email', 'sms'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'manager_id' => 'Manager ID',
            'telegram' => 'Telegram',
            'telegram_token' => 'Telegram Token',
            'email' => 'Email',
            'sms' => 'Sms',
        ];
    }
}
