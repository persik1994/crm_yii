<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "courier_service".
 *
 * @property integer $courier_id
 * @property string $name
 * @property double $price
 * @property integer $status
 */
class CourierService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courier_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courier_id','name','price','status'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['name', 'price', 'status'], 'required'],
            [['price'], 'number'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'courier_id' => 'Courier ID',
            'name' => 'Name',
            'price' => 'Price',
            'status' => 'Status',
        ];
    }
}
