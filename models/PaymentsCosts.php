<?php

namespace app\models;

use Yii;
use app\models\PaymentsSource;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "payments".
 *
 * @property integer $payment_id
 * @property integer $client_id
 * @property integer $manager_id
 * @property integer $branch_id
 * @property integer $source_id
 * @property integer $out_source_id
 * @property integer $date_payment
 * @property integer $payment_status
 * @property integer $payment_type
 * @property string $cash_usd
 * @property string $cash_fact
 * @property integer $cash_fact_currency
 * @property integer $status
 * @property string $comment
 * @property string $invoice
 * @property string $bankbill
 * @property integer $project_id
 */
class PaymentsCosts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id','client_id','manager_id','branch_id','source_id','out_source_id','date_payment', 'payment_status', 'payment_type', 'cash_usd', 'cash_fact', 'cash_fact_currency', 'status', 'comment','invoice','bankbill','project_id'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['manager_id', 'branch_id', 'payment_status', 'payment_type', 'cash_usd', 'cash_fact', 'cash_fact_currency', 'status'], 'required'],
            [['client_id', 'manager_id', 'branch_id', 'date_payment', 'payment_status', 'payment_type', 'cash_fact_currency', 'status', 'source_id', 'out_source_id','project_id'], 'integer'],
            [['cash_usd', 'cash_fact'], 'number'],
            [['comment', 'invoice', 'bankbill'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_id' => 'Payment ID',
            'client_id' => 'Client ID',
            'manager_id' => 'Manager',
            'branch_id' => 'Branch ID',
            'date_payment' => 'Data payment',
            'payment_status' => 'Payment status',
            'payment_type' => 'Payment type',
            'cash_usd' => 'Paid USD',
            'cash_fact' => 'Fact paid',
            'cash_fact_currency' => 'Fact paid currency',
            'status' => 'Status',
            'comment' => 'Payment comment',
            'invoice' => 'Invoice',
            'bankbill' => 'Bankbill',
            'source_id' => 'Income source',
            'out_source_id' => 'Outcome source',
            'project_id' => 'Project id'
        ];
    }

    public function branch($id){
        switch ($id) {
            case 1:
                return 'China';
                break;
            case 2:
                return 'Ukraine';
                break;
            default:
                return 'Noname';
                break;
        }
    }

    public function status($id){
        switch ($id) {
            case 1:
                return 'Executed';
                break;
            case 2:
                return 'New';
                break;
            default:
                return 'No status';
                break;
        }
    }

    static function types(){
        return array(1 => 'Income', 2 => 'Outcome');
    }

    public function type($id){
        switch ($id) {
            case 1:
                return Payments::types()[1];
                break;
            case 2:
                return Payments::types()[2];
                break;
            default:
                return 'No type';
                break;
        }
    }

    static function outSources(){
        return array(
            1 => 'Card', 
            2 => 'Cash', 
            3 => 'TT', 
            //4 => 'Project'
        );
    }

    public function outSource($id){
        switch ($id) {
            case 1:
                return 'Card';
                break;
            case 2:
                return 'Cash';
                break;
            case 3:
                return 'TT';
                break;
            //case 4:
            //    return 'Project';
            //    break;
            default:
                return 'No source';
                break;
        }
    }

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_id']);
    }

    public function getSource()
    {
        return $this->hasOne(PaymentsSource::className(), ['source_id' => 'source_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(CurrencyCash::className(), ['сurrency_id' => 'cash_fact_currency']);
    }

    static function currentSales(){
        $summ = 0;
        $allPayments = Payments::find()->where(['manager_id'=>Yii::$app->user->identity->id, 'status'=>1])->andWhere('date_payment >= '.strtotime('01'.date('.m.Y ').'00:00:00'))->all();
        foreach ($allPayments as $payment) {
            if($payment->out_source_id == 0)
                $summ += $payment->cash_usd;
            else
                $summ -= $payment->cash_usd;
        }
        return $summ;
    }

    static function paymentDropMenu($name, $code, $type = 0, $placeholder = ''){
        $input = '';
        $btns_sort = '';
        $dropdown = '<div class="dropdown-divider"></div>
                      <a class="dropdown-item dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$placeholder.'</a>
                      <div class="dropdown-menu dropdown-menu-table dropdown-menu-table-scroll" aria-labelledby="navbarDropdownMenuLink">';
        switch ($type) {
            case 'text':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'date':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" id="form-choose-date" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'select':
                switch ($code) {
                    case 'manager_id':
                        $managers = Manager::find()->all();
                        if(isset($managers)){
                            $input .= $dropdown;
                            foreach ($managers as $manager) {
                                $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="'.$manager->manager_id.'">'.$manager->username.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'payment_type':
                        $types = Payments::types();
                        if(isset($types)){
                            $input .= $dropdown;
                            foreach ($types as $type_key => $type_name) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$type_key.'">'.$type_name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'source_id':
                        $PaymentsSource = PaymentsSource::find()->all();
                        if(isset($PaymentsSource)){
                            $input .= $dropdown;
                            foreach ($PaymentsSource as $source) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$source->source_id.'">'.$source->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'out_source_id':
                        $outSources = Payments::outSources();
                        if(isset($outSources)){
                            $input .= $dropdown;
                            foreach ($outSources as $outSource_key => $outSource_name) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$outSource_key.'">'.$outSource_name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'cash_fact_currency':
                        $CurrencyCash = CurrencyCash::find()->orderBy(['сurrency_id' => SORT_ASC])->all();
                        if(isset($CurrencyCash)){
                            $input .= $dropdown;
                            foreach ($CurrencyCash as $currency) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$currency->сurrency_id.'">'.$currency->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'status':
                        $input .= $dropdown;
                        $input .= '<a class="dropdown-item search-menu-click" data-type="status" data-id="1">Approved</a>';
                        $input .= '<a class="dropdown-item search-menu-click" data-type="status" data-id="2">No Approved</a>';
                        $input .= '</div>';
                        break;
                }
                break;
        }
        if($code != 'local_track' && $code != 'description'){
            $btns_sort = '<a class="dropdown-item" href="?sort='.$code.'">Sort by Asc</a><a class="dropdown-item" href="?sort=-'.$code.'">Sort by Desc</a>';
        }

        return '<div class="dropdown dropdown-table">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      '.$name.'
                    </a>
                    <div class="dropdown-menu dropdown-menu-table" aria-labelledby="dropdownMenuLink">
                      '.$btns_sort.'
                      '.$input.'
                    </div>
                </div>';
    }

    static function SearchSession(){
        $session = Yii::$app->session;

        if(Yii::$app->request->get('payment_id')){
            $session['costs_payment_id'] = (int)str_replace('PAY-', '', Yii::$app->request->get('payment_id'));
        }
        if(Yii::$app->request->get('manager_id')){
            $session['costs_manager_id'] = Yii::$app->request->get('manager_id');
        }
        if(Yii::$app->request->get('client_id')){
            $session['costs_client_id'] = (int)str_replace('88-', '', Yii::$app->request->get('client_id'));
        }
        if(Yii::$app->request->get('date_payment')){
            $session['costs_date_payment'] = (int)strtotime(Yii::$app->request->get('date_payment'));
        }
        if(Yii::$app->request->get('payment_type')){
            $session['costs_payment_type'] = Yii::$app->request->get('payment_type');
        }
        if(Yii::$app->request->get('source_id')){
            $session['costs_source_id'] = Yii::$app->request->get('source_id');
        }
        if(Yii::$app->request->get('out_source_id')){
            $session['costs_out_source_id'] = Yii::$app->request->get('out_source_id');
        }
        if(Yii::$app->request->get('project_id')){
            $session['costs_project_id'] = (int)str_replace('PJ-', '', Yii::$app->request->get('project_id'));
        }
        if(Yii::$app->request->get('cash_usd')){
            $session['costs_cash_usd'] = Yii::$app->request->get('cash_usd');
        }
        if(Yii::$app->request->get('cash_fact')){
            $session['costs_cash_fact'] = Yii::$app->request->get('cash_fact');
        }
        if(Yii::$app->request->get('cash_fact_currency')){
            $session['costs_cash_fact_currency'] = Yii::$app->request->get('cash_fact_currency');
        }    
        if(Yii::$app->request->get('status')){
            if($session['costs_status'] == 2)
                $session['costs_status'] = 0;
            else
                $session['costs_status'] = Yii::$app->request->get('status');
        }
        if(Yii::$app->request->get('comment')){
            $session['costs_comment'] = Yii::$app->request->get('comment');
        }
        ////////////////////////
        Payments::SearchQuery();
    }

    static function SearchQuery(){
        $session = Yii::$app->session;

        ///////////
        $query = Payments::find()->where(['out_source_id'=>5]);
        /////////

        if($session->has('costs_payment_id')){ 
            $query->andFilterWhere(['=', 'payment_id', $session['costs_payment_id']]);
        }
        if($session->has('costs_manager_id')){ 
            $query->andFilterWhere(['=', 'manager_id', $session['costs_manager_id']]);
        }
        if($session->has('costs_client_id')){
            $query->andFilterWhere(['=', 'client_id', $session['costs_client_id']]);
        }
        if($session->has('costs_payment_type')){
            $query->andFilterWhere(['=', 'payment_type', $session['costs_payment_type']]);
        }
        if($session->has('costs_source_id')){
            $query->andFilterWhere(['=', 'source_id', $session['costs_source_id']]);
        }
        if($session->has('costs_out_source_id')){
            $query->andFilterWhere(['=', 'out_source_id', $session['costs_out_source_id']]);
        }
        if($session->has('costs_project_id')){
            $query->andFilterWhere(['=', 'project_id', $session['costs_project_id']]);
        }
        if($session->has('costs_cash_usd')){
            $query->andFilterWhere(['like', 'cash_usd', $session['costs_cash_usd']]);
        }
        if($session->has('costs_cash_fact')){
            $query->andFilterWhere(['like', 'cash_fact', $session['costs_cash_fact']]);
        }
        if($session->has('costs_cash_fact_currency')){
            $query->andFilterWhere(['=', 'cash_fact_currency', $session['costs_cash_fact_currency']]);
        }
        if($session->has('costs_status')){
            $query->andFilterWhere(['=', 'status', $session['costs_status']]);
        }else{
            $query->andWhere(['status'=>[0,1]]);
        }
        if($session->has('costs_comment')){
            $query->andFilterWhere(['like', 'comment', $session['costs_comment']]);
        }
        //////date////
        if($session->has('costs_date_payment')){
            $query->andFilterWhere(['>=', 'date_payment', (int)strtotime(date('d.m.Y 00:00',$session['costs_date_payment']))]);
            $query->andFilterWhere(['<=', 'date_payment', (int)strtotime(date('d.m.Y 23:59',$session['costs_date_payment']))]);
        }
        ////////////////////////
        return $query;
    }

    static function SearchShowBtn(){
        $session = Yii::$app->session;
        $btnAll = Payments::SearchSortBtn();
        foreach ($session as $type => $value){
            switch ($type) {
                case 'costs_payment_id':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'PAY-'.$value);
                    break;
                
                case 'costs_manager_id':
                    $manager = Manager::findOne(['manager_id'=>$value]);
                    $btnAll .= Payments::SearchHtmlBtn($type, $manager->username);
                    break;

                case 'costs_client_id':
                    $btnAll .= Payments::SearchHtmlBtn($type, '88-'.$value);
                    break;

                case 'costs_payment_type':
                    $btnAll .= Payments::SearchHtmlBtn($type, Payments::type($value));
                    break;

                case 'costs_source_id':
                    $source = PaymentsSource::findOne(['source_id'=>$value]);
                    $btnAll .= Payments::SearchHtmlBtn($type, $source->name);
                    break;
                
                case 'costs_out_source_id':
                    $btnAll .= Payments::SearchHtmlBtn($type, Payments::outSource($value));
                    break;
                
                case 'costs_project_id':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'PJ - '.$value);
                    break;
                
                case 'costs_cash_usd':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'Cash usd - '.$value);
                    break;
                
                case 'costs_cash_fact':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'Cash fact - '.$value);
                    break;
                
                case 'costs_cash_fact_currency':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'Currency - '.$value);
                    break;
                
                case 'costs_comment':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'Comment - '.$value);
                    break;
                
                case 'costs_status':
                    if($value == 2)
                        $btnAll .= Payments::SearchHtmlBtn($type, 'No Approved');
                    else 
                        $btnAll .= Payments::SearchHtmlBtn($type, 'Approved');
                    break;
                
                case 'costs_date_payment':
                    $btnAll .= Payments::SearchHtmlBtn($type, date('d.M.Y H:i',$value));
                    break;
            }
        }
        if(!empty($btnAll)){
            $btnAll .= '<p class="btn-search-filter-show btn-search-filter-delete">Remove all filters <a href="/payments/searchdeleteall"><i class="fas fa-times"></i></a></p>';
        }
        return $btnAll;
    }

    static function DeleteAllSession(){
        $session = Yii::$app->session;
        foreach ($session as $type => $value){
            if(stristr($type, 'costs_') !== FALSE) {
                $session->remove($type);
            }
        }
    }

    static function SearchSortBtn(){
        $row = '';
        if(Yii::$app->request->get('sort')){
            if(stristr(Yii::$app->request->get('sort'), '-') === FALSE) {
                $sort = 'ASC';
                if(isset(Payments::attributeLabels()[Yii::$app->request->get('sort')])){
                    $row = Payments::attributeLabels()[Yii::$app->request->get('sort')];
                }
            }
            else{
                $sort = 'DESC';
                if(isset(Payments::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))])){
                    $row = Payments::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))];
                }
            }
            return '<p class="btn-search-filter-show">Sort '.$sort.' '.$row.'<a href="index?sort="><i class="fas fa-times"></i></a></p>';
        }
        return '';
    }

    static function SearchHtmlBtn($type, $name){
        return '<p class="btn-search-filter-show">'.$name.'<a href="/payments/searchdelete?type='.$type.'&sort='.Yii::$app->request->get('sort').'"><i class="fas fa-times"></i></a></p>';
    }
}
