<?php

namespace app\models;

use Yii;
use app\models\PaymentsSource;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "payments".
 *
 * @property integer $payment_id
 * @property integer $client_id
 * @property integer $manager_id
 * @property integer $branch_id
 * @property integer $source_id
 * @property integer $out_source_id
 * @property integer $date_payment
 * @property integer $payment_status
 * @property integer $payment_type
 * @property string $cash_usd
 * @property string $cash_fact
 * @property integer $cash_fact_currency
 * @property integer $status
 * @property string $comment
 * @property string $invoice
 * @property string $bankbill
 * @property integer $project_id
 * @property string $balance
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id','client_id','manager_id','branch_id','source_id','out_source_id','date_payment', 'payment_status', 'payment_type', 'cash_usd', 'cash_fact', 'cash_fact_currency', 'status', 'comment','invoice','bankbill','project_id', 'balance'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['manager_id', 'branch_id', 'payment_status', 'payment_type', 'cash_usd', 'cash_fact', 'cash_fact_currency', 'status'], 'required'],
            [['client_id', 'manager_id', 'branch_id', 'date_payment', 'payment_status', 'payment_type', 'cash_fact_currency', 'status', 'source_id', 'out_source_id','project_id'], 'integer'],
            [['cash_usd', 'cash_fact', 'balance'], 'number'],
            [['comment', 'invoice', 'bankbill'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_id' => 'Payment ID',
            'client_id' => 'Client ID',
            'manager_id' => 'Manager',
            'branch_id' => 'Branch ID',
            'date_payment' => 'Data payment',
            'payment_status' => 'Payment status',
            'payment_type' => 'Payment type',
            'cash_usd' => 'Paid USD',
            'cash_fact' => 'Fact paid',
            'cash_fact_currency' => 'Fact paid currency',
            'status' => 'Status',
            'comment' => 'Payment comment',
            'invoice' => 'Invoice',
            'bankbill' => 'Bankbill',
            'source_id' => 'Income source',
            'out_source_id' => 'Outcome source',
            'project_id' => 'Project id',
            'chargers_project' => 'Chargers USD',
            'client_paid' => 'Client paid USD',
            'project_profit' => 'Project profit',
            'balance' => 'Balance after',
            'balance_only' => 'Balance',
        ];
    }

    public function branch($id){
        switch ($id) {
            case 1:
                return Payments::branchs()[1];
                break;
            case 2:
                return Payments::branchs()[2];
                break;
            case 3:
                return Payments::branchs()[3];
                break;
            default:
                return 'Noname';
                break;
        }
    }

    static function branchs(){
        return array(1 => 'China', 2 => 'Ukraine', 3 => 'Kiev Warehouse');
    }

    public function status($id){
        switch ($id) {
            case 1:
                return 'Executed';
                break;
            case 2:
                return 'New';
                break;
            default:
                return 'No status';
                break;
        }
    }

    static function types(){
        return array(1 => 'Income', 2 => 'Outcome');
    }

    public function type($id){
        switch ($id) {
            case 1:
                return Payments::types()[1];
                break;
            case 2:
                return Payments::types()[2];
                break;
            default:
                return 'No type';
                break;
        }
    }

    static function outSources(){
        return array(
            1 => 'Card', 
            2 => 'Cash', 
            3 => 'TT', 
            4 => 'Списание с баланса'
            //4 => 'Project'
        );
    }

    public function outSource($id){
        switch ($id) {
            case 1:
                return 'Card';
                break;
            case 2:
                return 'Cash';
                break;
            case 3:
                return 'TT';
                break;
            case 4:
                return 'Списание с баланса';
                //return 'Project';
                break;
            default:
                return 'No source';
                break;
        }
    }

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_id']);
    }

    public function getSource()
    {
        return $this->hasOne(PaymentsSource::className(), ['source_id' => 'source_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(CurrencyCash::className(), ['сurrency_id' => 'cash_fact_currency']);
    }

    static function currentSales(){
        $summ = 0;
        $allPayments = Payments::find()->where(['manager_id'=>Yii::$app->user->identity->id, 'status'=>1])->andWhere('date_payment >= '.strtotime('01'.date('.m.Y ').'00:00:00'))->all();
        foreach ($allPayments as $payment) {
            if($payment->out_source_id == 0)
                $summ += $payment->cash_usd;
            else
                $summ -= $payment->cash_usd;
        }
        return $summ;
    }

    static function paymentDropMenu($name, $code, $type = 0, $placeholder = ''){
        $input = '';
        $btns_sort = '';
        $dropdown = '<div class="dropdown-divider"></div>
                      <a class="dropdown-item dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$placeholder.'</a>
                      <div class="dropdown-menu dropdown-menu-table dropdown-menu-table-scroll" aria-labelledby="navbarDropdownMenuLink">';
        switch ($type) {
            case 'text':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'date':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" id="form-choose-date" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'select':
                switch ($code) {
                    case 'manager_id':
                        $managers = Manager::find()->all();
                        if(isset($managers)){
                            $input .= $dropdown;
                            foreach ($managers as $manager) {
                                $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="'.$manager->manager_id.'">'.$manager->username.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'payment_type':
                        $types = Payments::types();
                        if(isset($types)){
                            $input .= $dropdown;
                            foreach ($types as $type_key => $type_name) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$type_key.'">'.$type_name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'source_id':
                        $PaymentsSource = PaymentsSource::find()->all();
                        if(isset($PaymentsSource)){
                            $input .= $dropdown;
                            foreach ($PaymentsSource as $source) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$source->source_id.'">'.$source->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'out_source_id':
                        $outSources = Payments::outSources();
                        if(isset($outSources)){
                            $input .= $dropdown;
                            foreach ($outSources as $outSource_key => $outSource_name) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$outSource_key.'">'.$outSource_name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'cash_fact_currency':
                        $CurrencyCash = CurrencyCash::find()->orderBy(['сurrency_id' => SORT_ASC])->all();
                        if(isset($CurrencyCash)){
                            $input .= $dropdown;
                            foreach ($CurrencyCash as $currency) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$currency->сurrency_id.'">'.$currency->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'status':
                        $input .= $dropdown;
                        $input .= '<a class="dropdown-item search-menu-click" data-type="status" data-id="1">Approved</a>';
                        $input .= '<a class="dropdown-item search-menu-click" data-type="status" data-id="2">No Approved</a>';
                        $input .= '</div>';
                        break;
                }
                break;
        }
        if($code != 'local_track' && $code != 'description'){
            $btns_sort = '<a class="dropdown-item" href="?sort='.$code.'">Sort by Asc</a><a class="dropdown-item" href="?sort=-'.$code.'">Sort by Desc</a>';
        }

        return '<div class="dropdown dropdown-table">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      '.$name.'
                    </a>
                    <div class="dropdown-menu dropdown-menu-table" aria-labelledby="dropdownMenuLink">
                      '.$btns_sort.'
                      '.$input.'
                    </div>
                </div>';
    }

    static function SearchSession(){
        $session = Yii::$app->session;

        if(Yii::$app->request->get('payment_id')){
            $session['payment_payment_id'] = (int)str_replace('PAY-', '', Yii::$app->request->get('payment_id'));
        }
        if(Yii::$app->request->get('manager_id')){
            $session['payment_manager_id'] = Yii::$app->request->get('manager_id');
        }
        if(Yii::$app->request->get('client_id')){
            $session['payment_client_id'] = (int)str_replace('88-', '', Yii::$app->request->get('client_id'));
        }
        if(Yii::$app->request->get('date_payment')){
            $session['payment_date_payment'] = (int)strtotime(Yii::$app->request->get('date_payment'));
        }
        if(Yii::$app->request->get('payment_type')){
            $session['payment_payment_type'] = Yii::$app->request->get('payment_type');
        }
        if(Yii::$app->request->get('source_id')){
            $session['payment_source_id'] = Yii::$app->request->get('source_id');
        }
        if(Yii::$app->request->get('out_source_id')){
            $session['payment_out_source_id'] = Yii::$app->request->get('out_source_id');
        }
        if(Yii::$app->request->get('project_id')){
            $session['payment_project_id'] = (int)str_replace('PJ-', '', Yii::$app->request->get('project_id'));
        }
        if(Yii::$app->request->get('cash_usd')){
            $session['payment_cash_usd'] = Yii::$app->request->get('cash_usd');
        }
        if(Yii::$app->request->get('cash_fact')){
            $session['payment_cash_fact'] = Yii::$app->request->get('cash_fact');
        }
        if(Yii::$app->request->get('cash_fact_currency')){
            $session['payment_cash_fact_currency'] = Yii::$app->request->get('cash_fact_currency');
        }    
        if(Yii::$app->request->get('status')){
            $session['payment_status'] = Yii::$app->request->get('status');
        }
        if(Yii::$app->request->get('comment')){
            $session['payment_comment'] = Yii::$app->request->get('comment');
        }
        ////////////////////////
        Payments::SearchQuery();
    }

    static function SearchQuery(){
        $session = Yii::$app->session;

        ///////////
        if(Yii::$app->user->identity->role >= 10){
            $query = Payments::find();
        }
        else if(Yii::$app->user->identity->id == 34){ //Andrei_N
            $query = Payments::find()->where(['manager_id'=>[34,25,27,28,30,31,26,29,32]]);
        }
        else{
            $query = Payments::find()->where(['manager_id'=>Yii::$app->user->identity->id]);
        }
        /////////

        // if(Yii::$app->user->identity->role < 10){
        //     $query->andFilterWhere(['and',
        //         ['>','cash_usd',0],
        //         ['>','cash_fact',0]
        //     ]);
        // }

        $query->andFilterWhere(['!=', 'out_source_id', 4]);
        $query->andFilterWhere(['!=', 'out_source_id', 5]);
        $query->andFilterWhere(['!=', 'status', 3]);

        /////////

        if($session->has('payment_payment_id')){ 
            $query->andFilterWhere(['=', 'payment_id', $session['payment_payment_id']]);
        }
        if($session->has('payment_manager_id')){ 
            $query->andFilterWhere(['=', 'manager_id', $session['payment_manager_id']]);
        }
        if($session->has('payment_client_id')){
            $query->andFilterWhere(['=', 'client_id', $session['payment_client_id']]);
        }
        if($session->has('payment_payment_type')){
            $query->andFilterWhere(['=', 'payment_type', $session['payment_payment_type']]);
        }
        if($session->has('payment_source_id')){
            $query->andFilterWhere(['=', 'source_id', $session['payment_source_id']]);
        }
        if($session->has('payment_out_source_id')){
            $query->andFilterWhere(['=', 'out_source_id', $session['payment_out_source_id']]);
        }
        if($session->has('payment_project_id')){
            $query->andFilterWhere(['=', 'project_id', $session['payment_project_id']]);
        }
        if($session->has('payment_cash_usd')){
            $query->andFilterWhere(['like', 'cash_usd', $session['payment_cash_usd']]);
        }
        if($session->has('payment_cash_fact')){
            $query->andFilterWhere(['like', 'cash_fact', $session['payment_cash_fact']]);
        }
        if($session->has('payment_cash_fact_currency')){
            $query->andFilterWhere(['=', 'cash_fact_currency', $session['payment_cash_fact_currency']]);
        }
        if(isset($session['payment_status'])){
            if($session['payment_status'] == 1){
                $query->andFilterWhere(['=', 'status', 1]);
            }
            else if($session['payment_status'] == 2){
                $query->andFilterWhere(['=', 'status', 0]);
            }
        }else{
            $query->andWhere(['status'=>[0,1]]);
        }
        if($session->has('payment_comment')){
            $query->andFilterWhere(['like', 'comment', $session['payment_comment']]);
        }
        //////date////
        if($session->has('payment_date_payment')){
            $query->andFilterWhere(['>=', 'date_payment', (int)strtotime(date('d.m.Y 00:00',$session['payment_date_payment']))]);
            $query->andFilterWhere(['<=', 'date_payment', (int)strtotime(date('d.m.Y 23:59',$session['payment_date_payment']))]);
        }
        ////sort
        if(!Yii::$app->request->get('sort')){
            $query->orderBy(['date_payment'=>SORT_DESC, 'payment_id'=>SORT_DESC]);
        }
        ////////////////////////
        return $query;
    }

    static function SearchShowBtn(){
        $session = Yii::$app->session;
        $btnAll = Payments::SearchSortBtn();
        foreach ($session as $type => $value){
            switch ($type) {
                case 'payment_payment_id':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'PAY-'.$value);
                    break;
                
                case 'payment_manager_id':
                    $manager = Manager::findOne(['manager_id'=>$value]);
                    $btnAll .= Payments::SearchHtmlBtn($type, $manager->username);
                    break;

                case 'payment_client_id':
                    $btnAll .= Payments::SearchHtmlBtn($type, '88-'.$value);
                    break;

                case 'payment_payment_type':
                    $btnAll .= Payments::SearchHtmlBtn($type, Payments::type($value));
                    break;

                case 'payment_source_id':
                    $source = PaymentsSource::findOne(['source_id'=>$value]);
                    $btnAll .= Payments::SearchHtmlBtn($type, $source->name);
                    break;
                
                case 'payment_out_source_id':
                    $btnAll .= Payments::SearchHtmlBtn($type, Payments::outSource($value));
                    break;
                
                case 'payment_project_id':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'PJ - '.$value);
                    break;
                
                case 'payment_cash_usd':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'Cash usd - '.$value);
                    break;
                
                case 'payment_cash_fact':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'Cash fact - '.$value);
                    break;
                
                case 'payment_cash_fact_currency':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'Currency - '.$value);
                    break;
                
                case 'payment_comment':
                    $btnAll .= Payments::SearchHtmlBtn($type, 'Comment - '.$value);
                    break;
                
                case 'payment_status':
                    if($value == 2)
                        $btnAll .= Payments::SearchHtmlBtn($type, 'No Approved');
                    else 
                        $btnAll .= Payments::SearchHtmlBtn($type, 'Approved');
                    break;
                
                case 'payment_date_payment':
                    $btnAll .= Payments::SearchHtmlBtn($type, date('d.M.Y H:i',$value));
                    break;
            }
        }
        if(!empty($btnAll)){
            $btnAll .= '<p class="btn-search-filter-show btn-search-filter-delete">Remove all filters <a href="/payments/searchdeleteall"><i class="fas fa-times"></i></a></p>';
        }
        return $btnAll;
    }

    static function DeleteAllSession(){
        $session = Yii::$app->session;
        foreach ($session as $type => $value){
            if(stristr($type, 'payment_') !== FALSE) {
                $session->remove($type);
            }
        }
    }

    static function SearchSortBtn(){
        $row = '';
        if(Yii::$app->request->get('sort')){
            if(stristr(Yii::$app->request->get('sort'), '-') === FALSE) {
                $sort = 'ASC';
                if(isset(Payments::attributeLabels()[Yii::$app->request->get('sort')])){
                    $row = Payments::attributeLabels()[Yii::$app->request->get('sort')];
                }
            }
            else{
                $sort = 'DESC';
                if(isset(Payments::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))])){
                    $row = Payments::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))];
                }
            }
            return '<p class="btn-search-filter-show">Sort '.$sort.' '.$row.'<a href="index?sort="><i class="fas fa-times"></i></a></p>';
        }
        return '';
    }

    static function SearchHtmlBtn($type, $name){
        return '<p class="btn-search-filter-show">'.$name.'<a href="/payments/searchdelete?type='.$type.'&sort='.Yii::$app->request->get('sort').'"><i class="fas fa-times"></i></a></p>';
    }

    static function MathPayMoneyMovementProject($project_id = 0){
        $summ = 0;
        if(!empty($project_id)){
            $payments = Payments::find()->where(['project_id'=>$project_id, 'out_source_id'=>4])->all();
            if(isset($payments)){
                foreach ($payments as $payment) {
                    $summ += $payment->cash_usd;
                }
                if($summ > 0){
                    return '-'.$summ;
                }
            }
        }
        return $summ;
    }

    public static function payments_all_math_outcome_balance(){
        $projects = Project::find()->all();
        if(isset($projects)){
            foreach ($projects as $project) {
                $find_withdrow = Payments::find()->where(['project_id'=>$project->project_id,'payment_type'=>2,'out_source_id'=>4])->orderBy(['payment_id'=>SORT_DESC])->one();
                if(isset($find_withdrow)){
                    $findPayment = Payments::find()->where(['project_id'=>$project->project_id,'payment_type'=>2,'out_source_id'=>2])->orderBy(['payment_id'=>SORT_DESC])->one();
                    if(isset($findPayment)){
                        $findPayment->balance = $find_withdrow->balance;
                        $findPayment->update(false);
                    }
                }

            }
        }
    }

    static function PayOutCartNotProject_IsTransfer($project_id = 0){
        $data = ' | ';
        if(!empty($project_id)){
            $project = Project::findOne(['project_id'=>(int)$project_id]);
            if(isset($project)){
                $client = Client::findOne(['client_id'=>$project->client_id]);
                if(isset($client)){
                    ////просчет paid client
                    $payments_withdrow = Payments::find()->where('project_id = '.$project->project_id.' AND out_source_id = 4')->all();
                    $paid_all = 0;
                    foreach ($payments_withdrow as $paid) {
                        $paid_all += $paid->cash_usd;
                    }
                    ////просчет must pay
                    $must_pay = -1 * ($project->total - $paid_all);
                    ////money transfer - math chargers
                    if($project->project_type == 2){
                        $project->chargers = $project->total - $project->profit;
                    }
                    ///заміна must pay and paid client
                    $project->payed = $paid_all;
                    $project->must_pay = $must_pay;
                    if($project->must_pay == 0 && $project->total > 0){
                        $find_withdrow = Payments::find()->where(['project_id'=>$project->project_id,'payment_type'=>2,'out_source_id'=>4])->orderBy(['payment_id'=>SORT_DESC])->one();
                        // if(isset($find_withdrow)){
                        //     $project->date_close = $find_withdrow->date_payment;
                        // }else{
                        //     $project->date_close = $project->date_create;
                        // }
                        //$project->status = 1;
                        $findPayment = Payments::find()->where(['project_id'=>$project->project_id,'payment_type'=>2,'out_source_id'=>2])->orderBy(['payment_id'=>SORT_DESC])->one();
                        if(isset($findPayment)){
                            $findPayment->date_payment = strtotime(date('d-m-Y 2:00',$project->date_close));
                            if($findPayment->status == 3){
                                $back_id = $findPayment->payment_id;
                                $findPayment->status = 0;
                                $findPayment->balance = $find_withdrow->balance;
                                if($findPayment->save()){
                                    Logs::Create($findPayment->payment_id, $findPayment, 6, 1);
                                    //////удалити старий
                                    $pay_delete = Payments::findOne(['payment_id'=>$back_id]);
                                    Logs::Create($pay_delete->payment_id, $pay_delete, 6, 3);
                                    $pay_delete->delete();
                                }
                            }else{
                                //$findPayment->status = 1;
                                //$findPayment->update();
                            }
                        }
                    }else{
                        //$project->date_close = 0;
                        //$project->status = 0;
                        $findPayment = Payments::find()->where(['project_id'=>$project->project_id,'payment_type'=>2,'out_source_id'=>2])->orderBy(['payment_id'=>SORT_DESC])->one();
                        if(isset($findPayment)){
                            $findPayment->date_payment = strtotime(date('d-m-Y 2:00',$findPayment->date_payment));
                            if($findPayment->status != 1){
                                $findPayment->status = 0;//status = 3; //скрыть
                            }
                            Logs::Create($findPayment->payment_id, $findPayment, 6, 2);
                            $findPayment->update();
                        }
                    }
                    Logs::Create($project->project_id, $project, 3, 2);
                    $project->update();
                    ////создание нового ауткама
                    $payments = Payments::find()->where(['project_id'=>$project->project_id,'out_source_id'=>4])->one();
                    if(isset($payments)){
                        $data .= '1';
                        $payments_out_cart = Payments::find()->where('project_id = '.$project->project_id.' AND out_source_id < 4 AND out_source_id > 0')->one();
                        if(isset($payments_out_cart)){
                            $data .= ' 2';
                        }else{
                            $find_withdrow = Payments::find()->where(['project_id'=>$project->project_id,'payment_type'=>2,'out_source_id'=>4])->orderBy(['payment_id'=>SORT_DESC])->one();
                            if(isset($find_withdrow)){
                                $project->date_close = $find_withdrow->date_payment;
                            }else{
                                $project->date_close = $project->date_create;
                            }
                            $payment_new = new Payments;
                            $payment_new->client_id = $project->client_id;
                            $payment_new->manager_id = $project->manager_id;
                            $payment_new->branch_id = 2;
                            $payment_new->source_id = 0;
                            $payment_new->out_source_id = 2;
                            $payment_new->date_payment = strtotime(date('d-m-Y 2:00',$project->date_close));
                            $payment_new->payment_status = 2;
                            $payment_new->payment_type = 2;
                            $payment_new->cash_usd = $project->chargers;
                            $payment_new->cash_fact = $project->chargers;
                            $payment_new->cash_fact_currency = 4;
                            $payment_new->status = 0;
                            $payment_new->project_id = $project->project_id;
                            $payment_new->balance = $client->balance ?? 0;
                            if($project->project_type == 2){
                                $payment_new->cash_usd = 0;
                                $payment_new->cash_fact = 0;
                            }
                            if($payment_new->save()){
                                Logs::Create($payment_new->payment_id, $payment_new, 6, 1);
                            }
                        }
                    }
                }
            }
        }
        //return $data;
    }

    static function saveBalance($payment_id = 0){
        $payment = Payments::findOne(['payment_id'=>(int)$payment_id]);
        if(isset($payment)){

            //не обновлять баланс, если это автоматический платеж
                if($payment->payment_type == 2 && $payment->out_source_id == 2){ 
                    return '';
                }
            /////////
                
            $client = Client::findOne(['client_id'=>(int)$payment->client_id]);
            if(isset($client)){
                $payment->balance = $client->balance;
                    $find_withdrow = Payments::find()->where(['project_id'=>$payment->project_id,'payment_type'=>2,'out_source_id'=>4])->orderBy(['payment_id'=>SORT_DESC])->one();
                    if(isset($find_withdrow)){
                        $find_out = Payments::find()->where(['project_id'=>$payment->project_id,'payment_type'=>2,'out_source_id'=>2])->orderBy(['payment_id'=>SORT_DESC])->one();
                        if(isset($find_out)){
                            $find_out->balance = $payment->balance;
                            Logs::Create($find_out->payment_id, $find_out, 6, 2);
                            $find_out->update();
                        }
                    }
                Logs::Create($payment->payment_id, $payment, 6, 2);
                $payment->update();
                //Payments::MathBalanceClient($client->client_id);
            }
        }
    }

    static function MathBalanceClient($client_id = 0){
        $payments = Payments::find()->where(['status'=>1,'client_id'=>(int)$client_id])->orderBy(['payment_id'=>SORT_DESC])->all();
        $balance = 0;
        $client_id = 0;
        $cash_usd = 0;
        $access = 0;
        $payment_type = 0;
        $out_source_id = 0;
        $pay_id = 0;
        foreach ($payments as $payment) {
            $find_pay = Payments::findOne(['payment_id'=>$pay_id]);
            if((($payment->payment_type == 1) || ($payment->payment_type == 2 && $payment->out_source_id == 4)) && $payment->status == 1){
                if($balance == 0 && $client_id == 0 && $payment->client_id != 0){
                    $client_id = $payment->client_id;
                    $balance = $payment->balance;
                    $pay_id = $payment->payment_id;
                    $cash_usd = $payment->cash_usd;
                    $access = 0;
                    $payment_type = $payment->payment_type;
                    $out_source_id = $payment->out_source_id;
                }
                if($client_id != $payment->client_id && $payment->client_id != 0){
                    $client_id = $payment->client_id;
                    $balance = $payment->balance;
                    $pay_id = $payment->payment_id;
                    $cash_usd = $payment->cash_usd;
                    $access = 0;
                    $payment_type = $payment->payment_type;
                    $out_source_id = $payment->out_source_id;
                } 
                if($client_id == $payment->client_id && $payment->client_id != 0 && $access == 0){
                    $access = 1;
                    $client_id = $payment->client_id;
                    $balance = $payment->balance;
                    $pay_id = $payment->payment_id;
                    $cash_usd = $payment->cash_usd;
                    $payment_type = $payment->payment_type;
                    $out_source_id = $payment->out_source_id;
                }
                else if($client_id == $payment->client_id && $payment->client_id != 0 && $access == 1 && isset($find_pay)){
                    $client_id = $payment->client_id;
                    if($payment_type == 1){
                        $payment->balance = $find_pay->balance - $cash_usd;
                        $cash_usd = $payment->cash_usd;
                        $balance =  $find_pay->balance - $cash_usd;
                        Logs::Create($payment->payment_id, $payment, 6, 2);
                        $payment->update(false);
                        $access = 1;
                        $payment_type = $payment->payment_type;
                        $out_source_id = $payment->out_source_id;
                        $pay_id = $payment->payment_id;
                    }else if($payment_type == 2 && $out_source_id == 4){
                        $payment->balance = $find_pay->balance + $cash_usd;
                        $cash_usd = $payment->cash_usd;
                        $balance = $find_pay->balance + $cash_usd;
                        Logs::Create($payment->payment_id, $payment, 6, 2);
                        $payment->update(false);
                        $access = 1;
                        $payment_type = $payment->payment_type;
                        $out_source_id = $payment->out_source_id;
                        $pay_id = $payment->payment_id;
                    }
                }
            }
        }
    }

    static function ShowOutCome($project_id){
        $payments = Payments::find()->where(['project_id'=>(int)$project_id,'out_source_id'=>4])->one();
        if(isset($payments)){
            return '***';
        }
    }
}
