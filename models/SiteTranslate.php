<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_translate".
 *
 * @property integer $translate_id
 * @property integer $lang_id
 * @property integer $list_id
 * @property string $title
 * @property string $title_cut
 * @property string $description
 * @property string $description_cut
 * @property string $img
 * @property string $img_mini
 */
class SiteTranslate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_translate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id', 'list_id', 'title', 'title_cut', 'description', 'description_cut', 'img', 'img_mini'], 'required'],
            [['lang_id', 'list_id'], 'integer'],
            [['title', 'title_cut', 'description', 'description_cut', 'img', 'img_mini'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'translate_id' => 'Translate ID',
            'lang_id' => 'Lang ID',
            'list_id' => 'List ID',
            'title' => 'Title',
            'title_cut' => 'Title Cut',
            'description' => 'Description',
            'description_cut' => 'Description Cut',
            'img' => 'Img',
            'img_mini' => 'Img Mini',
        ];
    }
}
