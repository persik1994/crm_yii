<?php

namespace app\models;

use Yii;
use app\models\ShipmentCalculation;

/**
 * This is the model class for table "shipment_calculation_column".
 *
 * @property integer $column_id
 * @property string $title
 * @property string $type
 */
class ShipmentCalculationColumn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipment_calculation_column';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'type'], 'required'],
            [['title', 'type'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'column_id' => 'Column ID',
            'title' => 'Title',
            'type' => 'Type',
        ];
    }

    static function AllColumns(){
        $html = '';
        $allColumns = ShipmentCalculationColumn::find()->where('type != 0')->all();
        foreach ($allColumns as $column) {
            $html .= 
                '<div class="row">
                    <div class="col-3 shipment-title">
                        '.$column->title.'
                    </div>
                    <div class="col-3 ">
                        <input type="text" class="" value="0kg" disabled>
                    </div>
                    <div class="col-3">
                        <input type="text" class="shipment-calculation-'.$column->column_id.'" value="0" data-id="'.$column->column_id.'">
                    </div>
                    <div class="col-3 shipment-summ">0 USD</div>
                </div>';
        }
        return $html;
    }

    static function ColumnsType($shipment_type = 0, $shipment_id = 0){
        $html = '';
        $total_kg = 0;
        $total_summ = 0;
        if($shipment_type > 0){
            $allColumns = ShipmentCalculationColumn::find()->where('type != 0')->all();
            foreach ($allColumns as $column) {
                $types = explode(",", $column->type);
                foreach ($types as $type) {
                    if($type == $shipment_type || $type == 888 || $type == 999){
                        ////value/////
                        $value = 0;
                        if($shipment_id > 0){
                            $valueFind = ShipmentCalculation::findOne(['shipment_id'=>$shipment_id, 'column_id'=>$column->column_id]);
                            if(isset($valueFind) && $valueFind->amount > 0){
                                $value = $valueFind->amount;
                            }

                            if(strpos($column->title, 'General') !== false){
                                $kg = ShipmentCalculationColumn::MathCalculation_Kg($shipment_id, 1, $type);
                            }
                            else if(strpos($column->title, 'Electronics') !== false){
                                $kg = ShipmentCalculationColumn::MathCalculation_Kg($shipment_id, 2, $type);
                            }
                            else if(strpos($column->title, 'Brands') !== false){
                                $kg = ShipmentCalculationColumn::MathCalculation_Kg($shipment_id, 3, $type);
                            }
                            $total_kg += $kg;
                            if($type == 888 || $type == 999){
                                $total_summ += $value; 
                            }else{
                                $total_summ += ($kg * $value); 
                            }
                        }
                        //////////////
                        if($type < 888){
                            $html .= 
                                '<div class="row">
                                    <div class="col-3 shipment-title">
                                        '.$column->title.'
                                    </div>
                                    <div class="col-3 ">
                                        <input type="text" class="" value="'.$kg.'" disabled>
                                    </div>
                                    <div class="col-3">
                                        <input type="text" class="shipment-calculation-input" value="'.$value.'" data-id="'.$column->column_id.'">
                                    </div>
                                    <div class="col-3 shipment-summ">'.($kg * $value).' USD</div>
                                </div>';
                        }
                        else if($type == 888){
                            $html .= 
                                '<div class="border-width"></div>'.
                                '<div class="row">
                                    <div class="col-6 shipment-title">
                                        '.$column->title.'
                                    </div>
                                    <div class="col-3">
                                        <input type="text" class="shipment-calculation-input" value="'.$value.'" data-id="'.$column->column_id.'">
                                    </div>
                                    <div class="col-3 shipment-summ">'.$value.' USD</div>
                                </div>';
                        }
                        else if($type == 999){
                            $html .= 
                                '<div class="border-width"></div>'.
                                '<div class="row">
                                    <div class="col-3 shipment-title">
                                        '.$column->title.'
                                    </div>
                                    <div class="col-3 ">
                                        <input type="text" class="" value="'.$total_kg.'" disabled>
                                    </div>
                                    <div class="col-3 shipment-title">
                                        '.$column->title.'
                                    </div>
                                    <div class="col-3 shipment-summ">'.$total_summ.' USD</div>
                                </div>';
                        }
                    }
                }
                
            }
        }
        return $html;
    }

    static function MathCalculation_Kg($shipment_id = 0, $cargo_type = 0, $delivery_type = 0){
        $all_kg = 0;
        $cargos = Cargo::find()->where(['shipment_id'=>$shipment_id,'type_id'=>$cargo_type,'shipment_type_id'=>$delivery_type])->all(); //ищем все карго прикрепленные к шипменту
        if(isset($cargos)){
            foreach ($cargos as $cargo) {
                if($cargo->shipment_type_id == 5 || $cargo->shipment_type_id == 9){ //если карго Railway тип доставки
                    $all_kg += $cargo->volume;
                }
                else{ //если любой другой тип доставки
                    $all_kg += $cargo->weight;
                }
            }
        }
        return $all_kg;
    }
}
