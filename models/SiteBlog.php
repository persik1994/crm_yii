<?php

namespace app\models;

use Yii;
use app\models\SiteLang;
use app\models\SiteTranslate;
use app\helpers\HelperStr;

/**
 * This is the model class for table "site_blog".
 *
 * @property integer $blog_id
 * @property integer $category_id
 * @property integer $list_id
 * @property integer $date
 * @property integer $sort
 */
class SiteBlog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'list_id', 'date'], 'required'],
            [['category_id', 'list_id', 'date', 'sort'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'blog_id' => 'Blog ID',
            'category_id' => 'Category ID',
            'list_id' => 'List ID',
            'date' => 'Date',
            'sort' => 'Sort',
        ];
    }

    static function findBlog($blog_id, $language = 1){
        $blog = SiteBlog::findOne(['blog_id'=>(int)$blog_id]);
        if(isset($blog)){
            $list = SiteList::findOne(['list_id'=>$blog->list_id, 'status'=>1]);
            if(isset($list)){
                $translate = SiteTranslate::findOne(['list_id'=>$list->list_id, 'lang_id'=>(int)$language]);
                if(isset($translate)){
                    return array('translate'=>$translate,'blog'=>$blog,'list'=>$list);
                }
            }
        }
        return array();
    }

    static function uploadFile($file, $path = ''){
        if(!empty($_FILES[$file]['tmp_name'])){
            $info = pathinfo($_FILES[$file]['name']);
            $ext = $info['extension']; // get the extension of the file
            if($ext != 'html' && $ext != 'php'){
                $newname = time().HelperStr::randomText(5, 'string', 'int').".".$ext; 

                $target = '/home/inta/public_html/uploads/'.$path.$newname;
                move_uploaded_file( $_FILES[$file]['tmp_name'], $target);
                return '/uploads/'.$path.$newname;
            }
        }
        return '';
    }
}
