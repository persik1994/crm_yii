<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "manager".
 *
 * @property integer $manager_id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $avatar
 * @property string $phone
 * @property string $email
 * @property string $viber
 * @property string $telegram
 * @property integer $role
 * @property integer $status
 * @property string $auth_key
 */
class Manager extends ActiveRecord implements IdentityInterface
{
    public $rememberMe = true;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manager';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manager_id','username','password','name','avatar','phone','email', 'viber', 'telegram', 'role', 'status', 'auth_key'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['username', 'password', 'name', 'role', 'status'], 'required'],
            [['password', 'email'], 'string'],
            [['role', 'status'], 'integer'],
            [['username', 'name'], 'string', 'max' => 30],
            [['avatar'], 'string', 'max' => 50],
            [['phone', 'viber', 'telegram'], 'string', 'max' => 20],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'manager_id' => 'Manager ID',
            'username' => 'Username',
            'password' => 'Password',
            'name' => 'Name',
            'avatar' => 'Avatar',
            'phone' => 'Phone',
            'email' => 'Email',
            'viber' => 'Viber',
            'telegram' => 'Telegram',
            'role' => 'Role',
            'status' => 'Status',
            'auth_key' => 'Auth Key',
        ];
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    public function getId()
    {
        return $this->manager_id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }
    
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }  

    public function validatePassword($password)
    {
        return $this->password === sha1($password);
    }

    public function setPassword($password){
        $this->password = sha1($password);
    }

    static function UserList(){
        $users = self::find()->all();
        return $users;
    }
}
