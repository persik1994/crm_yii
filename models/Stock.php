<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "stock".
 *
 * @property integer $stock_id
 * @property integer $parent_id
 * @property string $name
 * @property string $flag
 * @property integer $status
 */
class Stock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stock_id','parent_id','name','flag','status'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['parent_id', 'name', 'flag', 'status'], 'required'],
            [['parent_id', 'status'], 'integer'],
            [['name', 'flag'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'stock_id' => 'Stock ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'flag' => 'Flag',
            'status' => 'Status',
        ];
    }
}
