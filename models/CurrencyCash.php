<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "currency_cash".
 *
 * @property integer $сurrency_id
 * @property string $name
 * @property string $flag
 * @property double $currency
 */
class CurrencyCash extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_cash';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['сurrency_id','name','flag','currency','date','manager_id'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['name', 'flag'], 'string'],
            [['currency', 'date','manager_id'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'сurrency_id' => 'сurrency ID',
            'name' => 'Name',
            'flag' => 'Flag',
            'currency' => 'Currency',
            'date' => 'Date edit',
            'manager_id' => 'Manager edit',
        ];
    }

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_id']);
    }
}
