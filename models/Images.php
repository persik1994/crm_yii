<?php

namespace app\models;

use Yii;
use app\helpers\HelperStr;

/**
 * This is the model class for table "images".
 *
 * @property integer $image_id
 * @property integer $type
 * @property integer $url
 * @property integer $date
 * @property integer $description
 * @property integer $post_id
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'url', 'date', 'post_id'], 'required'],
            [['type', 'date', 'post_id'], 'integer'],
            [['description', 'url'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image_id' => 'Image ID',
            'type' => 'Type',
            'url' => 'Url',
            'date' => 'Date',
            'description' => 'Description',
            'post_id' => 'Post ID',
        ];
    }

    static function uploadFiles($file, $path = '', $type = 0, $post_id = 0, $description = ''){
        if(isset($_FILES[$file]['name'])){
            for($file_count=0; $file_count < count($_FILES[$file]['name']); $file_count++){
                $info = pathinfo($_FILES[$file]['name'][$file_count]);
                $ext = $info['extension']; // get the extension of the file
                if($ext != 'html' && $ext != 'php'){
                    $newname = time().HelperStr::randomText(5, 'string', 'int').".".$ext; 
                    $target = '/home/inta/crm.inta.group/public_html/uploads/'.$path.$newname;
                    move_uploaded_file( $_FILES[$file]['tmp_name'][$file_count], $target);
                    ////wtite to DB
                        $image = new Images;
                        $image->type = (int)$type;
                        $image->url  = '/uploads/'.$path.$newname;
                        $image->date = time();
                        $image->description = $description;
                        $image->post_id = (int)$post_id;
                        $image->save();
                    /////
                }
            }
        }
    }

    static function findAllImages($type=0,$post_id=0){
        $images = Images::find()->where(['type'=>(int)$type,'post_id'=>(int)$post_id])->all();
        if(isset($images)){
            return $images;
        }
        return array();
    }
}
