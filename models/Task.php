<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "task".
 *
 * @property integer $task_id
 * @property integer $manager_id
 * @property integer $client_id
 * @property string $description
 * @property integer $date_create
 * @property integer $date_end
 * @property string $tag
 * @property integer $status
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id','manager_id','client_id','description','date_create','date_end','tag','status'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['manager_id', 'description', 'date_create', 'date_end', 'status'], 'required'],
            [['manager_id', 'client_id', 'date_create', 'date_end', 'status'], 'integer'],
            [['description', 'tag'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_id' => 'Task ID',
            'manager_id' => 'Manager ID',
            'client_id' => 'Client ID',
            'description' => 'Description',
            'date_create' => 'Date Create',
            'date_end' => 'Date End',
            'tag' => 'Tag',
            'status' => 'Status',
        ];
    }

    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }
}
