<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "locations".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $kaotuu
 * @property integer $kaotuu_level
 * @property string $type
 * @property string $subtype
 * @property string $status
 * @property string $name
 * @property string $info
 */
class Locations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'kaotuu_level'], 'integer'],
            [['type', 'subtype', 'status'], 'string'],
            [['kaotuu'], 'string', 'max' => 10],
            [['name', 'info'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'kaotuu' => 'Kaotuu',
            'kaotuu_level' => 'Kaotuu Level',
            'type' => 'Type',
            'subtype' => 'Subtype',
            'status' => 'Status',
            'name' => 'Name',
            'info' => 'Info',
        ];
    }
}
