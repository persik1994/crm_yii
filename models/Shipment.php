<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "shipment".
 *
 * @property integer $shipment_id
 * @property integer $manager_id
 * @property integer $from_id
 * @property integer $to_id
 * @property integer $type_id
 * @property integer $status_id
 * @property double $weight
 * @property double $volume
 * @property integer $cartons
 * @property integer $date_created
 * @property integer $date_received
 * @property integer $time
 * @property string $link
 * @property double $total_chargers
 * @property integer $status
 * @property string $comment
 * @property integer $checkpoint
 * @property double $price
 * @property double $price_customs
 * @property double $profit
 * @property string $awb
 * @property string $invoice
 * @property string $list
 * @property string $other
 */
class Shipment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipment_id','manager_id','from_id','to_id','type_id','status_id','weight', 'volume', 'cartons', 'date_created', 'date_received', 'time', 'link', 'total_chargers','status','name','comment','checkpoint','price','price_customs','profit','awb','invoice','list','other'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            //[['manager_id', 'from_id', 'to_id', 'type_id', 'status_id', 'weight', 'volume', 'cartons', 'date_created', 'date_received', 'time', 'link', 'total_chargers', 'status', 'checkpoint', 'awb', 'invoice', 'list', 'other'], 'required'],
            [['manager_id', 'from_id', 'to_id', 'type_id', 'status_id', 'cartons', 'date_created', 'date_received', 'time', 'status', 'checkpoint'], 'integer'],
            [['weight', 'volume', 'total_chargers', 'price', 'price_customs', 'profit'], 'number'],
            [['awb', 'invoice', 'list', 'other', 'link', 'name', 'comment'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'shipment_id' => 'Shipment ID',
            'manager_id' => 'Manager',
            'from_id' => 'From',
            'to_id' => 'To',
            'type_id' => 'Type',
            'status_id' => 'Status',
            'weight' => 'Weight',
            'volume' => 'Volume',
            'cartons' => 'Cartons Qty',
            'date_created' => 'Сreated',
            'date_received' => 'Shipment received',
            'time' => 'Time',
            'link' => 'Link',
            'total_chargers' => 'Total Chargers',
            'status' => 'Status',
            'name' => 'Name',
            'awb' => 'Awb',
            'invoice' => 'Invoice',
            'list' => 'List',
            'other' => 'Other',
            'checkpoint' => 'Checkpoint',
            'comment' => 'Comment',
        ];
    }

    public function getFrom()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'from_id']);
    }

    public function getTo()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'to_id']);
    }

    public function getCargoStatus()
    {
        return $this->hasOne(CargoStatus::className(), ['status_id' => 'status_id']);
    }

    public function getWarehouse()
    {
        return $this->hasOne(Stock::className(), ['stock_id' => 'checkpoint']);
    }

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_id']);
    }

    public function getType()
    {
        return $this->hasOne(ShipmentType::className(), ['type_id' => 'type_id']);
    }

    static function shipmentDropMenu($name, $code, $type = 0, $placeholder = ''){
        $input = '';
        $btns_sort = '';
        $dropdown = '<div class="dropdown-divider"></div>
                      <a class="dropdown-item dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$placeholder.'</a>
                      <div class="dropdown-menu dropdown-menu-table dropdown-menu-table-scroll" aria-labelledby="navbarDropdownMenuLink">';
        switch ($type) {
            case 'text':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'date':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" id="form-choose-date" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'select':
                switch ($code) {
                    case 'manager_id':
                        $managers = Manager::find()->all();
                        if(isset($managers)){
                            $input .= $dropdown;
                            foreach ($managers as $manager) {
                                $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="'.$manager->manager_id.'">'.$manager->username.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'from_id':
                        $countryes = Country::find()->all();
                        if(isset($countryes)){
                            $input .= $dropdown;
                            foreach ($countryes as $country) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$country->country_id.'">'.$country->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'to_id':
                        $countryes = Country::find()->all();
                        if(isset($countryes)){
                            $input .= $dropdown;
                            foreach ($countryes as $country) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$country->country_id.'">'.$country->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'status_id':
                        $statuses = CargoStatus::find()->all();
                        if(isset($statuses)){
                            $input .= $dropdown;
                            foreach ($statuses as $status) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$status->status_id.'">'.$status->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'checkpoint':
                        $stocks = Stock::find()->all();
                        if(isset($stocks)){
                            $input .= $dropdown;
                            foreach ($stocks as $stock) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$stock->stock_id.'">'.$stock->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'type_id':
                        $types = ShipmentType::find()->all();
                        if(isset($types)){
                            $input .= $dropdown;
                            foreach ($types as $type) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$type->type_id.'">'.$type->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                }
                break;
        }
        if($code != 'local_track' && $code != 'description'){
            $btns_sort = '<a class="dropdown-item" href="?sort='.$code.'">Sort by Asc</a><a class="dropdown-item" href="?sort=-'.$code.'">Sort by Desc</a>';
        }

        return '<div class="dropdown dropdown-table">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      '.$name.'
                    </a>
                    <div class="dropdown-menu dropdown-menu-table" aria-labelledby="dropdownMenuLink">
                      '.$btns_sort.'
                      '.$input.'
                    </div>
                </div>';
    }

    static function SearchSession($type = 0, $type_id = 0){
        $session = Yii::$app->session;

        $sesion_type = $type_id.'_';

        if(Yii::$app->request->get('shipment_id')){
            $session['shipment_'.$sesion_type.'shipment_id'] = (int)str_replace('SH-', '', Yii::$app->request->get('shipment_id'));
        }
        if(Yii::$app->request->get('date_created')){
            $session['shipment_'.$sesion_type.'date_created'] = (int)strtotime(Yii::$app->request->get('date_created'));
        }
        if(Yii::$app->request->get('from_id')){
            $session['shipment_'.$sesion_type.'from_id'] = Yii::$app->request->get('from_id');
        }
        if(Yii::$app->request->get('to_id')){
            $session['shipment_'.$sesion_type.'to_id'] = Yii::$app->request->get('to_id');
        }
        if(Yii::$app->request->get('manager_id')){
            $session['shipment_'.$sesion_type.'manager_id'] = Yii::$app->request->get('manager_id');
        }
        if(Yii::$app->request->get('status_id')){
            $session['shipment_'.$sesion_type.'status_id'] = Yii::$app->request->get('status_id');
        }
        if(Yii::$app->request->get('checkpoint')){
            $session['shipment_'.$sesion_type.'checkpoint'] = Yii::$app->request->get('checkpoint');
        }
        if(Yii::$app->request->get('type_id')){
            $session['shipment_'.$sesion_type.'type_id'] = Yii::$app->request->get('type_id');
        }
        if(Yii::$app->request->get('weight')){
            $session['shipment_'.$sesion_type.'weight'] = Yii::$app->request->get('weight');
        }
        if(Yii::$app->request->get('volume')){
            $session['shipment_'.$sesion_type.'volume'] = Yii::$app->request->get('volume');
        }
        if(Yii::$app->request->get('cartons')){
            $session['shipment_'.$sesion_type.'cartons'] = Yii::$app->request->get('cartons');
        }
        if(Yii::$app->request->get('name')){
            $session['shipment_'.$sesion_type.'name'] = Yii::$app->request->get('name');
        }
        if(Yii::$app->request->get('comment')){
            $session['shipment_'.$sesion_type.'comment'] = Yii::$app->request->get('comment');
        }
        ////////////////////////
        Shipment::SearchQuery($type, $type_id);
    }

    static function SearchQuery($type = 0, $type_id = 0){
        $session = Yii::$app->session;
        $sesion_type = $type_id.'_';
        ///////////
        if($type == 1){ ///filter
            if(Yii::$app->user->identity->role >= 8 || Yii::$app->user->identity->username == 'savenko'){
            $query = Shipment::find()->where(['status'=>1, 'status_id'=>$type_id]);
            }else{
                $query = Shipment::find()->where(['status'=>1, 'status_id'=>$type_id, 'manager_id'=>Yii::$app->user->identity->id]);
            }
        }
        else{ //all
            if(Yii::$app->user->identity->role >= 8 || Yii::$app->user->identity->username == 'savenko'){
                $query = Shipment::find()->where(['status'=>1]);
            }else{
                $query = Shipment::find()->where(['status'=>1, 'manager_id'=>Yii::$app->user->identity->id]);
            }
        }
        /////////

        if($session->has('shipment_'.$sesion_type.'shipment_id')){ 
            $query->andFilterWhere(['=', 'shipment_id', $session['shipment_'.$sesion_type.'shipment_id']]);
        }
        if($session->has('shipment_'.$sesion_type.'from_id')){ 
            $query->andFilterWhere(['=', 'from_id', $session['shipment_'.$sesion_type.'from_id']]);
        }
        if($session->has('shipment_'.$sesion_type.'to_id')){
            $query->andFilterWhere(['=', 'to_id', $session['shipment_'.$sesion_type.'to_id']]);
        }
        if($session->has('shipment_'.$sesion_type.'manager_id')){
            $query->andFilterWhere(['=', 'manager_id', $session['shipment_'.$sesion_type.'manager_id']]);
        }
        if($session->has('shipment_'.$sesion_type.'status_id')){
            $query->andFilterWhere(['=', 'status_id', $session['shipment_'.$sesion_type.'status_id']]);
        }
        if($session->has('shipment_'.$sesion_type.'checkpoint')){
            $query->andFilterWhere(['=', 'checkpoint', $session['shipment_'.$sesion_type.'checkpoint']]);
        }
        if($session->has('shipment_'.$sesion_type.'type_id')){
            $query->andFilterWhere(['=', 'type_id', $session['shipment_'.$sesion_type.'type_id']]);
        }
        if($session->has('shipment_'.$sesion_type.'weight')){
            $query->andFilterWhere(['like', 'weight', $session['shipment_'.$sesion_type.'weight']]);
        }
        if($session->has('shipment_'.$sesion_type.'volume')){
            $query->andFilterWhere(['like', 'volume', $session['shipment_'.$sesion_type.'volume']]);
        }
        if($session->has('shipment_'.$sesion_type.'cartons')){
            $query->andFilterWhere(['=', 'cartons', $session['shipment_'.$sesion_type.'cartons']]);
        }
        if($session->has('shipment_'.$sesion_type.'name')){
            $query->andFilterWhere(['like', 'name', $session['shipment_'.$sesion_type.'name']]);
        }
        if($session->has('shipment_'.$sesion_type.'comment')){
            $query->andFilterWhere(['like', 'comment', $session['shipment_'.$sesion_type.'comment']]);
        }
        //////date////
        if($session->has('shipment_'.$sesion_type.'date_created')){
            $query->andFilterWhere(['>=', 'date_created', (int)strtotime(date('d.m.Y 00:00',$session['shipment_'.$sesion_type.'date_created']))]);
            $query->andFilterWhere(['<=', 'date_created', (int)strtotime(date('d.m.Y 23:59',$session['shipment_'.$sesion_type.'date_created']))]);
        }
        ////////////////////////
        return $query;
    }

    static function SearchShowBtn($type = 0, $type_id = 0){
        $session = Yii::$app->session;
        $sesion_type = $type_id.'_';
        $btnAll = Shipment::SearchSortBtn();
        foreach ($session as $type => $value){
            switch ($type) {
                case 'shipment_'.$sesion_type.'shipment_id':
                    $btnAll .= Shipment::SearchHtmlBtn($type, 'SH-'.$value);
                    break;

                case 'shipment_'.$sesion_type.'date_created':
                    $btnAll .= Shipment::SearchHtmlBtn($type, date('d.M.Y',$value));
                    break;
                
                case 'shipment_'.$sesion_type.'from_id':
                    $Country = Country::findOne(['country_id'=>$value]);
                    $btnAll .= Shipment::SearchHtmlBtn($type, $Country->name);
                    break;

                case 'shipment_'.$sesion_type.'to_id':
                    $Country = Country::findOne(['country_id'=>$value]);
                    $btnAll .= Shipment::SearchHtmlBtn($type, $Country->name);
                    break;

                case 'shipment_'.$sesion_type.'manager_id':
                    $manager = Manager::findOne(['manager_id'=>$value]);
                    $btnAll .= Shipment::SearchHtmlBtn($type, $manager->username);
                    break;
                
                case 'shipment_'.$sesion_type.'status_id':
                    $CargoStatus = CargoStatus::findOne(['status_id'=>$value]);
                    $btnAll .= Shipment::SearchHtmlBtn($type, $CargoStatus->name);
                    break;
                
                case 'shipment_'.$sesion_type.'checkpoint':
                    $Stock = Stock::findOne(['stock_id'=>$value]);
                    $btnAll .= Shipment::SearchHtmlBtn($type, $Stock->name);
                    break;
                
                case 'shipment_'.$sesion_type.'type_id':
                    $ShipmentType = ShipmentType::findOne(['type_id'=>$value]);
                    $btnAll .= Shipment::SearchHtmlBtn($type, $ShipmentType->name);
                    break;
                
                case 'shipment_'.$sesion_type.'weight':
                    $btnAll .= Shipment::SearchHtmlBtn($type, 'Weight - '.$value);
                    break;
                
                case 'shipment_'.$sesion_type.'volume':
                    $btnAll .= Shipment::SearchHtmlBtn($type, 'Volume - '.$value);
                    break;
                
                case 'shipment_'.$sesion_type.'cartons':
                    $btnAll .= Shipment::SearchHtmlBtn($type, 'Cartons - '.$value);
                    break;
                
                case 'shipment_'.$sesion_type.'name':
                    $btnAll .= Shipment::SearchHtmlBtn($type, 'Name - '.$value);
                    break;
                
                case 'shipment_'.$sesion_type.'comment':
                    $btnAll .= Shipment::SearchHtmlBtn($type, 'Comment - '.$value);
                    break;
            }
        }
        if(!empty($btnAll)){
            $btnAll .= '<p class="btn-search-filter-show btn-search-filter-delete">Remove all filters <a href="/shipments/searchdeleteall"><i class="fas fa-times"></i></a></p>';
        }
        return $btnAll;
    }

    static function DeleteAllSession(){
        $session = Yii::$app->session;
        foreach ($session as $type => $value){
            if(stristr($type, 'shipment_') !== FALSE) {
                $session->remove($type);
            }
        }
    }

    static function SearchSortBtn(){
        $row = '';
        if(Yii::$app->request->get('sort')){
            if(stristr(Yii::$app->request->get('sort'), '-') === FALSE) {
                $sort = 'ASC';
                if(isset(Shipment::attributeLabels()[Yii::$app->request->get('sort')])){
                    $row = Shipment::attributeLabels()[Yii::$app->request->get('sort')];
                }
            }
            else{
                $sort = 'DESC';
                if(isset(Shipment::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))])){
                    $row = Shipment::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))];
                }
            }
            return '<p class="btn-search-filter-show">Sort '.$sort.' '.$row.'<a href="index?sort="><i class="fas fa-times"></i></a></p>';
        }
        return '';
    }

    static function SearchHtmlBtn($type, $name){
        return '<p class="btn-search-filter-show">'.$name.'<a href="/shipments/searchdelete?type='.$type.'&sort='.Yii::$app->request->get('sort').'"><i class="fas fa-times"></i></a></p>';
    }
}
