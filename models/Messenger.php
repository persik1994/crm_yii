<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "messenger".
 *
 * @property integer $messenger_id
 * @property string $name
 * @property string $link
 * @property integer $status
 */
class Messenger extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messenger';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['messenger_id','name','link','status'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['name', 'link', 'status'], 'required'],
            [['status'], 'integer'],
            [['name', 'link'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'messenger_id' => 'Messenger ID',
            'name' => 'Name',
            'link' => 'Link',
            'status' => 'Status',
        ];
    }
}
