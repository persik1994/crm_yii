<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "client".
 *
 * @property integer $client_id
 * @property string $password
 * @property integer $manager_id
 * @property integer $date_add
 * @property integer $source_id
 * @property integer $status_id
 * @property integer $activation
 * @property string $name
 * @property integer $phone_code
 * @property string $phone
 * @property string $email
 * @property string $comment
 * @property integer $date_next_connect
 * @property integer $status
 * @property number $balance
 * @property number $balance_total
 * @property number $allWeight
 * @property integer $site
 */
class Client extends \yii\db\ActiveRecord
{
    public $allWeight;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id','password','manager_id','date_add','source_id','status_id','activation','name','phone_code','phone','email','comment','date_next_connect','status','balance','balance_total','site'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],

            [['password', 'manager_id', 'date_add', 'source_id', 'activation', 'name', 'phone_code', 'phone', 'email', 'comment', 'date_next_connect', 'status', 'site'], 'required'],
            [['name', 'password', 'email', 'comment'], 'string'],
            [['balance','balance_total'], 'number'],
            [['manager_id', 'date_add', 'source_id', 'status_id', 'activation', 'phone_code', 'date_next_connect', 'status', 'site'], 'integer'],
            [['phone'], 'string', 'max' => 30],
            [['allWeight'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Client ID',
            'password' => 'Password',
            'manager_id' => 'Manager',
            'date_add' => 'Date Add',
            'source_id' => 'Client Source',
            'status_id' => 'Client Status',
            'activation' => 'Activation',
            'name' => 'Client Name',
            'phone_code' => 'Phone Code',
            'phone' => 'Phone',
            'email' => 'Email',
            'comment' => 'Comment',
            'date_next_connect' => 'Date Next Connect',
            'status' => 'Status',
            'site' => 'Site',
            'balance' => 'Balance',
            'balance_total' => 'Balance Total',
            'allWeight' => 'All weight',
        ];
    }

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_id']);
    }

    public function getSource()
    {
        return $this->hasOne(ClientSource::className(), ['source_id' => 'source_id']);
    }

    public function getClientstatus()
    {
        return $this->hasOne(ClientStatus::className(), ['status_id' => 'status_id']);
    }

    static function currentClients(){
        $summ = 0;
        $allClients = Client::find()->where(['manager_id'=>Yii::$app->user->identity->id, 'activation'=>1])->andWhere('date_add >= '.strtotime('01'.date('.m.Y ').'00:00:00'))->all();
        foreach ($allClients as $client) {
            $summ++;
        }
        return $summ;
    }

    static function clientDropMenu($name, $code, $type = 0, $placeholder = ''){
        $input = '';
        $btns_sort = '';
        $dropdown = '<div class="dropdown-divider"></div>
                      <a class="dropdown-item dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$placeholder.'</a>
                      <div class="dropdown-menu dropdown-menu-table dropdown-menu-table-scroll" aria-labelledby="navbarDropdownMenuLink">';
        switch ($type) {
            case 'text':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'date':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" id="form-choose-date" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'select':
                switch ($code) {
                    case 'manager_id':
                        $managers = Manager::find()->all();
                        if(isset($managers)){
                            $input .= $dropdown;
                            foreach ($managers as $manager) {
                                $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="'.$manager->manager_id.'">'.$manager->username.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'source_id':
                        $sources = ClientSource::find()->all();
                        if(isset($sources)){
                            $input .= $dropdown;
                            foreach ($sources as $source) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$source->source_id.'">'.$source->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'status_id':
                        $statuses = ClientStatus::find()->where('status=1')->all();
                        if(isset($statuses)){
                            $input .= $dropdown;
                            foreach ($statuses as $status) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$status->status_id.'">'.$status->name.'</a>';
                            }
                            $input .= '</div>';
                        }
                        break;
                    case 'activation':
                        $input .= $dropdown;
                        $input .= '<a class="dropdown-item search-menu-click" data-type="activation" data-id="1">Active</a>';
                        $input .= '<a class="dropdown-item search-menu-click" data-type="activation" data-id="2">No active</a>';
                        $input .= '</div>';
                        break;
                }
                break;
        }
        if($code != 'local_track' && $code != 'description'){
            $btns_sort = '<a class="dropdown-item" href="?sort='.$code.'">Sort by Asc</a><a class="dropdown-item" href="?sort=-'.$code.'">Sort by Desc</a>';
        }

        return '<div class="dropdown dropdown-table">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      '.$name.'
                    </a>
                    <div class="dropdown-menu dropdown-menu-table" aria-labelledby="dropdownMenuLink">
                      '.$btns_sort.'
                      '.$input.'
                    </div>
                </div>';
    }

    static function SearchSession(){
        $session = Yii::$app->session;

        if(Yii::$app->request->get('client_id')){
            $session['client_client_id'] = (int)str_replace('88-', '', Yii::$app->request->get('client_id'));
        }
        if(Yii::$app->request->get('manager_id')){
            $session['client_manager_id'] = Yii::$app->request->get('manager_id');
        }
        if(Yii::$app->request->get('balance')){
            $session['client_balance'] = Yii::$app->request->get('balance');
        }
        if(Yii::$app->request->get('date_add')){
            $session['client_date_add'] = (int)strtotime(Yii::$app->request->get('date_add'));
        }
        if(Yii::$app->request->get('name')){
            $session['client_name'] = Yii::$app->request->get('name');
        }
        if(Yii::$app->request->get('phone')){
            $session['client_phone'] = Yii::$app->request->get('phone');
        }
        if(Yii::$app->request->get('email')){
            $session['client_email'] = Yii::$app->request->get('email');
        }
        if(Yii::$app->request->get('source_id')){
            $session['client_source_id'] = Yii::$app->request->get('source_id');
        }
        if(Yii::$app->request->get('status_id')){
            $session['client_status_id'] = Yii::$app->request->get('status_id');
        }
        if(Yii::$app->request->get('activation')){
            if($session['client_activation'] == 2)
                $session['client_activation'] = 0;
            else
                $session['client_activation'] = Yii::$app->request->get('status');
        }
        if(Yii::$app->request->get('comment')){
            $session['client_comment'] = Yii::$app->request->get('comment');
        }
        ////////////////////////
        Client::SearchQuery();
    }

    static function SearchQuery(){
        $session = Yii::$app->session;

        ///////////
        if(Yii::$app->user->identity->role >= 10){
            $query = Client::find()->where(['status'=>1]);
        }
        else if(Yii::$app->user->identity->id == 34){ //Andrei_N
            $query = Client::find()->where(['status'=>1,'manager_id'=>[34,25,27,28,30,31,26,29,32]]);
        }
        else if(Yii::$app->user->identity->role == 2){
            $query = Client::find()->where(['status'=>1, 'source_id'=>6]);
        }else{
            $query = Client::find()->where(['status'=>1, 'manager_id'=>Yii::$app->user->identity->id]);
        }
        /////////

        if($session->has('client_client_id')){ 
            $query->andFilterWhere(['=', 'client_id', $session['client_client_id']]);
        }
        if($session->has('client_manager_id')){ 
            $query->andFilterWhere(['=', 'manager_id', $session['client_manager_id']]);
        }
        if($session->has('client_balance')){ 
            $query->andFilterWhere(['=', 'balance', $session['client_balance']]);
        }
        if($session->has('client_name')){
            $query->andFilterWhere(['like', 'name', $session['client_name']]);
        }
        if($session->has('client_phone')){
            $query->andFilterWhere(['like', 'phone', $session['client_phone']]);
        }
        if($session->has('client_email')){
            $query->andFilterWhere(['like', 'email', $session['client_email']]);
        }
        if($session->has('client_source_id')){
            $query->andFilterWhere(['=', 'source_id', $session['client_source_id']]);
        }
        if($session->has('client_status_id')){
            $query->andFilterWhere(['=', 'status_id', $session['client_status_id']]);
        }else{
            $query->andFilterWhere(['!=', 'status_id', 5]);
        }
        if($session->has('client_activation')){
            $query->andFilterWhere(['=', 'activation', $session['client_activation']]);
        }else{
            $query->andWhere(['activation'=>[0,1]]);
        }
        if($session->has('client_comment')){
            $query->andFilterWhere(['like', 'comment', $session['client_comment']]);
        }
        //////date////
        if($session->has('client_date_add')){
            $query->andFilterWhere(['>=', 'date_add', (int)strtotime(date('d.m.Y 00:00',$session['client_date_add']))]);
            $query->andFilterWhere(['<=', 'date_add', (int)strtotime(date('d.m.Y 23:59',$session['client_date_add']))]);
        }
        ////////////////////////
        return $query;
    }

    static function SearchShowBtn($page = 0){
        $session = Yii::$app->session;
        $btnAll = Client::SearchSortBtn();
        foreach ($session as $type => $value){
            switch ($type) {
                case 'client_client_id':
                    $btnAll .= Client::SearchHtmlBtn($type, '88-'.$value);
                    break;
                
                case 'client_manager_id':
                    $manager = Manager::findOne(['manager_id'=>$value]);
                    $btnAll .= Client::SearchHtmlBtn($type, $manager->username);
                    break;

                case 'client_balance':
                    $btnAll .= Client::SearchHtmlBtn($type, 'Balance - '.$value.' $');
                    break;

                case 'client_date_add':
                    $btnAll .= Client::SearchHtmlBtn($type, date('d.M.Y',$value));
                    break;
                
                case 'client_name':
                    $btnAll .= Client::SearchHtmlBtn($type, 'Name - '.$value);
                    break;
                
                case 'client_phone':
                    $btnAll .= Client::SearchHtmlBtn($type, 'Phone - '.$value);
                    break;
                
                case 'client_email':
                    $btnAll .= Client::SearchHtmlBtn($type, 'Email - '.$value);
                    break;
                
                case 'client_source_id':
                    $ClientSource = ClientSource::findOne(['source_id'=>$value]);
                    $btnAll .= Client::SearchHtmlBtn($type, $ClientSource->name);
                    break;
                
                case 'client_status_id':
                    $ClientStatus = ClientStatus::findOne(['status_id'=>$value]);
                    $btnAll .= Client::SearchHtmlBtn($type, $ClientStatus->name);
                    break;
                
                case 'client_activation':
                    if($value == 2)
                        $btnAll .= Client::SearchHtmlBtn($type, 'No active');
                    else 
                        $btnAll .= Client::SearchHtmlBtn($type, 'Active');
                    break;
                
                case 'client_comment':
                    $btnAll .= Client::SearchHtmlBtn($type, 'Comment - '.$value);
                    break;
            }
        }
        if(!empty($btnAll)){
            if($page == 1){
                $btnAll .= '<p class="btn-search-filter-show btn-search-filter-delete">Remove all filters <a href="/clients/searchbalancesdeleteall"><i class="fas fa-times"></i></a></p>';
            }
            else{
                $btnAll .= '<p class="btn-search-filter-show btn-search-filter-delete">Remove all filters <a href="/clients/searchdeleteall"><i class="fas fa-times"></i></a></p>';
            }
        }
        return $btnAll;
    }

    static function DeleteAllSession(){
        $session = Yii::$app->session;
        foreach ($session as $type => $value){
            if(stristr($type, 'client_') !== FALSE) {
                $session->remove($type);
            }
        }
    }

    static function SearchSortBtn(){
        $row = '';
        if(Yii::$app->request->get('sort')){
            if(stristr(Yii::$app->request->get('sort'), '-') === FALSE) {
                $sort = 'ASC';
                if(isset(Client::attributeLabels()[Yii::$app->request->get('sort')])){
                    $row = Client::attributeLabels()[Yii::$app->request->get('sort')];
                }
            }
            else{
                $sort = 'DESC';
                if(isset(Client::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))])){
                    $row = Client::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))];
                }
            }
            return '<p class="btn-search-filter-show">Sort '.$sort.' '.$row.'<a href="index?sort="><i class="fas fa-times"></i></a></p>';
        }
        return '';
    }

    static function SearchHtmlBtn($type, $name){
        return '<p class="btn-search-filter-show">'.$name.'<a href="/clients/searchdelete?type='.$type.'&sort='.Yii::$app->request->get('sort').'"><i class="fas fa-times"></i></a></p>';
    }
}
