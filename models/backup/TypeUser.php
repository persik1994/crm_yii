<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type_user".
 *
 * @property integer $id
 * @property string $name
 * @property integer $sale
 */
class TypeUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sale'], 'required'],
            [['sale'], 'integer'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sale' => 'Sale',
        ];
    }
}
