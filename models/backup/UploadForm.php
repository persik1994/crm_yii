<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg, pdf, zip, rar, doc, docs, exel'],
        ];
    }
    
    public function upload($name)
    {
        if ($this->validate()) {
            $this->file->saveAs('uploads/images/' . $name . '.' . $this->file->extension);
            return $name . '.' . $this->file->extension;
        } else {
            return '';
        }
    }
}