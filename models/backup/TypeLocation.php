<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type_location".
 *
 * @property integer $id
 * @property string $name
 * @property string $icon
 */
class TypeLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'icon'], 'required'],
            [['name', 'icon'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'icon' => 'Icon',
        ];
    }

    static function AdminLocations(){
        $locations = TypeLocation::find()->all();
        return $locations;
    }
}
