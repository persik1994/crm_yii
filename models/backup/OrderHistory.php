<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_history".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $admin_id
 * @property integer $date
 * @property string $comment
 * @property integer $status_id
 * @property integer $notification
 * @property string $file
 */
class OrderHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'date', 'status_id', 'notification', 'admin_id'], 'required'],
            [['order_id', 'date', 'status_id', 'notification', 'admin_id'], 'integer'],
            [['comment', 'file'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'admin_id' => 'Admin ID',
            'date' => 'Date',
            'comment' => 'Comment',
            'status_id' => 'Status ID',
            'notification' => 'Notification',
            'file' => 'File',
        ];
    }
}
