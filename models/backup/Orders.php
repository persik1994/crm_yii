<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property string $id
 * @property string $location_from
 * @property string $location_to
 * @property string $shipment_type
 * @property string $code_cc
 * @property string $given_to_client
 * @property string $warehouse
 * @property string $status
 * @property string $weight
 * @property string $volume
 * @property integer $cartons
 * @property string $description
 * @property string $local_tracking
 * @property string $date_received
 * @property string $client_code
 * @property string $manager
 * @property string $photos
 * @property string $date_add
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            //[['id', 'location_from', 'location_to', 'shipment_type', 'code_cc', 'given_to_client', 'warehouse', 'status', 'weight', 'volume', 'cartons', 'description', 'local_tracking', 'date_received', 'client_code', 'manager', 'photos', 'date_add'], 'required'],
            [['weight', 'volume'], 'number'],
            [['cartons'], 'integer'],
            [['description', 'photos'], 'string'],
            [['id'], 'string', 'max' => 20],
            [['location_from', 'location_to', 'shipment_type', 'given_to_client', 'warehouse', 'status', 'local_tracking', 'date_received', 'client_code', 'date_add'], 'string', 'max' => 25],
            [['code_cc'], 'string', 'max' => 10],
            [['manager'], 'string', 'max' => 35],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'location_from' => 'Location From',
            'location_to' => 'Location To',
            'shipment_type' => 'Shipment Type',
            'code_cc' => 'Code Cc',
            'given_to_client' => 'Given To Client',
            'warehouse' => 'Warehouse',
            'status' => 'Status',
            'weight' => 'Weight',
            'volume' => 'Volume',
            'cartons' => 'Cartons',
            'description' => 'Description',
            'local_tracking' => 'Local Tracking',
            'date_received' => 'Date Received',
            'client_code' => 'Client Code',
            'manager' => 'Manager',
            'photos' => 'Photos',
            'date_add' => 'Date Add',
        ];
    }

    public function location($location){
        switch ($location) {
            case 'China Mainland':
                return 'sprite-CN_mini';
                break;
                
            case 'Hong Kong':
                return 'sprite-CN_mini';
                break;
                
            case 'Poland':
                return 'sprite-PLD_mini';
                break;
                
            case 'USA':
                return 'sprite-USA_mini';
                break;
                
            case 'Ukraine':
                return 'sprite-UKR_mini';
                break;

            default:
                return 'sprite-noname_mini';
                break;
        }
    }

    public function type_shipment($type){
        switch ($type) {
            case 'Post':
                return 'sprite-auto_mini';
                break;
                
            case 'Special':
                return 'sprite-litak_mini';
                break;
                
            case 'Auto':
                return 'sprite-auto_mini';
                break;
                
            case 'Air+EU':
                return 'sprite-litak_mini';
                break;
                
            case 'Railway+EU':
                return 'sprite-bus_mini';
                break;

            case 'Sea+EU':
                return 'sprite-yakor_mini';
                break;
                
            case 'Air+Freight':
                return 'sprite-litak_mini';
                break;
                
            case 'Auto+Freight':
                return 'sprite-auto_mini';
                break;
                
            case 'Railway+Freight':
                return 'sprite-bus_mini';
                break;
                
            case 'Sea+Freight':
                return 'sprite-yakor_mini';
                break;

            default:
                return 'sprite-auto_mini';
                break;
        }
    }
}
