<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;
use app\models\Order;
use app\models\Users;

class SendEmail
{

	static function SendMessage($mailto, $body){
		Yii::$app->mailer->compose()
	    ->setFrom('antegi@gmail.com')
	    ->setTo($mailto)
	    ->setSubject('Изменен статус заказа')
	    ->setTextBody('Ваш заказ изменен')
	    ->setHtmlBody($body)
	    ->send();
	}

	static function SetOrderStatus($status, $order_id){
		$order = Order::findOne($order_id);
		if(isset($order)){
			$user = Users::findOne($order->user_id);
			if(isset($user)){
				$mailto = $user->email;
				$body = 'Статус заказа №'.$order_id.' изменен на "'.$status->name.'". <a href="http://prom2.webcustoms.pro/view/'.$order_id.'">Посмотреть детали заказа.</a>';
				SendEmail::SendMessage($mailto, $body);
			}
		}
	}
}