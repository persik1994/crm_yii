<?php
namespace app\models;
use Yii;

class Telegram
{
    static function TelegramSendAll($text){
        foreach (Yii::$app->params['telegram_users'] as $user_id) {
            Telegram::TelegramBotSend($user_id, $text);
        }
    }

    static function TelegramBotSend($user_id, $text){
        if(isset($user_id) && isset($text)){
            \Yii::$app->bot->sendMessage($user_id, $text);
            return 1;
        }
        return 0;
    }
}
?>