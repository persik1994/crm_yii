<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_position".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $url
 * @property integer $count
 * @property string $description
 * @property integer $status
 */
class OrderPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'url', 'count', 'description', 'status'], 'required'],
            [['order_id', 'count', 'status'], 'integer'],
            [['url', 'description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'url' => 'Url',
            'count' => 'Count',
            'description' => 'Description',
            'status' => 'Status',
        ];
    }
}
