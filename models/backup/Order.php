<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;
use app\models\OrderOptions;
use app\models\TypeLocation;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $admin_id
 * @property integer $type_delivery
 * @property integer $money
 * @property integer $status
 * @property integer $create_time
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'admin_id', 'type_delivery', 'money', 'status', 'create_time'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            [['user_id', 'admin_id', 'type_delivery', 'money', 'status', 'create_time'], 'required'],
            [['user_id', 'admin_id', 'type_delivery', 'money', 'status', 'create_time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'admin_id' => 'Admin ID',
            'type_delivery' => 'Type Delivery',
            'money' => 'Money',
            'status' => 'Status',
            'create_time' => 'Create Time',
        ];
    }

    static function AdminOrderOneShow(){
        $html = '';
        $allOrders = Order::find()->orderBy(['id'=>SORT_DESC])->all();
        foreach ($allOrders as $order) {
            $order_options = OrderOptions::find()->where(['order_id'=>$order->id])->one();
            $status = TypeStatusOrder::findOne($order->status);
            $date_create = Order::LoadDateOrder($order->create_time);
            if($order_options->date_delivered > 0)
                $date_finish = Order::LoadDateOrder($order_options->date_delivered);
            else
                $date_finish = 'в процессе';
            $user = Users::findOne($order->user_id);
            $order_positions_count = OrderPosition::find()->where(['order_id'=>$order->id])->count();
            $order_service_count = OrderService::find()->where(['order_id'=>$order->id])->count();
            $delivery = TypeDelivery::findOne($order->type_delivery);
            $locate_now = TypeLocation::findOne($order_options->locate_now);
            $locate_start = TypeLocation::findOne($order_options->locate_start);
            $locate_finish = TypeLocation::findOne($order_options->locate_finish);
            ////generate html
            $html .= Yii::$app->controller->renderPartial('/order/index_one', [
                'order'=>$order,
                'order_options'=>$order_options,
                'status'=>$status,
                'date_create'=>$date_create,
                'date_finish'=>$date_finish,
                'user'=>$user,
                'delivery'=>$delivery,
                'locate_now'=>$locate_now,
                'locate_start'=>$locate_start,
                'locate_finish'=>$locate_finish,
            ]);
        }
        return $html;
    }

    static function MyOrders($limit = 100){
        $html = '';
        $allOrders = Order::find()->where(['user_id'=>Yii::$app->user->identity->id])->orderBy(['id'=>SORT_DESC])->limit((int)$limit)->all();
        foreach ($allOrders as $order) {
            $order_options = OrderOptions::find()->where(['order_id'=>$order->id])->one();
            $status = TypeStatusOrder::findOne($order->status);
            $date_create = Order::LoadDateOrder($order->create_time);
            if($order_options->date_delivered > 0)
                $date_finish = Order::LoadDateOrder($order_options->date_delivered);
            else
                $date_finish = 'в процессе';
            $user = Users::findOne($order->user_id);
            $order_positions_count = OrderPosition::find()->where(['order_id'=>$order->id])->count();
            $order_service_count = OrderService::find()->where(['order_id'=>$order->id])->count();
            $delivery = TypeDelivery::findOne($order->type_delivery);
            $locate_now = TypeLocation::findOne($order_options->locate_now);
            $locate_start = TypeLocation::findOne($order_options->locate_start);
            $locate_finish = TypeLocation::findOne($order_options->locate_finish);
            ////generate html
            $html .= Yii::$app->controller->renderPartial('/order/orders_one', [
                'order'=>$order,
                'order_options'=>$order_options,
                'status'=>$status,
                'date_create'=>$date_create,
                'date_finish'=>$date_finish,
                'user'=>$user,
                'delivery'=>$delivery,
                'locate_now'=>$locate_now,
                'locate_start'=>$locate_start,
                'locate_finish'=>$locate_finish,
            ]);
        }
        return $html;
    }

    static function LoadDateOrder($date){
        switch (date('n', $date)) {
            case 1:
                $month = 'января';
                break;
            case 2:
                $month = 'февраля';
                break;
            case 3:
                $month = 'марта';
                break;
            case 4:
                $month = 'апреля';
                break;
            case 5:
                $month = 'мая';
                break;
            case 6:
                $month = 'июня';
                break;
            case 7:
                $month = 'июля';
                break;
            case 8:
                $month = 'августа';
                break;
            case 9:
                $month = 'сентября';
                break;
            case 10:
                $month = 'октября';
                break;
            case 11:
                $month = 'ноября';
                break;
            case 12:
                $month = 'декабря';
                break;
            default:
                $month = '';
                break;
        }
        return date('d ', $date).$month.date(' Y', $date);
    }
}
