<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type_service".
 *
 * @property integer $id
 * @property string $name
 * @property string $class
 * @property integer $price
 */
class TypeService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'class', 'price'], 'required'],
            [['price'], 'integer'],
            [['name', 'class'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'class' => 'Class',
            'price' => 'Price',
        ];
    }

    static function GenerateServices(){
        $html = '';
        $allServices = TypeService::find()->all();
        foreach ($allServices as $service) {
            $html .= Yii::$app->controller->renderPartial('/order/service', ['service'=>$service]);
        } 
        return $html;
    }
}
