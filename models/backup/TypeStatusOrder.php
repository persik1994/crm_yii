<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "type_status_order".
 *
 * @property integer $id
 * @property string $name
 * @property integer $color
 */
class TypeStatusOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_status_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'required'],
            [['color'], 'integer'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'color' => 'Color',
        ];
    }

    static function AdminTypes(){
        //$allTypes = array();
        $types = TypeStatusOrder::find()->all();
        //$allTypes['items'] = ArrayHelper::map(
        //    $types,
        //    'id',
        //    function($model) {
        //        return $model['name'];
        //    }
        //);
//
        //$allTypes['params'] = [
        //    'prompt' => 'Выберите тип',
        //];
        return $types;
    }
}
