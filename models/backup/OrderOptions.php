<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_options".
 *
 * @property integer $order_id
 * @property string $track
 * @property string $track_url
 * @property integer $track_local
 * @property string $cr_code
 * @property string $description
 * @property integer $locate_now
 * @property integer $locate_start
 * @property integer $locate_finish
 * @property string $weight
 * @property string $volume
 * @property integer $date_delivered
 * @property string $note
 */
class OrderOptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'track', 'track_url', 'track_local', 'cr_code', 'description', 'locate_now', 'locate_start', 'locate_finish', 'weight', 'volume', 'date_delivered'], 'required'],
            [['order_id', 'track_local', 'locate_now', 'locate_start', 'locate_finish', 'date_delivered'], 'integer'],
            [['track_url', 'description', 'note'], 'string'],
            [['weight', 'volume'], 'number'],
            [['track', 'cr_code'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'track' => 'Track',
            'track_url' => 'Track Url',
            'track_local' => 'Track Local',
            'cr_code' => 'Cr Code',
            'description' => 'Description',
            'locate_now' => 'Locate Now',
            'locate_start' => 'Locate Start',
            'locate_finish' => 'Locate Finish',
            'weight' => 'Weight',
            'volume' => 'Volume',
            'date_delivered' => 'Date Delivered',
            'note' => 'Note',
        ];
    }
}
