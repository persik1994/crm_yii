<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type_delivery".
 *
 * @property integer $id
 * @property string $name
 * @property integer $price
 */
class TypeDelivery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            [['price'], 'integer'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
        ];
    }

    static function GenerateDelivery(){
        $html = '';
        $allDelivery = TypeDelivery::find()->all();
        foreach ($allDelivery as $delivery) {
            $html .= Yii::$app->controller->renderPartial('/order/delivery', ['delivery'=>$delivery]);
        } 
        return $html;
    }

    static function DeliveryList(){
        $deliveries = TypeDelivery::find()->all();
        return $deliveries;
    }
}
