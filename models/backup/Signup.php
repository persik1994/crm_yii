<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Signup extends Model
{
    public $username;
    public $email;
    public $first;
    public $phone;
    public $password;
    public $password_repeat;
    public $status;
    public $role;
    public $money;
    public $type_id;
    public $create_time;

    public function rules()
    {
        return [
            [['username', 'email', 'first', 'phone', 'password', 'password_repeat'], 'required', 'message'=>'Это поле обязательное для заполнения.'],
            ['email', 'email', 'message'=>'Неверный формат почты.'],
            ['email', 'unique', 'targetClass'=>'app\models\Users', 'message'=>'Пользователь с таким e-mail уже зарегистрирован'],
            ['username', 'unique', 'targetClass'=>'app\models\Users', 'message'=>'Пользователь с таким логином уже зарегистрирован'],
            [['first', 'phone', 'password', 'password_repeat'], 'string'],
            [['status', 'role', 'money', 'type_id', 'create_time'], 'integer'],
            ['password', 'compare', 'message'=>'Пароли не совпадают'],
        ];
    }


    public function attributeLabels()
    {

        return [
            'username'        => 'Логин',
            'email'           => 'E-mail',
            'password'        => 'Пароль',
            'password_repeat'=> 'Повтор пароля',
        ];
    }

    public function CreateNew(){
        $singup = new Signup;
        if (Yii::$app->request->post('Singup')){
            $singup->attributes = Yii::$app->request->post('Singup');
            $userNew = new Users;
            $userNew->phone = (int)$singup->phone; 
            $userNew->status = 1; 
            $userNew->role = 1; 
            $userNew->money = rand(1,9999); 
            $userNew->type_id = 1; 
            $userNew->create_time = time();
            $userNew->password = sha1($singup->password);
            /////////
            $userNew->first = $singup->first;
            $userNew->email = $singup->email;
            $userNew->username = $singup->username;
            /////////
            $userNew->save(false);
            return $userNew->id;
        }
    }
}