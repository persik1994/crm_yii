<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "managers".
 *
 * @property string $username
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $telegram
 * @property string $address
 */
class Managers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'managers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'name', 'email', 'phone', 'telegram', 'address'], 'required'],
            [['username'], 'string', 'max' => 25],
            [['name'], 'string', 'max' => 35],
            [['email', 'address'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 20],
            [['telegram'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'telegram' => 'Telegram',
            'address' => 'Address',
        ];
    }
}
