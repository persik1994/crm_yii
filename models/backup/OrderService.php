<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_service".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $type_id
 */
class OrderService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'type_id'], 'required'],
            [['order_id', 'type_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'type_id' => 'Type ID',
        ];
    }
}
