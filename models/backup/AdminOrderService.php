<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admin_order_service".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $type
 * @property string $text
 * @property integer $status
 */
class AdminOrderService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_order_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'type', 'status'], 'required'],
            [['order_id', 'type', 'status'], 'integer'],
            [['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'type' => 'Type',
            'text' => 'Text',
            'status' => 'Status',
        ];
    }
}
