<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "country".
 *
 * @property integer $country_id
 * @property double $name
 * @property double $flag
 * @property integer $status
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id','name','flag','status'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['name', 'flag', 'status'], 'required'],
            [['status'], 'integer'],
            [['name', 'flag'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => 'Country ID',
            'name' => 'Name',
            'flag' => 'Flag',
            'status' => 'Status',
        ];
    }
}
