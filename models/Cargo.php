<?php

namespace app\models;

use Yii;
use app\helpers\HelperStr;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "cargo".
 *
 * @property integer $cargo_id
 * @property integer $shipment_id
 * @property integer $date_add
 * @property integer $date_received
 * @property integer $from_id
 * @property integer $to_id
 * @property integer $date_given_to_client
 * @property integer $delivery_time
 * @property integer $client_id
 * @property integer $manager_id
 * @property integer create_manager_id
 * @property integer $project_id
 * @property integer $warehouse_id
 * @property integer $shipment_type_id
 * @property integer $status_id
 * @property double $weight
 * @property double $volume
 * @property integer $cartons
 * @property double $volume_weight
 * @property integer $count_inside
 * @property string $local_track
 * @property double $local_delivery_price
 * @property string $local_expenses
 * @property string $track
 * @property integer $type_id
 * @property integer $type_special_id
 * @property integer $status
 * @property string $description
 * @property string $photos
 * @property string $invoice
 * @property string $file
 * @property integer $site
 */
class Cargo extends \yii\db\ActiveRecord
{
    public $file1;
    public $file2;
    public $file3;
    ///////
    public $row_shipment_type_id = '';
    public $row_shipment_id = '';
    public $row_status_id = '';
    ///////
    public $row_warehouse_id = '';
    ///////
    public $row_status_count = 0;
    public $row_weight = 0;
    public $row_volume = 0;
    public $row_volume_weight = 0;
    public $row_cartons = 0;
    public $row_count = 0;
    ///////
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cargo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cargo_id','shipment_id','date_add','date_received','from_id','to_id','date_given_to_client', 'delivery_time', 'client_id', 'manager_id', 'project_id', 'warehouse_id', 'shipment_type_id', 'status_id','weight','volume','cartons','volume_weight','count_inside','local_track','local_delivery_price','local_expenses','track','type_id','type_special_id','status' ,'description','photos','invoice','file','site'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['status_id', 'weight', 'volume', 'cartons'], 'required'],
            [['shipment_id', 'date_add', 'date_received', 'from_id', 'to_id', 'date_given_to_client', 'delivery_time', 'client_id', 'manager_id', 'create_manager_id', 'project_id', 'warehouse_id', 'shipment_type_id', 'status_id', 'cartons', 'count_inside', 'type_id','type_special_id', 'status', 'site'], 'integer'],
            [['weight', 'volume', 'volume_weight', 'local_delivery_price'], 'number'],
            [['photos', 'invoice', 'file', 'local_track', 'track', 'description','local_expenses'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cargo_id' => 'Cargo code',
            'shipment_id' => 'Shipment',
            'date_add' => 'Date Add',
            'date_received' => 'Date received',
            'from_id' => 'From ID',
            'to_id' => 'To ID',
            'date_given_to_client' => 'Date Given To Client',
            'delivery_time' => 'Delivery Time',
            'client_id' => 'Client ID',
            'manager_id' => 'Manager',
            'create_manager_id' => 'Manager created',
            'project_id' => 'Project',
            'warehouse_id' => 'Checkpoint',
            'shipment_type_id' => 'Shipment type',
            'status_id' => 'Status',
            'weight' => 'Weight',
            'volume' => 'Volume',
            'cartons' => 'Cartons',
            'volume_weight' => 'Volume weight',
            'count_inside' => 'Qty inside',
            'local_track' => 'Local tracking',
            'local_delivery_price' => 'Local Delivery Price',
            'local_expenses' => 'Local Additional Expenses',
            'track' => 'Track',
            'type_id' => 'Cargo type',
            'type_special_id' => 'Type special',
            'status' => 'Status',
            'description' => 'Description',
            'photos' => 'Photos',
            'invoice' => 'Invoice',
            'file' => 'File',
            'site' => 'Site',
        ];
    }

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_id']);
    }

    public function getManagerCreate()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'create_manager_id']);
    }

    public function getShipmentType()
    {
        return $this->hasOne(ShipmentType::className(), ['type_id' => 'shipment_type_id']);
    }

    public function getShipment()
    {
        return $this->hasOne(Shipment::className(), ['shipment_id' => 'shipment_id']);
    }

    public function getCargoStatus()
    {
        return $this->hasOne(CargoStatus::className(), ['status_id' => 'status_id']);
    }

    public function getFrom()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'from_id']);
    }

    public function getTo()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'to_id']);
    }

    public function getWarehouse()
    {
        return $this->hasOne(Stock::className(), ['stock_id' => 'warehouse_id']);
    }

    public function getType()
    {
        return $this->hasOne(CargoType::className(), ['type_id' => 'type_id']);
    }

    static function special_types(){
        return array(1 => 'Mobile', 4 => 'Liquid', 5 => 'Powder', 6 => 'Battery'); //2 => 'DG Cargo (battery)', 3 => 'Brands', 
    }

    public function special_type($id){
        switch ($id) {
            case 1:
                return Cargo::special_types()[1];
                break;
            // case 2:
            //     return Cargo::special_types()[2];
            //     break;
            // case 3:
            //     return Cargo::special_types()[3];
            //     break;
            case 4:
                return Cargo::special_types()[4];
                break;
            case 5:
                return Cargo::special_types()[5];
                break;
            case 6:
                return Cargo::special_types()[6];
                break;
            default:
                return '';
                break;
        }
    }

    public function uploadFile($file, $path = ''){
        if(!empty($_FILES[$file]['tmp_name'])){
            $info = pathinfo($_FILES[$file]['name']);
            $ext = $info['extension']; // get the extension of the file
            if($ext != 'html' && $ext != 'php'){
                $newname = time().HelperStr::randomText(5, 'string', 'int').".".$ext; 

                $target = '/home/inta/crm.inta.group/public_html/uploads/'.$path.$newname;
                move_uploaded_file( $_FILES[$file]['tmp_name'], $target);
                return '/uploads/'.$path.$newname;
            }
        }
        return '';
    }

    public function uploadFiles($file, $path = ''){
        $all_files = '';
        if(isset($_FILES[$file]['name'])){
            for($file_count=0; $file_count < count($_FILES[$file]['name']); $file_count++){
                $info = pathinfo($_FILES[$file]['name'][$file_count]);
                $ext = $info['extension']; // get the extension of the file
                if($ext != 'html' && $ext != 'php'){
                    $newname = time().HelperStr::randomText(5, 'string', 'int').".".$ext; 

                    $target = '/home/inta/crm.inta.group/public_html/uploads/'.$path.$newname;
                    move_uploaded_file( $_FILES[$file]['tmp_name'][$file_count], $target);
                    if($all_files != ''){
                        $all_files .= ',';
                    }
                    $all_files .= '/uploads/'.$path.$newname;
                }
            }
        }
        return $all_files;
    }

    static function currentCargos(){
        return Cargo::find()->where(['manager_id'=>Yii::$app->user->identity->id])->andWhere('date_received >= '.strtotime('01'.date('.m.Y ').'00:00:00'))->count();
    }

    static function currentCargosKg(){
        $summ = 0;
        $allCargos = Cargo::find()->where(['manager_id'=>Yii::$app->user->identity->id])->andWhere('date_received >= '.strtotime('01'.date('.m.Y ').'00:00:00'))->all();
        foreach ($allCargos as $cargo) {
            $summ += $cargo->weight;
        }
        return $summ;
    }

    static function cargoDropMenu($name, $code, $type = 0, $placeholder = ''){
        $input = '';
        $btns_sort = '';
        $dropdown = '<div class="dropdown-divider"></div>
                      <a class="dropdown-item dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$placeholder.'</a>
                      <div class="dropdown-menu dropdown-menu-table dropdown-menu-table-scroll" aria-labelledby="navbarDropdownMenuLink">';
        switch ($type) {
            case 'text':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'date':
                $input = '<input type="text" class="dropdown-item dropdown-search-input" id="form-choose-date" placeholder="'.$placeholder.'" data-type="'.$code.'">';
                break;
            case 'select':
                switch ($code) {
                    case 'manager_id':
                        $managers = Manager::find()->all();
                        if(isset($managers)){
                            $input .= $dropdown;
                            foreach ($managers as $manager) {
                                $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="'.$manager->manager_id.'">'.$manager->username.'</a>';
                            }
                            $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="9999">Empty field</a>';
                            $input .= '</div>';
                        }
                        break;
                    case 'create_manager_id':
                        $managers = Manager::find()->all();
                        if(isset($managers)){
                            $input .= $dropdown;
                            foreach ($managers as $manager) {
                                $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="'.$manager->manager_id.'">'.$manager->username.'</a>';
                            }
                            $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="9999">Empty field</a>';
                            $input .= '</div>';
                        }
                        break;
                    case 'shipment_type_id':
                        $ShipmentTypes = ShipmentType::find()->all();
                        if(isset($ShipmentTypes)){
                            $input .= $dropdown;
                            foreach ($ShipmentTypes as $type) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$type->type_id.'">'.$type->name.'</a>';
                            }
                            $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="9999">Empty field</a>';
                            $input .= '</div>';
                        }
                        break;
                    case 'warehouse_id':
                        $warehouses = Stock::find()->all();
                        if(isset($warehouses)){
                            $input .= $dropdown;
                            foreach ($warehouses as $warehouse) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$warehouse->stock_id.'">'.$warehouse->name.'</a>';
                            }
                            $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="9999">Empty field</a>';
                            $input .= '</div>';
                        }
                        break;
                    case 'type_id':
                        $types = CargoType::find()->all();
                        if(isset($types)){
                            $input .= $dropdown;
                            foreach ($types as $type) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$type->type_id.'">'.$type->name.'</a>';
                            }
                            $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="9999">Empty field</a>';
                            $input .= '</div>';
                        }
                        break;
                    case 'type_special_id':
                        $special_type = Cargo::special_types();
                        if(isset($special_type)){
                            $input .= $dropdown;
                            foreach ($special_type as $type_key => $type_name) {
                                $input .= '<a class="dropdown-item search-menu-click" data-type="'.$code.'" data-id="'.$type_key.'">'.$type_name.'</a>';
                            }
                            $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="9999">Empty field</a>';
                            $input .= '</div>';
                        }
                        break;
                    case 'status_id':
                        $CargoStatus = CargoStatus::find()->all();
                        if(isset($CargoStatus)){
                            $input .= $dropdown;
                            foreach ($CargoStatus as $status) {
                                $input .= '<a class="dropdown-item search-menu-click " data-type="'.$code.'" data-id="'.$status->status_id.'">'.$status->name.'</a>';
                            }
                            $input .= '<a class="dropdown-item search-menu-click" tabindex="-1" data-type="'.$code.'" data-id="9999">Empty field</a>';
                            $input .= '</div>';
                        }
                        break;
                }
                break;
        }
        if($code != 'local_track' && $code != 'description'){
            $btns_sort = '<a class="dropdown-item" href="?sort='.$code.'">Sort by Asc</a><a class="dropdown-item" href="?sort=-'.$code.'">Sort by Desc</a>';
        }

        return '<div class="dropdown dropdown-table">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      '.$name.'
                    </a>
                    <div class="dropdown-menu dropdown-menu-table" aria-labelledby="dropdownMenuLink">
                      '.$btns_sort.'
                      '.$input.'
                    </div>
                </div>';
    }

    static function SearchSession($type = 0, $type_id = 0){
        $session = Yii::$app->session;

        switch ($type) {
            case 1: //warehouses
                $sesion_type = 'warehouse_'.$type_id.'_';
                break;
            case 2: //filter
                $sesion_type = 'filter_'.$type_id.'_';
                break;
            case 3: //delivered
                $sesion_type = 'delivered_'.$type_id.'_';
                break;
            
            default: //all cargos
                $sesion_type = '';
                break;
        }

        if(Yii::$app->request->get('manager_id')){
            $session['cargo_'.$sesion_type.'manager_id'] = Yii::$app->request->get('manager_id');
            if($session['cargo_'.$sesion_type.'manager_id'] == 9999)
                $session['cargo_'.$sesion_type.'manager_id'] = 0;
        }
        if(Yii::$app->request->get('create_manager_id')){
            $session['cargo_'.$sesion_type.'create_manager_id'] = Yii::$app->request->get('create_manager_id');
            if($session['cargo_'.$sesion_type.'create_manager_id'] == 9999)
                $session['cargo_'.$sesion_type.'create_manager_id'] = 0;
        }
        if(Yii::$app->request->get('warehouse_id')){
            $session['cargo_'.$sesion_type.'warehouse_id'] = Yii::$app->request->get('warehouse_id');
            if($session['cargo_'.$sesion_type.'warehouse_id'] == 9999)
                $session['cargo_'.$sesion_type.'warehouse_id'] = 0;
        }
        if(Yii::$app->request->get('shipment_type_id')){
            $session['cargo_'.$sesion_type.'shipment_type_id'] = Yii::$app->request->get('shipment_type_id');
            if($session['cargo_'.$sesion_type.'shipment_type_id'] == 9999)
                $session['cargo_'.$sesion_type.'shipment_type_id'] = 0;
        }
        if(Yii::$app->request->get('type_id')){
            $session['cargo_'.$sesion_type.'type_id'] = Yii::$app->request->get('type_id');
            if($session['cargo_'.$sesion_type.'type_id'] == 9999)
                $session['cargo_'.$sesion_type.'type_id'] = 0;
        }
        if(Yii::$app->request->get('type_special_id')){
            $session['cargo_'.$sesion_type.'type_special_id'] = Yii::$app->request->get('type_special_id');
            if($session['cargo_'.$sesion_type.'type_special_id'] == 9999)
                $session['cargo_'.$sesion_type.'type_special_id'] = 0;
        }
        if(Yii::$app->request->get('status_id')){
            $session['cargo_'.$sesion_type.'status_id'] = Yii::$app->request->get('status_id');
            if($session['cargo_'.$sesion_type.'status_id'] == 9999)
                $session['cargo_'.$sesion_type.'status_id'] = 0;
        }
        if(Yii::$app->request->get('weight')){
            $session['cargo_'.$sesion_type.'weight'] = Yii::$app->request->get('weight');
        }
        if(Yii::$app->request->get('volume')){
            $session['cargo_'.$sesion_type.'volume'] = Yii::$app->request->get('volume');
        }
        if(Yii::$app->request->get('volume_weight')){
            $session['cargo_'.$sesion_type.'volume_weight'] = Yii::$app->request->get('volume_weight');
        }
        if(Yii::$app->request->get('cartons')){
            $session['cargo_'.$sesion_type.'cartons'] = Yii::$app->request->get('cartons');
        }
        if(Yii::$app->request->get('count_inside')){
            $session['cargo_'.$sesion_type.'count_inside'] = Yii::$app->request->get('count_inside');
        }
        if(Yii::$app->request->get('description')){
            $session['cargo_'.$sesion_type.'description'] = Yii::$app->request->get('description');
        }
        if(Yii::$app->request->get('local_delivery_price')){
            $session['cargo_'.$sesion_type.'local_delivery_price'] = Yii::$app->request->get('local_delivery_price');
        }
        if(Yii::$app->request->get('local_track')){
            $session['cargo_'.$sesion_type.'local_track'] = Yii::$app->request->get('local_track');
        }
        ////////////////////////
        if(Yii::$app->request->get('cargo_id')){
            $session['cargo_'.$sesion_type.'cargo_id'] = (int)str_replace('СС-', '', Yii::$app->request->get('cargo_id'));
        }
        if(Yii::$app->request->get('client_id')){
            $session['cargo_'.$sesion_type.'client_id'] = (int)str_replace('88-', '', Yii::$app->request->get('client_id'));
        }
        if(Yii::$app->request->get('project_id')){
            $session['cargo_'.$sesion_type.'project_id'] = (int)str_replace('PJ-', '', Yii::$app->request->get('project_id'));
        }
        if(Yii::$app->request->get('shipment_id')){
            $session['cargo_'.$sesion_type.'shipment_id'] = (int)str_replace('SH-', '', Yii::$app->request->get('shipment_id'));
        }
        if(Yii::$app->request->get('date_received')){
            $session['cargo_'.$sesion_type.'date_received'] = (int)strtotime(Yii::$app->request->get('date_received'));
        }
        Cargo::SearchQuery($type, $type_id);
    }

    static function SearchQuery($type = 0, $type_id = 0, $limit = 100, $offset = 0){
        $session = Yii::$app->session;

        ///////////
        switch ($type) {
            case 1: //warehouses
                if(Yii::$app->user->identity->role >= 5 || Yii::$app->user->identity->username == 'savenko'){
                    $query = Cargo::find()->where(['status'=>1, 'warehouse_id'=>(int)$type_id])->orderBy(['shipment_type_id' => SORT_DESC, 'shipment_id' => SORT_DESC, 'status_id' => SORT_ASC, 'cargo_id' => SORT_DESC]);
                }else if(Yii::$app->user->identity->username == 'chinawh1' && $type_id == 10){ //chinawh1 = Foshan1 Warehouse все грузы
                    $query = Cargo::find()->where(['status'=>1, 'warehouse_id'=>(int)$type_id])->orderBy(['shipment_type_id' => SORT_DESC, 'shipment_id' => SORT_DESC, 'status_id' => SORT_ASC, 'cargo_id' => SORT_DESC]);
                }else if((Yii::$app->user->identity->username == 'stanislav_rakitskiy' || Yii::$app->user->identity->username == 'chinawh1') && $type_id == 11){ //stanislav_rakitskiy = Foshan2 Warehouse все грузы
                    $query = Cargo::find()->where(['status'=>1, 'warehouse_id'=>(int)$type_id])->orderBy(['shipment_type_id' => SORT_DESC, 'shipment_id' => SORT_DESC, 'status_id' => SORT_ASC, 'cargo_id' => SORT_DESC]);
                }
                else if(Yii::$app->user->identity->username == 'sergei22' && $type_id == 45){ //chinawh1 = Kiev Vidubychi Warehouse все грузы
                    $query = Cargo::find()->where(['status'=>1, 'warehouse_id'=>(int)$type_id])->orderBy(['shipment_type_id' => SORT_DESC, 'shipment_id' => SORT_DESC, 'status_id' => SORT_ASC, 'cargo_id' => SORT_DESC]);
                }else{
                    $query = Cargo::find()->where(['warehouse_id'=>(int)$type_id, 'status'=>1])->orderBy(['shipment_type_id' => SORT_DESC, 'shipment_id' => SORT_DESC, 'status_id' => SORT_ASC, 'cargo_id' => SORT_DESC]);
                    $query->andFilterWhere(['or',
                        ['=','manager_id',Yii::$app->user->identity->id],
                        ['=','create_manager_id',Yii::$app->user->identity->id]
                    ]);
                }
                $sesion_type = 'warehouse_'.$type_id.'_';
                break;
            case 2: //cargo filter
                if(Yii::$app->user->identity->role >= 5 || Yii::$app->user->identity->username == 'savenko'){
                    $query = Cargo::find()->where(['status'=>1, 'status_id'=>(int)$type_id])->orderBy(['warehouse_id' => SORT_ASC, 'shipment_id' => SORT_DESC, 'cargo_id' => SORT_DESC]);
                }else{
                    $query = Cargo::find()->where(['status_id'=>(int)$type_id, 'status'=>1])->orderBy(['warehouse_id' => SORT_ASC, 'shipment_id' => SORT_DESC, 'cargo_id' => SORT_DESC]);
                    $query->andFilterWhere(['or',
                        ['=','manager_id',Yii::$app->user->identity->id],
                        ['=','create_manager_id',Yii::$app->user->identity->id]
                    ]);
                }
                $sesion_type = 'filter_'.$type_id.'_';
                break;
            case 3: //cargo delivered
                if(Yii::$app->user->identity->role >= 5 || Yii::$app->user->identity->username == 'savenko'){
                    $query = Cargo::find()->where(['status'=>1, 'status_id'=>(int)$type_id])->orderBy(['shipment_type_id' => SORT_DESC, 'shipment_id' => SORT_DESC, 'status_id' => SORT_ASC, 'cargo_id' => SORT_DESC]);
                }else{
                    $query = Cargo::find()->where(['status'=>1, 'status_id'=>(int)$type_id])->orderBy(['shipment_type_id' => SORT_DESC, 'shipment_id' => SORT_DESC, 'status_id' => SORT_ASC, 'cargo_id' => SORT_DESC]);
                    $query->andFilterWhere(['or',
                        ['=','manager_id',Yii::$app->user->identity->id],
                        ['=','create_manager_id',Yii::$app->user->identity->id]
                    ]);
                }
                $sesion_type = 'delivered_'.$type_id.'_';
                break;
            
            default: //all cargos
                if(Yii::$app->user->identity->role >= 5 || Yii::$app->user->identity->username == 'savenko'){
                    $query = Cargo::find()->where(['status'=>1])->orderBy(['cargo_id' => SORT_DESC]);
                }else{
                    $query = Cargo::find()->where(['status'=>1])->orderBy(['cargo_id' => SORT_DESC]);
                    $query->andFilterWhere(['or',
                        ['=','manager_id',Yii::$app->user->identity->id],
                        ['=','create_manager_id',Yii::$app->user->identity->id]
                    ]);
                }
                $sesion_type = '';
                break;
        }
        
        /////////

        // $query->offset($offset);
        // $query->limit($limit);

        if($session->has('cargo_'.$sesion_type.'manager_id')){ //
            $query->andFilterWhere(['=', 'manager_id', $session['cargo_'.$sesion_type.'manager_id']]);
        }
        if($session->has('cargo_'.$sesion_type.'create_manager_id')){ //
            $query->andFilterWhere(['=', 'create_manager_id', $session['cargo_'.$sesion_type.'create_manager_id']]);
        }
        if($session->has('cargo_'.$sesion_type.'warehouse_id')){ //
            $query->andFilterWhere(['=', 'warehouse_id', $session['cargo_'.$sesion_type.'warehouse_id']]);
        }
        if($session->has('cargo_'.$sesion_type.'shipment_type_id')){ //
            $query->andFilterWhere(['=', 'shipment_type_id', $session['cargo_'.$sesion_type.'shipment_type_id']]);
        }
        if($session->has('cargo_'.$sesion_type.'type_id')){ //
            $query->andFilterWhere(['=', 'type_id', $session['cargo_'.$sesion_type.'type_id']]);
        }
        if($session->has('cargo_'.$sesion_type.'type_special_id')){ //
            $query->andFilterWhere(['=', 'type_special_id', $session['cargo_'.$sesion_type.'type_special_id']]);
        }
        if($session->has('cargo_'.$sesion_type.'status_id')){ //
            $query->andFilterWhere(['=', 'status_id', $session['cargo_'.$sesion_type.'status_id']]);
        }else if($type_id != 11){
            //$query->andFilterWhere(['!=', 'status_id', 11]);
        }
        if($session->has('cargo_'.$sesion_type.'weight')){
            $query->andFilterWhere(['=', 'weight', $session['cargo_'.$sesion_type.'weight']]);
        }
        if($session->has('cargo_'.$sesion_type.'volume')){
            $query->andFilterWhere(['=', 'volume', $session['cargo_'.$sesion_type.'volume']]);
        }
        if($session->has('cargo_'.$sesion_type.'volume_weight')){
            $query->andFilterWhere(['=', 'volume_weight', $session['cargo_'.$sesion_type.'volume_weight']]);
        }
        if($session->has('cargo_'.$sesion_type.'cartons')){
            $query->andFilterWhere(['=', 'cartons', $session['cargo_'.$sesion_type.'cartons']]);
        }
        if($session->has('cargo_'.$sesion_type.'count_inside')){
            $query->andFilterWhere(['=', 'count_inside', $session['cargo_'.$sesion_type.'count_inside']]);
        }
        if($session->has('cargo_'.$sesion_type.'description')){
            $query->andFilterWhere(['like', 'description', $session['cargo_'.$sesion_type.'description']]);
        }
        if($session->has('cargo_'.$sesion_type.'local_delivery_price')){
            $query->andFilterWhere(['like', 'local_delivery_price', $session['cargo_'.$sesion_type.'local_delivery_price']]);
        }
        if($session->has('cargo_'.$sesion_type.'local_track')){
            $query->andFilterWhere(['like', 'local_track', $session['cargo_'.$sesion_type.'local_track']]);
        }
        ////////////////////////
        if($session->has('cargo_'.$sesion_type.'cargo_id')){ //
            $query->andFilterWhere(['=', 'cargo_id', $session['cargo_'.$sesion_type.'cargo_id']]);
        }
        if($session->has('cargo_'.$sesion_type.'client_id')){ //
            $query->andFilterWhere(['=', 'client_id', $session['cargo_'.$sesion_type.'client_id']]);
        }
        if($session->has('cargo_'.$sesion_type.'project_id')){ //
            $query->andFilterWhere(['=', 'project_id', $session['cargo_'.$sesion_type.'project_id']]);
        }
        if($session->has('cargo_'.$sesion_type.'shipment_id')){ //
            $query->andFilterWhere(['=', 'shipment_id', $session['cargo_'.$sesion_type.'shipment_id']]);
        }
        if($session->has('cargo_'.$sesion_type.'date_received')){ //
            $query->andFilterWhere(['=', 'date_received', $session['cargo_'.$sesion_type.'date_received']]);
        }
        //////date////
        if($session->has('cargo_'.$sesion_type.'date_received')){
            $query->andFilterWhere(['>=', 'date_received', (int)strtotime(date('d.m.Y 00:00',$session['cargo_'.$sesion_type.'date_received']))]);
            $query->andFilterWhere(['<=', 'date_received', (int)strtotime(date('d.m.Y 23:59',$session['cargo_'.$sesion_type.'date_received']))]);
        }
        
        return $query;
    }

    static function SearchShowBtn($type = 0, $type_id = 0){
        $session = Yii::$app->session;
        
        switch ($type) {
            case 1: //warehouses
                $sesion_type = 'warehouse_'.$type_id.'_';
                break;
            case 2: //filter
                $sesion_type = 'filter_'.$type_id.'_';
                break;
            case 3: //delivered
                $sesion_type = 'delivered_'.$type_id.'_';
                break;
            
            default: //all cargos
                $sesion_type = '';
                break;
        }
        
        $btnAll = Cargo::SearchSortBtn();
        foreach ($session as $type => $value){
            switch ($type) {
                case 'cargo_'.$sesion_type.'cargo_id':
                    $btnAll .= Cargo::SearchHtmlBtn($type, 'CC-'.$value);
                    break;

                case 'cargo_'.$sesion_type.'client_id':
                    $btnAll .= Cargo::SearchHtmlBtn($type, '88-'.$value);
                    break;
                
                case 'cargo_'.$sesion_type.'project_id':
                    $btnAll .= Cargo::SearchHtmlBtn($type, 'PJ-'.$value);
                    break;
                
                case 'cargo_'.$sesion_type.'shipment_id':
                    $btnAll .= Cargo::SearchHtmlBtn($type, 'SH-'.$value);
                    break;
                
                case 'cargo_'.$sesion_type.'manager_id':
                    $manager = Manager::findOne(['manager_id'=>$value]);
                    if($value == 0)
                        $btnAll .= Cargo::SearchHtmlBtn($type, 'Manager - Empty field');
                    else
                        $btnAll .= Cargo::SearchHtmlBtn($type, $manager->username);
                    break;
                
                case 'cargo_'.$sesion_type.'create_manager_id':
                    $manager = Manager::findOne(['manager_id'=>$value]);
                    if($value == 0)
                        $btnAll .= Cargo::SearchHtmlBtn($type, 'Manager created - Empty field');
                    else
                        $btnAll .= Cargo::SearchHtmlBtn($type, $manager->username);
                    break;
                
                case 'cargo_'.$sesion_type.'warehouse_id':
                    $warehouse = Stock::findOne(['stock_id'=>$value]);
                    if($value == 0)
                        $btnAll .= Cargo::SearchHtmlBtn($type, 'Ckeckpoint - Empty field');
                    else
                        $btnAll .= Cargo::SearchHtmlBtn($type, $warehouse->name);
                    break;
                
                case 'cargo_'.$sesion_type.'shipment_type_id':
                    $ShipmentType = ShipmentType::findOne(['type_id'=>$value]);
                    if($value == 0)
                        $btnAll .= Cargo::SearchHtmlBtn($type, 'Shipment type - Empty field');
                    else
                        $btnAll .= Cargo::SearchHtmlBtn($type, $ShipmentType->name);
                    break;
                
                case 'cargo_'.$sesion_type.'type_id':
                    $Type = CargoType::findOne(['type_id'=>$value]);
                    if($value == 0)
                        $btnAll .= Cargo::SearchHtmlBtn($type, 'Type - Empty field');
                    else
                        $btnAll .= Cargo::SearchHtmlBtn($type, $Type->name);
                    break;
                
                case 'cargo_'.$sesion_type.'type_special_id':
                    if($value == 0)
                        $btnAll .= Cargo::SearchHtmlBtn($type, 'Special type - Empty field');
                    else
                        $btnAll .= Cargo::SearchHtmlBtn($type, Cargo::special_type($value));
                    break;

                case 'cargo_'.$sesion_type.'date_received':
                    $btnAll .= Cargo::SearchHtmlBtn($type, date('d.M.Y',$value));
                    break;

                case 'cargo_'.$sesion_type.'status_id':
                    $CargoStatus = CargoStatus::findOne(['status_id'=>$value]);
                    if($value == 0)
                        $btnAll .= Cargo::SearchHtmlBtn($type, 'Status - Empty field');
                    else
                        $btnAll .= Cargo::SearchHtmlBtn($type, $CargoStatus->name);
                    break;

                case 'cargo_'.$sesion_type.'weight':
                    $btnAll .= Cargo::SearchHtmlBtn($type, 'Weight = '.$value);
                    break;

                case 'cargo_'.$sesion_type.'volume':
                    $btnAll .= Cargo::SearchHtmlBtn($type, 'Volume = '.$value);
                    break;

                case 'cargo_'.$sesion_type.'volume_weight':
                    $btnAll .= Cargo::SearchHtmlBtn($type, 'Volume Weight = '.$value);
                    break;

                case 'cargo_'.$sesion_type.'cartons':
                    $btnAll .= Cargo::SearchHtmlBtn($type, 'Cartons = '.$value);
                    break;

                case 'cargo_'.$sesion_type.'count_inside':
                    $btnAll .= Cargo::SearchHtmlBtn($type, 'Qty = '.$value);
                    break;

                case 'cargo_'.$sesion_type.'description':
                    $btnAll .= Cargo::SearchHtmlBtn($type, 'Desc = '.$value);
                    break;

                case 'cargo_'.$sesion_type.'local_delivery_price':
                    $btnAll .= Cargo::SearchHtmlBtn($type, 'Local price = '.$value);
                    break;

                case 'cargo_'.$sesion_type.'local_track':
                    $btnAll .= Cargo::SearchHtmlBtn($type, 'Track = '.$value);
                    break;
            }
        }
        if(!empty($btnAll)){
            $btnAll .= '<p class="btn-search-filter-show btn-search-filter-delete">Remove all filters <a href="/cargo/searchdeleteall"><i class="fas fa-times"></i></a></p>';
        }
        return $btnAll;
    }

    static function DeleteAllSession(){
        $session = Yii::$app->session;
        foreach ($session as $type => $value){
            if(stristr($type, 'cargo_') !== FALSE) {
                $session->remove($type);
            }
        }
    }

    static function SearchSortBtn(){
        $row = '';
        if(Yii::$app->request->get('sort')){
            if(stristr(Yii::$app->request->get('sort'), '-') === FALSE) {
                $sort = 'ASC';
                if(isset(Cargo::attributeLabels()[Yii::$app->request->get('sort')])){
                    $row = Cargo::attributeLabels()[Yii::$app->request->get('sort')];
                }
            }
            else{
                $sort = 'DESC';
                if(isset(Cargo::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))])){
                    $row = Cargo::attributeLabels()[str_replace('-', '', Yii::$app->request->get('sort'))];
                }
            }
            return '<p class="btn-search-filter-show">Sort '.$sort.' '.$row.'<a href="index?sort="><i class="fas fa-times"></i></a></p>';
        }
        return '';
    }

    static function SearchHtmlBtn($type, $name){
        return '<p class="btn-search-filter-show" data-type="'.$type.'"><span>'.$name.'</span><a href="/cargo/searchdelete?type='.$type.'&sort='.Yii::$app->request->get('sort').'"><i class="fas fa-times"></i></a></p>';
    }
}
