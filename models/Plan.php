<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plan".
 *
 * @property integer $plan_id
 * @property integer $manager_id
 * @property integer $date
 * @property string $plan_money
 * @property string $amount_money
 * @property string $plan_air
 * @property string $fact_air
 * @property string $fix_salary
 * @property string $bonus
 * @property string $fines
 * @property string $profit
 * @property string $total
 * @property string $bonus_percent
 * @property integer $payment_id
 * @property integer $status
 */
class Plan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manager_id', 'date'], 'required'],
            [['manager_id', 'date', 'payment_id', 'status'], 'integer'],
            [['plan_money', 'amount_money', 'plan_air', 'fact_air', 'fix_salary', 'bonus', 'fines', 'profit', 'total', 'bonus_percent'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => 'Plan ID',
            'manager_id' => 'Manager ID',
            'date' => 'Date',
            'plan_money' => 'Plan Money',
            'amount_money' => 'Amount Money',
            'plan_air' => 'Plan Air',
            'fact_air' => 'Fact Air',
            'fix_salary' => 'Fix Salary',
            'bonus' => 'Bonus',
            'fines' => 'Fines',
            'profit' => 'Profit',
            'total' => 'Total',
            'bonus_percent' => 'Bonus Percent',
            'payment_id' => 'Payment ID',
            'status' => 'Status',
        ];
    }

    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['manager_id' => 'manager_id']);
    }
}
