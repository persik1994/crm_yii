<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "tariff_measure".
 *
 * @property integer $tariff_id
 * @property string $name
 * @property string $math
 * @property integer $status
 */
class TariffMeasure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff_measure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id','name','math','status'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['name', 'math', 'status'], 'required'],
            [['math'], 'string'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id' => 'Tariff ID',
            'name' => 'Name',
            'math' => 'Math',
            'status' => 'Status',
        ];
    }
}
