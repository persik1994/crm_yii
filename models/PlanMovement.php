<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plan_movement".
 *
 * @property integer $id
 * @property integer $plan_id
 * @property integer $type
 * @property integer $date
 * @property string $amount
 * @property string $description
 */
class PlanMovement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan_movement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_id', 'type', 'date', 'amount'], 'required'],
            [['plan_id', 'type', 'date'], 'integer'],
            [['amount'], 'number'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plan_id' => 'Plan ID',
            'type' => 'Type',
            'date' => 'Date',
            'amount' => 'Amount',
            'description' => 'Description',
        ];
    }
}
