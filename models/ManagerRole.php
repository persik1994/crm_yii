<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "manager_role".
 *
 * @property integer $role_id
 * @property string $name
 * @property string $permission
 * @property integer $status
 */
class ManagerRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manager_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id','name','permission','status'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['name', 'permission', 'status'], 'required'],
            [['permission'], 'string'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'role_id' => 'Role ID',
            'name' => 'Name',
            'permission' => 'Permission',
            'status' => 'Status',
        ];
    }
}
