<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "client_delivery".
 *
 * @property integer $delivery_id
 * @property integer $client_id
 * @property string $courier_id
 * @property string $receiver
 * @property string $mobile
 * @property string $city
 * @property string $address
 */
class ClientDelivery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id','client_id','courier_id','receiver','mobile','city','state','address'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['client_id', 'receiver', 'mobile', 'city', 'state', 'address', 'courier_id'], 'required'],
            [['courier_id', 'client_id', 'state', 'mobile', 'city'], 'integer'],
            [['receiver', 'address'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'delivery_id' => 'Delivery ID',
            'client_id' => 'Client ID',
            'courier_id' => 'Courier',
            'receiver' => 'Receiver',
            'mobile' => 'Mobile',
            'city' => 'City',
            'address' => 'Address',
        ];
    }
}
