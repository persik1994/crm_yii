<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shipment_calculation".
 *
 * @property integer $calculation_id
 * @property integer $column_id
 * @property integer $shipment_id
 * @property string $amount
 */
class ShipmentCalculation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipment_calculation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['column_id', 'shipment_id', 'amount'], 'required'],
            [['column_id', 'shipment_id'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'calculation_id' => 'Calculation ID',
            'column_id' => 'Column ID',
            'shipment_id' => 'Shipment ID',
            'amount' => 'Amount',
        ];
    }

    static function MathChargersCargo($cargo){
        $chargers = 0;
        switch ($cargo->type_id) {
            case 1:
                if($cargo->shipment_type_id == 5 || $cargo->shipment_type_id == 9){
                    $calc = ShipmentCalculation::findOne(['shipment_id'=>$cargo->shipment_id, 'column_id'=>4]);
                    $chargers = (!empty($calc->amount)) ? $calc->amount * $cargo->volume : 0;
                }
                else{
                    $calc = ShipmentCalculation::findOne(['shipment_id'=>$cargo->shipment_id, 'column_id'=>1]);
                    $chargers = (!empty($calc->amount)) ? $calc->amount * $cargo->weight : 0;
                }
                break;
            case 2:
                if($cargo->shipment_type_id == 5 || $cargo->shipment_type_id == 9){
                    $calc = ShipmentCalculation::findOne(['shipment_id'=>$cargo->shipment_id, 'column_id'=>5]);
                    $chargers = (!empty($calc->amount)) ? $calc->amount * $cargo->volume : 0;
                }
                else{
                    $calc = ShipmentCalculation::findOne(['shipment_id'=>$cargo->shipment_id, 'column_id'=>2]);
                    $chargers = (!empty($calc->amount)) ? $calc->amount * $cargo->weight : 0;
                }
                break;
            case 3:
                if($cargo->shipment_type_id == 5 || $cargo->shipment_type_id == 9){
                    $calc = ShipmentCalculation::findOne(['shipment_id'=>$cargo->shipment_id, 'column_id'=>6]);
                    $chargers = (!empty($calc->amount)) ? $calc->amount * $cargo->volume : 0;
                }
                else{
                    $calc = ShipmentCalculation::findOne(['shipment_id'=>$cargo->shipment_id, 'column_id'=>3]);
                    $chargers = (!empty($calc->amount)) ? $calc->amount * $cargo->weight : 0;
                }
                break;
        }
        return $chargers;
    }

    static function MathPriceProject($cargo){
        $price = 0;
        if(isset($cargo->project_id) && $cargo->project_id > 0){
            $project = Project::findOne(['project_id'=>$cargo->project_id]);

            if(isset($project)){
                if($cargo->shipment_type_id == 5 || $cargo->shipment_type_id == 9){
                    $price = ClientTariff::find()->where(['project_id'=>$project->project_id, 'client_id'=>$project->client_id, 'tariff_id'=>3,'type_cargo_id'=>$cargo->type_id])->one();
                    if(isset($price)){
                        $price = $price->number * $cargo->volume;
                    }
                }
                else{
                    $price = ClientTariff::find()->where(['project_id'=>$project->project_id, 'client_id'=>$project->client_id, 'tariff_id'=>1,'type_cargo_id'=>$cargo->type_id])->one();
                    if(isset($price)){
                        $price = $price->number * $cargo->weight;
                    }
                }
            }
        }
        return $price;
    }
}
