<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_services".
 *
 * @property integer $services_id
 * @property integer $category_id
 * @property integer $list_id
 * @property integer $sort
 */
class SiteServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'list_id', 'sort'], 'integer'],
            [['list_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'services_id' => 'Services ID',
            'category_id' => 'Category ID',
            'list_id' => 'List ID',
            'sort' => 'Sort',
        ];
    }

    static function findService($services_id, $language = 1){
        $service = SiteServices::findOne(['services_id'=>(int)$services_id]);
        if(isset($service)){
            $list = SiteList::findOne(['list_id'=>$service->list_id, 'status'=>1]);
            if(isset($list)){
                $translate = SiteTranslate::findOne(['list_id'=>$list->list_id, 'lang_id'=>(int)$language]);
                if(isset($translate)){
                    return array('translate'=>$translate,'service'=>$service,'list'=>$list);
                }
            }
        }
        return array();
    }
}
