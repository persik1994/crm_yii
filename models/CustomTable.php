<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "custom_table".
 *
 * @property integer $id
 * @property integer $manager_id
 * @property string $page
 * @property string $page_two
 * @property string $type
 */
class CustomTable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manager_id', 'page', 'page_two', 'type'], 'required'],
            [['manager_id'], 'integer'],
            [['page', 'page_two', 'type'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manager_id' => 'Manager ID',
            'page' => 'Page',
            'page_two' => 'Page Two',
            'type' => 'Type',
        ];
    }

    static function rowVisible($page = '', $page_two = '', $type = ''){
        //////якщо немає кастомізации на цій сторінці, тоді все відображати
        if(CustomTable::find()->where(['manager_id'=>Yii::$app->user->identity->id,'page'=>$page,'page_two'=>$page_two])->count() == 0){
            return '';
        }
        //////якщо є кастомізация на цій сторінці, тоді...
        $find = CustomTable::findOne([
            'manager_id'=>Yii::$app->user->identity->id,
            'page'=>$page,
            'page_two'=>$page_two,
            'type'=>$type,
        ]);
        if(!isset($find)){
            return 'display:none;';
        }else{
            return '';
        }
    }

    static function rowCounts($page = 'cargo', $page_two = '', $weight = 0, $volume = 0, $volume_weight = 0, $cartons = 0){
        $html = '';
        $allTypes = array('cargo_id','photos','date_received','client_id','manager_id','create_manager_id','warehouse_id','shipment_type_id','status_id', 'weight','volume','volume_weight','cartons','count_inside','local_delivery_price','type_id', 'type_special_id', 'project_id','shipment_id','local_track','description');

        if(CustomTable::find()->where(['manager_id'=>Yii::$app->user->identity->id,'page'=>$page,'page_two'=>$page_two])->count() == 0){
            $html = '<tr class="cargo-row"><td></td>';
            foreach ($allTypes as $type) {
                switch ($type) {
                    case 'weight':
                        $html .= '<td class="table-row-count-summ">'.$weight.'</td>';
                        break;
                    case 'volume':
                        $html .= '<td class="table-row-count-summ">'.$volume.'</td>';
                        break;
                    case 'volume_weight':
                        $html .= '<td class="table-row-count-summ">'.$volume_weight.'</td>';
                        break;
                    case 'cartons':
                        $html .= '<td class="table-row-count-summ">'.$cartons.'</td>';
                        break;
                    case 'photos':
                        $html .= '<td style="width: 50px;min-width: 50px;max-width: 50px;"></td>';
                        break;
                    default:
                        $html .= '<td></td>';
                        break;
                }
            }
            $html .= '<tr class="cargo-row">';
        }
        else{
            $html = '<tr class="cargo-row"><td></td>';
            foreach ($allTypes as $type) {
                $find = CustomTable::findOne([
                    'manager_id'=>Yii::$app->user->identity->id,
                    'page'=>$page,
                    'page_two'=>$page_two,
                    'type'=>$type,
                ]);
                if(isset($find)){
                    switch ($type) {
                        case 'weight':
                            $html .= '<td class="table-row-count-summ">'.$weight.'</td>';
                            break;
                        case 'volume':
                            $html .= '<td class="table-row-count-summ">'.$volume.'</td>';
                            break;
                        case 'volume_weight':
                            $html .= '<td class="table-row-count-summ">'.$volume_weight.'</td>';
                            break;
                        case 'cartons':
                            $html .= '<td class="table-row-count-summ">'.$cartons.'</td>';
                            break;
                        case 'photos':
                            $html .= '<td style="width: 50px;min-width: 50px;max-width: 50px;"></td>';
                            break;
                        default:
                            $html .= '<td></td>';
                            break;
                    }
                }
            }
            $html .= '<tr class="cargo-row">';
        }
        return $html;
    }

    static function menuOptions($page = '', $page_two = ''){
        $checkboxes = '';
        switch ($page_two) {
            case 'index':
            case 'delivered':
            case 'filter':
            case 'warehouse':
                $allTypes = array('cargo_id','photos','date_received','client_id','manager_id','create_manager_id','warehouse_id','shipment_type_id','type_id', 'type_special_id','project_id','shipment_id', 'status_id', 'weight','volume','cartons','volume_weight','count_inside','local_delivery_price','local_track','description');
                break;

            case 'client':
                $allTypes = array('client_id','manager_id','balance','date_add','name','phone','email','source_id','status_id','activation', 'comment');
                break;

            case 'project':
                $allTypes = array('project_id','name','manager_id','client_id','date_create','date_close','project_type','delivery_type','buying_id', 'total', 'profit', 'payed', 'must_pay', 'chargers', 'plan_bonuse', 'comment');
                break;

            case 'payment':
                $allTypes = array('payment_id','manager_id','client_id','balance','date_payment','payment_type','source_id','out_source_id','project_id','cash_usd', 'cash_fact','chargers_project', 'client_paid','project_profit','balance' ,'cash_fact_currency','status','comment');
                break;

            case 'shipment':
            case 'shipment_filter':
                $allTypes = array('shipment_id','date_created','from_id','to_id','manager_id','status_id','checkpoint','type_id', 'weight', 'volume','cartons','comment');
                break;

            case 'buying':
                $allTypes = array('buying_id','date_create','client_id','manager_id','manager_buy_id','project_id','status_id','total','name','date_file', 'date_file_two', 'comment');
                break;
            
            default:
                $allTypes = array();
                break;
        }

        if(CustomTable::find()->where(['manager_id'=>Yii::$app->user->identity->id,'page'=>$page,'page_two'=>$page_two])->count() == 0){
            foreach ($allTypes as $type) {
                switch ($page) {
                    case 'cargo':
                        $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.Cargo::attributeLabels()[$type].'<Br>';
                        break;
                    
                    case 'client':
                        $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.Client::attributeLabels()[$type].'<Br>';
                        break;
                    
                    case 'project':
                        $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.Project::attributeLabels()[$type].'<Br>';
                        break;
                    
                    case 'shipment':
                        $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.Shipment::attributeLabels()[$type].'<Br>';
                        break;
                    
                    case 'payment':
                            $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.Payments::attributeLabels()[$type].'<Br>';
                        break;
                    
                    case 'buying':
                        $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.BuyingList::attributeLabels()[$type].'<Br>';
                        break;
                }
                
            }
        }
        else{
            foreach ($allTypes as $type) {
                $find = CustomTable::findOne([
                    'manager_id'=>Yii::$app->user->identity->id,
                    'page'=>$page,
                    'page_two'=>$page_two,
                    'type'=>$type,
                ]);
                switch ($page) {
                    case 'cargo':
                        if(!isset($find)){
                            $checkboxes .= '<input type="checkbox" value="'.$type.'">'.Cargo::attributeLabels()[$type].'<Br>';
                        }else{
                            $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.Cargo::attributeLabels()[$type].'<Br>';
                        }
                        break;
                    
                    case 'client':
                        if(!isset($find)){
                            $checkboxes .= '<input type="checkbox" value="'.$type.'">'.Client::attributeLabels()[$type].'<Br>';
                        }else{
                            $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.Client::attributeLabels()[$type].'<Br>';
                        }
                        break;
                    
                    case 'project':
                        if(!isset($find)){
                            $checkboxes .= '<input type="checkbox" value="'.$type.'">'.Project::attributeLabels()[$type].'<Br>';
                        }else{
                            $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.Project::attributeLabels()[$type].'<Br>';
                        }
                        break;
                    
                    case 'shipment':
                        if(!isset($find)){
                            $checkboxes .= '<input type="checkbox" value="'.$type.'">'.Shipment::attributeLabels()[$type].'<Br>';
                        }else{
                            $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.Shipment::attributeLabels()[$type].'<Br>';
                        }
                        break;
                    
                    case 'payment':
                        if(!isset($find)){
                            $checkboxes .= '<input type="checkbox" value="'.$type.'">'.Payments::attributeLabels()[$type].'<Br>';
                        }else{
                            $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.Payments::attributeLabels()[$type].'<Br>';
                        }
                        break;
                    
                    case 'buying':
                        if(!isset($find)){
                            $checkboxes .= '<input type="checkbox" value="'.$type.'">'.BuyingList::attributeLabels()[$type].'<Br>';
                        }else{
                            $checkboxes .= '<input type="checkbox" checked value="'.$type.'">'.BuyingList::attributeLabels()[$type].'<Br>';
                        }
                        break;
                }
                
                
            }
        }
        return $checkboxes;
    }

    static function widthTable($width){
        return 'width: '.$width.'px;min-width: '.$width.'px;max-width: '.$width.'px;';
    }
}
