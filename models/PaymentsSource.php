<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "payments_source".
 *
 * @property integer $source_id
 * @property string $name
 * @property string $flag
 * @property integer $status
 */
class PaymentsSource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_source';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_id','name','flag','status'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['name', 'flag', 'status'], 'required'],
            [['name', 'flag'], 'string'],
            [['status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'source_id' => 'Source ID',
            'name' => 'Name',
            'flag' => 'Flag',
            'status' => 'Status',
        ];
    }
}
