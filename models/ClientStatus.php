<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_status".
 *
 * @property integer $status_id
 * @property string $name
 * @property integer $status
 */
class ClientStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id' => 'Status ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }
}
