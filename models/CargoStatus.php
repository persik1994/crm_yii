<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "cargo_status".
 *
 * @property integer $status_id
 * @property string $name
 * @property string $flag
 * @property integer $status
 */
class CargoStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cargo_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id','name','flag','status'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            [['name', 'flag', 'status'], 'required'],
            [['status'], 'integer'],
            [['name', 'flag'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id' => 'Status ID',
            'name' => 'Name',
            'flag' => 'Flag',
            'status' => 'Status',
        ];
    }
}
