<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_lang".
 *
 * @property integer $lang_id
 * @property string $name
 * @property string $code
 * @property string $flag
 * @property integer $sort
 * @property integer $status
 */
class SiteLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'flag'], 'required'],
            [['name', 'code', 'flag'], 'string'],
            [['sort', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lang_id' => 'Lang ID',
            'name' => 'Name',
            'code' => 'Code',
            'flag' => 'Flag',
            'sort' => 'Sort',
            'status' => 'Status',
        ];
    }
}
