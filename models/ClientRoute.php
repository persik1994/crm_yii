<?php

namespace app\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "client_route".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $from_id
 * @property integer $to_id
 */
class ClientRoute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_route';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','client_id','from_id','to_id'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            
            [['client_id', 'from_id', 'to_id'], 'required'],
            [['client_id', 'from_id', 'to_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'from_id' => 'From ID',
            'to_id' => 'To ID',
        ];
    }
}
