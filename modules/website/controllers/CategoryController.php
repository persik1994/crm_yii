<?php

namespace app\modules\website\controllers;

use Yii;
use app\models\SiteCategory;
use app\models\SiteTranslate;
use app\models\SiteList;
use app\models\SiteLang;

use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for SiteCategory model.
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SiteCategory::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SiteCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SiteCategory;
        $languages = SiteLang::find()->all();
        if(Yii::$app->request->post('category-title') && Yii::$app->request->post('lang')){
            ///create list///
            $list = new SiteList;
            $list->status = 1;
            if($list->save()){
                ///create category///
                $model->list_id = $list->list_id;
                $model->sort = 0;
                $model->type = 1;
                if($model->save()){
                    ///create translate
                    if(Yii::$app->request->post('lang') != 1){
                        $translate = new SiteTranslate;
                        $translate->lang_id = 1;
                        $translate->list_id = $list->list_id;
                        $translate->title = Yii::$app->request->post('category-title');
                        $translate->title_cut = Yii::$app->request->post('category-title');
                        $translate->save(false);
                    }
                    $translate = new SiteTranslate;
                    $translate->lang_id = (int)Yii::$app->request->post('lang');
                    $translate->list_id = $list->list_id;
                    $translate->title = Yii::$app->request->post('category-title');
                    $translate->title_cut = Yii::$app->request->post('category-title');
                    $translate->save(false);
                    return $this->redirect(['update', 'id' => $model->category_id]);
                }
            }  
        } else {
            return $this->render('create', [
                'model' => $model,
                'languages' => $languages,
            ]);
        }
    }

    /**
     * Updates an existing SiteCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $languages = SiteLang::find()->all();

        if (Yii::$app->request->post()) {
            if(Yii::$app->request->post('sort')){
                $model->sort = (int)Yii::$app->request->post('sort');
                $model->update();
            }
            else if(Yii::$app->request->post('category-title') && Yii::$app->request->post('lang') && Yii::$app->request->post('list')){
                $translate = SiteTranslate::findOne(['translate_id'=>(int)Yii::$app->request->post('translate')]);
                if(isset($translate)){
                    $translate->title = Yii::$app->request->post('category-title');
                    $translate->title_cut = Yii::$app->request->post('category-title');
                    $translate->update(false);
                }else{
                    $translate = new SiteTranslate;
                    $translate->lang_id = (int)Yii::$app->request->post('lang');
                    $translate->list_id = (int)Yii::$app->request->post('list');
                    $translate->title = Yii::$app->request->post('category-title');
                    $translate->title_cut = Yii::$app->request->post('category-title');
                    $translate->save(false);
                }
            }
            return $this->redirect(['update', 'id' => $model->category_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'languages' => $languages,
            ]);
        }
    }

    /**
     * Deletes an existing SiteCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SiteCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SiteCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SiteCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
