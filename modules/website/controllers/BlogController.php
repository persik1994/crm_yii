<?php

namespace app\modules\website\controllers;

use Yii;
use app\models\SiteBlog;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\SiteCategory;
use app\models\SiteTranslate;
use app\models\SiteList;
use app\models\SiteLang;
use app\models\SiteMeta;

/**
 * BlogController implements the CRUD actions for SiteBlog model.
 */
class BlogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SiteBlog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SiteBlog::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteBlog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SiteBlog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    public function actionCreate()
    {
        $model = new SiteBlog;
        $languages = SiteLang::find()->all();
        $category_one = SiteCategory::find()->one();

        if(Yii::$app->request->post('blog-title') && Yii::$app->request->post('blog-title-cut') && Yii::$app->request->post('lang')){
            ///create list///
            $list = new SiteList;
            $list->status = 1;
            if($list->save()){
                ///create blog///
                $model->category_id = $category_one->category_id;
                $model->list_id = $list->list_id;
                $model->sort = 0;
                $model->date = time();
                if($model->save()){
                    ///create translate
                    if(Yii::$app->request->post('lang') != 1){
                        $translate = new SiteTranslate;
                        $translate->lang_id = (int)Yii::$app->request->post('lang');
                        $translate->list_id = $list->list_id;
                        $translate->title = Yii::$app->request->post('blog-title');
                        $translate->title_cut = Yii::$app->request->post('blog-title-cut');
                        $translate->description = Yii::$app->request->post('blog-description');
                        $translate->description_cut = Yii::$app->request->post('blog-description-cut');
                        ///////files
                        $file1 = SiteBlog::uploadFile('img', 'images/');
                        if(!empty($file1))
                            $translate->img = $file1;
                        else
                            $translate->img = '/web/img/about-bg.jpg';
                        $file2 = SiteBlog::uploadFile('img_mini', 'images/');
                        if(!empty($file2))
                            $translate->img_mini = $file2;
                        else
                            $translate->img_mini = '/web/img/new-3.png';
                        $translate->save(false);
                    }
                    $translate = new SiteTranslate;
                    $translate->lang_id = 1;
                    $translate->list_id = $list->list_id;
                    $translate->title = Yii::$app->request->post('blog-title');
                    $translate->title_cut = Yii::$app->request->post('blog-title-cut');
                    $translate->description = Yii::$app->request->post('blog-description');
                    $translate->description_cut = Yii::$app->request->post('blog-description-cut');
                    ///////files
                    $file1 = SiteBlog::uploadFile('img', 'images/');
                    if(!empty($file1))
                        $translate->img = $file1;
                    $file2 = SiteBlog::uploadFile('img_mini', 'images/');
                    if(!empty($file2))
                        $translate->img_mini = $file2;
                    $translate->save(false);
                    return $this->redirect(['update', 'id' => $model->blog_id]);
                }
            }  
            $translate = new SiteTranslate;
            $translate->lang_id = (int)Yii::$app->request->post('lang');
            $translate->list_id = (int)Yii::$app->request->post('list');
            $translate->title = Yii::$app->request->post('blog-title');
            $translate->title_cut = Yii::$app->request->post('blog-title-cut');
            $translate->description = Yii::$app->request->post('blog-description');
            $translate->description_cut = Yii::$app->request->post('blog-description-cut');
            ///////files
            $file1 = SiteBlog::uploadFile('img', 'images/');
            if(!empty($file1))
                $translate->img = $file1;
            $file2 = SiteBlog::uploadFile('img_mini', 'images/');
            if(!empty($file2))
                $translate->img_mini = $file2;
            $translate->save(false);
            return $this->redirect(['update', 'id' => $model->blog_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'languages' => $languages,
            ]);
        }
    }

    /**
     * Updates an existing SiteBlog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $languages = SiteLang::find()->all();
        $categories = SiteCategory::find()->where(['type'=>1])->all();

        if (Yii::$app->request->post()) {
            if(Yii::$app->request->post('sort') || Yii::$app->request->post('category')){
                if(Yii::$app->request->post('sort')){
                    $model->sort = (int)Yii::$app->request->post('sort');
                }
                $model->category_id = (int)Yii::$app->request->post('category');
                $model->update(false);
            }
            else if(Yii::$app->request->post('blog-title') && Yii::$app->request->post('blog-title-cut') && Yii::$app->request->post('lang') && Yii::$app->request->post('list')){
                $translate = SiteTranslate::findOne(['translate_id'=>(int)Yii::$app->request->post('translate')]);
                if(isset($translate)){
                    $translate->title = Yii::$app->request->post('blog-title');
                    $translate->title_cut = Yii::$app->request->post('blog-title-cut');
                    $translate->description = Yii::$app->request->post('blog-description');
                    $translate->description_cut = Yii::$app->request->post('blog-description-cut');
                    ///////files
                    $file1 = SiteBlog::uploadFile('img', 'images/');
                    if(!empty($file1))
                        $translate->img = $file1;
                    $file2 = SiteBlog::uploadFile('img_mini', 'images/');
                    if(!empty($file2))
                        $translate->img_mini = $file2;
                    $translate->update(false);
                }else{
                    $translate = new SiteTranslate;
                    $translate->lang_id = (int)Yii::$app->request->post('lang');
                    $translate->list_id = (int)Yii::$app->request->post('list');
                    $translate->title = Yii::$app->request->post('blog-title');
                    $translate->title_cut = Yii::$app->request->post('blog-title-cut');
                    $translate->description = Yii::$app->request->post('blog-description');
                    $translate->description_cut = Yii::$app->request->post('blog-description-cut');
                    ///////files
                    $file1 = SiteBlog::uploadFile('img', 'images/');
                    if(!empty($file1))
                        $translate->img = $file1;
                    else
                        $translate->img = '/web/img/about-bg.jpg';
                    $file2 = SiteBlog::uploadFile('img_mini', 'images/');
                    if(!empty($file2))
                        $translate->img_mini = $file2;
                    else
                        $translate->img_mini = '/web/img/new-3.png';
                    $translate->save(false);
                }
            }
            if(Yii::$app->request->post('lang') && 
                (
                    Yii::$app->request->post('meta-title') || 
                    Yii::$app->request->post('meta-description') || 
                    Yii::$app->request->post('meta-keywords') || 
                    Yii::$app->request->post('meta-url')
                )
            ){
                $find_meta = SiteMeta::findOne(['type'=>6,'post_id'=>$model->blog_id,'lang_id'=>(int)Yii::$app->request->post('lang')]);
                if(isset($find_meta)){
                    $find_meta->type = 6;
                    $find_meta->post_id = $model->blog_id;
                    $find_meta->lang_id = (int)Yii::$app->request->post('lang');
                    $find_meta->meta_title = (Yii::$app->request->post('meta-title')) ? Yii::$app->request->post('meta-title') : '';
                    $find_meta->meta_description = (Yii::$app->request->post('meta-description')) ? Yii::$app->request->post('meta-description') : '';
                    $find_meta->meta_keywords = (Yii::$app->request->post('meta-keywords')) ? Yii::$app->request->post('meta-keywords') : '';
                    $find_meta->meta_url = (Yii::$app->request->post('meta-url')) ? Yii::$app->request->post('meta-url') : '';
                    $find_meta->update(false);
                }else{
                    $new_meta = new SiteMeta;
                    $new_meta->type = 6;
                    $new_meta->post_id = $model->blog_id;
                    $new_meta->lang_id = (int)Yii::$app->request->post('lang');
                    $new_meta->meta_title = (Yii::$app->request->post('meta-title')) ? Yii::$app->request->post('meta-title') : '';
                    $new_meta->meta_description = (Yii::$app->request->post('meta-description')) ? Yii::$app->request->post('meta-description') : '';
                    $new_meta->meta_keywords = (Yii::$app->request->post('meta-keywords')) ? Yii::$app->request->post('meta-keywords') : '';
                    $new_meta->meta_url = (Yii::$app->request->post('meta-url')) ? Yii::$app->request->post('meta-url') : '';
                    $new_meta->save(false);
                }
            }
            return $this->redirect(['update', 'id' => $model->blog_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'languages' => $languages,
                'categories' => $categories,
            ]);
        }
    }

    public function actionImgdelete(){
        if(Yii::$app->request->get('img')){
            $model = SiteTranslate::findOne(['translate_id'=>(int)Yii::$app->request->get('img')]);
            $model->img = '/web/img/about-bg.jpg';
            $model->update(false);
        }else if(Yii::$app->request->get('img_mini')){
            $model = SiteTranslate::findOne(['translate_id'=>(int)Yii::$app->request->get('img_mini')]);
            $model->img_mini = '/web/img/new-3.png';
            $model->update(false);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deletes an existing SiteBlog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SiteBlog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SiteBlog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SiteBlog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
