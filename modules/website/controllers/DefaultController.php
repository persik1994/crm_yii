<?php

namespace app\modules\website\controllers;

use yii\web\Controller;
use app\models\Category;

/**
 * Default controller for the `website` module
 */
class DefaultController extends Controller
{
    public function actionIndex(){
        return $this->render('index');
    }
}
