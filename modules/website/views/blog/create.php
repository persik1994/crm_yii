<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SiteBlog */

$this->title = Yii::t('app', 'Создание статьи');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', ' > Статьи > '), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-blog-create">

    <?= $this->render('_form_create', [
        'model' => $model,
        'languages' => $languages,
    ]) ?>

</div>
