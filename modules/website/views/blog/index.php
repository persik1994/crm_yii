<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\SiteCategory;
use app\models\SiteList;
use app\models\SiteTranslate;
use app\models\SiteBlog;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Статьи');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-blog-index" style="margin: 0px 25px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Создать статью'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'blog_id',
            [
                'attribute' => 'title',
                'label' => 'Title',
                'class' => 'yii\grid\DataColumn',
                'headerOptions' => ['style' => 'width:350px;min-width:350px;max-width:350px;'],
                'contentOptions' => ['style' => 'width:350px;min-width:350px;max-width:350px;'],
                'value' => function ($data) {
                    $blog = SiteBlog::findOne(['blog_id'=>$data->blog_id]);
                    if(isset($blog)){
                        $list = SiteList::findOne(['list_id'=>$blog->list_id, 'status'=>1]);
                        if(isset($list)){
                            $translate = SiteTranslate::findOne(['list_id'=>$list->list_id, 'lang_id'=>1]);
                            if(isset($translate)){
                                return $translate->title_cut;
                            }
                        }
                    }
                    return ''; 
                },
            ],
            [
                'attribute' => 'category_id',
                'label' => 'Category',
                'class' => 'yii\grid\DataColumn',
                'headerOptions' => ['style' => 'width:250px;min-width:250px;max-width:250px;'],
                'contentOptions' => ['style' => 'width:250px;min-width:250px;max-width:250px;'],
                'value' => function ($data) {
                    $category = SiteCategory::findOne(['category_id'=>$data->category_id]);
                    if(isset($category)){
                        $list = SiteList::findOne(['list_id'=>$category->list_id, 'status'=>1]);
                        if(isset($list)){
                            $translate = SiteTranslate::findOne(['list_id'=>$list->list_id, 'lang_id'=>1]);
                            if(isset($translate)){
                                return $translate->title_cut;
                            }
                        }
                    }
                    return ''; 
                },
            ],
            [
                'attribute' => 'date',
                'label' => 'Date',
                'class' => 'yii\grid\DataColumn',
                'headerOptions' => ['style' => 'width:150px;min-width:150px;max-width:150px;'],
                'contentOptions' => ['style' => 'width:150px;min-width:150px;max-width:150px;'],
                'value' => function ($data) {
                    return date('d.m.Y', $data->date); 
                },
            ],
            'sort',

            [
                'attribute' => 'icons',
                'label' => '',
                'class' => 'yii\grid\DataColumn',
                "format" => 'raw',
                'value' => function ($data) {
                    return '
                        <a href="/website/blog/update/'.$data->blog_id.'" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                        <a href="/website/blog/delete/'.$data->blog_id.'" title="Delete"><i class="fas fa-trash-alt"></i></a>
                        ';
                },
            ],
        ],
    ]); ?>
</div>
