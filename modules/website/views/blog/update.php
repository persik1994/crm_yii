<?php

use yii\helpers\Html;
use app\models\SiteBlog;

/* @var $this yii\web\View */
/* @var $model app\models\SiteBlog */

$this->title = Yii::t('app', 'Редактирование статьи');
$this->params['breadcrumbs'][] = ['label' => ' > '.Yii::t('app', 'Статьи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', ' > '.'Редактирование');
?>
<div class="site-blog-update">

    <?= $this->render('_form', [
        'model' => $model,
        'languages' => $languages,
        'categories' => $categories,
    ]) ?>

</div>
