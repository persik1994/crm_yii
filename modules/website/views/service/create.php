<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SiteServices */

$this->title = Yii::t('app', 'Создание услуги');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', ' > Услуги > '), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-services-create" style="margin: 0px 25px;">


    <?= $this->render('_form_create', [
        'model' => $model,
        'languages' => $languages,
    ]) ?>

</div>
