<?php

use yii\helpers\Html;
use dosamigos\tinymce\TinyMce;
use yii\widgets\ActiveForm;

use app\models\SiteCategory;
use app\models\SiteServices;


?>
<ul class="nav nav-tabs" id="myTab" role="tablist">
	<?php foreach ($languages as $lang_id => $lang) { ?>
		<li class="nav-item">
		    <a class="nav-link <?=($lang->lang_id == 1)?'active':''?>" id="<?=$lang->code?>-tab" data-toggle="tab" href="#<?=$lang->code?>" role="tab" aria-controls="<?=$lang->code?>" aria-selected="<?=($lang->lang_id == 1)?'true':'false'?>"><?=$lang->name?></a>
		</li>
	<?php } ?>
</ul>

<div class="tab-content" id="myTabContent">
	<?php foreach ($languages as $lang_id => $lang) { ?>
		<div class="tab-pane fade <?=($lang->lang_id == 1)?'show active':''?>" id="<?=$lang->code?>" role="tabpanel" aria-labelledby="<?=$lang->code?>-tab">
			<div class="add-client-form-wrap">
                <form method="post" action="/website/service/create" enctype="multipart/form-data" >
                    <div class="bottom-part row no-fw-row">
                        <div class="bottom-part-title">
                            Создание услуги
                        </div>

                        <div class="col-lg-4 col-12">

                            <div class="add-client-form-line">
                                <label>Краткое название услуги</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="" value="<?=$service->title_cut ?? ''?>" name="service-title-cut">
                                    <input type="hidden" placeholder="" value="<?=$lang->lang_id?>" name="lang">
                                    <?=Html::hiddenInput(\Yii::$app->getRequest()->csrfParam,\Yii::$app->getRequest()->getCsrfToken(),[])?>
                                </div>
                            </div>
                            <div class="add-client-form-line">
                                <label>Полное название услуги</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="" value="<?=$service->title ?? ''?>" name="service-title">
                                </div>
                            </div>
                            <div class="add-img-header-<?=$lang->lang_id?>">
	                            <div class="add-client-form-line add-client-form-line-file">
	                                <label>Изображение шапки</label>
	                                <div class="input-addclient-wrap border-span">
	                                    <input type="" placeholder="Select File">
	                                    <input type="file" placeholder="" class="file" id="get-invoice-file-<?=$lang->lang_id?>" name="img">
	                                    <textarea class="invoice-base64" style="display: none;"></textarea>
	                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
	                                </div>
	                            </div>
	                            <?php if(empty($service->img)){ ?>
	                                <div class="get-invoice-file load-file"><img src="" id='preview_invoice' alt="">
	                                    <span class="ava-name"></span>
	                                    <span class="ava-size"></span>
	                                    <span class="close-preview-invoice-<?=$lang->lang_id?>"><i class="fas fa-times"></i></span>
	                                </div>
	                            <?php } else { ?>
	                                <div class="get-invoice-file load-file" style="height: auto;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhE..." id="preview_invoice" alt=""><i class="far fa-file"></i>
	                                    <span class="ava-name"><a href="https://inta.group<?=$service->img?>" target="_blank"><?=str_replace('/uploads/images/', '', $service->img)?></a></span>
	                                    <span class="ava-size"></span>
	                                    <span class="close-preview-invoice-<?=$lang->lang_id?>" style="display: inline;"><a href="/website/service/imgdelete?img=<?=$service->translate_id?>"><i class="fas fa-times"></i></a></span>
	                                </div>
	                            <?php } ?>
	                            <hr>
	                        </div>
	                        <div class="add-img-mini-<?=$lang->lang_id?>">
	                            <div class="add-client-form-line add-client-form-line-file">
	                                <label>Маленькое изображение</label>
	                                <div class="input-addclient-wrap border-span">
	                                    <input type="" placeholder="Select File">
	                                    <input type="file" placeholder="" class="file" id="get-other-file-<?=$lang->lang_id?>" name="img_mini">
	                                    <textarea class="other-base64" style="display: none;"></textarea>
	                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
	                                </div>
	                            </div>
	                            <?php if(empty($service->img_mini)){ ?>
	                                <div class="get-other-file load-file"><img src="" id='preview_other' alt="">
	                                    <span class="ava-name"></span>
	                                    <span class="ava-size"></span>
	                                    <span class="close-preview-other-<?=$lang->lang_id?>"><i class="fas fa-times"></i></span>
	                                </div>
	                            <?php } else { ?>
	                                <div class="get-other-file load-file" style="height: auto;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhE..." id="preview_other" alt=""><i class="far fa-file"></i>
	                                    <span class="ava-name"><a href="https://inta.group<?=$service->img_mini?>" target="_blank"><?=str_replace('/uploads/images/', '', $service->img_mini)?></a></span>
	                                    <span class="ava-size"></span>
	                                    <span class="close-preview-other-<?=$lang->lang_id?>" style="display: inline;"><a href="/website/service/imgdelete?img_mini=<?=$service->translate_id?>"><i class="fas fa-times"></i></a></span>
	                                </div>
	                            <?php } ?>
	                            <hr>
	                        </div>
                        </div>
                        <div class="col-lg-12 col-12">
                            <div class="add-client-form-line" style="display: block;">
                                <label>Краткое описание</label>
                                <div class="input-addclient-wrap" style="padding-left: 15px;">
                                	<textarea name="service-description-cut" style="width: 100%;height: 100px;"><?=$service->description_cut ?? ''?></textarea>
                                </div>
                            </div>
                            <div class="add-client-form-line" style="display: block;">
                                <label>Текст</label>
                                <div class="input-addclient-wrap" style="padding-left: 15px;">
                                	<textarea name="service-description" style="width: 100%;height: 100px;"><?=$service->description ?? ''?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="add-new-form-submit-buttons col-12">
                            <div class="add-new-form-submit-buttons-wrap col-4">
                                <div class="add-new-form-submit-buttons-wrap-width">
                                    <button class="submit-form">Create</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
		</div>
	<?php } ?>
</div>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ev77citx2kpl9edp5f4egrd56wmlyrzs00zgmclyppzgnd4k"></script>
<script type="text/javascript">
	tinymce.init({
	  selector: 'textarea',
	  height: 500,
	  menubar: false,
	  plugins: [
	    'advlist autolink lists link image charmap print preview anchor textcolor',
	    'searchreplace visualblocks code fullscreen',
	    'insertdatetime media table contextmenu paste code help wordcount',
        'code'
	  ],
	  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help | code',
	  content_css: [
	    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
	    '//www.tinymce.com/css/codepen.min.css']
	});

		var invoice = new FileReader();
        var other = new FileReader();

        $('.close-preview-invoice-1').on("click", function() {
            $('.add-img-header-1 #get-invoice-file').val('');
            $('.add-img-header-1 .get-invoice-file .ava-name').text('');
            $('.add-img-header-1 .get-invoice-file .ava-size').text('');
            $(".add-img-header-1 #preview_invoice").attr("src", "");
            $('.add-img-header-1 .get-invoice-file').css('height', '0');
            $('.add-img-header-1 .close-preview-invoice').hide();
            $('.add-img-header-1 .add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-invoice-file-1").change(function(event) {

            $('.add-img-header-1 .close-preview-invoice').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.add-img-header-1 .get-invoice-file .ava-name').text(filename[fileLen]);

            invoice.onload = function() {
                $('.add-img-header-1 .get-invoice-file').css('height', 'auto');
                var preview = $(".add-img-header-1 #preview_invoice");
                preview.attr("src", invoice.result);
                $('.add-img-header-1 .invoice-base64').val(invoice.result);
            }

            invoice.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.add-img-header-1 .get-invoice-file .ava-size').text(size.toFixed(2) + ' KB');

        });

        $('.close-preview-invoice-2').on("click", function() {
            $('.add-img-header-2 #get-invoice-file').val('');
            $('.add-img-header-2 .get-invoice-file .ava-name').text('');
            $('.add-img-header-2 .get-invoice-file .ava-size').text('');
            $(".add-img-header-2 #preview_invoice").attr("src", "");
            $('.add-img-header-2 .get-invoice-file').css('height', '0');
            $('.add-img-header-2 .close-preview-invoice').hide();
            $('.add-img-header-2 .add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-invoice-file-2").change(function(event) {

            $('.add-img-header-2 .close-preview-invoice').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.add-img-header-2 .get-invoice-file .ava-name').text(filename[fileLen]);

            invoice.onload = function() {
                $('.add-img-header-2 .get-invoice-file').css('height', 'auto');
                var preview = $(".add-img-header-2 #preview_invoice");
                preview.attr("src", invoice.result);
                $('.add-img-header-2 .invoice-base64').val(invoice.result);
            }

            invoice.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.add-img-header-2 .get-invoice-file .ava-size').text(size.toFixed(2) + ' KB');

        });

        $('.close-preview-invoice-3').on("click", function() {
            $('.add-img-header-3 #get-invoice-file').val('');
            $('.add-img-header-3 .get-invoice-file .ava-name').text('');
            $('.add-img-header-3 .get-invoice-file .ava-size').text('');
            $(".add-img-header-3 #preview_invoice").attr("src", "");
            $('.add-img-header-3 .get-invoice-file').css('height', '0');
            $('.add-img-header-3 .close-preview-invoice').hide();
            $('.add-img-header-3 .add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-invoice-file-3").change(function(event) {

            $('.add-img-header-3 .close-preview-invoice').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.add-img-header-3 .get-invoice-file .ava-name').text(filename[fileLen]);

            invoice.onload = function() {
                $('.add-img-header-3 .get-invoice-file').css('height', 'auto');
                var preview = $(".add-img-header-3 #preview_invoice");
                preview.attr("src", invoice.result);
                $('.add-img-header-3 .invoice-base64').val(invoice.result);
            }

            invoice.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.add-img-header-3 .get-invoice-file .ava-size').text(size.toFixed(2) + ' KB');

        });

        $('.close-preview-invoice-4').on("click", function() {
            $('.add-img-header-4 #get-invoice-file').val('');
            $('.add-img-header-4 .get-invoice-file .ava-name').text('');
            $('.add-img-header-4 .get-invoice-file .ava-size').text('');
            $(".add-img-header-4 #preview_invoice").attr("src", "");
            $('.add-img-header-4 .get-invoice-file').css('height', '0');
            $('.add-img-header-4 .close-preview-invoice').hide();
            $('.add-img-header-4 .add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-invoice-file-4").change(function(event) {

            $('.add-img-header-4 .close-preview-invoice').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.add-img-header-4 .get-invoice-file .ava-name').text(filename[fileLen]);

            invoice.onload = function() {
                $('.add-img-header-4 .get-invoice-file').css('height', 'auto');
                var preview = $(".add-img-header-4 #preview_invoice");
                preview.attr("src", invoice.result);
                $('.add-img-header-4 .invoice-base64').val(invoice.result);
            }

            invoice.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.add-img-header-4 .get-invoice-file .ava-size').text(size.toFixed(2) + ' KB');

        });

        $('.close-preview-other-1').on("click", function() {
            $('.add-img-mini-1 #get-other-file').val('');
            $('.add-img-mini-1 .get-other-file .ava-name').text('');
            $('.add-img-mini-1 .get-other-file .ava-size').text('');
            $(".add-img-mini-1 #preview_other").attr("src", "");
            $('.add-img-mini-1 .get-other-file').css('height', '0');
            $('.add-img-mini-1 .close-preview-other').hide();
            $('.add-img-mini-1 .add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-other-file-1").change(function(event) {

            $('.add-img-mini-1 .close-preview-other').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.add-img-mini-1 .get-other-file .ava-name').text(filename[fileLen]);

            other.onload = function() {
                $('.add-img-mini-1 .get-other-file').css('height', 'auto');
                var preview = $(".add-img-mini-1 #preview_other");
                preview.attr("src", other.result);
                $('.add-img-mini-1 .other-base64').val(other.result);
            }

            other.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.add-img-mini-1 .get-other-file .ava-size').text(size.toFixed(2) + ' KB');

        });

        $('.close-preview-other-2').on("click", function() {
            $('.add-img-mini-2 #get-other-file').val('');
            $('.add-img-mini-2 .get-other-file .ava-name').text('');
            $('.add-img-mini-2 .get-other-file .ava-size').text('');
            $(".add-img-mini-2 #preview_other").attr("src", "");
            $('.add-img-mini-2 .get-other-file').css('height', '0');
            $('.add-img-mini-2 .close-preview-other').hide();
            $('.add-img-mini-2 .add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-other-file-2").change(function(event) {

            $('.add-img-mini-2 .close-preview-other').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.add-img-mini-2 .get-other-file .ava-name').text(filename[fileLen]);

            other.onload = function() {
                $('.add-img-mini-2 .get-other-file').css('height', 'auto');
                var preview = $(".add-img-mini-2 #preview_other");
                preview.attr("src", other.result);
                $('.add-img-mini-2 .other-base64').val(other.result);
            }

            other.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.add-img-mini-2 .get-other-file .ava-size').text(size.toFixed(2) + ' KB');

        });

        $('.close-preview-other-3').on("click", function() {
            $('.add-img-mini-3 #get-other-file').val('');
            $('.add-img-mini-3 .get-other-file .ava-name').text('');
            $('.add-img-mini-3 .get-other-file .ava-size').text('');
            $(".add-img-mini-3 #preview_other").attr("src", "");
            $('.add-img-mini-3 .get-other-file').css('height', '0');
            $('.add-img-mini-3 .close-preview-other').hide();
            $('.add-img-mini-3 .add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-other-file-3").change(function(event) {

            $('.add-img-mini-3 .close-preview-other').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.add-img-mini-3 .get-other-file .ava-name').text(filename[fileLen]);

            other.onload = function() {
                $('.add-img-mini-3 .get-other-file').css('height', 'auto');
                var preview = $(".add-img-mini-3 #preview_other");
                preview.attr("src", other.result);
                $('.add-img-mini-3 .other-base64').val(other.result);
            }

            other.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.add-img-mini-3 .get-other-file .ava-size').text(size.toFixed(2) + ' KB');

        });

        $('.close-preview-other-4').on("click", function() {
            $('.add-img-mini-4 #get-other-file').val('');
            $('.add-img-mini-4 .get-other-file .ava-name').text('');
            $('.add-img-mini-4 .get-other-file .ava-size').text('');
            $(".add-img-mini-4 #preview_other").attr("src", "");
            $('.add-img-mini-4 .get-other-file').css('height', '0');
            $('.add-img-mini-4 .close-preview-other').hide();
            $('.add-img-mini-4 .add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-other-file-4").change(function(event) {

            $('.add-img-mini-4 .close-preview-other').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.add-img-mini-4 .get-other-file .ava-name').text(filename[fileLen]);

            other.onload = function() {
                $('.add-img-mini-4 .get-other-file').css('height', 'auto');
                var preview = $(".add-img-mini-4 #preview_other");
                preview.attr("src", other.result);
                $('.add-img-mini-4 .other-base64').val(other.result);
            }

            other.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.add-img-mini-4 .get-other-file .ava-size').text(size.toFixed(2) + ' KB');

        });
</script>

