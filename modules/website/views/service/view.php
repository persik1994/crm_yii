<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SiteServices */

$this->title = $model->services_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-services-view" style="margin: 0px 25px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->services_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->services_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'services_id',
            'category_id',
            'list_id',
            'sort',
        ],
    ]) ?>

</div>
