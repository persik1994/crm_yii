<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SiteServices */

$this->title = Yii::t('app', 'Редактирование услуги');
$this->params['breadcrumbs'][] = ['label' => ' > '.Yii::t('app', 'Услуги'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', ' > '.'Редактирование');

?>
<div class="site-services-update" style="margin: 0px 25px;">

    <?= $this->render('_form', [
        'model' => $model,
        'languages' => $languages,
    ]) ?>

</div>
