<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\SiteCategory;
use app\models\SiteServices;
use app\models\SiteList;
use app\models\SiteTranslate;
use app\models\SiteBlog;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Услуги');
$this->params['breadcrumbs'][] = ' > Услуги';
?>
<div class="site-services-index" style="margin: 0px 25px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Создать услугу'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'services_id',
            [
                'attribute' => 'title',
                'label' => 'Title',
                'class' => 'yii\grid\DataColumn',
                'headerOptions' => ['style' => 'width:450px;min-width:450px;max-width:450px;'],
                'contentOptions' => ['style' => 'width:450px;min-width:450px;max-width:450px;'],
                'value' => function ($data) {
                    $service = SiteServices::findOne(['services_id'=>$data->services_id]);
                    if(isset($service)){
                        $list = SiteList::findOne(['list_id'=>$service->list_id, 'status'=>1]);
                        if(isset($list)){
                            $translate = SiteTranslate::findOne(['list_id'=>$list->list_id, 'lang_id'=>1]);
                            if(isset($translate)){
                                return $translate->title_cut;
                            }
                        }
                    }
                    return ''; 
                },
            ],
            'sort',

            [
                'attribute' => 'icons',
                'label' => '',
                'class' => 'yii\grid\DataColumn',
                "format" => 'raw',
                'value' => function ($data) {
                    return '
                        <a href="/website/service/update/'.$data->services_id.'" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                        <a href="/website/service/delete/'.$data->services_id.'" title="Delete"><i class="fas fa-trash-alt"></i></a>
                        ';
                },
            ],
        ],
    ]); ?>
</div>
