<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SiteCategory */

$this->title = Yii::t('app', 'Создание категории');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', ' > Категории > '), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-category-create" style="margin: 0px 25px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_create', [
        'model' => $model,
        'languages' => $languages,
    ]) ?>

</div>
