<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\SiteCategory;
use app\models\SiteList;
use app\models\SiteTranslate;

/* @var $this yii\web\View */
/* @var $model app\models\SiteCategory */

$this->title = 'Категория ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', ' > Категории > '), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-category-view" style="margin: 0px 25px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->category_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->category_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'category_id',
            [
                'attribute' => 'title',
                'label' => 'Title',
                'class' => 'yii\grid\DataColumn',
                'headerOptions' => ['style' => 'width:250px;min-width:250px;max-width:250px;'],
                'contentOptions' => ['style' => 'width:250px;min-width:250px;max-width:250px;'],
                'value' => function ($data) {
                    $category = SiteCategory::findOne(['category_id'=>$data->category_id]);
                    if(isset($category)){
                        $list = SiteList::findOne(['list_id'=>$category->list_id, 'status'=>1]);
                        if(isset($list)){
                            $translate = SiteTranslate::findOne(['list_id'=>$list->list_id, 'lang_id'=>1]);
                            if(isset($translate)){
                                return $translate->title_cut;
                            }
                        }
                    }
                    return ''; 
                },
            ],
            'sort',
            //'type',
        ],
    ]) ?>

</div>
