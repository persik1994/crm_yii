<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\SiteCategory;
use app\models\SiteList;
use app\models\SiteTranslate;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Категории');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-category-index" style="margin: 0px 25px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Создать новую категорию'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'category_id',
            [
				'attribute' => 'title',
			    'label' => 'Title',
			    'class' => 'yii\grid\DataColumn',
				'headerOptions' => ['style' => 'width:250px;min-width:250px;max-width:250px;'],
				'contentOptions' => ['style' => 'width:250px;min-width:250px;max-width:250px;'],
			    'value' => function ($data) {
			    	$category = SiteCategory::findOne(['category_id'=>$data->category_id]);
			        if(isset($category)){
			            $list = SiteList::findOne(['list_id'=>$category->list_id, 'status'=>1]);
			            if(isset($list)){
			                $translate = SiteTranslate::findOne(['list_id'=>$list->list_id, 'lang_id'=>1]);
			                if(isset($translate)){
			                    return $translate->title_cut;
			                }
			            }
			        }
			        return ''; 
			    },
			],
            'sort',
            'type',
            [
				'attribute' => 'icons',
			    'label' => '',
			    'class' => 'yii\grid\DataColumn',
				"format" => 'raw',
			    'value' => function ($data) {
			    	return '
			    		<a href="/website/category/update/'.$data->category_id.'" title="Edit"><i class="fas fa-pencil-alt"></i></a>
			    		<a href="/website/category/delete/'.$data->category_id.'" title="Delete"><i class="fas fa-trash-alt"></i></a>
			    		'; //<a href="/website/category/view/'.$data->category_id.'" title="View"><i class="far fa-eye"></i></a>
			    },
			],
        ],
    ]); ?>
</div>
