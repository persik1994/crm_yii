<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SiteCategory;

?>
<ul class="nav nav-tabs" id="myTab" role="tablist">
	<?php foreach ($languages as $lang_id => $lang) { ?>
		<li class="nav-item">
		    <a class="nav-link <?=($lang_id == 1)?'active':''?>" id="<?=$lang->code?>-tab" data-toggle="tab" href="#<?=$lang->code?>" role="tab" aria-controls="<?=$lang->code?>" aria-selected="<?=($lang_id == 1)?'true':'false'?>"><?=$lang->name?></a>
		</li>
	<?php } ?>
</ul>

<div class="tab-content" id="myTabContent">
	<?php foreach ($languages as $lang_id => $lang) { ?>
		<div class="tab-pane fade <?=($lang_id == 1)?'show active':''?>" id="<?=$lang->code?>" role="tabpanel" aria-labelledby="<?=$lang->code?>-tab">
			<div class="add-client-form-wrap">
                <form method="post" action="/website/category/create">
                    <div class="bottom-part row no-fw-row">
                        <div class="bottom-part-title">
                            Создание категории
                        </div>

                        <div class="col-lg-4 col-12">

                            <div class="add-client-form-line">
                                <label>Название категории</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="" value="<?=SiteCategory::findCategory($model->category_id,$lang->lang_id)['translate']->title_cut ?? ''?>" name="category-title">
                                    <input type="hidden" placeholder="" value="<?=SiteCategory::findCategory($model->category_id,$lang->lang_id)['translate']->translate_id ?? 0?>" name="translate">
                                    <input type="hidden" placeholder="" value="<?=$model->list_id?>" name="list">
                                    <input type="hidden" placeholder="" value="<?=$lang->lang_id?>" name="lang">
                                    <?=Html::hiddenInput(\Yii::$app->getRequest()->csrfParam,\Yii::$app->getRequest()->getCsrfToken(),[])?>
                                </div>
                            </div>
                        </div>

                        <div class="add-new-form-submit-buttons col-12">
                            <div class="add-new-form-submit-buttons-wrap col-4">
                                <div class="add-new-form-submit-buttons-wrap-width">
                                    <button class="submit-form">Create</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
		</div>
	<?php } ?>
</div>
