<?php

use yii\helpers\Html;
use app\models\SiteCategory;

/* @var $this yii\web\View */
/* @var $model app\models\SiteCategory */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Site Category',
]) . $model->category_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', ' > '.'Категории'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => ' > '.SiteCategory::findCategory($model->category_id)['translate']->title_cut ?? '', 'url' => ['view', 'id' => $model->category_id]];
$this->params['breadcrumbs'][] = Yii::t('app', ' > '.'Редактирование');
?>
<div class="site-category-update" style="margin: 0px 25px;">


    <?= $this->render('_form', [
        'model' => $model,
        'languages' => $languages,
    ]) ?>

</div>
