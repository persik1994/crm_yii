<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\SiteLang;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Мета главных страниц');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-meta-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- <?= Html::a(Yii::t('app', 'Создать'), ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>
    <style type="text/css">
        .table-bordered{
            width: auto !important;
            min-width: 600px;
        }
    </style>
    *** Мета теги для услуг и статьи, редактируються на страницах статьи и услуги!
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'meta_id',
            [
                'label' => 'Тип',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($data) {
                    return $data->types[$data->type];
                },
            ],
            [
                'label' => 'Язык',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($data) {
                    $lang = SiteLang::findOne(['lang_id'=>$data->lang_id]);
                    return $lang->name;
                },
            ],
            'meta_title:ntext',
            'meta_description:ntext',
            'meta_keywords:ntext',

            ['class' => 'yii\grid\ActionColumn',
                'buttons'=>[
                    'view'=>function ($url) {
                        return Html::a('<i class="fas fa-eye"></i>', $url, ['class' => '']);
                    },
                    'update'=>function ($url) {
                        return Html::a('<i class="fas fa-pencil-alt"></i>', $url, ['class' => '']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
