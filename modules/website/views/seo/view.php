<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\SiteLang;

/* @var $this yii\web\View */
/* @var $model app\models\SiteMeta */

$this->title = $model->meta_title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'СЕО'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$lang = SiteLang::findOne(['lang_id'=>$model->lang_id]);
?>
<div class="site-meta-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->meta_id], ['class' => 'btn btn-primary']) ?>
        <!-- <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->meta_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы точно хотите удалить?'),
                'method' => 'post',
            ],
        ]) ?> -->
    </p>

    <style type="text/css">
        .table-bordered{
            width: auto !important;
            min-width: 600px;
        }
    </style>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'meta_id',
            [                  
                'label' => 'Тип',
                'value' => $model->types[$model->type],            
            ],
            [                  
                'label' => 'Язык',
                'value' => $lang->name,            
            ],
            'meta_title:ntext',
            'meta_description:ntext',
            'meta_keywords:ntext',
        ],
    ]) ?>

</div>
