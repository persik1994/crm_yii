<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SiteMeta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="add-client-form-wrap">
    <form method="post" action="/website/seo/<?=($model->isNewRecord) ? 'create' : 'update/'.$model->meta_id?>">
        <div class="bottom-part row no-fw-row">
            <div class="bottom-part-title">
                <?=($model->isNewRecord) ? 'Создание мета' : 'Редактирование мета "'.$model->meta_title.'"'?>
            </div>
            <div class="col-lg-4 col-12">
                <div class="add-client-form-line">
                    <label>Название страницы</label>
                    <div class="input-addclient-wrap">
                        <input type="text" placeholder="" value="<?= $model->meta_title ?>" name="SiteMeta[meta_title]">
                        <?=Html::hiddenInput(\Yii::$app->getRequest()->csrfParam,\Yii::$app->getRequest()->getCsrfToken(),[])?>
                    </div>
                </div>
                <div class="add-client-form-line">
                    <label>Описание страницы</label>
                    <div class="input-addclient-wrap">
                        <input type="text" placeholder="" value="<?= $model->meta_description ?>" name="SiteMeta[meta_description]">
                    </div>
                </div>
                <div class="add-client-form-line">
                    <label>Ключевые слова страницы</label>
                    <div class="input-addclient-wrap">
                        <input type="text" placeholder="" value="<?= $model->meta_keywords ?>" name="SiteMeta[meta_keywords]">
                    </div>
                </div>
            </div>

            <div class="add-new-form-submit-buttons col-12">
                <div class="add-new-form-submit-buttons-wrap col-4">
                    <div class="add-new-form-submit-buttons-wrap-width">
                        <button class="submit-form"><?=($model->isNewRecord) ? 'Создать' : 'Сохранить'?></button>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>
