<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SiteMeta */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Site Meta',
]) . $model->meta_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'СЕО'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->meta_title, 'url' => ['view', 'id' => $model->meta_id]];
$this->params['breadcrumbs'][] = ($model->isNewRecord) ? 'Создание мета' : 'Редактирование мета';
?>
<div class="site-meta-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
