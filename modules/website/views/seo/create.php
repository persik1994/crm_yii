<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SiteMeta */

$this->title = ($model->isNewRecord) ? 'Создание мета' : 'Редактирование мета';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Metas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-meta-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
