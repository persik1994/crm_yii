<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Project;
use app\models\Manager;
use app\models\Client;
use app\models\Cargo;
use app\models\ShipmentType;
use app\models\CustomTable;
use app\models\Payments;
use app\models\ClientTariff;
use app\models\BuyingList;
use app\models\Logs;

/**
 * ProjectsController implements the CRUD actions for Project model.
 */
class ProjectsController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex(){
        $session = Yii::$app->session;

        Project::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Project::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
            'sort'=> ['defaultOrder' => ['project_id'=>SORT_DESC]],
        ]);

        return $this->render('table', [
            'dataProvider' => $dataProvider,
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }

    public function actionPaid(){
        $session = Yii::$app->session;

        Project::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Project::SearchQuery(1),
            'pagination' => [
                'pageSize' => 500, 
            ],
            'sort'=> ['defaultOrder' => ['project_id'=>SORT_DESC]],
        ]);

        return $this->render('table', [
            'dataProvider' => $dataProvider,
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }

    public function actionNopaid(){
        $session = Yii::$app->session;

        Project::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Project::SearchQuery(2),
            'pagination' => [
                'pageSize' => 500, 
            ],
            'sort'=> ['defaultOrder' => ['project_id'=>SORT_DESC]],
        ]);

        return $this->render('table', [
            'dataProvider' => $dataProvider,
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }

    
    public function actionView($id) //ajax
    {
        $projects_Obj = new Project;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = (int)$id;
        if(!empty($id)){
            $project = Project::findOne(['project_id'=>$id]);
            $cargos = 0;
            if(isset($project)){

                foreach ($project as $key => $value) {
                    if($value == "0"){
                        $project[$key] = '';
                    }
                }

                $cargos = Cargo::find()->where(['project_id'=>$project->project_id])->all();

                $project->client_id = (!empty($project->client->client_id)) ? ((!empty($project->client->name)) ? '88-'.$project->client->client_id.' - '.$project->client->name : '88-'.$project->client->client_id) : '';
                $project->manager_id = (!empty($project->manager->username)) ? $project->manager->username : '';
                $project->date_create = (!empty($project->date_create)) ? date('d.m.Y H:i', $project->date_create) : '';
                $project->date_close = (!empty($project->date_close)) ? date('d.m.Y H:i', $project->date_close) : '';
                $project->project_type = (!empty($project->project_type)) ? $projects_Obj->type($project->project_type) : '';
                $project->delivery_type = (!empty($project->shipmentType->name)) ? $project->shipmentType->name : '';
                $project->project_id = (!empty($project->project_id)) ? $project->project_id : '';
                $project->total = (!empty($project->total)) ? $project->total : '0.00';
                $project->profit = (!empty($project->profit)) ? $project->profit : '0.00';

                $project->payed = (!empty($project->payed)) ? $project->payed : '0.00';
                $project->must_pay = (!empty($project->must_pay)) ? $project->must_pay : '0.00';
                $project->chargers = (!empty($project->chargers)) ? $project->chargers : '0.00';
                $project->plan_bonuse = (!empty($project->plan_bonuse)) ? $project->plan_bonuse : '0.00';
                $project->cargo_cost = (!empty($project->cargo_cost)) ? $project->cargo_cost : '0.00';
                $project->insurance = (!empty($project->insurance)) ? $project->insurance : '0.00';
                $project->insurance_sum = (!empty($project->insurance_sum)) ? $project->insurance_sum : '0.00';

                if(!empty($project->status))
                    $project->status = 'Yes';
                else
                    $project->status = 'No';
            }
            return array('project'=>$project, 'cargos'=>$cargos);
        }
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();
        $model->manager_id = Yii::$app->user->identity->id;
        $model->status = 5;
        $model->client_id = 0;
        $model->name = '';
        $model->date_create = time();
        $model->date_close = 0;
        $model->project_type = 0;
        $model->delivery_type = 0;
        $model->comment = '';
        $model->total = 0;
        $model->profit = 0;
        $model->payed = 0;
        $model->must_pay = 0;
        $model->chargers = 0;
        $model->plan_bonuse = 0;
        $model->cargo_cost = 0;
        $model->insurance = 0;
        $model->insurance_sum = 0;
        if($model->save(false)){
            return $this->redirect('/projects/edit/'.$model->project_id);
        }else{
            return $this->redirect('/projects/index?sort='.Yii::$app->request->get('sort'));
        }

        // $model = new Project();
        // $managers = Manager::find()->where('status = 1')->all();
        // $shipment_types = ShipmentType::find()->where('status = 1')->all();
        // $buyings = BuyingList::find()->where(['status'=>[0,1]])->all();

        // if(Yii::$app->user->identity->role >= 10)
        //     $clients = Client::find()->where('status = 1')->all();
        // else
        //     $clients = Client::find()->where(['status'=>1,'manager_id'=>Yii::$app->user->identity->id])->all();

        // if (Yii::$app->request->post()) {
        //     //////////////////////
        //     if(Yii::$app->user->identity->role >= 9)
        //         $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
        //     else
        //         $model->manager_id = Yii::$app->user->identity->id;
        //     /////////////////////
        //     $model->project_type    = (Yii::$app->request->post('type')) ? (int)Yii::$app->request->post('type') : 0;
        //     $model->delivery_type   = (Yii::$app->request->post('delivery_type')) ? (int)Yii::$app->request->post('delivery_type') : 0;
        //     $model->client_id       = (Yii::$app->request->post('client')) ? (int)Yii::$app->request->post('client') : 0;
        //     $model->name            = (Yii::$app->request->post('name')) ? Yii::$app->request->post('name') : '';
        //     $model->date_create     = (Yii::$app->request->post('created')) ? (int)strtotime(Yii::$app->request->post('created')) : 0;
        //     $model->date_close      = (Yii::$app->request->post('closed')) ? (int)strtotime(Yii::$app->request->post('closed')) : 0;
        //     $model->comment         = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
        //     $model->total           = (Yii::$app->request->post('total')) ? floatval(Yii::$app->request->post('total')) : 0;
        //     $model->profit          = (Yii::$app->request->post('profit')) ? floatval(Yii::$app->request->post('profit')) : 0;
        //     $model->status          = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;

        //     $model->payed           = (Yii::$app->request->post('payed')) ? floatval(Yii::$app->request->post('payed')) : 0;
        //     $model->must_pay        = (Yii::$app->request->post('must_pay')) ? floatval(Yii::$app->request->post('must_pay')) : 0;
        //     $model->chargers        = (Yii::$app->request->post('chargers')) ? floatval(Yii::$app->request->post('chargers')) : 0;
        //     $model->plan_bonuse     = (Yii::$app->request->post('plan_bonuse')) ? floatval(Yii::$app->request->post('plan_bonuse')) : 0;
        //     $model->cargo_cost      = (Yii::$app->request->post('cargo_cost')) ? floatval(Yii::$app->request->post('cargo_cost')) : 0;
        //     $model->insurance       = (Yii::$app->request->post('insurance')) ? floatval(Yii::$app->request->post('insurance')) : 0;
        //     $model->insurance_sum   = (Yii::$app->request->post('insurance_sum')) ? floatval(Yii::$app->request->post('insurance_sum')) : 0;
        //     $model->buying_id       = (Yii::$app->request->post('buying')) ? (int)Yii::$app->request->post('buying') : 0;
        //     if($model->save(false)){
        //         Yii::$app->session->setFlash('success', "Project created.");
        //         Logs::Create($model->project_id, $model, 3, 1);
        //         if($model->buying_id > 0){
        //             $buying_find = BuyingList::findOne(['buying_id'=>(int)$model->buying_id]);
        //             if(isset($buying_find)){
        //                 $buying_find->project_id = $model->project_id;
        //                 Logs::Create($model->buying_id, $buying_find, 4, 2);
        //                 $buying_find->update(false);
        //             }
        //         }
        //         return $model->project_id;
        //     }else{
        //         return 0;
        //     }
        // } else {
        //     return $this->render('create', [
        //         'model' => $model,
        //         'managers' => $managers,
        //         'clients' => $clients,
        //         'delivery_types' => $shipment_types,
        //         'buyings' => $buyings,
        //     ]);
        // }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        $managers = Manager::find()->where('status = 1')->all();
        $shipment_types = ShipmentType::find()->where('status = 1')->all();
        /////////
        $client_select = Client::findOne(['client_id'=>$model->client_id]);
        $balance = (isset($client_select) ? $client_select->balance : 0);
        /////////
        $cargos = Cargo::find()->where(['project_id'=>$model->project_id])->all();
        $cargos_count = Cargo::find()->where(['project_id'=>$model->project_id])->count();
        /////////
        $withdraws = Payments::find()->where(['out_source_id'=>4,'project_id'=>$model->project_id])->all();
        $tariffs = ClientTariff::find()->where(['client_id'=>$model->client_id,'project_id'=>$model->project_id])->all();
        /////////
        $buyings = BuyingList::find()->where(['status'=>[0,1]])->all();
        ////////
        if(Yii::$app->user->identity->role >= 10)
            $clients = Client::find()->where('status = 1')->all();
        else
            $clients = Client::find()->where(['status'=>1,'manager_id'=>Yii::$app->user->identity->id])->all();
        ////////
        if (Yii::$app->request->post() && isset($model)) {
            //////////////////////
            if(Yii::$app->user->identity->role >= 9)
                $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            else
                $model->manager_id = Yii::$app->user->identity->id;
            /////////////////////
            $model->project_type    = (Yii::$app->request->post('type')) ? (int)Yii::$app->request->post('type') : 0;
            if($model->payed == 0){
                $model->client_id       = (Yii::$app->request->post('client')) ? (int)Yii::$app->request->post('client') : 0;
            }
            $model->name            = (Yii::$app->request->post('name')) ? Yii::$app->request->post('name') : '';
            $model->delivery_type   = (Yii::$app->request->post('delivery_type')) ? (int)Yii::$app->request->post('delivery_type') : 0;
            // $model->date_create     = (Yii::$app->request->post('created')) ? (int)strtotime(Yii::$app->request->post('created')) : 0;
            // $model->date_close      = (Yii::$app->request->post('closed')) ? (int)strtotime(Yii::$app->request->post('closed')) : 0;
            $model->comment         = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
            $model->total           = (Yii::$app->request->post('total')) ? floatval(Yii::$app->request->post('total')) : 0;
            $model->profit          = (Yii::$app->request->post('profit')) ? floatval(Yii::$app->request->post('profit')) : 0;
            $model->status          = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;

            $model->payed           = (Yii::$app->request->post('payed')) ? floatval(Yii::$app->request->post('payed')) : 0;
            $model->must_pay        = (Yii::$app->request->post('must_pay')) ? floatval(Yii::$app->request->post('must_pay')) : 0;
            $model->chargers        = (Yii::$app->request->post('chargers')) ? floatval(Yii::$app->request->post('chargers')) : 0;
            $model->plan_bonuse     = (Yii::$app->request->post('plan_bonuse')) ? floatval(Yii::$app->request->post('plan_bonuse')) : 0;
            $model->cargo_cost      = (Yii::$app->request->post('cargo_cost')) ? floatval(Yii::$app->request->post('cargo_cost')) : 0;
            $model->insurance       = (Yii::$app->request->post('insurance')) ? floatval(Yii::$app->request->post('insurance')) : 0;
            $model->insurance_sum   = (Yii::$app->request->post('insurance_sum')) ? floatval(Yii::$app->request->post('insurance_sum')) : 0;
            $model->buying_id       = (Yii::$app->request->post('buying')) ? (int)Yii::$app->request->post('buying') : 0;
            Logs::Create($model->project_id, $model, 3, 2);
            if($model->update()){
                Yii::$app->session->setFlash('success', "ПРОЕКТ PJ-".$model->project_id." СОХРАНЕН!");
                if($model->buying_id > 0){
                    $buying_find = BuyingList::findOne(['buying_id'=>(int)$model->buying_id]);
                    if(isset($buying_find)){
                        $buying_find->project_id = $model->project_id;
                        Logs::Create($model->buying_id, $buying_find, 4, 2);
                        $buying_find->update(false);
                    }
                }
                Payments::PayOutCartNotProject_IsTransfer($model->project_id);
                Project::MathTotalPricing($model->project_id);
                return '1';
            }else{
                Project::MathTotalPricing($model->project_id);
            }
        } else {
            $model = $this->findModel($model->project_id);
            Project::MathTotalPricing($model->project_id);
            Payments::PayOutCartNotProject_IsTransfer($model->project_id);
            $model = $this->findModel($model->project_id);
            return $this->render('update', [
                'model' => $model,
                'managers' => $managers,
                'clients' => $clients,
                'delivery_types' => $shipment_types,
                'balance' => $balance,
                'cargos' => $cargos,
                'cargos_count' => $cargos_count,
                'withdraws' => $withdraws,
                'tariffs' => $tariffs,
                'buyings' => $buyings,
            ]);
        }
    }

    public function actionMoneycabbage(){

    }

    ///////

    public function actionSearch(){
        Project::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Project::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
        ]);

        return $this->renderPartial('/projects/table', [
            'dataProvider' => $dataProvider,
            'menu' => '',
        ]);
    }

    public function actionSearchdelete(){
        if(Yii::$app->request->get('type')){
            $session = Yii::$app->session;
            if($session->has(Yii::$app->request->get('type'))){
                $session->remove(Yii::$app->request->get('type'));
            }
        }
        return $this->redirect('/projects/index?sort='.Yii::$app->request->get('sort'));
    }

    public function actionSearchdeleteall(){
        Project::DeleteAllSession();
        return $this->redirect('/projects/index');
    }

    ////////


    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel((int)$id);
        if(isset($model) && Yii::$app->user->identity->role >= 9){
            $model->status = 5;
            Logs::Create($model->buying_id, $model, 4, 2);
            $model->update(false);
        }

        return $this->redirect(['index']);
    }

    public function actionCustomtable(){
        if(Yii::$app->request->get('page') && Yii::$app->request->get('type')){
            $attributes = substr(Yii::$app->request->get('type'), 0, -1);
            $attributes = explode(",", $attributes);
            if(isset($attributes)){
                CustomTable::deleteAll([
                    'manager_id'=>Yii::$app->user->identity->id,
                    'page'=>'project',
                    'page_two'=>Yii::$app->request->get('page')
                ]);
                foreach ($attributes as $type) {
                    $customTable = new CustomTable;
                    $customTable->manager_id = Yii::$app->user->identity->id;
                    $customTable->page = 'project';
                    $customTable->page_two = Yii::$app->request->get('page');
                    $customTable->type = $type;
                    $customTable->save();
                }
            }
        }
        return 1;
    }

    public function actionProjectselect($id = 0){
        $html = '<select class="select-project selectpicker" name="manager" data-live-search="true"><option value="" selected class="placeholder-select">-Select-</option>';
        if($id > 0){
            $payment_project = 0;
            $projects_client = Project::find()->where(['client_id'=>(int)$id])->all();
            if(Yii::$app->request->get('payment_id')){
                $payment = Payments::findOne(['payment_id'=>(int)Yii::$app->request->get('payment_id')]);
                if(isset($payment) && $payment->project_id){
                    $payment_project = $payment->project_id;
                }else{
                    $payment_project = 0;
                }
            }
            else if(Yii::$app->request->get('cargo_id')){
                $cargo = Cargo::findOne(['cargo_id'=>(int)Yii::$app->request->get('cargo_id')]);
                if(isset($cargo) && $cargo->project_id){
                    $payment_project = $cargo->project_id;
                }else{
                    $payment_project = 0;
                }
            }
            else if(Yii::$app->request->get('buying_id')){
                $buying = BuyingList::findOne(['buying_id'=>(int)Yii::$app->request->get('buying_id')]);
                if(isset($buying) && $buying->project_id){
                    $payment_project = $buying->project_id;
                }else{
                    $payment_project = 0;
                }
            }

            if(isset($projects_client)){
                foreach ($projects_client as $project) {
                    $selected = ($payment_project == $project->project_id) ? 'selected' : '';
                    $html .= '<option '.$selected.' value="'.$project->project_id.'">PJ-'.$project->project_id.' '.$project->name.'</option>';
                }
            }
        }
        return $html.'<script type="text/javascript">$(".selectpicker").selectpicker();</script></select>';
    }

    public function actionPricing($id)
    {
        if(Yii::$app->request->post('type') && Yii::$app->request->post('measure') && Yii::$app->request->post('tariff')){
            if(Yii::$app->request->post('tariff') > 0){
                $projectObj = new Project;
                $project = Project::findOne(['project_id'=>(int)$id]);

                if(isset($project) && !empty($project->client_id)){
                    $price = ClientTariff::find()->where(['project_id'=>$project->project_id, 'client_id'=>$project->client_id, 'tariff_id'=>(int)Yii::$app->request->post('measure'),'type_cargo_id'=>(int)Yii::$app->request->post('type')])->one();
                    if(isset($price) && $price->number == 0){
                        $client = Client::findOne(['client_id'=>$project->client_id]);
                        if(isset($client)){
                            // //есть ли уже такой тип прайсинга
                            // $if_pricing = ClientTariff::find()->where([
                            //     'client_id'=>$client->client_id,
                            //     'project_id'=>$project->project_id,
                            //     'type_cargo_id'=>(int)Yii::$app->request->post('type'),
                            //     'tariff_id'=>(int)Yii::$app->request->post('measure'),
                            // ])->count();
                            // if($if_pricing == 0){ //если такого pricing не найдено, тогда создать новый
                                $price->number = (int)Yii::$app->request->post('tariff');
                                if($price->update()){
                                    Logs::Create($price->id, $price, 7, 1);
                                    return 'create';
                                }
                            // }else{
                            //     return 'Такой тип pricing уже существует!';
                            // }
                        }
                    }else{
                        return 'Вы не можете изменять тариф, который больше 0!';
                    }
                }
            }else{
                return 'Tariff не может быть меньше 1';
            }
        }else{
            return 'Пожалуйста заполните все поля в pricing';
        }
    }

    public function actionClientsearch($id = 0){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!empty($id)) {
            $client = Client::find()
                ->select('client_id,manager_id,balance,balance_total')
                ->where(['client_id'=>(int)$id])
                ->one();
            if(isset($client->manager_id))
                return $client;

        }
        return array('client_id'=>0);
    }

    // public function actionTariffdelete($id = 0)
    // {
    //     if($id > 0){
    //         $tariff = ClientTariff::findOne(['id'=>(int)$id]);
    //         if(isset($tariff)){
    //             Logs::Create($tariff->id, $tariff, 7, 3);
    //             $tariff->delete();
    //             return $this->redirect(Yii::$app->request->referrer);
    //         }
    //     }
    //     return 'Error';
    // }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
