<?php

namespace app\controllers;

use Yii;

use yii\helpers\Html;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\Login;
use app\models\Telegram;
use app\models\Manager;
use app\models\Cargo;
use app\models\Client;
use app\models\Payments;
use app\models\Plan;
use app\models\Task;



class SiteController extends Controller
{
    public $enableCsrfValidation = false;
    
    //public function behaviors()
    //{
    //    return [
    //        'access' => [
    //            'class' => AccessControl::className(),
    //            'only' => ['logout'],
    //            'rules' => [
    //                [
    //                    'actions' => ['logout'],
    //                    'allow' => true,
    //                    'roles' => ['@'],
    //                ],
    //            ],
    //        ],
    //        'verbs' => [
    //            'class' => VerbFilter::className(),
    //            'actions' => [
    //                'logout' => ['post'],
    //            ],
    //        ],
    //    ];
    //}

    public function actions()
    {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest){
            return true;
        }
        if (Yii::$app->user->identity->role >= 4){
            return true;
        }
        else if($action->id == 'logout'){
            return true;
        }
        elseif (Yii::$app->user->identity->role == 2){
            Yii::$app->getResponse()->redirect('/clients/index');
            return false;
        }else{
            Yii::$app->getResponse()->redirect('/website');
            return false;
        }
        return true;
        // if($action->id == 'login'){
        //     return true;
        // }

        // if (!parent::beforeAction($action))
        // {
        //     return false;
        // }


        // else
        // {
        //     Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
        //     //для перестраховки вернем false
        //     return false;
        // }
    }

    public function actionResources(){
        return $this->render('/bogdan/resources');
    }
    

    // public function actionIndex()
    // {
    //     if(Yii::$app->user->identity->role <= 5){
    //         return $this->redirect('/cargo/index');
    //     }

    //     $user = Manager::findOne(Yii::$app->user->identity->id);
    //     $current_sales = Payments::currentSales();
    //     $current_clients = Client::currentClients();
    //     $current_cargos = Cargo::currentCargos();
    //     $current_cargos_kg = Cargo::currentCargosKg();
    //     $plan = Plan::findOne(['manager_id'=>$user->manager_id,'date'=>strtotime(date('Y-m-2', time()))]);

    //     if(Yii::$app->user->identity->role >= 9){
    //         $cargos = Cargo::find()
    //         ->where(['status'=>1])
    //         ->orderBy(['cargo_id' => SORT_DESC])
    //         ->limit(20)
    //         ->all();
    //     }
    //     else{
    //         $cargos = Cargo::find()
    //         ->where(['status'=>1, 'manager_id'=>Yii::$app->user->identity->id])
    //         ->orderBy(['cargo_id' => SORT_DESC])
    //         ->limit(20)
    //         ->all();
    //     }

    //     if(isset($cargos)){
    //         $all_table_rows = '';
    //         foreach ($cargos as $cargo) {
    //             $all_table_rows .= Yii::$app->controller->renderPartial('/cargo/one_table', [
    //                 'cargo'=>$cargo,
    //             ]);
    //         }
    //     }
    //     else{
    //         $all_table_rows = 'У вас нет Cargo!';
    //     }

    //     if(Yii::$app->user->identity->role >= 9){
    //         $clients = Client::find()
    //         ->orderBy(['status'=>[0,1], 'client_id' => SORT_DESC])
    //         ->limit(20)
    //         ->all();
    //     }
    //     else{
    //         $clients = Client::find()
    //         ->where(['status'=>[0,1], 'manager_id'=>Yii::$app->user->identity->id])
    //         ->orderBy(['client_id' => SORT_DESC])
    //         ->limit(20)
    //         ->all();
    //     }

    //     if(isset($clients)){
    //         $all_table_rows_clients = '';
    //         foreach ($clients as $client) {
    //             $all_table_rows_clients .= Yii::$app->controller->renderPartial('/clients/one_table', [
    //                 'client'=>$client,
    //             ]);
    //         }
    //     }
    //     else{
    //         $all_table_rows_clients = 'У вас нет Clients!';
    //     }

    //     return $this->render('index', [
    //         'all_table_rows' => $all_table_rows,
    //         'all_table_rows_clients' => $all_table_rows_clients,
    //         'user' => $user,
    //         'plan' => $plan,
    //         'current_sales' => $current_sales,
    //         'current_clients' => $current_clients,
    //         'current_cargos' => $current_cargos,
    //         'current_cargos_kg' => $current_cargos_kg,
    //     ]);
    // }

    public function actionIndex()
    {

        $tasks = Task::find()
            ->filterWhere(['=', 'manager_id', Yii::$app->user->identity->id])
            ->andFilterWhere(['=', 'status', 0])
            ->orderBy(['date_end' => SORT_ASC])
            ->all();


        /* Another part  */
        if(Yii::$app->user->identity->role <= 5){
            return $this->redirect('/cargo/index');
        }

        $user = Manager::findOne(Yii::$app->user->identity->id);
        $current_sales = Payments::currentSales();
        $current_clients = Client::currentClients();
        $current_cargos = Cargo::currentCargos();
        $current_cargos_kg = Cargo::currentCargosKg();
        $plan = Plan::findOne(['manager_id'=>$user->manager_id,'date'=>strtotime(date('Y-m-2', time()))]);

        return $this->render('new_index', [
            'user' => $user,
            'plan' => $plan,
            'current_sales' => $current_sales,
            'current_clients' => $current_clients,
            'current_cargos' => $current_cargos,
            'current_cargos_kg' => $current_cargos_kg,
            'tasks' => $tasks,
        ]);
    }

    public function actionMoney()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        return $this->redirect(['site/index']);
        //if (Yii::$app->request->post('money') && !empty((int)Yii::$app->request->post('money'))){
        //    $user = Users::findOne(Yii::$app->user->identity->id);
        //    $user->money += (int)Yii::$app->request->post('money');
        //    $user->update(false);
        //}
        $user = Users::findOne(Yii::$app->user->identity->id);
        return $this->render('money',['user'=>$user]);
    }

    public function actionLogin()
    {
      if (!Yii::$app->user->isGuest) {
          return $this->goHome();
      }
      
      $login = new Login();
      
      if (Yii::$app->request->post('Login')) {
          $login->attributes = Yii::$app->request->post('Login');
      
          if ($login->validate()){
              Yii::$app->user->login($login->getUser(), 3600*24*30);
              return $this->redirect('/index');
              //return $this->goBack();
          }
      }
      
      return Yii::$app->controller->renderPartial('login', [
          'login'=>$login,
      ]);
    }

    public function actionLogout()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        Yii::$app->user->logout();

        return $this->goHome();
    }


    
}
