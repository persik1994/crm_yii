<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Payments;
use app\models\PaymentsSource;
use app\models\PaymentsCosts;
use app\models\Client;
use app\models\Manager;
use app\models\CurrencyCash;
use app\models\CurrencyHistory;
use app\models\CustomTable;
use app\models\Project;
use app\models\Cargo;
use app\models\ShipmentType;
use app\models\BuyingList;
use app\models\Logs;

/**
 * PaymentsController implements the CRUD actions for Payments model.
 */
class PaymentsController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    
    public function actionIndex(){
        $session = Yii::$app->session;

        Payments::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Payments::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
            'sort'=> ['defaultOrder' => ['date_payment'=>SORT_DESC, 'payment_id'=>SORT_DESC]],
        ]);

        if(Yii::$app->user->identity->role >= 10){
            return $this->render('table', [
                'dataProvider' => $dataProvider,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }

        return $this->render('table_managers', [
            'dataProvider' => $dataProvider,
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }

    public function actionBalances(){
        $session = Yii::$app->session;

        Client::SearchSession(1);

        $dataProvider = new ActiveDataProvider([
            'query' => Client::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
            'sort'=> ['defaultOrder' => ['client_id'=>SORT_DESC]],
        ]);

        return $this->render('table_balances', [
            'dataProvider' => $dataProvider,
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }

    public function actionEditbalance($id)
    {
        $model = Client::findOne((int)$id);

        $managers = Manager::find()->where('status = 1')->all();
        

        $transfers = Payments::find()
            ->where(['client_id'=>(int)$id, 'status'=>1])
            ->andWhere('(payment_type = 2 AND (out_source_id = 4 OR out_source_id = 0)) OR payment_type = 1')
            ->orderBy(['payment_id'=>SORT_DESC])
            ->all();
        ///////
        return $this->render('update_balance', [
            'model' => $model,
            'managers' => $managers,
            'transfers' => $transfers,
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }
    
    public function actionCosts(){
        $session = Yii::$app->session;

        PaymentsCosts::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => PaymentsCosts::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
            'sort'=> ['defaultOrder' => ['date_payment'=>SORT_DESC]],
        ]);

        return $this->render('table_costs', [
            'dataProvider' => $dataProvider,
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }

    public function actionApproved()
    {
        Payments::DeleteAllSession();
        $session = Yii::$app->session;
        $session['payment_status'] = 1;
        return $this->redirect('/payments/index');
    }

    public function actionNoapproved()
    {
        Payments::DeleteAllSession();
        $session = Yii::$app->session;
        $session['payment_status'] = 2;
        return $this->redirect('/payments/index');
    }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) //ajax
    {
        $payments_Obj = new Payments;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = (int)$id;
        if(!empty($id)){
            $payment = Payments::findOne(['payment_id'=>$id]);
            if(isset($payment)){
                $transfers = Payments::find()
                    ->where(['client_id'=>(int)$payment->client_id, 'status'=>1])
                    ->andWhere('(payment_type = 2 AND (out_source_id = 4 OR out_source_id = 0)) OR payment_type = 1')
                    ->orderBy(['payment_id'=>SORT_DESC])
                    ->all();

                foreach ($transfers as $transfer) {
                    $transfer->date_payment = (!empty($transfer->date_payment)) ? date('d.m.Y', $transfer->date_payment) : '';
                    $transfer->project_id = (!empty($transfer->project_id)) ? '<a target="_blank" href="/projects/edit/'.$transfer->project_id.'">PRJ-'.$transfer->project_id.'</a>' : '';
                    $transfer->cash_usd = ($transfer->payment_type == 1) ? '+'.$transfer->cash_usd : '-'.$transfer->cash_usd;
                    if(Yii::$app->user->identity->role < 10){
                        $transfer->balance = '';
                    }
                }
                /////////

                foreach ($payment as $key => $value) {
                    if($value == "0"){
                        $payment[$key] = '';
                    }
                }

                $payment->client_id = (!empty($payment->client_id)) ? '88-'.$payment->client_id : '';
                $payment->manager_id = (!empty($payment->manager->username)) ? $payment->manager->username : '';
                $payment->branch_id = $payments_Obj->branch($payment->branch_id);
                $payment->source_id = (!empty($payment->source->name)) ? $payment->source->name : '';
                $payment->date_payment = (!empty($payment->date_payment)) ? date('d.m.Y', $payment->date_payment) : '';
                $payment->payment_type = $payments_Obj->type($payment->payment_type);
                $payment->out_source_id = $payments_Obj->outSource($payment->out_source_id);
                $payment->payment_status = $payments_Obj->status($payment->payment_status);

                if(!empty($payment->cash_fact_currency))
                    $payment->cash_fact_currency = $payment->currency->name;
                else
                    $payment->cash_fact_currency = '';

                if(!empty($payment->status))
                    $payment->status = 'Yes';
                else
                    $payment->status = 'No';

                // if(!empty($payment->source->name))
                //     $payment->source_id = $payment->source->name;
                // else
                //     '';

                // if(empty($payment->balance))
                //     $payment->balance = '0.00';

                // $payment->date_add = (!empty($payment->date_add)) ? date('d.m.Y', $payment->date_add) : '';
            }
            return ['payment'=>$payment,'transfers'=>$transfers];
        }
    }

    /**
     * Creates a new Payments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payments();
        $managers = Manager::find()->where('status = 1')->all();
        $currencies = CurrencyCash::find()->orderBy(['сurrency_id' => SORT_ASC])->all();
        $sources = PaymentsSource::find()->all();
        $projects = Project::find()->where('status != 5')->all();

        if(Yii::$app->user->identity->role >= 10)
            $clients = Client::find()->where('status = 1')->all();
        else
            $clients = Client::find()->where(['status'=>1,'manager_id'=>Yii::$app->user->identity->id])->all();

        if (Yii::$app->request->post()) {
            //////////////////////
            if(Yii::$app->user->identity->role >= 10)
                $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            else
                $model->manager_id = Yii::$app->user->identity->id;
            /////////////////////
            $model->branch_id = (Yii::$app->request->post('branch')) ? (int)Yii::$app->request->post('branch') : 0;
            $model->date_payment = (Yii::$app->request->post('date_add')) ? strtotime(date('d-m-Y 2:00',strtotime(Yii::$app->request->post('date_add')))) : 0;
            $model->payment_status = (Yii::$app->request->post('payment_status')) ? (int)Yii::$app->request->post('payment_status') : 0;
            $model->payment_type = (Yii::$app->request->post('payment_type')) ? Yii::$app->request->post('payment_type') : 0;
            $model->client_id = (Yii::$app->request->post('client_id')) ? (int)Yii::$app->request->post('client_id') : 0;
            $model->cash_usd = (Yii::$app->request->post('paid_usd')) ? Yii::$app->request->post('paid_usd') : 0;
            //////////////////////
            if(Yii::$app->user->identity->role >= 10){
                $model->status = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;
            }else{
                $model->status = 0;
                $status_pay = 0;
            }
            /////////////////////
            $model->comment = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
            $model->cash_fact = (Yii::$app->request->post('paid_fact')) ? Yii::$app->request->post('paid_fact') : 0;
            $model->cash_fact_currency = (Yii::$app->request->post('paid_currency_id')) ? Yii::$app->request->post('paid_currency_id') : 0;
            $model->source_id = (Yii::$app->request->post('source')) ? Yii::$app->request->post('source') : 0;
            $model->out_source_id = (Yii::$app->request->post('out_source')) ? Yii::$app->request->post('out_source') : 0;
            $model->project_id = (Yii::$app->request->post('project')) ? Yii::$app->request->post('project') : 0;
            //files//
            $model->invoice = Cargo::uploadFile('invoice', 'files/');
            $model->bankbill = Cargo::uploadFile('bankbill', 'files/');
            $model->balance = 0;
            //
            if($model->payment_type == 2 && $model->out_source_id < 4 && $model->project_id <= 0){
                return 'Please enter a project!';
            } 
            if($model->client_id <= 0){
                return 'Please enter a client!';
            }
            if($model->save()){
                Logs::Create($model->payment_id, $model, 6, 1);
                if($model->status == 1){ //if(!empty($model->client_id) && $model->status == 1){
                    switch ($model->payment_type) {
                        case 1:
                                $client = Client::findOne(['client_id'=>(int)$model->client_id]);
                                if(isset($client)){
                                   $client->balance += $model->cash_usd;
                                   $client->balance_total += $model->cash_usd; 
                                   Logs::Create($client->client_id, $client, 1, 2);
                                   $client->update(false);
                                }
                            break;
                        
                        case 2:
                                if($model->project_id > 0){
                                    $project = Project::findOne(['project_id'=>$model->project_id]);
                                    if(isset($project)){
                                        if($project->project_type == 2 || $project->project_type == 3){
                                            $project->chargers += $model->cash_usd;
                                            Logs::Create($project->project_id, $project, 3, 2);
                                            $project->update(false);
                                        }
                                    }
                                }
                            break;
                    }
                }
                Payments::saveBalance($model->payment_id);
                if($model->project_id > 0){
                    Payments::PayOutCartNotProject_IsTransfer($model->project_id);
                }
                return '1';
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'managers' => $managers,
                'clients' => $clients,
                'currencies' => $currencies,
                'sources' => $sources,
                'projects' => $projects,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }
    }

    public function actionCreatecosts()
    {
        $model = new Payments();
        $managers = Manager::find()->where('status = 1')->all();
        $clients = Client::find()->where('status = 1')->all();
        $currencies = CurrencyCash::find()->orderBy(['сurrency_id' => SORT_ASC])->all();
        $sources = PaymentsSource::find()->all();
        $projects = Project::find()->where('status != 5')->all();

        if (Yii::$app->request->post()) {
            //////////////////////
            if(Yii::$app->user->identity->role >= 10)
                $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            else
                $model->manager_id = Yii::$app->user->identity->id;
            /////////////////////
            $model->branch_id = 2;
            $model->date_payment = (Yii::$app->request->post('date_add')) ? strtotime(Yii::$app->request->post('date_add')) : 0;
            $model->payment_status = 2;
            $model->payment_type = 2;
            $model->client_id = 0;
            $model->cash_usd = (Yii::$app->request->post('paid_usd')) ? Yii::$app->request->post('paid_usd') : 0;
            $model->status = 1;
            $model->comment = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
            $model->cash_fact = (Yii::$app->request->post('paid_fact')) ? Yii::$app->request->post('paid_fact') : 0;
            $model->cash_fact_currency = (Yii::$app->request->post('paid_currency_id')) ? Yii::$app->request->post('paid_currency_id') : 0;
            $model->source_id = 0;
            $model->out_source_id = 5;
            $model->project_id = 0;
            //files//
            $model->invoice = '';
            $model->bankbill = '';
            $model->balance = 0;
            //
            if($model->save()){
                return '1';
            }
        } else {
            return $this->render('create_cost', [
                'model' => $model,
                'managers' => $managers,
                'clients' => $clients,
                'currencies' => $currencies,
                'sources' => $sources,
                'projects' => $projects,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }
    }

    public function actionEditcosts($id)
    {
        $model = $this->findModel((int)$id);
        $managers = Manager::find()->where('status = 1')->all();
        $clients = Client::find()->where('status = 1')->all();
        $currencies = CurrencyCash::find()->orderBy(['сurrency_id' => SORT_ASC])->all();
        $sources = PaymentsSource::find()->all();
        $projects = Project::find()->where('status != 5')->all();

        if (Yii::$app->request->post()) {
            //////////////////////
            if(Yii::$app->user->identity->role >= 10)
                $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            else
                $model->manager_id = Yii::$app->user->identity->id;
            /////////////////////
            $model->branch_id = 2;
            $model->date_payment = (Yii::$app->request->post('date_add')) ? strtotime(Yii::$app->request->post('date_add')) : 0;
            $model->payment_status = 2;
            $model->payment_type = 2;
            $model->client_id = 0;
            $model->cash_usd = (Yii::$app->request->post('paid_usd')) ? Yii::$app->request->post('paid_usd') : 0;
            $model->status = 1;
            $model->comment = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
            $model->cash_fact = (Yii::$app->request->post('paid_fact')) ? Yii::$app->request->post('paid_fact') : 0;
            $model->cash_fact_currency = (Yii::$app->request->post('paid_currency_id')) ? Yii::$app->request->post('paid_currency_id') : 0;
            $model->source_id = 0;
            $model->out_source_id = 5;
            $model->project_id = 0;
            //files//
            $model->invoice = '';
            $model->bankbill = '';
            //
            if($model->update()){
                return '1';
            }
        } else {
            return $this->render('update_cost', [
                'model' => $model,
                'managers' => $managers,
                'clients' => $clients,
                'currencies' => $currencies,
                'sources' => $sources,
                'projects' => $projects,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }
    }

    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEdit($id)
    {
        $model = $this->findModel((int)$id);

        $status_pay = $model->status;

        $managers = Manager::find()->where('status = 1')->all();
        $clients = Client::find()->where('status = 1')->all();
        $currencies = CurrencyCash::find()->orderBy(['сurrency_id' => SORT_ASC])->all();
        $sources = PaymentsSource::find()->all();
        $projects = Project::find()->where('status != 5')->all();

        if(Yii::$app->user->identity->role >= 10)
            $clients = Client::find()->where('status = 1')->all();
        else
            $clients = Client::find()->where(['status'=>1,'manager_id'=>Yii::$app->user->identity->id])->all();

        if (Yii::$app->request->post() && isset($model)) {
            // if(Yii::$app->user->identity->role < 10){
            //     return 'Вы не из финансового отдела. Вы не имеете прав редактировать платеж!';
            // }
            if(Yii::$app->user->identity->role < 10 && $model->out_source_id == 4){
                return 'Платеж создан в проекте, его нельзя редактировать!';
            }
            //////////////////////
            if(Yii::$app->user->identity->role >= 10)
                $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            else
                $model->manager_id = Yii::$app->user->identity->id;
            /////////////////////
            $model->branch_id = (Yii::$app->request->post('branch')) ? (int)Yii::$app->request->post('branch') : 0;
            $model->date_payment = (Yii::$app->request->post('date_add')) ? strtotime(date('d-m-Y 2:00',strtotime(Yii::$app->request->post('date_add')))) : 0;
            $model->payment_status = (Yii::$app->request->post('payment_status')) ? (int)Yii::$app->request->post('payment_status') : 0;
            $model->payment_type = (Yii::$app->request->post('payment_type')) ? Yii::$app->request->post('payment_type') : 0;
            $model->client_id = (Yii::$app->request->post('client_id')) ? (int)Yii::$app->request->post('client_id') : 0;
            $model->cash_usd = (Yii::$app->request->post('paid_usd')) ? Yii::$app->request->post('paid_usd') : 0;
            //////////////////////
            if(Yii::$app->user->identity->role >= 10){
                $model->status = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;
            }
            /////////////////////
            $model->comment = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
            $model->cash_fact = (Yii::$app->request->post('paid_fact')) ? Yii::$app->request->post('paid_fact') : 0;
            $model->cash_fact_currency = (Yii::$app->request->post('paid_currency_id')) ? Yii::$app->request->post('paid_currency_id') : 0;
            if($model->payment_type == 1){
                $model->source_id = (Yii::$app->request->post('source')) ? Yii::$app->request->post('source') : 0;
                $model->project_id = 0;
                $model->out_source_id = 0;
            }
            else if($model->payment_type == 2){
                $model->out_source_id = (Yii::$app->request->post('out_source')) ? Yii::$app->request->post('out_source') : 0;
                $model->project_id = (Yii::$app->request->post('project')) ? Yii::$app->request->post('project') : 0;
                $model->source_id = 0;
            }
            //files//
            $file2 = Cargo::uploadFile('invoice', 'files/');
            if(!empty($file2))
                $model->invoice = $file2;
            $file3 = Cargo::uploadFile('bankbill', 'files/');
            if(!empty($file3))
                $model->bankbill = $file3;
            //
            if($model->status == 0 && $model->status != $status_pay){
                $model = $this->findModel((int)$id);
                $model->status = 0;
                $new_status = 1;
            }
            if($status_pay == 0 || !empty($new_status)){
                Logs::Create($model->payment_id, $model, 6, 2);
                if($model->update()){
                    if($model->status != $status_pay){
                        switch ($model->payment_type) {
                            case 1:
                                if ($model->status == 0 && !empty($model->client_id)) {
                                    $client = Client::findOne(['client_id'=>(int)$model->client_id]);
                                    if(isset($client)){
                                        $client->balance -= $model->cash_usd;
                                        $client->balance_total -= $model->cash_usd;
                                        Logs::Create($client->client_id, $client, 1, 2);
                                        $client->update(false);
                                    }
                                }
                                else if(!empty($model->client_id)){
                                    $client = Client::findOne(['client_id'=>(int)$model->client_id]);
                                    if(isset($client)){
                                        $client->balance += $model->cash_usd;
                                        $client->balance_total += $model->cash_usd;
                                        Logs::Create($client->client_id, $client, 1, 2);
                                        $client->update(false);
                                    }
                                }
                                break;
                            
                            case 2:
                                if($model->status == 0 && $model->out_source_id == 4){
                                    $client = Client::findOne(['client_id'=>(int)$model->client_id]);
                                    if(isset($client)){
                                        $client->balance += $model->cash_usd;
                                        Logs::Create($client->client_id, $client, 1, 2);
                                        $client->update(false);
                                        return '1';
                                    }
                                }
                                if ($model->status == 1) {
                                    if($model->project_id > 0){
                                        $project = Project::findOne(['project_id'=>$model->project_id]);
                                        if(isset($project)){
                                            if($project->project_type == 2 || $project->project_type == 3){
                                                $project->chargers += $model->cash_usd;
                                                Logs::Create($project->project_id, $project, 3, 2);
                                                $project->update(false);
                                            }
                                        }
                                    }
                                }
                                else {
                                   if($model->project_id > 0){
                                        $project = Project::findOne(['project_id'=>$model->project_id]);
                                        if(isset($project)){
                                            if($project->project_type == 2 || $project->project_type == 3){
                                                $project->chargers -= $model->cash_usd;
                                                Logs::Create($project->project_id, $project, 3, 2);
                                                $project->update(false);
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                    Payments::saveBalance($model->payment_id);
                    if($model->project_id > 0){
                        Payments::PayOutCartNotProject_IsTransfer($model->project_id);
                    }
                    return '1';
                }
            }else{
                return 'Платеж подтвержден или удален, его нельзя редактировать! Сначала нужно снять подтверждение, а тогда редактировать';
            }
        } else {
            if($model->project_id > 0){
                Payments::PayOutCartNotProject_IsTransfer($model->project_id);
            }
            return $this->render('update', [
                'model' => $model,
                'managers' => $managers,
                'clients' => $clients,
                'currencies' => $currencies,
                'sources' => $sources,
                'projects' => $projects,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }
    }

    public function actionApprove($id){ //context menu approve
        $model = $this->findModel((int)$id);
        $status_pay = $model->status;
        if (Yii::$app->user->identity->role >= 10) {
            $model->status = 1;
            if($status_pay == 0){
                Logs::Create($model->payment_id, $model, 6, 2);
                if($model->update()){
                    if($model->status != $status_pay){
                        switch ($model->payment_type) {
                            case 1:
                                if(!empty($model->client_id)){
                                    $client = Client::findOne(['client_id'=>(int)$model->client_id]);
                                    if(isset($client)){
                                        $client->balance += $model->cash_usd;
                                        $client->balance_total += $model->cash_usd;
                                        Logs::Create($client->client_id, $client, 1, 2);
                                        $client->update(false);
                                    }
                                }
                                break;
                            
                            case 2:
                                if ($model->status == 1) {
                                    if($model->project_id > 0){
                                        $project = Project::findOne(['project_id'=>$model->project_id]);
                                        if(isset($project)){
                                            if($project->project_type == 2 || $project->project_type == 3){
                                                $project->chargers += $model->cash_usd;
                                                Logs::Create($project->project_id, $project, 3, 2);
                                                $project->update(false);
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                    Payments::saveBalance($model->payment_id);
                }
            }
        } 
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionEditsuper($id)
    {
        $model = $this->findModel((int)$id);

        $status_pay = $model->status;

        $managers = Manager::find()->where('status = 1')->all();
        $clients = Client::find()->where('status = 1')->all();
        $currencies = CurrencyCash::find()->orderBy(['сurrency_id' => SORT_ASC])->all();
        $sources = PaymentsSource::find()->all();
        $projects = Project::find()->where('status != 5')->all();

        /////Client
        $model_Client = Client::findOne((int)$model->client_id);
        if(isset($model_Client)){
            $transfers = Payments::find()
                ->where(['client_id'=>(int)$model_Client->client_id, 'status'=>1])
                ->andWhere('(payment_type = 2 AND (out_source_id = 4 OR out_source_id = 0)) OR payment_type = 1')
                ->orderBy(['payment_id'=>SORT_DESC])
                ->all();
            
            $balance = $model_Client->balance;
            $balance_total = $model_Client->balance_total;
        }else{
            $model_Client = new Client;
        }

        ///////Project///////
        $shipment_types = ShipmentType::find()->where('status = 1')->all();
        $buyings = BuyingList::find()->where(['status'=>[0,1]])->all();
        ///////
        $model_Project = Project::findOne((int)$model->project_id);
        if(isset($model_Project)){
            $withdraws = Payments::find()->where(['out_source_id'=>4,'project_id'=>$model_Project->project_id])->all();
        }else{
            $model_Project = new Project;
        }

        return $this->render('update_super', [
            'model' => $model,
            'managers' => $managers,
            'clients' => $clients,
            'currencies' => $currencies,
            'sources' => $sources,
            'projects' => $projects,
            'menu' => Yii::$app->controller->renderPartial('menu'),

            //////client
            'transfers' => $transfers ?? array(),
            'balance' => $balance ?? 0,
            'balance_total' => $balance_total ?? 0,

            //////project
            'model_Project' => $model_Project,
            'delivery_types' => $shipment_types,
            'buyings' => $buyings,
            'client_select' => $client_select ?? array(),
            'balance' => $balance ?? 0,
            'withdraws' => $withdraws ?? array(),

        ]);
    }

    ///////

    public function actionWithdraw($id)
    {
        if(Yii::$app->request->post('date') && Yii::$app->request->post('amount')){
            if(Yii::$app->request->post('amount') <= 0){
                return 'Сумма списания не может быть меньше 1';
            }
            $projectObj = new Project;
            $project = Project::findOne(['project_id'=>(int)$id]);

            if(isset($project) && !empty($project->client_id)){
                $client = Client::findOne(['client_id'=>$project->client_id]);
                if(isset($client)){
                    if($project->total < ($project->payed + floatval(Yii::$app->request->post('amount')))){
                        return 'Total не может быть меньше, чем paid';
                    }
                    //У клиента недостаточно средств
                    //
                    if($client->balance < floatval(Yii::$app->request->post('amount'))){
                        return 'У клиента недостаточно средств на балансе';
                    }
                    //
                    $payment = new Payments;
                    $payment->client_id = $client->client_id;
                    $payment->manager_id = Yii::$app->user->identity->id;
                    $payment->branch_id = 2;
                    $payment->source_id = 0;
                    $payment->out_source_id = 4;
                    $payment->date_payment = strtotime(date('d-m-Y 2:00',time()));
                    $payment->payment_status = 1;
                    $payment->payment_type = 2;
                    $payment->cash_usd = floatval(Yii::$app->request->post('amount'));
                    $payment->cash_fact = floatval(Yii::$app->request->post('amount'));
                    $payment->cash_fact_currency = 4;
                    $payment->status = 1;
                    $payment->project_id = $project->project_id;
                    $payment->comment = Yii::$app->request->post('description');
                    $payment->balance = 0;
                    if($payment->save()){
                        $client->balance -= floatval(Yii::$app->request->post('amount'));
                        Logs::Create($client->client_id, $client, 1, 2);
                        $client->update(false);
                        Payments::PayOutCartNotProject_IsTransfer($payment->project_id);
                        Payments::saveBalance($payment->payment_id);
                        return 'create';
                    }
                }else{
                    return 'Клиент не найден!';
                }
            }else{
                return 'Клиент не выбран!';
            }
        }
    }

    public function actionSearch(){
        Payments::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Payments::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
        ]);

        if(Yii::$app->user->identity->role >= 10){
            return $this->renderPartial('/payments/table', [
                'dataProvider' => $dataProvider,
                'menu' => '',
            ]);
        }

        return $this->renderPartial('/payments/table_managers', [
            'dataProvider' => $dataProvider,
            'menu' => '',
        ]);
    }

    public function actionSearchdelete(){
        if(Yii::$app->request->get('type')){
            $session = Yii::$app->session;
            if($session->has(Yii::$app->request->get('type'))){
                $session->remove(Yii::$app->request->get('type'));
            }
        }
        return $this->redirect('/payments/index?sort='.Yii::$app->request->get('sort'));
    }

    public function actionSearchdeleteall(){
        Payments::DeleteAllSession();
        return $this->redirect('/payments/index');
    }

    public function actionExchanges(){
        if(Yii::$app->user->identity->role < 10){
            return $this->redirect('/payments/index');
        }
        $allExchanges = CurrencyCash::find()->all();
        if (Yii::$app->request->post()) {
            $model = CurrencyCash::findOne((int)Yii::$app->request->post('id'));
            if(isset($model)){
                $model->currency = floatval(Yii::$app->request->post('money'));
                $model->date = time();
                $model->manager_id = Yii::$app->user->identity->manager_id;
                Logs::Create($model->сurrency_id, $model, 9, 2);
                if($model->update()){
                    $new_history = new CurrencyHistory;
                    $new_history->currency_id   = $model->сurrency_id;
                    $new_history->manager_id    = $model->manager_id;
                    $new_history->date          = $model->date;
                    $new_history->currency      = $model->currency;
                    $new_history->save();
                }
            }
        } else {
            return $this->render('exchanges', [
                'exchanges' => $allExchanges,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }
    }

    public function actionExchangeshistory(){
        $allExchanges = CurrencyHistory::find()->orderBy(['date' => SORT_DESC])->all();
        return $this->render('exchanges_history', [
            'exchanges_history' => $allExchanges,
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }

    ////////


    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel((int)$id);
        if(isset($model) && Yii::$app->user->identity->role >= 10){
            if($model->status == 1){
                switch ($model->payment_type) {
                    case 1:
                        $client = Client::findOne(['client_id'=>(int)$model->client_id]);
                        if(isset($client)){
                            $client->balance -= $model->cash_usd;
                            $client->balance_total -= $model->cash_usd;
                            Logs::Create($client->client_id, $client, 1, 2);
                            $client->update(false);
                        }
                    break;
                    
                    case 2:
                        if($model->out_source_id == 4){
                            $client = Client::findOne(['client_id'=>(int)$model->client_id]);
                            if(isset($client)){
                                $client->balance += $model->cash_usd;
                                Logs::Create($client->client_id, $client, 1, 2);
                                $client->update(false);
                            }
                        }
                        else if($model->project_id > 0){
                            $project = Project::findOne(['project_id'=>$model->project_id]);
                            if(isset($project)){
                                if($project->project_type == 2 || $project->project_type == 3){
                                    $project->chargers -= $model->cash_usd;
                                    Logs::Create($project->project_id, $project, 3, 2);
                                    $project->update(false);
                                }
                            }
                        }
                        break;
                }
            }
            $model->status = 5;
            Logs::Create($model->payment_id, $model, 6, 3);
            $model->update(false);
            Payments::saveBalance($model->payment_id);
        }

        return $this->redirect(['index']);
    }

    public function actionCustomtable(){
        if(Yii::$app->request->get('page') && Yii::$app->request->get('type')){
            $attributes = substr(Yii::$app->request->get('type'), 0, -1);
            $attributes = explode(",", $attributes);
            if(isset($attributes)){
                CustomTable::deleteAll([
                    'manager_id'=>Yii::$app->user->identity->id,
                    'page'=>'payment',
                    'page_two'=>Yii::$app->request->get('page')
                ]);
                foreach ($attributes as $type) {
                    $customTable = new CustomTable;
                    $customTable->manager_id = Yii::$app->user->identity->id;
                    $customTable->page = 'payment';
                    $customTable->page_two = Yii::$app->request->get('page');
                    $customTable->type = $type;
                    $customTable->save();
                }
            }
        }
        return 1;
    }

    // public function actionWritebalance(){
    //     $payments = Payments::find()->all();
    //     foreach ($payments as $payment) {
    //         $client = Client::findOne(['client_id'=>$payment->client_id]);
    //         if(isset($client)){
    //             $payment->balance = $client->balance;
    //             $payment->update(false);
    //         }
    //     }
    // }

    // public function actionBalanceremath(){
    //     $payments = Payments::find()->where('status = 1')->orderBy(['client_id'=>SORT_ASC,'payment_id'=>SORT_DESC])->all();
    //     $balance = 0;
    //     $client_id = 0;
    //     $cash_usd = 0;
    //     $access = 0;
    //     $payment_type = 0;
    //     $out_source_id = 0;
    //     $pay_id = 0;
    //     foreach ($payments as $payment) {
    //         $find_pay = Payments::findOne(['payment_id'=>$pay_id]);
    //         if((($payment->payment_type == 1) || ($payment->payment_type == 2 && $payment->out_source_id == 4)) && $payment->status == 1){
    //             if($balance == 0 && $client_id == 0 && $payment->client_id != 0){
    //                 $client_id = $payment->client_id;
    //                 $balance = $payment->balance;
    //                 $pay_id = $payment->payment_id;
    //                 $cash_usd = $payment->cash_usd;
    //                 $access = 0;
    //                 $payment_type = $payment->payment_type;
    //                 $out_source_id = $payment->out_source_id;
    //                 print('!!!!!!!!!');
    //             }
    //             if($client_id != $payment->client_id && $payment->client_id != 0){
    //                 $client_id = $payment->client_id;
    //                 $balance = $payment->balance;
    //                 $pay_id = $payment->payment_id;
    //                 $cash_usd = $payment->cash_usd;
    //                 $access = 0;
    //                 $payment_type = $payment->payment_type;
    //                 $out_source_id = $payment->out_source_id;
    //                 print('&&'.$pay_id.'&&&&&');
    //             } 
    //             if($client_id == $payment->client_id && $payment->client_id != 0 && $access == 0){
    //                 $access = 1;
    //                 $client_id = $payment->client_id;
    //                 $balance = $payment->balance;
    //                 $pay_id = $payment->payment_id;
    //                 $cash_usd = $payment->cash_usd;
    //                 $payment_type = $payment->payment_type;
    //                 $out_source_id = $payment->out_source_id;
    //                 print('****'.$pay_id.'****');
    //             }
    //             else if($client_id == $payment->client_id && $payment->client_id != 0 && $access == 1 && isset($find_pay)){
    //                 $client_id = $payment->client_id;
    //                 if($payment_type == 1){
    //                     $payment->balance = $find_pay->balance - $cash_usd;
    //                     echo '<br> ID:'.$payment->payment_id.' cash_usd:-'.$cash_usd.' balance:'.$balance.' payment balance:'.$balance.' SUMM: '.($cash_usd - $balance).'| ';
    //                     $cash_usd = $payment->cash_usd;
    //                     $balance =  $find_pay->balance - $cash_usd;
    //                     $payment->update(false);
    //                     $access = 1;
    //                     $payment_type = $payment->payment_type;
    //                     $out_source_id = $payment->out_source_id;
    //                     $pay_id = $payment->payment_id;
    //                 }else if($payment_type == 2 && $out_source_id == 4){
    //                     $payment->balance = $find_pay->balance + $cash_usd;
    //                     echo '<br> ID:'.$payment->payment_id.' cash_usd:+'.$cash_usd.' balance:'.$balance.' payment balance:'.$payment->balance.' SUMM: '.($cash_usd + $balance).'| ';
    //                     $cash_usd = $payment->cash_usd;
    //                     $balance = $find_pay->balance + $cash_usd;
    //                     $payment->update(false);
    //                     $access = 1;
    //                     $payment_type = $payment->payment_type;
    //                     $out_source_id = $payment->out_source_id;
    //                     $pay_id = $payment->payment_id;
    //                 }
    //             }
    //         }
    //     }
    // }

    /**
     * Finds the Payments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    // public function actionMathincomeauto(){
    //     $payments_income = Payments::find()->where(['payment_type'=>1])->all();
    //     foreach ($payments_income as $payment) {
    //         if($payment->date_payment > 0){
    //             $payment->date_payment = strtotime(date('d-m-Y 2:00',$payment->date_payment));
    //             $payment->update();
    //         }
    //     }
    // }

    public function actionPaymentsbalancemath(){
        Payments::payments_all_math_outcome_balance();
    }

    protected function findModel($id)
    {
        if (($model = Payments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
