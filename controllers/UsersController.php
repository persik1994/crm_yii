<?php

namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\TypeUser;
use app\models\Users;

class UsersController extends Controller
{
    static function TypeAccountText($type){
        $typeQuery = TypeUser::findOne((int)$type);
        $user = Users::findOne(Yii::$app->user->identity->id);
        if((int)$type == $user->type_id){
            return 'Ваш текущий тарифный план';
        }
        else{
            return 'ПЕРЕЙТИ НА '.$typeQuery->name;
        }
    }
}
