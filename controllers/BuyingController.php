<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\BuyingList;
use app\models\Project;
use app\models\Manager;
use app\models\Client;
use app\models\Cargo;
use app\models\Payments;
use app\models\CustomTable;
use app\models\Images;
use app\models\Logs;

/**
 * ProjectsController implements the CRUD actions for Project model.
 */
class BuyingController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex(){
        $session = Yii::$app->session;

        BuyingList::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => BuyingList::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
            'sort'=> ['defaultOrder' => ['project_id'=>SORT_DESC]],
        ]);

        return $this->render('table', [
            'dataProvider' => $dataProvider,
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }

    
    public function actionView($id) //ajax
    {
        $buyings_Obj = new BuyingList;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = (int)$id;
        if(!empty($id)){
            $buying = $this->findModel((int)$id);
            if(isset($buying)){
                foreach ($buying as $key => $value) {
                    if($value == "0"){
                        $buying[$key] = '';
                    }
                }
                $buying->buying_id = (!empty($buying->buying_id)) ? $buying->buying_id : '';
                $buying->date_create = (!empty($buying->date_create)) ? date('d.m.Y', $buying->date_create) : '';
                $buying->client_id = (!empty($buying->client_id)) ? '88-'.$buying->client_id : '';
                $buying->manager_id = (!empty($buying->manager->username)) ? $buying->manager->username : '';
                $buying->manager_buy_id = (!empty($buying->managerbuying->username)) ? $buying->managerbuying->username : '';
                $buying->project_id = (!empty($buying->project_id)) ? 'PRJ-'.$buying->project_id : '';
                $buying->status_id = (!empty($buying->status_id)) ? $buyings_Obj->status($buying->status_id) : '';
                $buying->total = (!empty($buying->total)) ? $buying->project_id : 0;
                $buying->comment = (!empty($buying->comment)) ? $buying->comment : '';
                $buying->name = (!empty($buying->name)) ? $buying->name : '';
                ///////
                $buying->date_file = '';
                $files_manager = Images::findAllImages(1,$buying->buying_id);
                if(isset($files_manager)){
                    foreach ($files_manager as $file) {
                        $buying->date_file .= '<a href="'.$file->url.'" target="_blank"><i class="fas fa-camera-retro"></i></a>';
                    }
                }
                ////////
                $buying->file_two = (!empty($buying->file_two)) ? '<a href="'.$buying->file_two.'" target="_blank"><i class="fas fa-camera-retro"></i></a> '.date('d.m.Y', $buying->file_two) : '';
                $buying->status = (!empty($buying->status)) ? 'Approved' : 'No Approved';
            }
            return $buying;
        }
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BuyingList();
        $managers = Manager::find()->where('status = 1')->all();
        $clients = Client::find()->where('status = 1')->all();
        $projects = Project::find()->where('status != 5')->all();

        if (Yii::$app->request->post()) {
            $model->manager_id          = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            $model->manager_buy_id      = (Yii::$app->request->post('manager-buyer')) ? (int)Yii::$app->request->post('manager-buyer') : 0;
            $model->status_id           = (Yii::$app->request->post('status_id')) ? (int)Yii::$app->request->post('status_id') : 0;
            $model->client_id           = (Yii::$app->request->post('client')) ? (int)Yii::$app->request->post('client') : 0;
            $model->project_id          = (Yii::$app->request->post('project')) ? (int)Yii::$app->request->post('project') : 0;
            $model->name                = (Yii::$app->request->post('name')) ? Yii::$app->request->post('name') : '';
            $model->comment             = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
            $model->total               = (Yii::$app->request->post('total')) ? (int)Yii::$app->request->post('total') : 0;
            $model->date_create         = (Yii::$app->request->post('date-create')) ? (int)strtotime(Yii::$app->request->post('date-create')) : 0;
            $model->status              = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;
            $model->file                = '';
            $model->file_two            = '';
            $model->description_file    = '';
            $model->description_file_two= '';
            if($model->save(false)){
                Logs::Create($model->buying_id, $model, 4, 1);
                Images::uploadFiles('file', 'files/', 1, $model->buying_id, $model->status((Yii::$app->request->post('status_id')) ? (int)Yii::$app->request->post('status_id') : 0));
                if($model->project_id > 0){
                    $project_find = Project::findOne(['project_id'=>(int)$model->project_id]);
                    if(isset($project_find)){
                        $project_find->buying_id = $model->buying_id;
                        Logs::Create($model->project_id, $project_find, 3, 2);
                        $project_find->update(false);
                    }
                }
                return '1';
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'managers' => $managers,
                'clients' => $clients,
                'projects' => $projects,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEdit($id)
    {
        $model = $this->findModel((int)$id);
        $managers = Manager::find()->where('status = 1')->all();
        $clients = Client::find()->where('status = 1')->all();
        $projects = Project::find()->where('status != 5')->all();
        $files_manager = Images::findAllImages(1,$id);

        if (Yii::$app->request->post()) {
            $model->manager_id          = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            $model->manager_buy_id      = (Yii::$app->request->post('manager-buyer')) ? (int)Yii::$app->request->post('manager-buyer') : 0;
            $model->status_id           = (Yii::$app->request->post('status_id')) ? (int)Yii::$app->request->post('status_id') : 0;
            $model->client_id           = (Yii::$app->request->post('client')) ? (int)Yii::$app->request->post('client') : 0;
            $model->project_id          = (Yii::$app->request->post('project')) ? (int)Yii::$app->request->post('project') : 0;
            $model->name                = (Yii::$app->request->post('name')) ? Yii::$app->request->post('name') : '';
            $model->comment             = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
            $model->total               = (Yii::$app->request->post('total')) ? (int)Yii::$app->request->post('total') : 0;
            $model->date_create         = (Yii::$app->request->post('date-create')) ? (int)strtotime(Yii::$app->request->post('date-create')) : 0;
            $model->status              = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;
            $model->file                = '';
            Images::uploadFiles('file', 'files/', 1, $model->buying_id, $model->status((Yii::$app->request->post('status_id')) ? (int)Yii::$app->request->post('status_id') : 0));
            
            Logs::Create($model->buying_id, $model, 4, 2);
            if($model->update(false)){
                if($model->project_id > 0){
                    $project_find = Project::findOne(['project_id'=>(int)$model->project_id]);
                    if(isset($project_find)){
                        $project_find->buying_id = $model->buying_id;
                        Logs::Create($model->project_id, $project_find, 3, 2);
                        $project_find->update(false);
                    }
                }
            }
            return '1';
        } else {
            return $this->render('update', [
                'model' => $model,
                'managers' => $managers,
                'clients' => $clients,
                'projects' => $projects,
                'files_manager'=>$files_manager,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }
    }

    public function actionSearch(){
        BuyingList::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => BuyingList::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
        ]);

        return $this->renderPartial('/buying/table', [
            'dataProvider' => $dataProvider,
            'menu' => '',
        ]);
    }

    ///////

    public function actionSearchdelete(){
        if(Yii::$app->request->get('type')){
            $session = Yii::$app->session;
            if($session->has(Yii::$app->request->get('type'))){
                $session->remove(Yii::$app->request->get('type'));
            }
        }
        return $this->redirect('/buying/index?sort='.Yii::$app->request->get('sort'));
    }

    public function actionSearchdeleteall(){
        BuyingList::DeleteAllSession();
        return $this->redirect('/buying/index');
    }

    ////////


    public function actionDelete($id)
    {
        $model = $this->findModel((int)$id);
        if(isset($model) && Yii::$app->user->identity->role >= 9){
            $model->status = 5;
            Logs::Create($model->buying_id, $model, 4, 3);
            $model->update(false);
        }

        return $this->redirect(['index']);
    }

    public function actionCustomtable(){
        if(Yii::$app->request->get('page') && Yii::$app->request->get('type')){
            $attributes = substr(Yii::$app->request->get('type'), 0, -1);
            $attributes = explode(",", $attributes);
            if(isset($attributes)){
                CustomTable::deleteAll([
                    'manager_id'=>Yii::$app->user->identity->id,
                    'page'=>'buying',
                    'page_two'=>Yii::$app->request->get('page')
                ]);
                foreach ($attributes as $type) {
                    $customTable = new CustomTable;
                    $customTable->manager_id = Yii::$app->user->identity->id;
                    $customTable->page = 'buying';
                    $customTable->page_two = Yii::$app->request->get('page');
                    $customTable->type = $type;
                    $customTable->save();
                }
            }
        }
        return 1;
    }

    public function actionProjectselect($id = 0){
        $html = '<select class="select-buying selectpicker" name="manager" data-live-search="true"><option value="" selected class="placeholder-select">-Select-</option>';
        if($id > 0){
            $projects_client = Project::find()->where(['client_id'=>(int)$id])->all();
            if(Yii::$app->request->get('payment_id')){
                $payment = Payments::findOne(['payment_id'=>(int)Yii::$app->request->get('payment_id')]);
                if(isset($payment) && $payment->project_id){
                    $payment_project = $payment->project_id;
                }else{
                    $payment_project = 0;
                }
                
            }
            if(isset($projects_client)){
                foreach ($projects_client as $project) {
                    $selected = ($payment_project == $project->project_id) ? 'selected' : '';
                    $html .= '<option '.$selected.' value="'.$project->project_id.'">PRJ-'.$project->project_id.'</option>';
                }
            }
        }
        return $html.'<script type="text/javascript">$(".selectpicker").selectpicker();</script></select>';
    }

    public function actionFiledelete(){
        if(Yii::$app->request->get('buying') && Yii::$app->request->get('type')){
            $buying_id_GET = (int)Yii::$app->request->get('buying');
            $buying = BuyingList::findOne(['buying_id'=>$buying_id_GET]);
            if(isset($buying)){
                switch (Yii::$app->request->get('type')) {
                    case 'file':
                        $buying->file = '';
                        $buying->date_file = 0;
                        break;
                }
                $buying->update(false);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionFilesdelete(){
        if(Yii::$app->request->get('image_id')){
            $image_id_GET = (int)Yii::$app->request->get('image_id');
            $image = Images::findOne(['image_id'=>$image_id_GET]);
            if(isset($image)){
                $image->delete();
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    protected function findModel($id)
    {
        if (($model = BuyingList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
