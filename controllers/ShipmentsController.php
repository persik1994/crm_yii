<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Shipment;
use app\models\Manager;
use app\models\Country;
use app\models\ShipmentType;
use app\models\CargoStatus;
use app\models\Stock;
use app\models\Cargo;
use app\models\CustomTable;
use app\models\ShipmentCalculationColumn;
use app\models\ShipmentCalculation;
use app\models\Logs;

/**
 * ShipmentsController implements the CRUD actions for Shipment model.
 */
class ShipmentsController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shipment models.
     * @return mixed
     */
    public function actionIndex(){
        $session = Yii::$app->session;

        Shipment::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Shipment::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
            'sort'=> ['defaultOrder' => ['shipment_id'=>SORT_DESC]],
        ]);

        return $this->render('table', [
            'dataProvider' => $dataProvider,
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }

    public function actionFilter($id = 0)
    {
        if(!empty($id)){
            $session = Yii::$app->session;

            Shipment::SearchSession(1, (int)$id);
            
            $dataProvider = new ActiveDataProvider([
                'query' => Shipment::SearchQuery(1, (int)$id),
                'pagination' => [
                    'pageSize' => 1000, 
                ],
                'sort'=> ['defaultOrder' => ['shipment_id'=>SORT_DESC]],
            ]);

            return $this->render('table_filter', [
                'dataProvider' => $dataProvider,
                'menu' => Yii::$app->controller->renderPartial('menu'),
                'id' => (int)$id,
            ]);
        }
    }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) //ajax
    {
        $shipment_Obj = new Shipment;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = (int)$id;
        if(!empty($id)){
            $shipment = Shipment::findOne(['shipment_id'=>$id]);
            if(isset($shipment)){

                foreach ($shipment as $key => $value) {
                    if($value == "0"){
                        $shipment[$key] = '';
                    }
                }

                // $project->client_id = (!empty($project->client->client_id)) ? ((!empty($project->client->name)) ? '88-'.$project->client->client_id.' - '.$project->client->name : '88-'.$project->client->client_id) : '';
                $shipment->shipment_id      = (!empty($shipment->shipment_id)) ? $shipment->shipment_id : '';
                $shipment->name             = (!empty($shipment->name)) ? $shipment->name : '';
                $shipment->manager_id       = (!empty($shipment->manager->username)) ? $shipment->manager->username : '';
                $shipment->date_created     = (!empty($shipment->date_created)) ? date('d.m.Y', $shipment->date_created) : '';
                $shipment->date_received    = (!empty($shipment->date_received)) ? date('d.m.Y', $shipment->date_received) : '';
                $shipment->from_id          = (!empty($shipment->from->name)) ? $shipment->from->name : '';
                $shipment->to_id            = (!empty($shipment->to->name)) ? $shipment->to->name : '';
                $shipment->status_id        = (!empty($shipment->cargoStatus->name)) ? $shipment->cargoStatus->name : '';
                $shipment->type_id          = (!empty($shipment->type->name)) ? $shipment->type->name : '';
                $shipment->checkpoint       = (!empty($shipment->warehouse->name)) ? $shipment->warehouse->name : '';
                //////////
                $shipment->awb              = (!empty($shipment->awb) ? '<a href="'.$shipment->awb.'" target="_blank">View file</a>' : '');
                $shipment->invoice          = (!empty($shipment->invoice) ? '<a href="'.$shipment->invoice.'" target="_blank">View file</a>' : '');
                $shipment->list             = (!empty($shipment->list) ? '<a href="'.$shipment->list.'" target="_blank">View file</a>' : '');
                $shipment->other            = (!empty($shipment->other) ? '<a href="'.$shipment->other.'" target="_blank">View file</a>' : '');
            }
            return $shipment;
        }
    }

    public function actionCreate()
    {
        $model = new Shipment();
        $managers = Manager::find()->where('status = 1')->all();
        $countries = Country::find()->all();
        $shipment_types = ShipmentType::find()->where('status = 1')->all();
        $statuses = CargoStatus::find()->where('status = 1')->all();
        $stocks = Stock::find()->where('status = 1')->all();
        $calculation_columns = ShipmentCalculationColumn::AllColumns();

        if (Yii::$app->request->post()) {
            //////////////////////
            //if(Yii::$app->user->identity->role >= 9)
                $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            //else
            //    $model->manager_id = Yii::$app->user->identity->id;
            /////////////////////
            $model->from_id          = (Yii::$app->request->post('from')) ? (int)Yii::$app->request->post('from') : 0;
            $model->to_id            = (Yii::$app->request->post('to')) ? (int)Yii::$app->request->post('to') : 0;
            $model->type_id          = (Yii::$app->request->post('type')) ? (int)Yii::$app->request->post('type') : 0;
            $model->status_id        = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;
            $model->weight           = (Yii::$app->request->post('weight')) ? floatval(Yii::$app->request->post('weight')) : 0;
            $model->volume           = (Yii::$app->request->post('volume')) ? floatval(Yii::$app->request->post('volume')) : 0;
            $model->cartons          = (Yii::$app->request->post('cartons')) ? (int)Yii::$app->request->post('cartons') : 0;
            $model->checkpoint       = (Yii::$app->request->post('stock')) ? (int)Yii::$app->request->post('stock') : 0;
            $model->date_created     = (Yii::$app->request->post('created')) ? (int)strtotime(Yii::$app->request->post('created')) : 0;
            $model->date_received    = (Yii::$app->request->post('received')) ? (int)strtotime(Yii::$app->request->post('received')) : 0;
            $model->time             = (Yii::$app->request->post('time')) ? (int)Yii::$app->request->post('time') : 0;
            $model->link             = (Yii::$app->request->post('link')) ? Yii::$app->request->post('link') : '';
            $model->total_chargers   = (Yii::$app->request->post('total_chargers')) ? floatval(Yii::$app->request->post('total_chargers')) : 0;
            $model->name             = (Yii::$app->request->post('name')) ? Yii::$app->request->post('name') : '';
            $model->comment          = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
            $model->status           = 1;
            //////////
            $model->price            = 0;
            $model->price_customs    = 0;
            $model->profit           = 0;
            //////////
            $model->awb = Cargo::uploadFile('awb', 'files/');
            $model->invoice = Cargo::uploadFile('invoice', 'files/');
            $model->list = Cargo::uploadFile('list', 'files/');
            $model->other = Cargo::uploadFile('other', 'files/');
            if($model->save()){
                Logs::Create($model->shipment_id, $model, 5, 1);
                $all_calculation_columns = ShipmentCalculationColumn::find()->where('type != 0')->all();
                if(isset($all_calculation_columns)){
                    foreach ($all_calculation_columns as $column) {
                        if(Yii::$app->request->post('calculation-'.$column->column_id)){
                            $calculation = new ShipmentCalculation;
                            $calculation->column_id = $column->column_id;
                            $calculation->shipment_id = $model->shipment_id;
                            $calculation->amount = floatval(Yii::$app->request->post('calculation-'.$column->column_id));
                            if($calculation->save()){
                                Logs::Create($calculation->calculation_id, $calculation, 8, 1);
                            }
                        }
                    }
                }
                return '1';
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'managers' => $managers,
                'countries' => $countries,
                'shipment_types' => $shipment_types,
                'statuses' => $statuses,
                'stocks' => $stocks,
                'calculation_columns'=>$calculation_columns,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        $managers = Manager::find()->where('status = 1')->all();
        $countries = Country::find()->all();
        $shipment_types = ShipmentType::find()->where('status = 1')->all();
        $statuses = CargoStatus::find()->where('status = 1')->all();
        $stocks = Stock::find()->where('status = 1')->all();
        $calculation_columns = ShipmentCalculationColumn::AllColumns();
        ////лист карго вложеных
            $cargos = Cargo::find()->where(['shipment_id'=>$model->shipment_id])->all();
            $cargos_count = Cargo::find()->where(['shipment_id'=>$model->shipment_id])->count();
        /////

        $count = 0;

        if (Yii::$app->request->post() && isset($model)) {
            //////////////////////
            //if(Yii::$app->user->identity->role >= 9)
                $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            //else
                //$model->manager_id = Yii::$app->user->identity->id;
            /////////////////////
            $model->from_id          = (Yii::$app->request->post('from')) ? (int)Yii::$app->request->post('from') : 0;
            $model->to_id            = (Yii::$app->request->post('to')) ? (int)Yii::$app->request->post('to') : 0;
            $model->type_id          = (Yii::$app->request->post('type')) ? (int)Yii::$app->request->post('type') : 0;
            $model->status_id        = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;
            $model->weight           = (Yii::$app->request->post('weight')) ? floatval(Yii::$app->request->post('weight')) : 0;
            $model->volume           = (Yii::$app->request->post('volume')) ? floatval(Yii::$app->request->post('volume')) : 0;
            $model->cartons          = (Yii::$app->request->post('cartons')) ? (int)Yii::$app->request->post('cartons') : 0;
            $model->checkpoint       = (Yii::$app->request->post('stock')) ? (int)Yii::$app->request->post('stock') : 0;
            $model->date_created     = (Yii::$app->request->post('created')) ? (int)strtotime(Yii::$app->request->post('created')) : 0;
            $model->date_received    = (Yii::$app->request->post('received')) ? (int)strtotime(Yii::$app->request->post('received')) : 0;
            $model->time             = (Yii::$app->request->post('time')) ? (int)Yii::$app->request->post('time') : 0;
            $model->link             = (Yii::$app->request->post('link')) ? Yii::$app->request->post('link') : '';
            $model->total_chargers   = (Yii::$app->request->post('total_chargers')) ? floatval(Yii::$app->request->post('total_chargers')) : 0;
            $model->name             = (Yii::$app->request->post('name')) ? Yii::$app->request->post('name') : '';
            $model->comment          = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
            //$model->status           = 1;
            //////////
            $model->price            = 0;
            $model->price_customs    = 0;
            $model->profit           = 0;
            //////////

            $file1 = Cargo::uploadFile('awb', 'files/');
            if(!empty($file1))
                $model->awb = $file1;
            $file2 = Cargo::uploadFile('invoice', 'files/');
            if(!empty($file2))
                $model->invoice = $file2;
            $file3 = Cargo::uploadFile('list', 'files/');
            if(!empty($file3))
                $model->list = $file3;
            $file4 = Cargo::uploadFile('other', 'files/');
            if(!empty($file4))
                $model->other = $file4;
            
            Logs::Create($model->shipment_id, $model, 5, 2);
            if($model->update()){
                $count = 1;
            }
            $all_calculation_columns = ShipmentCalculationColumn::find()->where('type != 0')->all();
            if(isset($all_calculation_columns)){
                foreach ($all_calculation_columns as $column) {
                    if(Yii::$app->request->post('calculation-'.$column->column_id)){
                        $find_column = ShipmentCalculation::findOne(['shipment_id'=>$model->shipment_id,'column_id'=>$column->column_id]);
                        if(isset($find_column)){
                            $find_column->amount = floatval(Yii::$app->request->post('calculation-'.$column->column_id));
                            Logs::Create($calculation->calculation_id, $calculation, 8, 2);
                            $find_column->update();
                            $count = 1;
                        }else{
                            $calculation = new ShipmentCalculation;
                            $calculation->column_id = $column->column_id;
                            $calculation->shipment_id = $model->shipment_id;
                            $calculation->amount = floatval(Yii::$app->request->post('calculation-'.$column->column_id));
                            $calculation->save();
                            Logs::Create($calculation->calculation_id, $calculation, 8, 1);
                            $count = 1;
                        }
                    }
                }
            }
            return $count;
        } else {
            return $this->render('update', [
                'model' => $model,
                'managers' => $managers,
                'countries' => $countries,
                'shipment_types' => $shipment_types,
                'statuses' => $statuses,
                'stocks' => $stocks,
                'calculation_columns'=>$calculation_columns,
                'cargos' => $cargos,
                'cargos_count' => $cargos_count,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }
    }

    ///////

    public function actionSearch(){
        Shipment::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Shipment::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
            'sort'=> ['defaultOrder' => ['shipment_id'=>SORT_DESC]],
        ]);

        return $this->renderPartial('/shipments/table', [
            'dataProvider' => $dataProvider,
            'menu' => '',
        ]);
    }

    public function actionSearchfilter($id){
        Shipment::SearchSession(1, (int)$id);
        
        $dataProvider = new ActiveDataProvider([
            'query' => Shipment::SearchQuery(1, (int)$id),
            'pagination' => [
                'pageSize' => 500, 
            ],
            'sort'=> ['defaultOrder' => ['shipment_id'=>SORT_DESC]],
        ]);

        return $this->renderPartial('table_filter', [
            'dataProvider' => $dataProvider,
            'menu' => '',
            'id' => (int)$id,
        ]);
    }

    public function actionSearchdelete(){
        if(Yii::$app->request->get('type')){
            $session = Yii::$app->session;
            if($session->has(Yii::$app->request->get('type'))){
                $session->remove(Yii::$app->request->get('type'));
            }
        }
        return $this->redirect('/shipments/index?sort='.Yii::$app->request->get('sort'));
    }

    public function actionSearchdeleteall(){
        Shipment::DeleteAllSession();
        return $this->redirect('/shipments/index');
    }

    ////////
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel((int)$id);
        if(isset($model) && Yii::$app->user->identity->role >= 9){
            $model->status = 0;
            Logs::Create($model->shipment_id, $model, 5, 3);
            $model->update(false);
        }

        return $this->redirect(['index']);
    }

    public function actionCustomtable(){
        if(Yii::$app->request->get('page') && Yii::$app->request->get('type')){
            $attributes = substr(Yii::$app->request->get('type'), 0, -1);
            $attributes = explode(",", $attributes);
            if(isset($attributes)){
                CustomTable::deleteAll([
                    'manager_id'=>Yii::$app->user->identity->id,
                    'page'=>'shipment',
                    'page_two'=>Yii::$app->request->get('page')
                ]);
                foreach ($attributes as $type) {
                    $customTable = new CustomTable;
                    $customTable->manager_id = Yii::$app->user->identity->id;
                    $customTable->page = 'shipment';
                    $customTable->page_two = Yii::$app->request->get('page');
                    $customTable->type = $type;
                    $customTable->save();
                }
            }
        }
        return 1;
    }

    public function actionWhatmanager($id){
        if (!empty($id)) {
            $model = $this->findModel((int)$id);
            if(isset($model->manager_id)){
                $manager = Manager::findOne(['manager_id'=>$model->manager_id]);
                if(isset($manager)){
                    return $manager->username;
                }
            }
        }
        return '';
    }

    public function actionFiledelete(){
        if(Yii::$app->request->get('shipment') && Yii::$app->request->get('type')){
            $shipment_id_GET = (int)Yii::$app->request->get('shipment');
            $shipment = Shipment::findOne(['shipment_id'=>$shipment_id_GET]);
            if(isset($shipment)){
                switch (Yii::$app->request->get('type')) {
                    case 'awb':
                        $shipment->awb = '';
                        break;
                    case 'invoice':
                        $shipment->invoice = '';
                        break;
                    case 'list':
                        $shipment->list = '';
                        break;
                    case 'other':
                        $shipment->other = '';
                        break;
                }
                $shipment->update(false);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionCalculationcolumns($id = 0){
        if($id > 0){
            if(Yii::$app->request->get('shipment_id')){
                $shipment_id = (int)Yii::$app->request->get('shipment_id');
            }else{
                $shipment_id = 0;
            }

            return ShipmentCalculationColumn::ColumnsType((int)$id, $shipment_id);
        }
        return '';
    }

    /**
     * Finds the Shipment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shipment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shipment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
