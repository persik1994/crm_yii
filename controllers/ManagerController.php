<?php

namespace app\controllers;

use Yii;

use yii\helpers\Html;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\Login;
use app\models\Telegram;
use app\models\Manager;
use app\models\Cargo;
use app\models\Client;
use app\models\Plan;
use app\models\PlanMovement;
use app\models\Project;
use app\models\NotificationOption;
use app\models\CurrencyCash;

class ManagerController extends Controller
{
    public $enableCsrfValidation = false;
    //public $enableCsrfValidation = false;
    
    //public function behaviors()
    //{
    //    return [
    //        'access' => [
    //            'class' => AccessControl::className(),
    //            'only' => ['logout'],
    //            'rules' => [
    //                [
    //                    'actions' => ['logout'],
    //                    'allow' => true,
    //                    'roles' => ['@'],
    //                ],
    //            ],
    //        ],
    //        'verbs' => [
    //            'class' => VerbFilter::className(),
    //            'actions' => [
    //                'logout' => ['post'],
    //            ],
    //        ],
    //    ];
    //}

    public function actions()
    {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionEdit(){
        $error = '';
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        if(Yii::$app->request->post('current_pass') || Yii::$app->request->post('new_pass') || Yii::$app->request->post('repeat_pass')){
            if(Yii::$app->request->post('current_pass') && Yii::$app->request->post('new_pass') && Yii::$app->request->post('repeat_pass')){
                $user_update = Manager::findOne(Yii::$app->user->identity->id);
                if(sha1(Yii::$app->request->post('current_pass')) == $user_update->password){
                    if(Yii::$app->request->post('new_pass') == Yii::$app->request->post('repeat_pass')){
                        $user_update->password = sha1(Yii::$app->request->post('new_pass'));
                        $user_update->update(false);
                    }
                    else{
                        $error = "<script>alert('Пароли не совпадают!')</script>";
                    }
                }
                else{
                    $error = "<script>alert('Текущий пароль не совпадает!')</script>";
                }
            }
            else {
                $error = "<script>alert('Вы заполнили НЕ ВСЕ поля, для смены пароля!')</script>";
            }
        }

        else if(Yii::$app->request->post('phone') || Yii::$app->request->post('email') || Yii::$app->request->post('name')){
            $user_update = Manager::findOne(Yii::$app->user->identity->id);
            if(Yii::$app->request->post('name'))
                $user_update->name = Yii::$app->request->post('name');
            if(Yii::$app->request->post('phone'))
                $user_update->phone = (int)Yii::$app->request->post('phone');
            if(Yii::$app->request->post('phone-code'))
                $user_update->phone = Yii::$app->request->post('phone-code').$user_update->phone;
            if(Yii::$app->request->post('email'))
                $user_update->email = Yii::$app->request->post('email');
            if(Yii::$app->request->post('viber'))
                $user_update->viber = Yii::$app->request->post('viber');
            if(Yii::$app->request->post('telegram'))
                $user_update->telegram = Yii::$app->request->post('telegram');

            /////notification_option/////
            $notification_option = NotificationOption::findOne(['manager_id'=>$user_update->manager_id]);
            if(isset($notification_option)){
                if(Yii::$app->request->post('notification_telegram'))
                    $notification_option->telegram = Yii::$app->request->post('notification_telegram');
                else
                    $notification_option->telegram = 0;
                if(Yii::$app->request->post('notification_email'))
                    $notification_option->email = Yii::$app->request->post('notification_email');
                else
                    $notification_option->email = 0;
                if(Yii::$app->request->post('notification_sms'))
                    $notification_option->sms = Yii::$app->request->post('notification_sms');
                else
                    $notification_option->sms = 0;
                $notification_option->update();
            }else{
                $notification_option = new NotificationOption;
                $notification_option->manager_id = $user_update->manager_id;
                if(Yii::$app->request->post('notification_telegram'))
                    $notification_option->telegram = Yii::$app->request->post('notification_telegram');
                else
                    $notification_option->telegram = 0;
                if(Yii::$app->request->post('notification_email'))
                    $notification_option->email = Yii::$app->request->post('notification_email');
                else
                    $notification_option->email = 0;
                if(Yii::$app->request->post('notification_sms'))
                    $notification_option->sms = Yii::$app->request->post('notification_sms');
                else
                    $notification_option->sms = 0;
                $notification_option->save();
            }
            //////////////

            ////avatar////
            $avatar = Cargo::uploadFile('avatar-file', 'image/');
            if(!empty($avatar)){
                $user_update->avatar = $avatar;
            }
            ///////////
            $user_update->update(false);
            $this->refresh();
        }

        $user = Manager::findOne(Yii::$app->user->identity->id);
        $notification_option = NotificationOption::findOne(['manager_id'=>Yii::$app->user->identity->id]);
        return $this->render('edit', ['user'=>$user, 'notification_option'=>$notification_option, 'error' => $error]);
    }

    public function actionManageradmin($id = 0){

        if($id > 0){
            $now_date = strtotime(date('Y-m-2', (int)$id));
        }else{
            $now_date = strtotime(date('Y-m-2', time()));
        }

        if(!empty($now_date)){
            $plans = Plan::find()->where(['date'=>$now_date])->andFilterWhere(['!=', 'manager_id', 1])->andFilterWhere(['!=', 'manager_id', 2])->all();
            $currency = CurrencyCash::findOne(['сurrency_id'=>3]);
            if(!isset($plans[0])){
                $plans = '';
            }else{
                foreach ($plans as $plan) {
                    $amount_all = 0;
                    $airKg_all = 0;
                    $profit_all = 0;
                    ///////////
                    $projects = Project::find()->where(['manager_id'=>$plan->manager_id,'status'=>1])
                    ->andFilterWhere(['>', 'date_close', strtotime(date('Y-m-1 00:00', (int)$now_date))])
                    ->andFilterWhere(['<', 'date_close', strtotime(date('Y-m-t 23:59', (int)$now_date))])
                    ->all();
                    foreach ($projects as $project) {
                        $amount_all += $project->total;
                        $profit_all += $project->profit;
                    }
                    /////////
                    $cargos = Cargo::find()->where(['manager_id'=>$plan->manager_id])
                    ->andFilterWhere(['>', 'date_received', strtotime(date('Y-m-1 00:00', (int)$now_date))])
                    ->andFilterWhere(['<', 'date_received', strtotime(date('Y-m-t 23:59', (int)$now_date))])
                    ->andFilterWhere(['or',
                        ['=','shipment_type_id',7],
                        ['=','shipment_type_id',4]])
                    ->all();
                    foreach ($cargos as $cargo) {
                        $airKg_all += $cargo->weight;
                    }
                    //////////
                    $plan->amount_money = floatval($amount_all);
                    $plan->fact_air = floatval($airKg_all);
                    $plan->profit = $profit_all;
                    $plan->total = ($plan->fix_salary / $currency->currency) + $plan->bonus - $plan->fines + (($plan->profit/100)*$plan->bonus_percent);
                    $plan->update();
                }
                $plans = Plan::find()->where(['date'=>$now_date])->andFilterWhere(['!=', 'manager_id', 1])->andFilterWhere(['!=', 'manager_id', 2])->all();
            }
        }
        return $this->render('admin_sales', ['plans'=>$plans, 'now_date'=>$now_date]);
    }

    public function actionCreateplans($id = 0){
        if($id > 0 && Yii::$app->user->identity->role >= 10){
            $managers = Manager::find()->where(['role'=>8])
            ->andFilterWhere(['!=', 'manager_id', 45])
            ->andFilterWhere(['!=', 'manager_id', 46])
            ->andFilterWhere(['!=', 'manager_id', 41])
            ->andFilterWhere(['!=', 'manager_id', 24])
            ->orFilterWhere(['=', 'manager_id', 34])
            ->all();
            foreach ($managers as $manager) {
                $plan = new Plan;
                $plan->manager_id = $manager->manager_id;
                $plan->date = (int)$id;
                $plan->plan_money = 0;
                $plan->amount_money = 0;
                $plan->plan_air = 0;
                $plan->fact_air = 0;
                $plan->fix_salary = 0;
                $plan->bonus = 0;
                $plan->bonus_percent = 0;
                $plan->fines = 0;
                $plan->profit = 0;
                $plan->total = 0;
                $plan->payment_id = 0;
                $plan->status = 0;
                $plan->save();
            }
        }
        else{
            return 'Error access!';
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionEditplanmanager($id = 0){
        if($id > 0 && (Yii::$app->user->identity->role >= 10 || Yii::$app->user->identity->id == 34)){
            $plan = Plan::findOne(['plan_id'=>(int)$id]);
            if(isset($plan) && Yii::$app->request->post('now_date')){
                if(Yii::$app->request->post('plan_money'))
                    $plan->plan_money = floatval(Yii::$app->request->post('plan_money'));
                if(Yii::$app->request->post('plan_air'))
                    $plan->plan_air = floatval(Yii::$app->request->post('plan_air'));
                if(Yii::$app->request->post('fix_salary'))
                    $plan->fix_salary = floatval(Yii::$app->request->post('fix_salary'));
                // if(Yii::$app->request->post('bonus'))
                //     $plan->bonus = floatval(Yii::$app->request->post('bonus'));
                if(Yii::$app->request->post('bonus_percent'))
                    $plan->bonus_percent = floatval(Yii::$app->request->post('bonus_percent'));
                // if(Yii::$app->request->post('fines'))
                //     $plan->fines = floatval(Yii::$app->request->post('fines'));
                $plan->update();
                return $this->redirect(['manager/manageradmin/'.Yii::$app->request->post('now_date')]);
            }else if(isset($plan)){
                $bonuses = PlanMovement::find()->where(['type'=>1, 'plan_id'=>$plan->plan_id, 'date'=>$plan->date])->all();
                $fines = PlanMovement::find()->where(['type'=>2, 'plan_id'=>$plan->plan_id, 'date'=>$plan->date])->all();
                $currency = CurrencyCash::findOne(['сurrency_id'=>3]);
                $salary_all = PlanMovement::find()->where(['type'=>3, 'plan_id'=>$plan->plan_id, 'date'=>$plan->date])->all();
                return $this->render('edit_plan', ['plan'=>$plan, 'bonuses'=>$bonuses, 'fines'=>$fines, 'currency'=>$currency->currency,'salary_all'=>$salary_all]);
            }else{
                return 'Error access!';
            }
        }
        else{
            return 'Error access!';
        }
    }

    public function actionCabinet($id = 0){
        if($id > 0){
            $now_date = strtotime(date('Y-m-2', (int)$id));
        }else{
            $now_date = strtotime(date('Y-m-2', time()));
        }

        if(!empty($now_date)){
            $user = Manager::findOne(Yii::$app->user->identity->id);
            $currency = CurrencyCash::findOne(['сurrency_id'=>3]);
            ///////
            $plan = Plan::findOne(['manager_id'=>$user->manager_id, 'date'=>$now_date]);
            $amount_all = 0;
            $airKg_all = 0;
            $profit_all = 0;
            ///////////
            if(isset($plan)){
                $projects = Project::find()->where(['manager_id'=>$plan->manager_id,'status'=>1])
                ->andFilterWhere(['>', 'date_close', strtotime(date('Y-m-1 00:00', (int)$now_date))])
                ->andFilterWhere(['<', 'date_close', strtotime(date('Y-m-t 23:59', (int)$now_date))])
                ->all();
                foreach ($projects as $project) {
                    $amount_all += $project->total;
                    $profit_all += $project->profit;
                }
                /////////
                $cargos = Cargo::find()->where(['manager_id'=>$plan->manager_id])
                ->andFilterWhere(['>', 'date_received', strtotime(date('Y-m-1 00:00', (int)$now_date))])
                ->andFilterWhere(['<', 'date_received', strtotime(date('Y-m-t 23:59', (int)$now_date))])
                ->andFilterWhere(['or',
                    ['=','shipment_type_id',7],
                    ['=','shipment_type_id',4]])
                ->all();
                foreach ($cargos as $cargo) {
                    $airKg_all += $cargo->weight;
                }
                //////////
                $plan->amount_money = floatval($amount_all);
                $plan->fact_air = floatval($airKg_all);
                $plan->profit = $profit_all;
                $plan->total = ($plan->fix_salary / $currency->currency) + $plan->bonus - $plan->fines + (($plan->profit/100)*$plan->bonus_percent);
                $plan->update();
                ///////
                $plan = Plan::findOne(['manager_id'=>$user->manager_id, 'date'=>$now_date]);
                $payed = 0;
                $plan_movements = PlanMovement::find()->where(['plan_id'=>$plan->plan_id,'type'=>3])->all();
                foreach ($plan_movements as $plan_movements_one) {
                    $payed += $plan_movements_one->amount;
                }
                ///////
                if(isset($plan)){
                    return $this->render('cabinet', [
                        'user'=>$user, 
                        'plan'=>$plan, 
                        'payed'=>$payed,
                        'now_date'=>$now_date
                    ]);
                }
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionTelegramread(){
        //set_time_limit(0);
        // Получаем запрос от Telegram 
        $content = file_get_contents("php://input");
        $update = json_decode($content, TRUE);
        $message = $update["message"];
        // Получаем внутренний номер чата Telegram и команду, введённую пользователем в   чате 
        $chatId = $message["chat"]["id"];
        $text = $message["text"];
        // Пример обработки команды /start
        if ($text == '/start') {
            $manager = Manager::findOne(['telegram'=>$message["chat"]["username"]]);
            if(isset($manager)){
                $notification_option = NotificationOption::findOne(['manager_id'=>$manager->manager_id]);
                if(isset($notification_option) && $notification_option->telegram_token == $message["chat"]["id"]){
                    $message = 'Здравствуйте '.$message["chat"]["first_name"].'. Вы уже зарегистрированы и получаете уведомления из CRM';
                }
                else if(isset($notification_option) && $notification_option->telegram_token != $message["chat"]["id"]){
                    $notification_option->telegram = 1;
                    $notification_option->telegram_token = $message["chat"]["id"];
                    $notification_option->update();
                    $message = 'Здравствуйте '.$message["chat"]["first_name"].', спасибо за регистрацию, теперь Вы будете получать уведомления из CRM';
                }
                else{
                    $new_option = new NotificationOption;
                    $new_option->manager_id = $manager->manager_id;
                    $new_option->telegram = 1;
                    $new_option->telegram_token = $message["chat"]["id"];
                    $new_option->save();
                    $message = 'Здравствуйте '.$message["chat"]["first_name"].', спасибо за регистрацию, теперь Вы будете получать уведомления из CRM';
                }
            }
            else{
                $message = 'Ошибка! Ваш ник не указан в Edit Profile в CRM. Пожалуйста ужажите на "Profile settings" в поле "Telegram" ник '.$message["chat"]["username"].', сохраните и нажмите повторно /start';
            }
        }
        else if ($text != '') {
           $message = 'Простите, этот бот может только присылать Вам уведомления! Если хотите зарегистрироваться нажмите /start';
        }
        // Отправляем сформированное сообщение обратно в Telegram пользователю   
        if(!empty($chatId) && !empty($message)){
           \Yii::$app->bot->sendMessage($chatId, $message);   
        }
    }

    public function actionTelegramnewclient(){
        if(isset($_GET['token']) && $_GET['token'] == 489237432012899120401291742721411){
            if(isset($_GET['text'])){
                $notification_option = NotificationOption::findOne(['manager_id'=>51]); //office manager tatiana
                if(!empty($notification_option->telegram_token)){
                    \Yii::$app->bot->sendMessage($notification_option->telegram_token, $_GET['text']);
                    return 1;
                }
            }
        }
        return 0;
    }

    public function actionTelegrambotsend(){
        if(isset($_GET['token']) && $_GET['token'] == 4892374320128991204012917427214){
            if(isset($_GET['id']) && isset($_GET['text'])){
                \Yii::$app->bot->sendMessage($_GET['id'], $_GET['text']);
                return 1;
            }
        }
        return 0;
    }

    public function actionAddplanmovement($id = 0){
        if($id > 0){
            $plan = Plan::findOne(['plan_id'=>(int)$id]);
            $currency = CurrencyCash::findOne(['сurrency_id'=>3]);
            if(isset($plan)){
                $plan_movement = new PlanMovement;
                $plan_movement->plan_id = $plan->plan_id;
                $plan_movement->type = (int)Yii::$app->request->post('type');
                $plan_movement->date = $plan->date;
                $plan_movement->amount = floatval(Yii::$app->request->post('amount'));
                $plan_movement->description = Yii::$app->request->post('description');
                if($plan_movement->save()){
                    $plan->bonus = 0;
                    $plan_movements = PlanMovement::find()->where(['plan_id'=>$plan->plan_id,'type'=>1])->all();
                    foreach ($plan_movements as $plan_movements_one) {
                        $plan->bonus += $plan_movements_one->amount;
                    }
                    /////
                    $plan->fines = 0;
                    $plan_movements = PlanMovement::find()->where(['plan_id'=>$plan->plan_id,'type'=>2])->all();
                    foreach ($plan_movements as $plan_movements_one) {
                        $plan->fines += $plan_movements_one->amount;
                    }
                    $plan->total = ($plan->fix_salary / $currency->currency) + $plan->bonus - $plan->fines + (($plan->profit/100)*$plan->bonus_percent);
                    $plan->save();
                    return 'create';
                }
            }
        }
        return 'Add error!';
    }
}
