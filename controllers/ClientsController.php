<?php

namespace app\controllers;

use Yii;
use app\models\Client;
use app\models\Cargo;
use app\models\Manager;
use app\models\ClientSource;
use app\models\ClientStatus;
use app\models\CourierService;
use app\models\ClientDelivery;
use app\models\ClientMessenger;
use app\models\ClientRoute;
use app\models\ClientTariff;
use app\models\Payments;
use app\models\Task;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\CustomTable;
use app\models\Locations;
use app\models\Logs;

/**
 * ClientsController implements the CRUD actions for Client model.
 */
class ClientsController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Client models.
     * @return mixed
     */

    public function actionIndex(){
        $session = Yii::$app->session;

        Client::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Client::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
            'sort'=> ['defaultOrder' => ['client_id'=>SORT_DESC]],
        ]);

        return $this->render('table', [
            'dataProvider' => $dataProvider,
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }

    public function actionActive()
    {
        Client::DeleteAllSession();
        $session = Yii::$app->session;
        $session['client_status'] = 1;
        return $this->redirect('/clients/index');
    }

    public function actionLeads()
    {
        Client::DeleteAllSession();
        $session = Yii::$app->session;
        $session['client_status'] = 2;
        return $this->redirect('/clients/index');
    }

    public function actionArchive()
    {
        Client::DeleteAllSession();
        $session = Yii::$app->session;
        $session['client_status_id'] = 5;
        return $this->redirect('/clients/index');
    }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) //ajax
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = (int)$id;
        if(!empty($id)){
            $client = Client::findOne(['client_id'=>$id]);
            if(isset($client)){
                $client->password = ''; 

                foreach ($client as $key => $value) {
                    if($value == "0"){
                        $client[$key] = '';
                    }
                }

                if(!empty($client->manager_id))
                    $client->manager_id = $client->manager->username;
                else
                    '';

                if(!empty($client->source->name))
                    $client->source_id = $client->source->name;
                else
                    '';

                if(!empty($client->activation))
                    $client->activation = 'Yes';
                else
                    $client->activation = 'No';

                if(empty($client->balance))
                    $client->balance = '0.00';

                if(empty($client->balance_total))
                    $client->balance_total = '0.00';

                //////////підрахунок всії ваги карго/////////
                $client->allWeight = 0;
                $allCargosClient = Cargo::find()->where(['client_id'=>$client->client_id])->all();
                foreach ($allCargosClient as $cargo) {
                    $client->allWeight += $cargo->weight;
                }
                ///////

                $client->phone = (!empty($client->phone_code)) ? '+'.$client->phone_code.' '.$client->phone : $client->phone;

                $client->date_add = (!empty($client->date_add)) ? date('d.m.Y', $client->date_add) : '';

                ///////
                $transfers = Payments::find()
                    ->where(['client_id'=>(int)$id, 'status'=>1])
                    ->andWhere('(payment_type = 2 AND (out_source_id = 4 OR out_source_id = 0)) OR payment_type = 1')
                    ->orderBy(['payment_id'=>SORT_DESC])
                    ->all();

                foreach ($transfers as $transfer) {
                    $transfer->date_payment = (!empty($transfer->date_payment)) ? date('d.m.Y', $transfer->date_payment) : '';
                    $transfer->project_id = (!empty($transfer->project_id)) ? '<a target="_blank" href="/projects/edit/'.$transfer->project_id.'">PRJ-'.$transfer->project_id.'</a>' : '';
                    $transfer->cash_usd = ($transfer->payment_type == 1) ? '+'.$transfer->cash_usd : '-'.$transfer->cash_usd;
                    if(Yii::$app->user->identity->role < 10){
                        $transfer->balance = '';
                    }
                }

                return ['client'=>$client,'weight'=>$client->allWeight, 'transfers'=>$transfers];
            }
        }
    }

    public function actionViewbalance($id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = (int)$id;
        if(!empty($id)){
            $client = Client::findOne(['client_id'=>$id]);
            if(isset($client)){
                $client->password = ''; 

                foreach ($client as $key => $value) {
                    if($value == "0"){
                        $client[$key] = '';
                    }
                }

                if(!empty($client->manager_id))
                    $client->manager_id = $client->manager->username;
                else
                    '';

                if(empty($client->balance))
                    $client->balance = '0.00';

                if(empty($client->balance_total))
                    $client->balance_total = '0.00';

                //////////підрахунок всії ваги карго/////////
                $client->allWeight = 0;
                $allCargosClient = Cargo::find()->where(['client_id'=>$client->client_id])->all();
                foreach ($allCargosClient as $cargo) {
                    $client->allWeight += $cargo->weight;
                }
                ///////

                $client->phone = (!empty($client->phone_code)) ? '+'.$client->phone_code.' '.$client->phone : $client->phone;

                $client->date_add = (!empty($client->date_add)) ? date('d.m.Y', $client->date_add) : '';


                $transfers = Payments::find()
                    ->where(['client_id'=>(int)$id, 'status'=>1])
                    ->andWhere('(payment_type = 2 AND (out_source_id = 4 OR out_source_id = 0)) OR payment_type = 1')
                    ->orderBy(['payment_id'=>SORT_DESC])
                    ->all();

                foreach ($transfers as $transfer) {
                    $transfer->date_payment = (!empty($transfer->date_payment)) ? date('d.m.Y', $transfer->date_payment) : '';
                    $transfer->project_id = (!empty($transfer->project_id)) ? '<a target="_blank" href="/projects/edit/'.$transfer->project_id.'">PRJ-'.$transfer->project_id.'</a>' : '';
                    $transfer->cash_usd = ($transfer->payment_type == 1) ? '+'.$transfer->cash_usd : '-'.$transfer->cash_usd;
                    if(Yii::$app->user->identity->role < 10){
                        $transfer->balance = '';
                    }
                }

                return ['client'=>$client,'weight'=>$client->allWeight, 'transfers'=>$transfers];
            }
        }
    }

    public function actionCreate()
    {
        $model = new Client();
        $managers = Manager::find()->where('status = 1')->all();
        $sources = ClientSource::find()->where('status = 1')->all();
        $statuses = ClientStatus::find()->where('status = 1')->all();
        $deliveries = CourierService::find()->where('status = 1')->all();
        $states = Locations::find()->where(['type'=>'область','status'=>'active'])->all();

        if (Yii::$app->request->post()) {
            $model->password = '';
            //////////////////////
            if(Yii::$app->user->identity->role >= 9 || Yii::$app->user->identity->role == 2)
                $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            else
                $model->manager_id = Yii::$app->user->identity->id;
            /////////////////////
            $model->date_add = (Yii::$app->request->post('date_add')) ? strtotime(Yii::$app->request->post('date_add')) : 0;
            $model->source_id = (Yii::$app->request->post('source')) ? (int)Yii::$app->request->post('source') : 0;
            $model->status_id = (Yii::$app->request->post('status_id')) ? (int)Yii::$app->request->post('status_id') : 0;
            $model->activation = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;
            $model->name = (Yii::$app->request->post('name')) ? Yii::$app->request->post('name') : '';
            $model->phone_code = (Yii::$app->request->post('phone_code')) ? (int)Yii::$app->request->post('phone_code') : 0;
            $model->phone = (Yii::$app->request->post('phone')) ? Yii::$app->request->post('phone') : '';
            $model->email = (Yii::$app->request->post('email')) ? Yii::$app->request->post('email') : '';
            $model->comment = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
            $model->date_next_connect = (Yii::$app->request->post('date_next')) ? strtotime(Yii::$app->request->post('date_next')) : 0;
            $model->status = 1;
            $model->site = 2;
            $model->balance = 0;
            $model->balance_total = 0;
            if($model->save(false)){
                Logs::Create($model->client_id, $model, 1, 1);
                $model->password = sha1('88-'.$model->client_id);
                $model->update(false);
                $delivery = new ClientDelivery();
                $delivery->client_id = $model->client_id;
                $delivery->courier_id = (Yii::$app->request->post('delivery_courier')) ? (int)Yii::$app->request->post('delivery_courier') : 0;
                $delivery->receiver = (Yii::$app->request->post('delivery_receiver')) ? Yii::$app->request->post('delivery_receiver') : '';
                $delivery->mobile = (Yii::$app->request->post('delivery_phone')) ? Yii::$app->request->post('delivery_phone') : ''; 
                $delivery->state = (Yii::$app->request->post('delivery_state')) ? Yii::$app->request->post('delivery_state') : '';   
                $delivery->city = (Yii::$app->request->post('delivery_city')) ? Yii::$app->request->post('delivery_city') : '';   
                $delivery->address = (Yii::$app->request->post('delivery_address')) ? Yii::$app->request->post('delivery_address') : '';
                if($delivery->save()){
                }

                $messenger = new ClientMessenger();
                $messenger->client_id = $model->client_id;
                $messenger->messenger_id = (Yii::$app->request->post('client_messanger_type')) ? (int)Yii::$app->request->post('client_messanger_type') : 0;
                $messenger->number = (Yii::$app->request->post('client_messanger_number')) ? Yii::$app->request->post('client_messanger_number') : '';
                if($messenger->save()){
                }

                $route = new ClientRoute();
                $route->client_id = $model->client_id;
                $route->from_id = (Yii::$app->request->post('route_from')) ? (int)Yii::$app->request->post('route_from') : 0;
                $route->to_id = (Yii::$app->request->post('route_to')) ? (int)Yii::$app->request->post('route_to') : 0;
                if($route->save()){
                }

                $tariff = new ClientTariff();
                $tariff->client_id = $model->client_id;
                $tariff->type_cargo_id = (Yii::$app->request->post('pricing_type')) ? (int)Yii::$app->request->post('pricing_type') : 0;
                $tariff->tariff_id = (Yii::$app->request->post('pricing_measure')) ? (int)Yii::$app->request->post('pricing_measure') : 0;
                $tariff->number = (Yii::$app->request->post('pricing_tariff')) ? Yii::$app->request->post('pricing_tariff') : 0;
                if($tariff->save()){
                }

                // if(!empty(Yii::$app->request->post('comment')) && !empty(Yii::$app->request->post('date_next'))){
                //     $task = Task::findOne(['date_end'=>$model->date_next_connect, 'client_id'=>$model->client_id, 'manager_id'=>Yii::$app->user->identity->id]);
                //     if(isset($task)){
                //         $task->description = Yii::$app->request->post('comment');
                //         $task->update();
                //     }
                //     else{
                //         $task = new Task;
                //         $task->manager_id = Yii::$app->user->identity->id;
                //         $task->description = Yii::$app->request->post('comment');
                //         $task->date_create = time();
                //         $task->client_id = $model->client_id;
                //         $task->status = 0;
                //         $task->tag = '';
                //         $task->date_end = strtotime(Yii::$app->request->post('date_next'));
                //         $task->save();
                //     }
                // }
                return '1';
            }
            return 'error';
        } else {
            return $this->render('create', [
                'model' => $model,
                'managers' => $managers,
                'sources' => $sources,
                'statuses' => $statuses,
                'deliveries' => $deliveries,
                'states' => $states,
            ]);
        }
    }

    /**
     * Updates an existing Client model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionEdit($id)
    {
        $model = $this->findModel((int)$id);

        $managers = Manager::find()->where('status = 1')->all();
        $sources = ClientSource::find()->where('status = 1')->all();
        $statuses = ClientStatus::find()->where('status = 1')->all();
        $deliveries = CourierService::find()->where('status = 1')->all();
        $delivery_now = ClientDelivery::find()->where(['client_id'=>(int)$id])->one();
        /////
        $messenger = ClientMessenger::find()->where(['client_id'=>(int)$id])->one();
        $route = ClientRoute::find()->where(['client_id'=>(int)$id])->one();
        $tariff = ClientTariff::find()->where(['client_id'=>(int)$id])->one();
        $states = Locations::find()->where(['type'=>'область','status'=>'active'])->all();

        $transfers = Payments::find()
            ->where(['client_id'=>(int)$id, 'status'=>1])
            ->andWhere('(payment_type = 2 AND (out_source_id = 4 OR out_source_id = 0)) OR payment_type = 1')
            ->orderBy(['payment_id'=>SORT_DESC])
            ->all();

        if (Yii::$app->request->post() && isset($model)) {
            // if($model->password == ''){
            //    $model->password = sha1('88-'.$model->client_id);
            // }
            //////////////////////
            if(Yii::$app->user->identity->role >= 9 || Yii::$app->user->identity->role == 2)
                $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            else
                $model->manager_id = Yii::$app->user->identity->id;
            /////////////////////
            $model->date_add = (Yii::$app->request->post('date_add')) ? strtotime(Yii::$app->request->post('date_add')) : 0;
            $model->source_id = (Yii::$app->request->post('source')) ? (int)Yii::$app->request->post('source') : 0;
            $model->status_id = (Yii::$app->request->post('status_id')) ? (int)Yii::$app->request->post('status_id') : 0;
            $model->activation = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;
            $model->name = (Yii::$app->request->post('name')) ? Yii::$app->request->post('name') : '';
            $model->phone_code = (Yii::$app->request->post('phone_code')) ? (int)Yii::$app->request->post('phone_code') : 0;
            $model->phone = (Yii::$app->request->post('phone')) ? Yii::$app->request->post('phone') : '';
            $model->email = (Yii::$app->request->post('email')) ? Yii::$app->request->post('email') : '';
            $model->comment = (Yii::$app->request->post('comment')) ? Yii::$app->request->post('comment') : '';
            $model->date_next_connect = (Yii::$app->request->post('date_next')) ? strtotime(Yii::$app->request->post('date_next')) : 0;
            $model->status = 1;
            $model->site = 2;
            $model->balance = 0;
            $model->balance_total = 0;
            Logs::Create($model->client_id, $model, 1, 2);
            if($model->update(false)){
            }
            /////////////////
                $delivery = ClientDelivery::findOne(['client_id'=>$model->client_id]);
                if(!isset($delivery->delivery_id)){
                    $delivery = new ClientDelivery();
                }
                $delivery->client_id = $model->client_id;
                $delivery->courier_id = (Yii::$app->request->post('delivery_courier')) ? (int)Yii::$app->request->post('delivery_courier') : 0;
                $delivery->receiver = (Yii::$app->request->post('delivery_receiver')) ? Yii::$app->request->post('delivery_receiver') : '';
                $delivery->mobile = (Yii::$app->request->post('delivery_phone')) ? Yii::$app->request->post('delivery_phone') : '';  
                $delivery->state = (Yii::$app->request->post('delivery_state')) ? Yii::$app->request->post('delivery_state') : '';    
                $delivery->city = (Yii::$app->request->post('delivery_city')) ? Yii::$app->request->post('delivery_city') : '';   
                $delivery->address = (Yii::$app->request->post('delivery_address')) ? Yii::$app->request->post('delivery_address') : '';
                if(!isset($delivery->delivery_id)){
                    $delivery->save();
                }
                else{
                    $delivery->update();
                }
                //////////////////
                $messenger = ClientMessenger::findOne(['client_id'=>$model->client_id]);
                if(!isset($messenger->id)){
                    $messenger = new ClientMessenger();
                }
                $messenger->client_id = $model->client_id;
                $messenger->messenger_id = (Yii::$app->request->post('client_messanger_type')) ? (int)Yii::$app->request->post('client_messanger_type') : 0;
                $messenger->number = (Yii::$app->request->post('client_messanger_number')) ? Yii::$app->request->post('client_messanger_number') : '';
                if(!isset($messenger->id)){
                    $messenger->save();
                }
                else{
                    $messenger->update();
                }
                ///////////////////
                $route = ClientRoute::findOne(['client_id'=>$model->client_id]);
                if(!isset($route->id)){
                    $route = new ClientRoute();
                }
                $route->client_id = $model->client_id;
                $route->from_id = (Yii::$app->request->post('route_from')) ? (int)Yii::$app->request->post('route_from') : 0;
                $route->to_id = (Yii::$app->request->post('route_to')) ? (int)Yii::$app->request->post('route_to') : 0;
                if(!isset($route->id)){
                    $route->save();
                }
                else{
                    $route->update();
                }
                ////////////////
                $tariff = ClientTariff::findOne(['client_id'=>$model->client_id]);
                if(!isset($tariff->id)){
                    $tariff = new ClientTariff();
                }
                $tariff->client_id = $model->client_id;
                $tariff->type_cargo_id = (Yii::$app->request->post('pricing_type')) ? (int)Yii::$app->request->post('pricing_type') : 0;
                $tariff->tariff_id = (Yii::$app->request->post('pricing_measure')) ? (int)Yii::$app->request->post('pricing_measure') : 0;
                $tariff->number = (Yii::$app->request->post('pricing_tariff')) ? Yii::$app->request->post('pricing_tariff') : 0;
                if(!isset($tariff->id)){
                    $tariff->save();
                }
                else{
                    $tariff->update();
                }
                ///////////
                return '1';
        } else {
            //////////підрахунок всії ваги карго/////////
            $allWeight = 0;
            $allCargosClient = Cargo::find()->where(['client_id'=>$model->client_id])->all();
            foreach ($allCargosClient as $cargo) {
                $allWeight += $cargo->weight;
            }
            ///////
            return $this->render('update', [
                'model' => $model,
                'managers' => $managers,
                'sources' => $sources,
                'statuses' => $statuses,
                'deliveries' => $deliveries,
                'delivery_now' => $delivery_now,

                'messenger' => $messenger,
                'route' => $route,
                'tariff' => $tariff,
                'allWeight' => $allWeight,
                'transfers' => $transfers,
                'states' => $states,
            ]);
        }
    }

    public function actionTasknew($id = 0){
        if(!empty(Yii::$app->request->post('date')) && !empty($id)){
            $date_next_connect = (Yii::$app->request->post('date')) ? strtotime(Yii::$app->request->post('date')) : 0;
            if(!empty($date_next_connect)){
                $task = new Task;
                $task->manager_id = Yii::$app->user->identity->id;
                $task->description = (Yii::$app->request->post('description')) ? Yii::$app->request->post('description') : 'Обратный звонок клиенту';
                $task->date_create = time();
                $task->client_id = (int)$id;
                $task->status = 0;
                $task->tag = '';
                $task->date_end = $date_next_connect;
                if($task->save()){
                    $client = Client::findOne(['client_id'=>$task->client_id]);
                    if(isset($client)){
                        $client->date_next_connect = $date_next_connect;
                        $client->update(false);
                    }
                    return 1;
                }
            }
        }
    }

    public function actionSearch(){
        Client::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Client::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
        ]);

        return $this->renderPartial('/clients/table', [
            'dataProvider' => $dataProvider,
            'menu' => '',
        ]);
    }

    public function actionSearchbalances(){
        Client::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Client::SearchQuery(),
            'pagination' => [
                'pageSize' => 500, 
            ],
        ]);

        return $this->renderPartial('/payments/table_balances', [
            'dataProvider' => $dataProvider,
            'menu' => '',
        ]);
    }

    public function actionSearchdelete(){
        if(Yii::$app->request->get('type')){
            $session = Yii::$app->session;
            if($session->has(Yii::$app->request->get('type'))){
                $session->remove(Yii::$app->request->get('type'));
            }
        }
        //return $this->redirect('/clients/index?sort='.Yii::$app->request->get('sort'));
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSearchdeleteall(){
        Client::DeleteAllSession();
        return $this->redirect('/clients/index');
    }

    public function actionSearchbalancesdeleteall(){
        Client::DeleteAllSession();
        return $this->redirect('/payments/balances');
    }


    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel((int)$id);
        if(isset($model) && Yii::$app->user->identity->role >= 9){
            $model->status = 5;
            Logs::Create($model->client_id, $model, 1, 3);
            $model->update(false);
        }

        return $this->redirect(['index']);
    }

    public function actionCustomtable(){
        if(Yii::$app->request->get('page') && Yii::$app->request->get('type')){
            $attributes = substr(Yii::$app->request->get('type'), 0, -1);
            $attributes = explode(",", $attributes);
            if(isset($attributes)){
                CustomTable::deleteAll([
                    'manager_id'=>Yii::$app->user->identity->id,
                    'page'=>'client',
                    'page_two'=>Yii::$app->request->get('page')
                ]);
                foreach ($attributes as $type) {
                    $customTable = new CustomTable;
                    $customTable->manager_id = Yii::$app->user->identity->id;
                    $customTable->page = 'client';
                    $customTable->page_two = Yii::$app->request->get('page');
                    $customTable->type = $type;
                    $customTable->save();
                }
            }
        }
        return 1;
    }

    public function actionCity($id = 0){
        $html = '';
        if($id > 0){
            $cityAll = Locations::find()->where([
                'type'=>'місто',
                'status'=>'active',
                'parent_id'=>(int)$id
            ])->all();
            if(isset($cityAll)){
                foreach ($cityAll as $city) {
                    if(Yii::$app->request->post('city') && Yii::$app->request->post('city') == $city->id){
                        $html .= '<option value="'.$city->id.'" selected>'.$city->name.'</option>';
                    }else{
                        $html .= '<option value="'.$city->id.'">'.$city->name.'</option>';
                    }
                }
            }
        }
        return $html;
    }

    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
