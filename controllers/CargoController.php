<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

use arturoliveira\ExcelView;

use app\models\Cargo;
use app\models\CargoExel;
use app\models\Shipment;
use app\models\Country;
use app\models\Client;
use app\models\Manager;
use app\models\Project;
use app\models\Stock;
use app\models\ShipmentType;
use app\models\CargoStatus;
use app\models\CargoType;
use app\models\Qrcode;
use app\models\CustomTable;
use app\models\Logs;

/**
 * CargoController implements the CRUD actions for Cargo model.
 */
class CargoController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex(){
        if(Yii::$app->user->identity->id == 50){
            return $this->redirect(['/cargo/warehouse/45']);
        }
        $session = Yii::$app->session;

        Cargo::SearchSession();
        
        $dataProvider = new ActiveDataProvider([
            'query' => Cargo::SearchQuery(),
            //'pagination' => false,
            'pagination' => [
                'pageSize' => 100, 
            ],
            'sort'=> ['defaultOrder' => ['cargo_id'=>SORT_DESC]],
        ]);

        return $this->render('table', [
            'dataProvider' => $dataProvider,
            'FormEditMany' => $this->FormEditMany(),
            'menu' => Yii::$app->controller->renderPartial('menu'),
        ]);
    }

    public function actionIndexajax(){ //подгрузка ajax при прокрутке в окна таблицы
        if(Yii::$app->user->identity->id == 50){
            return '';
        }
            $session = Yii::$app->session;

            Cargo::SearchSession();
            
            $dataProvider = new ActiveDataProvider([
                'query' => Cargo::SearchQuery(0,0),
                //'pagination' => false,
                'pagination' => [
                    'pageSize' => 100, 
                ],
                'sort'=> ['defaultOrder' => ['cargo_id'=>SORT_DESC]],
            ]);

            $html = $this->renderPartial('table_ajax', [
                'dataProvider' => $dataProvider,
            ]);

            if(stristr($html, 'No results found.') !== FALSE || $dataProvider->getTotalCount() < 100) {
                return '';
            }

            return str_replace('</tbody></table></div>',
                '',
                str_replace(
                    '<div id="w0" class="grid-view"><table class="table table-striped table-bordered"><tbody>', 
                    '', 
                    $html
                )
            );
    }

    public function actionWarehouse($id = 0)
    {
        if(!empty($id)){
            $session = Yii::$app->session;

            Cargo::SearchSession(1, (int)$id);
            
            $dataProvider = new ActiveDataProvider([
                'query' => Cargo::SearchQuery(1, (int)$id),
                'pagination' => [
                    'pageSize' => 100, 
                ],
                'sort'=> ['defaultOrder' => ['cargo_id'=>SORT_DESC]],
            ]);

            return $this->render('table_warehouse', [
                'dataProvider' => $dataProvider,
                'FormEditMany' => $this->FormEditMany(),
                'menu' => Yii::$app->controller->renderPartial('menu'),
                'id' => (int)$id,
            ]);
        }
    }

    public function actionWarehouseajax($id = 0){ //подгрузка ajax при прокрутке в окна таблицы
        if(!empty($id)){
            $session = Yii::$app->session;

            Cargo::SearchSession(1, (int)$id);
            
            $dataProvider = new ActiveDataProvider([
                'query' => Cargo::SearchQuery(1, (int)$id),
                'pagination' => [
                    'pageSize' => 100, 
                ],
                'sort'=> ['defaultOrder' => ['cargo_id'=>SORT_DESC]],
            ]);

            $html = $this->renderPartial('table_warehouse_ajax', [
                'dataProvider' => $dataProvider,
                'id' => (int)$id,
            ]);

            if(stristr($html, 'No results found.') !== FALSE || $dataProvider->getTotalCount() < 100) {
                return '';
            }

            return str_replace('</tbody></table></div>',
                '',
                str_replace(
                    '<div id="w0" class="grid-view"><table class="table table-striped table-bordered"><tbody>', 
                    '', 
                    $html
                )
            );
        }
    }

    public function actionFilter($id = 0)
    {
        if(!empty($id)){
            $session = Yii::$app->session;

            Cargo::SearchSession(2, (int)$id);
            
            $dataProvider = new ActiveDataProvider([
                'query' => Cargo::SearchQuery(2, (int)$id),
                'pagination' => [
                    'pageSize' => 100, 
                ],
                'sort'=> ['defaultOrder' => ['cargo_id'=>SORT_DESC]],
            ]);

            return $this->render('table_filter', [
                'dataProvider' => $dataProvider,
                'FormEditMany' => $this->FormEditMany(),
                'menu' => Yii::$app->controller->renderPartial('menu'),
                'id' => (int)$id,
            ]);
        }
    }

    public function actionFilterajax($id = 0){ //подгрузка ajax при прокрутке в окна таблицы
        if(!empty($id)){
            $session = Yii::$app->session;

            Cargo::SearchSession(2, (int)$id);
            
            $dataProvider = new ActiveDataProvider([
                'query' => Cargo::SearchQuery(2, (int)$id),
                'pagination' => [
                    'pageSize' => 100, 
                ],
                'sort'=> ['defaultOrder' => ['cargo_id'=>SORT_DESC]],
            ]);

            $html = $this->renderPartial('table_filter_ajax', [
                'dataProvider' => $dataProvider,
                'id' => (int)$id,
            ]);

            if(stristr($html, 'No results found.') !== FALSE || $dataProvider->getTotalCount() < 100) {
                return '';
            }

            return str_replace('</tbody></table></div>',
                '',
                str_replace(
                    '<div id="w0" class="grid-view"><table class="table table-striped table-bordered"><tbody>', 
                    '', 
                    $html
                )
            );
        }
    }

    public function actionDelivered($id = 0)
    {
        if(!empty($id)){
            $session = Yii::$app->session;

            Cargo::SearchSession(3, (int)$id);
            
            $dataProvider = new ActiveDataProvider([
                'query' => Cargo::SearchQuery(3, (int)$id),
                'pagination' => [
                    'pageSize' => 100, 
                ],
                'sort'=> ['defaultOrder' => ['cargo_id'=>SORT_DESC]],
            ]);

            return $this->render('table_delivered', [
                'dataProvider' => $dataProvider,
                'FormEditMany' => $this->FormEditMany(),
                'menu' => Yii::$app->controller->renderPartial('menu'),
                'id' => (int)$id,
            ]);
        }
    }

    public function actionDeliveredajax($id = 0){ //подгрузка ajax при прокрутке в окна таблицы
        if(!empty($id)){
            $session = Yii::$app->session;

            Cargo::SearchSession(3, (int)$id);
            
            $dataProvider = new ActiveDataProvider([
                'query' => Cargo::SearchQuery(3, (int)$id),
                'pagination' => [
                    'pageSize' => 100, 
                ],
                'sort'=> ['defaultOrder' => ['cargo_id'=>SORT_DESC]],
            ]);

            $html = $this->renderPartial('table_delivered_ajax', [
                'dataProvider' => $dataProvider,
                'id' => (int)$id,
            ]);

            if(stristr($html, 'No results found.') !== FALSE || $dataProvider->getTotalCount() < 100) {
                return '';
            }

            return str_replace('</tbody></table></div>',
                '',
                str_replace(
                    '<div id="w0" class="grid-view"><table class="table table-striped table-bordered"><tbody>', 
                    '', 
                    $html
                )
            );
        }
    }

    
    public function actionView($id) //ajax
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = (int)$id;
        if(!empty($id)){
            $cargo = Cargo::findOne(['cargo_id'=>$id]);
            if(isset($cargo)){

                foreach ($cargo as $key => $value) {
                    if($value == "0"){
                        $cargo[$key] = '';
                    }
                }

                $cargo->manager_id = (!empty($cargo->manager_id)) ? $cargo->manager->username : '';
                $cargo->shipment_type_id = (!empty($cargo->shipment_type_id)) ? $cargo->shipmentType->name : '';
                $cargo->status_id = (!empty($cargo->status_id)) ? $cargo->cargoStatus->name : '';
                $cargo->from_id = (!empty($cargo->from_id)) ? $cargo->from->name : '';
                $cargo->to_id = (!empty($cargo->to_id)) ? $cargo->to->name : '';
                $cargo->warehouse_id = (!empty($cargo->warehouse_id)) ? $cargo->warehouse->name : '';
                $cargo->type_id = (!empty($cargo->type_id)) ? $cargo->type->name : '';
                $cargo->type_special_id = (!empty($cargo->type_special_id)) ? Cargo::special_type($cargo->type_special_id) : '';

                $cargo->date_add = (!empty($cargo->date_add)) ? date('d.m.Y', $cargo->date_add) : '';
                $cargo->date_received = (!empty($cargo->date_received)) ? date('d.m.Y', $cargo->date_received) : '';
                $cargo->date_given_to_client = (!empty($cargo->date_given_to_client)) ? date('d.m.Y', $cargo->date_given_to_client) : '';

                $cargo->weight = strval($cargo->weight);
                $cargo->volume = strval($cargo->volume);
                $cargo->volume_weight = strval($cargo->volume_weight);

                $cargo->invoice = (!empty($cargo->invoice) ? '<a href="'.$cargo->invoice.'" target="_blank">View invoice</a>' : '');
                $cargo->file = (!empty($cargo->file) ? '<a href="'.$cargo->file.'" target="_blank">View file</a>' : '');

                if(!empty($cargo->photos)){
                    $photos_all = explode(",", $cargo->photos);
                    $cargo->photos = '';
                    foreach ($photos_all as $photo) {
                        if(!empty($photo))
                            $cargo->photos .= '<a href="'.$photo.'" target="_blank" style="margin-right:5px;"><i class="fas fa-camera-retro"></i></a> ';
                    }
                }
                $cargo->photos = (!empty($cargo->photos) ? $cargo->photos : '');
            }
            return $cargo;
        }
    }
    
    public function actionCreate()
    {
        $model = new Cargo();
        $shipments = Shipment::find()->orderBy(['shipment_id' => SORT_DESC])->all();
        $countries = Country::find()->all();
        if(Yii::$app->user->identity->role == 4 || Yii::$app->user->identity->role == 5 || Yii::$app->user->identity->role >= 9)
            $clients = Client::find()->where(['status'=>1])->orderBy(['client_id' => SORT_DESC])->all();
        else
            $clients = Client::find()->where(['status'=>1, 'manager_id'=>Yii::$app->user->identity->id])->orderBy(['client_id' => SORT_DESC])->all();
        $managers = Manager::find()->where('status = 1')->all();
        $projects = Project::find()->where('status != 5')->all();
        $stocks = Stock::find()->where('status = 1')->all();
        // if(Yii::$app->user->identity->role == 4)
        //     $stocks = Stock::find()->where('status = 1 AND stock_id != 11')->all();

        $shipment_types = ShipmentType::find()->where('status = 1')->all();
        $statuses = CargoStatus::find()->where('status = 1')->all();
        $types = CargoType::find()->where('status = 1')->all();

        if (Yii::$app->request->post()) {
            $model->shipment_id = (Yii::$app->request->post('shipment')) ? (int)Yii::$app->request->post('shipment') : 0;
            $model->date_add = (Yii::$app->request->post('date_add')) ? (int)strtotime(Yii::$app->request->post('date_add')) : 0;
            $model->date_received = (Yii::$app->request->post('date_received')) ? (int)strtotime(Yii::$app->request->post('date_received')) : 0;
            $model->from_id = (Yii::$app->request->post('from')) ? (int)Yii::$app->request->post('from') : 0;
            $model->to_id = (Yii::$app->request->post('to')) ? (int)Yii::$app->request->post('to') : 0;
            $model->date_given_to_client = (Yii::$app->request->post('date_given')) ? (int)strtotime(Yii::$app->request->post('date_given')) : 0;
            $model->delivery_time = 0;
            $model->client_id = (Yii::$app->request->post('client')) ? (int)Yii::$app->request->post('client') : 0;
            $model->create_manager_id = Yii::$app->user->identity->id;
            //////////////////////
            if(!empty($model->client_id)){
                $findManagerToClient = Client::findOne(['client_id'=>$model->client_id]);
                if(!empty($findManagerToClient->manager_id))
                    $model->manager_id = $findManagerToClient->manager_id;
                else
                    $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            }
            else{
                $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            }
            /////////////////////
            $model->project_id = (Yii::$app->request->post('project')) ? (int)Yii::$app->request->post('project') : 0;
            $model->warehouse_id = (Yii::$app->request->post('stock')) ? (int)Yii::$app->request->post('stock') : 0;
            $model->shipment_type_id = (Yii::$app->request->post('shipment_type')) ? (int)Yii::$app->request->post('shipment_type') : 0;
            $model->status_id = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;
            $model->weight = (Yii::$app->request->post('weight')) ? floatval(Yii::$app->request->post('weight')) : 0;
            $model->volume = (Yii::$app->request->post('volume')) ? floatval(Yii::$app->request->post('volume')) : 0;
            $model->cartons = (Yii::$app->request->post('cartons')) ? (int)Yii::$app->request->post('cartons') : 0;
            $model->volume_weight = (Yii::$app->request->post('volume')) ? floatval(Yii::$app->request->post('volume')*167) : 0;
            $model->count_inside = (Yii::$app->request->post('inside')) ? (int)Yii::$app->request->post('inside') : 0;
            $model->local_track = (Yii::$app->request->post('local_tracking')) ? Yii::$app->request->post('local_tracking') : '';
            $model->local_delivery_price = (Yii::$app->request->post('local_price')) ? Yii::$app->request->post('local_price') : 0;
            $model->local_expenses = (Yii::$app->request->post('local_expenses')) ? Yii::$app->request->post('local_expenses') : '';
            $model->track = (Yii::$app->request->post('delivery_tracking')) ? Yii::$app->request->post('delivery_tracking') : '';
            $model->type_id = (Yii::$app->request->post('type')) ? (int)Yii::$app->request->post('type') : 0;
            $model->type_special_id = (Yii::$app->request->post('type_special')) ? (int)Yii::$app->request->post('type_special') : 0;
            $model->description = (Yii::$app->request->post('description')) ? Yii::$app->request->post('description') : '';
            $model->status = 1;
            $model->photos = Cargo::uploadFiles('photos', 'image/');
            $model->invoice = Cargo::uploadFile('invoice', 'files/');
            $model->file = Cargo::uploadFile('file', 'files/');
            $model->site = 2;
            if($model->save()){
                Logs::Create($model->cargo_id, $model, 2, 1);
                $this->generatorQrcode($model->cargo_id);
                if($model->project_id > 0){
                    Project::MathTotalPricing($model->project_id);
                }
                return '1';
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'shipments' => $shipments,
                'countries' => $countries,
                'clients' => $clients,
                'managers' => $managers,
                'projects' => $projects,
                'stocks' => $stocks,
                'shipment_types' => $shipment_types,
                'statuses' => $statuses,
                'types' => $types,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }
    }

    
    public function actionEdit($id)
    {
        $model = $this->findModel((int)$id);

        $shipments = Shipment::find()->all();
        $countries = Country::find()->all();
        // if(Yii::$app->user->identity->role == 4 || Yii::$app->user->identity->role == 5 || Yii::$app->user->identity->role >= 9)
        //     $clients = Client::find()->where(['status'=>1])->orderBy(['client_id' => SORT_DESC])->all();
        // else if($model->manager_id > 0 && ($model->manager->role == 4 || $model->manager->role == 5))
        //     $clients = Client::find()->where(['status'=>1])->orderBy(['client_id' => SORT_DESC])->all();
        // else if($model->manager_id > 0)
        //     $clients = Client::find()->where(['status'=>1, 'manager_id'=>$model->manager_id])->orderBy(['client_id' => SORT_DESC])->all();
        // else
        //     $clients = Client::find()->where(['status'=>1, 'manager_id'=>Yii::$app->user->identity->id])->orderBy(['client_id' => SORT_DESC])->all();
        $clients = Client::find()->where(['status'=>1])->orderBy(['client_id' => SORT_DESC])->all();
        $managers = Manager::find()->where('status = 1')->all();
        $projects = Project::find()->where('status != 5')->all();
        $stocks = Stock::find()->where('status = 1')->all();
        // if(Yii::$app->user->identity->role == 4)
        //     $stocks = Stock::find()->where('status = 1 AND stock_id != 11')->all();
        $shipment_types = ShipmentType::find()->where('status = 1')->all();
        $statuses = CargoStatus::find()->where('status = 1')->all();
        $types = CargoType::find()->where('status = 1')->all();

        if (Yii::$app->request->post() && isset($model)) {
            $model->shipment_id = (Yii::$app->request->post('shipment')) ? (int)Yii::$app->request->post('shipment') : 0;
            $model->date_add = (Yii::$app->request->post('date_add')) ? (int)strtotime(Yii::$app->request->post('date_add')) : 0;
            $model->date_received = (Yii::$app->request->post('date_received')) ? (int)strtotime(Yii::$app->request->post('date_received')) : 0;
            $model->from_id = (Yii::$app->request->post('from')) ? (int)Yii::$app->request->post('from') : 0;
            $model->to_id = (Yii::$app->request->post('to')) ? (int)Yii::$app->request->post('to') : 0;
            $model->date_given_to_client = (Yii::$app->request->post('date_given')) ? (int)strtotime(Yii::$app->request->post('date_given')) : 0;
            $model->delivery_time = (Yii::$app->request->post('time')) ? (int)Yii::$app->request->post('time') : 0;
            $model->client_id = (Yii::$app->request->post('client')) ? (int)Yii::$app->request->post('client') : 0;
            //////////////////////
            if(!empty($model->client_id)){
                $findManagerToClient = Client::findOne(['client_id'=>$model->client_id]);
                if(!empty($findManagerToClient->manager_id))
                    $model->manager_id = $findManagerToClient->manager_id;
                else
                    $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            }
            else{
                $model->manager_id = (Yii::$app->request->post('manager')) ? (int)Yii::$app->request->post('manager') : 0;
            }
            /////////////////////
            $model->project_id = (Yii::$app->request->post('project')) ? (int)Yii::$app->request->post('project') : 0;
            $model->warehouse_id = (Yii::$app->request->post('stock')) ? (int)Yii::$app->request->post('stock') : 0;
            $model->shipment_type_id = (Yii::$app->request->post('shipment_type')) ? (int)Yii::$app->request->post('shipment_type') : 0;
            $model->status_id = (Yii::$app->request->post('status')) ? (int)Yii::$app->request->post('status') : 0;
            $model->weight = (Yii::$app->request->post('weight')) ? floatval(Yii::$app->request->post('weight')) : 0;
            $model->volume = (Yii::$app->request->post('volume')) ? floatval(Yii::$app->request->post('volume')) : 0;
            $model->cartons = (Yii::$app->request->post('cartons')) ? (int)Yii::$app->request->post('cartons') : 0;
            $model->volume_weight = (Yii::$app->request->post('volume')) ? floatval(Yii::$app->request->post('volume')*167) : 0;
            $model->count_inside = (Yii::$app->request->post('inside')) ? (int)Yii::$app->request->post('inside') : 0;
            $model->local_track = (Yii::$app->request->post('local_tracking')) ? Yii::$app->request->post('local_tracking') : '';
            $model->local_delivery_price = (Yii::$app->request->post('local_price')) ? Yii::$app->request->post('local_price') : 0;
            $model->local_expenses = (Yii::$app->request->post('local_expenses')) ? Yii::$app->request->post('local_expenses') : '';
            $model->track = (Yii::$app->request->post('delivery_tracking')) ? Yii::$app->request->post('delivery_tracking') : '';
            $model->type_id = (Yii::$app->request->post('type')) ? (int)Yii::$app->request->post('type') : 0;
            $model->type_special_id = (Yii::$app->request->post('type_special')) ? (int)Yii::$app->request->post('type_special') : 0;
            $model->description = (Yii::$app->request->post('description')) ? Yii::$app->request->post('description') : '';
            
            $file1 = Cargo::uploadFiles('photos', 'image/');
            if(!empty($file1))
                $model->photos .= ($file1 != '') ? ','.$file1 : $file1;
            $file2 = Cargo::uploadFile('invoice', 'files/');
            if(!empty($file2))
                $model->invoice = $file2;
            $file3 = Cargo::uploadFile('file', 'files/');
            if(!empty($file3))
                $model->file = $file3;
            ////////
            //if(Yii::$app->user->identity->role > 5){
                //if(Yii::$app->user->identity->role > 7 || Yii::$app->user->identity->id == $model->manager_id){
                    Logs::Create($model->cargo_id, $model, 2, 2);
                    if($model->update()){
                        if($model->project_id > 0){
                            Project::MathTotalPricing($model->project_id);
                        }
                        return '1';
                    }
                //}
            //}
        } else {
            return $this->render('update', [
                'model' => $model,
                'shipments' => $shipments,
                'countries' => $countries,
                'clients' => $clients,
                'managers' => $managers,
                'projects' => $projects,
                'stocks' => $stocks,
                'shipment_types' => $shipment_types,
                'statuses' => $statuses,
                'types' => $types,
                'menu' => Yii::$app->controller->renderPartial('menu'),
            ]);
        }
    }

    public function actionEditmany()
    {
        $count = 0;
        if(Yii::$app->request->post('cargos')){
            $cargos = explode(",", Yii::$app->request->post('cargos'));
            foreach ($cargos as $cargo) {
                if(!empty($cargo)){
                    $model = $this->findModel((int)$cargo);
                    if (isset($model)) {
                        if(Yii::$app->request->post('date_add'))
                            $model->date_add = (int)strtotime(Yii::$app->request->post('date_add'));
                        if(Yii::$app->request->post('date_received'))
                            $model->date_received = (int)strtotime(Yii::$app->request->post('date_received'));
                        if(Yii::$app->request->post('date_given'))
                            $model->date_given_to_client = (int)strtotime(Yii::$app->request->post('date_given'));
                        if(Yii::$app->request->post('client'))
                            $model->client_id = (int)Yii::$app->request->post('client');
                        if(Yii::$app->request->post('manager'))
                            $model->manager_id = (int)Yii::$app->request->post('manager');
                        if(Yii::$app->request->post('shipment'))
                            $model->shipment_id = (int)Yii::$app->request->post('shipment');
                        if(Yii::$app->request->post('from'))
                            $model->from_id = (int)Yii::$app->request->post('from');
                        if(Yii::$app->request->post('to'))
                            $model->to_id = (int)Yii::$app->request->post('to');
                        if(Yii::$app->request->post('stock'))
                            $model->warehouse_id = (int)Yii::$app->request->post('stock');
                        if(Yii::$app->request->post('shipment_type') && Yii::$app->user->identity->role != 8)
                            $model->shipment_type_id = (int)Yii::$app->request->post('shipment_type');
                        if(Yii::$app->request->post('status'))
                            $model->status_id = (int)Yii::$app->request->post('status');
                        if(Yii::$app->request->post('type') && Yii::$app->user->identity->role != 8)
                            $model->type_id = (int)Yii::$app->request->post('type');
                        if(Yii::$app->request->post('description'))
                            $model->description = Yii::$app->request->post('description');
                        if(Yii::$app->request->post('local_tracking'))
                            $model->local_track = Yii::$app->request->post('local_tracking');
                        //////прикрепление карго к проекту
                        if(Yii::$app->request->post('project')){
                            $project = Project::findOne(['project_id'=>(int)Yii::$app->request->post('project')]);
                            if(isset($project->project_id)){
                                if($project->client_id == $model->client_id){
                                    $model->project_id = $project->project_id;
                                }else{
                                    Yii::$app->session->setFlash('success', "Вы пытаетесь добавить не свои грузы, будьте внимательней, мы следим за вами :)");
                                }
                            }
                        }
                        ////////
                        //if(Yii::$app->user->identity->role > 4 || Yii::$app->user->identity->id == $model->manager_id){
                            Logs::Create($model->cargo_id, $model, 2, 2);
                            if($model->update()){
                                if($model->project_id > 0){
                                    Project::MathTotalPricing($model->project_id);
                                }
                                $count = 1;
                            }
                        //}
                    } 
                }
            }
        }
        if($count > 0)
            return 1;
        else
            return 'error';
    }

    public function FormEditMany(){
        $model = new Cargo();
        $shipments = Shipment::find()->orderBy(['shipment_id' => SORT_DESC])->all();
        $countries = Country::find()->all();
        if(Yii::$app->user->identity->role == 4 || Yii::$app->user->identity->role == 5 || Yii::$app->user->identity->role == 9)
            $clients = Client::find()->where(['status'=>1])->orderBy(['client_id' => SORT_DESC])->all();
        else
            $clients = Client::find()->where(['status'=>1, 'manager_id'=>Yii::$app->user->identity->id])->orderBy(['client_id' => SORT_DESC])->all();
        $managers = Manager::find()->where('status = 1')->all();
        if(Yii::$app->user->identity->role >= 10){
            $projects = Project::find()->where(['status'=>[0,1]])->all();
        }else{
            $projects = Project::find()->where(['status'=>[0,1],'manager_id'=>Yii::$app->user->identity->id])->all();
        }
        $stocks = Stock::find()->where('status = 1')->all();
        if(Yii::$app->user->identity->role == 4)
            $stocks = Stock::find()->where('status = 1 AND stock_id != 11')->all();

        $shipment_types = ShipmentType::find()->where('status = 1')->all();
        $statuses = CargoStatus::find()->where('status = 1')->all();
        $types = CargoType::find()->where('status = 1')->all();

        return $this->renderPartial('edit_many', [
            'model' => $model,
            'shipments' => $shipments,
            'countries' => $countries,
            'clients' => $clients,
            'managers' => $managers,
            'projects' => $projects,
            'stocks' => $stocks,
            'shipment_types' => $shipment_types,
            'statuses' => $statuses,
            'types' => $types,
        ]);
    }

    public function actionClientsearch($id = 0){
        if (!empty($id)) {
            $client = Client::findOne(['client_id'=>$id]);
            if(isset($client->manager_id))
                return $client->manager_id;

        }
        return 0;
    }

    public function actionSearchsesion(){

    }

    public function actionSearch(){
        $session = Yii::$app->session;
        Cargo::SearchSession();

        $dataProvider = new ActiveDataProvider([
            'query' => Cargo::SearchQuery(),
            'pagination' => [
                'pageSize' => 100, 
            ],
            'sort'=> ['defaultOrder' => ['cargo_id'=>SORT_DESC]],
        ]);

        return $this->renderPartial('table', [
            'dataProvider' => $dataProvider,
            'FormEditMany' => $this->FormEditMany(),
            'menu' => '',
        ]);
    }

    public function actionSearchwarehouse($id){
        Cargo::SearchSession(1,(int)$id);

        $dataProvider = new ActiveDataProvider([
            'query' => Cargo::SearchQuery(1,(int)$id),
            'pagination' => [
                'pageSize' => 1000, 
            ],
        ]);

        return $this->renderPartial('/cargo/table_warehouse', [
            'dataProvider' => $dataProvider,
            'FormEditMany' => $this->FormEditMany(),
            'menu' => '',
            'id' => (int)$id,
        ]);
    }

    public function actionSearchfilter($id){
        Cargo::SearchSession(2,(int)$id);

        $dataProvider = new ActiveDataProvider([
            'query' => Cargo::SearchQuery(2,(int)$id),
            'pagination' => [
                'pageSize' => 1000, 
            ],
        ]);

        return $this->renderPartial('/cargo/table_filter', [
            'dataProvider' => $dataProvider,
            'FormEditMany' => $this->FormEditMany(),
            'menu' => '',
            'id' => (int)$id,
        ]);
    }

    public function actionSearchdelivered($id){
        Cargo::SearchSession(3,(int)$id);

        $dataProvider = new ActiveDataProvider([
            'query' => Cargo::SearchQuery(3,(int)$id),
            'pagination' => [
                'pageSize' => 1000, 
            ],
        ]);

        return $this->renderPartial('/cargo/table_delivered', [
            'dataProvider' => $dataProvider,
            'FormEditMany' => $this->FormEditMany(),
            'menu' => '',
            'id' => (int)$id,
        ]);
    }

    public function actionSearchdelete(){
        if(Yii::$app->request->get('type')){
            $session = Yii::$app->session;
            if($session->has(Yii::$app->request->get('type'))){
                $session->remove(Yii::$app->request->get('type'));
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSearchdeleteall(){
        Cargo::DeleteAllSession();
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSetfilter(){
        if(Yii::$app->request->get('type') && Yii::$app->request->get('value')){
            $session = Yii::$app->session;
            if($session->has(Yii::$app->request->get('type'))){
                $session[Yii::$app->request->get('type')] = Yii::$app->request->get('value');
            }
        }
    }

    
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel((int)$id);
        if(isset($model) && Yii::$app->user->identity->role >= 9){
            $model->status = 0;
            Logs::Create($model->cargo_id, $model, 2, 3);
            $model->update(false);
        }

        return $this->redirect(['index']);
    }

    private function generatorQrcode($cargo_id){
        // QR_BarCode object 
        $qr = new Qrcode; 
         
        // create text QR code 
        $qr->url('http://crm.inta.group/cargo/edit/'.$cargo_id); 
         
        // display QR code image
        $qr->qrCode(200, $cargo_id);
    }

    public function actionExportxls() {
        if(Yii::$app->request->get('cargos') && Yii::$app->request->get('type')){
            $cargos = explode(",", Yii::$app->request->get('cargos'));

            $attributes = substr(Yii::$app->request->get('type'), 0, -1);
            $attributes = explode(",", $attributes);

            $session = Yii::$app->session;
            $session['cargo_export_type'] = $attributes;

            $file = \Yii::createObject([
                'class' => 'codemix\excelexport\ExcelFile',

                //'writerClass' => '\PHPExcel_Writer_Excel5',

                'sheets' => [

                    'Active Users' => [
                        'class' => 'codemix\excelexport\ActiveExcelSheet',
                        'query' => CargoExel::find()->where(['cargo_id'=>$cargos]),

                        'attributes' => $attributes,
                    ],

                ],
            ]);
            $file->send('Cargo Export '.date('d.m.Y H:i:s',time()).'.xlsx');
        }
    }

    public function actionCustomtable(){
        if(Yii::$app->request->get('page') && Yii::$app->request->get('type')){
            $attributes = substr(Yii::$app->request->get('type'), 0, -1);
            $attributes = explode(",", $attributes);
            if(isset($attributes)){
                CustomTable::deleteAll([
                    'manager_id'=>Yii::$app->user->identity->id,
                    'page'=>'cargo',
                    'page_two'=>Yii::$app->request->get('page')
                ]);
                foreach ($attributes as $type) {
                    $customTable = new CustomTable;
                    $customTable->manager_id = Yii::$app->user->identity->id;
                    $customTable->page = 'cargo';
                    $customTable->page_two = Yii::$app->request->get('page');
                    $customTable->type = $type;
                    $customTable->save();
                }
            }
        }
        return 1;
    }

    public function actionFiledelete(){
        if(Yii::$app->request->get('cargo') && Yii::$app->request->get('type')){
            $cargo_id_GET = (int)Yii::$app->request->get('cargo');
            $cargo = Cargo::findOne(['cargo_id'=>$cargo_id_GET]);
            if(isset($cargo)){
                switch (Yii::$app->request->get('type')) {
                    case 'photos':
                        if(!empty($cargo->photos)){
                            $photos_all = explode(",", $cargo->photos);
                            $cargo->photos = '';
                            foreach ($photos_all as $photo) {
                                if(!empty($photo)){
                                    if(Yii::$app->request->get('link') != $photo){
                                        $cargo->photos .= ','.$photo;
                                    }
                                } 
                            }
                        }
                        break;
                    case 'invoice':
                        $cargo->invoice = '';
                        break;
                    case 'file':
                        $cargo->file = '';
                        break;
                }
                $cargo->update(false);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    // public function actionGeneratorqrcode(){
    //     $allCargos = Cargo::find()->all();
    //     foreach ($allCargos as $cargo) {
    //         $this->generatorQrcode($cargo->cargo_id);
    //     }
    // }

    // public function actionPerenos(){
    //     $allCargos = Cargo::find()->all();
    //     foreach ($allCargos as $cargo) {
    //         if($cargo->warehouse_id == 12){
    //             $cargo->warehouse_id = 10;
    //             $cargo->update(false);
    //         }
    //         else if($cargo->warehouse_id == 35){
    //             $cargo->warehouse_id = 11;
    //             $cargo->update(false);
    //         }
    //     }
    // }

    
    protected function findModel($id)
    {
        if (($model = Cargo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
