<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Task;

/**
 * CargoController implements the CRUD actions for Cargo model.
 */
class TaskController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {   
        $all_table_rows = '';

        if(Yii::$app->user->identity->id){
            if(Yii::$app->request->get('type') == 1){
                $tasks = Task::find()
                    ->filterWhere(['=', 'manager_id', Yii::$app->user->identity->id])
                    ->orderBy(['status' => SORT_ASC,'date_end' => SORT_DESC])
                    ->all();
            }else{
               $tasks = Task::find()
                    ->filterWhere(['=', 'manager_id', Yii::$app->user->identity->id])
                    ->andFilterWhere(['=', 'status', 0])
                    ->orderBy(['date_end' => SORT_ASC])
                    ->all(); 
            }

            
            if(isset($tasks)){
                foreach ($tasks as $task) {
                    /////tag////
                    if(($task->date_end - time()) < 900)
                        $tag = '#b62929';
                    else if(date('d.m.Y', time()) == date('d.m.Y', $task->date_end))
                        $tag = '#efc80c';
                    else
                        $tag = '#32b629';
                    /////tag end////
                    
                    //client//
                    if(isset($task->client->client_id)){
                        $client = '<a href="/clients/edit/'.$task->client->client_id.'">'.$task->client->client_id.'</a> ';
                    }
                    else{
                        $client = '';
                    }
                    //////////

                    $all_table_rows .= Yii::$app->controller->renderPartial('/task/one_task_modal', [
                        'task'=>$task,
                        'tag'=>$tag,
                        'client'=>$client,
                    ]);
                }
                return $all_table_rows;
            }
        }
        return 'You have no tasks!';
    }
    
    public function actionCreate()
    {   
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if(!empty($_POST['name'])){
            $task = new Task;
            $task->manager_id = Yii::$app->user->identity->id;
            $task->description = $_POST['name'];
            $task->date_create = time();
            $task->client_id = 0;
            $task->status = 0;
            $task->tag = '';
            if(!empty($_POST['date']))
                $task->date_end = (int)strtotime($_POST['date']);
            else
                $task->date_end = time();
            if($task->save())
                return 1;
        }
        return 'error';
    }
    
    public function actionStatus()
    {   
        if(!empty($_POST['id'])){
            $task = $this->findModel((int)$_POST['id']);
            $task->status = 1;
            $task->update();
        }
    }

    
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
