<?php

namespace app\controllers;
use Yii;

use app\models\Users;

use app\models\Cargo;
use app\models\Country;
use app\models\Manager;
use app\models\Stock;
use app\models\Shipment;
use app\models\ShipmentType;
use app\models\CargoStatus;
use app\models\CargoType;
use app\models\Client;
use app\models\ClientSource;
use app\models\Payments;
use app\models\PaymentsSource;
use app\models\CurrencyCash;
use app\models\Project;

class ParseController extends \yii\web\Controller
{
	public function actionIndex() {
		$code = '8MuwVB9qpVsKNXrbLZBqRdRRnYXg3hyNBsK5AUs7';
		if(Yii::$app->request->get('code') == $code){
			$this->parseCargo();
			$this->parseClients();
			$this->parsePayments();
			$this->parseProjects();
			$this->parseShipments();
		}
	}
    private function parseCargo() {
        $url_dostavki = 'https://creator.zoho.com/api/json/cargos/view/Cargos_Report?authtoken=f8626a4c3e62ad29d71c5bead7a38274';
		$content_CURL = $this->CURL($url_dostavki);

        if(isset($content_CURL)){
            $content_CURL = str_replace("var zohozoho_ivan966view2 = ","",$content_CURL);
            $content_CURL = substr($content_CURL, 0, -1);
            $json = json_decode($content_CURL);
            if(isset($json->Cargos)){
                foreach ($json->Cargos as $zakaz) {
                	$cargo = Cargo::findOne((int)str_replace('CC-', '', $zakaz->Cargo_code_cc));
                	if(isset($cargo)){
						$cargo->shipment_id = (int)str_replace(' SH- ', '', $zakaz->Add_shipment); 
						$cargo->date_add = (!empty($zakaz->Cargo_date_add)) ? strtotime($zakaz->Cargo_date_add) : 0;
						$cargo->date_received = (!empty($zakaz->Cargo_date_received)) ? strtotime($zakaz->Cargo_date_received) : 0;
						$cargo->from_id = $this->cargoFromFindID($zakaz->Cargo_from);
						$cargo->to_id = $this->cargoFromFindID($zakaz->Cargo_to);
						$cargo->date_given_to_client = (!empty($zakaz->Cargo_given_to_client)) ? strtotime($zakaz->Cargo_given_to_client) : 0;
						$cargo->delivery_time = (!empty($zakaz->Cargo_delivery_time)) ? (int)$zakaz->Cargo_delivery_time : 0;
						$cargo->client_id = (int)str_replace('88-', '', $zakaz->Client_code); 
						$cargo->manager_id = $this->managerFindID($zakaz->Manager);
						$cargo->project_id = (!empty(str_replace('PRJ - ', '', $zakaz->Project))) ? (int)str_replace('PRJ - ', '', $zakaz->Project) : 0;
						$cargo->warehouse_id = $this->warehouseFindID($zakaz->Cargo_Warehouse);
						$cargo->shipment_type_id = $this->shipmentTypeFindID($zakaz->Cargo_shipment_type);
						$cargo->status_id = $this->cargoStatusFindID($zakaz->Cargo_status);
						$cargo->weight = $zakaz->Cargo_Weight;
						$cargo->volume = $zakaz->Cargo_volume1;
						$cargo->cartons = (!empty($zakaz->Cargo_cartons)) ? (int)$zakaz->Cargo_cartons : 0;
						$cargo->volume_weight = $zakaz->Cargo_volume_weight;
						$cargo->count_inside = (!empty($zakaz->Cargo_cartons)) ? (int)$zakaz->Qty_inside : 0;
						$cargo->local_track = $zakaz->Local_Tracking;
						$cargo->local_delivery_price = floatval(str_replace('¥ ', '', $zakaz->Local_delivery_price));
						$cargo->track = $zakaz->Tracking;
						$cargo->type_id = $this->cargoTypeFindID($zakaz->Cargo_type);
						$cargo->status = 1;
						$cargo->description = $zakaz->Cargo_description;
						$cargo->photos = $zakaz->Cargo_photo;
						$cargo->invoice = $zakaz->Cargo_invoice;
						$cargo->file = $zakaz->Other_Files;
						$cargo->site = 1;
	                    $cargo->update(false);
                	}
                	else{
	                    $cargo = new Cargo;
	                    $cargo->cargo_id = (int)str_replace('CC-', '', $zakaz->Cargo_code_cc);
	                    $cargo->shipment_id = (int)str_replace(' SH- ', '', $zakaz->Add_shipment); 
						$cargo->date_add = (!empty($zakaz->Cargo_date_add)) ? strtotime($zakaz->Cargo_date_add) : 0;
						$cargo->date_received = (!empty($zakaz->Cargo_date_received)) ? strtotime($zakaz->Cargo_date_received) : 0;
						$cargo->from_id = $this->cargoFromFindID($zakaz->Cargo_from);
						$cargo->to_id = $this->cargoFromFindID($zakaz->Cargo_to);
						$cargo->date_given_to_client = (!empty($zakaz->Cargo_given_to_client)) ? strtotime($zakaz->Cargo_given_to_client) : 0;
						$cargo->delivery_time = (!empty($zakaz->Cargo_delivery_time)) ? (int)$zakaz->Cargo_delivery_time : 0;
						$cargo->client_id = (int)str_replace('88-', '', $zakaz->Client_code); 
						$cargo->manager_id = $this->managerFindID($zakaz->Manager);
						$cargo->project_id = (!empty(str_replace('PRJ - ', '', $zakaz->Project))) ? (int)str_replace('PRJ - ', '', $zakaz->Project) : 0;
						$cargo->warehouse_id = $this->warehouseFindID($zakaz->Cargo_Warehouse);
						$cargo->shipment_type_id = $this->shipmentTypeFindID($zakaz->Cargo_shipment_type);
						$cargo->status_id = $this->cargoStatusFindID($zakaz->Cargo_status);
						$cargo->weight = $zakaz->Cargo_Weight;
						$cargo->volume = $zakaz->Cargo_volume1;
						$cargo->cartons = (!empty($zakaz->Cargo_cartons)) ? (int)$zakaz->Cargo_cartons : 0;
						$cargo->volume_weight = $zakaz->Cargo_volume_weight;
						$cargo->count_inside = (!empty($zakaz->Cargo_cartons)) ? (int)$zakaz->Qty_inside : 0;
						$cargo->local_track = $zakaz->Local_Tracking;
						$cargo->local_delivery_price = floatval(str_replace('¥ ', '', $zakaz->Local_delivery_price));
						$cargo->track = $zakaz->Tracking;
						$cargo->type_id = $this->cargoTypeFindID($zakaz->Cargo_type);
						$cargo->status = 1;
						$cargo->description = $zakaz->Cargo_description;
						$cargo->photos = $zakaz->Cargo_photo;
						$cargo->invoice = $zakaz->Cargo_invoice;
						$cargo->file = $zakaz->Other_Files;
						$cargo->site = 1;
	                    $cargo->save(false);
	                }
                    //print_r($cargo);
                }
            }
        }
    }

    private function parseClients() {
        $url_users = 'https://creator.zoho.com/api/json/clients/view/Head_of_sales_active_clients_report?authtoken=f8626a4c3e62ad29d71c5bead7a38274';
		$content_CURL = $this->CURL($url_users);

		if(isset($content_CURL)){
            $content_CURL = str_replace("var zohozoho_ivan966view94 = ","",$content_CURL);
            $content_CURL = substr($content_CURL, 0, -1);
            $json = json_decode($content_CURL);
            if(isset($json->Add_Client)){
                foreach ($json->Add_Client as $client_parse) {
                	$client = Client::findOne((int)str_replace('88-', '', $client_parse->Client_code));
                	if(isset($client)){
	                    $client->password = sha1($client_parse->Client_code);
	                    $client->manager_id = $this->managerFindID($client_parse->Manager);
	                    $client->date_add = time();
	                    $client->source_id = $this->clientSourceFindID($client_parse->Request_source);
	                    $client->activation = (int)$client_parse->Activate_client;
	                    $client->name = $client_parse->Name;
	                    $client->phone_code = 0;
	                    $client->phone = (int)$client_parse->Phone_Number;
	                    $client->email = $client_parse->Email;
	                    $client->comment = $client_parse->Comment;
	                    $client->date_next_connect = time();
	                    $client->status = 1;
	                    $client->balance = 0;
	                    $client->balance_total = 0;
	                    $client->site = 1;
	                    $client->update(false);
                	}
                	else{
                		$client = new Client;
	                    $client->client_id = (int)str_replace('88-', '', $client_parse->Client_code);
	                    $client->password = sha1($client_parse->Client_code);
	                    $client->manager_id = $this->managerFindID($client_parse->Manager);
	                    $client->date_add = time();
	                    $client->source_id = $this->clientSourceFindID($client_parse->Request_source);
	                    $client->activation = $client_parse->Activate_client;
	                    $client->name = $client_parse->Name;
	                    $client->phone_code = 0;
	                    $client->phone = (int)$client_parse->Phone_Number;
	                    $client->email = $client_parse->Email;
	                    $client->comment = $client_parse->Comment;
	                    $client->date_next_connect = time();
	                    $client->status = 1;
	                    $client->balance = 0;
	                    $client->balance_total = 0;
	                    $client->site = 1;
	                    $client->save(false);
	                }
                    //print_r($cargo);
                }
            }
        }
    }

    private function parsePayments() {
        $url_users = 'https://creator.zoho.com/api/json/clients/view/Payments_Report?authtoken=f8626a4c3e62ad29d71c5bead7a38274';
		$content_CURL = $this->CURL($url_users);

		if(isset($content_CURL)){
            $content_CURL = str_replace("var zohozoho_ivan966view81 = ","",$content_CURL);
            $content_CURL = substr($content_CURL, 0, -1);
            $json = json_decode($content_CURL);
            if(isset($json->Add_Payment)){
                foreach ($json->Add_Payment as $payment_parse) {
                	$payment = Payments::findOne((int)str_replace('3697905000000', '', $payment_parse->ID));
                	if(isset($payment)){
	                    $payment->client_id = (int)str_replace('88-', '', $payment_parse->Client_Code);
	                    $payment->manager_id = $this->managerFindID($payment_parse->users);
	                    $payment->branch_id = $this->paymentBranch($payment_parse->Branch);
	                    $payment->date_payment = (!empty($payment_parse->Date_payment)) ? strtotime($payment_parse->Date_payment) : 0;
	                    $payment->payment_status = $this->paymentStatus($payment_parse->Payment_status);
	                    $payment->payment_type = $this->paymentType($payment_parse->Payment_type);
	                    $payment->cash_usd = (float)$payment_parse->Paid_USD;
	                    $payment->cash_fact = (float)$payment_parse->Fact_paid;
	                    $payment->cash_fact_currency = $this->paymentCurrencyFindID($payment_parse->Fact_paid_currency);
	                    $payment->status = $payment_parse->Approved;
	                    $payment->comment = $payment_parse->Payment_comment;
	                    $payment->invoice = $payment_parse->Invoice;
	                    $payment->bankbill = $payment_parse->Bankbill;
	                    $payment->source_id = $this->paymentSourceFindID($payment_parse->Income_source);
	                    $payment->out_source_id = $this->outSource($payment_parse->Outcome_source);
	                    $payment->update(false);
                	}
                	else{
                		$payment = new Payments;
	                    $payment->payment_id = (int)str_replace('3697905000000', '', $payment_parse->ID);
						$payment->client_id = (int)str_replace('88-', '', $payment_parse->Client_Code);
	                    $payment->manager_id = $this->managerFindID($payment_parse->users);
	                    $payment->branch_id = $this->paymentBranch($payment_parse->Branch);
	                    $payment->date_payment = (!empty($payment_parse->Date_payment)) ? strtotime($payment_parse->Date_payment) : 0;
	                    $payment->payment_status = $this->paymentStatus($payment_parse->Payment_status);
	                    $payment->payment_type = $this->paymentType($payment_parse->Payment_type);
	                    $payment->cash_usd = (float)$payment_parse->Paid_USD;
	                    $payment->cash_fact = (float)$payment_parse->Fact_paid;
	                    $payment->cash_fact_currency = $this->paymentCurrencyFindID($payment_parse->Fact_paid_currency);
	                    $payment->status = $payment_parse->Approved;
	                    $payment->comment = $payment_parse->Payment_comment;
	                    $payment->invoice = $payment_parse->Invoice;
	                    $payment->bankbill = $payment_parse->Bankbill;
	                    $payment->source_id = $this->paymentSourceFindID($payment_parse->Income_source);
	                    $payment->out_source_id = $this->outSource($payment_parse->Outcome_source);
	                    $payment->save(false);
	                }
                    //print_r($cargo);
                }
            }
        }
    }

    private function parseProjects() {
        $url_users = 'https://creator.zoho.com/api/json/clients/view/Projects_Head_of_Sales?authtoken=f8626a4c3e62ad29d71c5bead7a38274';
		$content_CURL = $this->CURL($url_users);

		if(isset($content_CURL)){
            $content_CURL = str_replace("var zohozoho_ivan966view97 = ","",$content_CURL);
            $content_CURL = substr($content_CURL, 0, -1);
            $json = json_decode($content_CURL);
            if(isset($json->Projects)){
                foreach ($json->Projects as $project_parse) {
                	$project = Project::findOne((int)str_replace('PRJ - ', '', $project_parse->Project_number));
                	if(isset($project)){
	                    $project->manager_id = $this->managerFindID($project_parse->Manager);
						$project->client_id = (int)str_replace('88-', '', $project_parse->Add_Client); 
	                    $project->name = $project_parse->Project_name;
	                    $project->date_create = (!empty($project_parse->Project_created)) ? strtotime($project_parse->Project_created) : 0;
	                    $project->date_close = (!empty($project_parse->Project_closed)) ? strtotime($project_parse->Project_closed) : 0;
	                    $project->project_type = $this->projectType($project_parse->Project_Type);
	                    $project->delivery_type = $this->shipmentTypeFindID($project_parse->Delivery_type);
	                    $project->comment = $project_parse->Comment;
	                    $project->total = (float)$project_parse->Total_to_pay;
	                    $project->profit = (float)$project_parse->Profit;
	                    $project->status = $project_parse->Project_approved;
	                    $project->update(false);
                	}
                	else{
                		$project = new Project;
	                    $project->project_id = (int)str_replace('PRJ - ', '', $project_parse->Project_number);
						$project->manager_id = $this->managerFindID($project_parse->Manager);
						$project->client_id = (int)str_replace('88-', '', $project_parse->Add_Client); 
	                    $project->name = $project_parse->Project_name;
	                    $project->date_create = (!empty($project_parse->Project_created)) ? strtotime($project_parse->Project_created) : 0;
	                    $project->date_close = (!empty($project_parse->Project_closed)) ? strtotime($project_parse->Project_closed) : 0;
	                    $project->project_type = $this->projectType($project_parse->Project_Type);
	                    $project->delivery_type = $this->shipmentTypeFindID($project_parse->Delivery_type);
	                    $project->comment = $project_parse->Comment;
	                    $project->total = (float)$project_parse->Total_to_pay;
	                    $project->profit = (float)$project_parse->Profit;
	                    $project->status = $project_parse->Project_approved;
	                    $project->save(false);
	                }
                    //print_r($cargo);
                }
            }
        }
    }

    private function parseShipments() {
        $url_users = 'https://creator.zoho.com/api/json/cargos/view/All_shipments?authtoken=f8626a4c3e62ad29d71c5bead7a38274';
		$content_CURL = $this->CURL($url_users);

		if(isset($content_CURL)){
            $content_CURL = str_replace("var zohozoho_ivan966view77 = ","",$content_CURL);
            $content_CURL = substr($content_CURL, 0, -1);
            $json = json_decode($content_CURL);
            if(isset($json->Add_shipment)){
                foreach ($json->Add_shipment as $shipment_parse) {
                	$shipment = Shipment::findOne((int)str_replace(' SH- ', '', $shipment_parse->Shipment_number));
                	if(isset($shipment)){
                		$cargo = Cargo::findOne(['shipment_id'=>$shipment->shipment_id]);

	                    $shipment->manager_id 		= $this->managerFindID($shipment_parse->Manager);
	                    $shipment->from_id 			= $this->cargoFromFindID($shipment_parse->From);
	                    $shipment->to_id 			= $this->cargoFromFindID($shipment_parse->To);
	                    $shipment->type_id 			= $this->shipmentTypeFindID($shipment_parse->Shipment_Type);
	                    $shipment->status_id 		= $this->cargoStatusFindID($shipment_parse->Shipment_Status);
	                    $shipment->weight 			= (float)$shipment_parse->Shipment_Weight;
	                    $shipment->volume 			= (float)$shipment_parse->Shipment_Volume;
	                    $shipment->cartons 			= (int)$shipment_parse->Cartons_Quantity;

	                    $shipment->checkpoint 		= (!empty($cargo->warehouse_id) && $cargo->warehouse_id != 7) ? $cargo->warehouse_id : 0;

	                    $shipment->date_created 	= (!empty($shipment_parse->Shipment_created)) ? strtotime($shipment_parse->Shipment_created) : 0;
	                    $shipment->date_received 	= (!empty($shipment_parse->Shipment_received)) ? strtotime($shipment_parse->Shipment_received) : 0;
	                    $shipment->time 			= (!empty($shipment_parse->Shipment_time)) ? ($shipment_parse->Shipment_time < 0) ? $shipment_parse->Shipment_time : -$shipment_parse->Shipment_time : 0;
	                    $shipment->link 			= $shipment_parse->Tracking_link;
	                    $shipment->total_chargers 	= (!empty($shipment_parse->Total_shipment_chargers)) ? (float)$shipment_parse->Total_shipment_chargers : 0;
	                    $shipment->status 			= 1;
	                    $shipment->awb 				= $shipment_parse->AWB;
	                    $shipment->invoice 			= $shipment_parse->Invoice;
	                    $shipment->list 			= $shipment_parse->Packing_list;
	                    $shipment->other 			= $shipment_parse->Other;
	                    $shipment->comment 			= (!empty($shipment_parse->Comment)) ? $shipment_parse->Comment : '';
	                    $shipment->price 			= 0;
	                    $shipment->price_customs 	= 0;
	                    $shipment->profit 			= 0;
	                    $shipment->update(false);
                	}
                	else{
                		$shipment = new Shipment;
	                    $shipment->shipment_id 		= (int)str_replace(' SH- ', '', $shipment_parse->Shipment_number);
						$shipment->manager_id 		= $this->managerFindID($shipment_parse->Manager);
	                    $shipment->from_id 			= $this->cargoFromFindID($shipment_parse->From);
	                    $shipment->to_id 			= $this->cargoFromFindID($shipment_parse->To);
	                    $shipment->type_id 			= $this->shipmentTypeFindID($shipment_parse->Shipment_Type);
	                    $shipment->status_id 		= $this->cargoStatusFindID($shipment_parse->Shipment_Status);
	                    $shipment->weight 			= (float)$shipment_parse->Shipment_Weight;
	                    $shipment->volume 			= (float)$shipment_parse->Shipment_Volume;
	                    $shipment->cartons 			= (int)$shipment_parse->Cartons_Quantity;

						$cargo = Cargo::findOne(['shipment_id'=>$shipment->shipment_id]);
	                    $shipment->checkpoint 		= (!empty($cargo->warehouse_id) && $cargo->warehouse_id != 7) ? $cargo->warehouse_id : 0;

	                    $shipment->date_created 	= (!empty($shipment_parse->Shipment_created)) ? strtotime($shipment_parse->Shipment_created) : 0;
	                    $shipment->date_received 	= (!empty($shipment_parse->Shipment_received)) ? strtotime($shipment_parse->Shipment_received) : 0;
	                    $shipment->time 			= (!empty($shipment_parse->Shipment_time)) ? ($shipment_parse->Shipment_time < 0) ? $shipment_parse->Shipment_time : -$shipment_parse->Shipment_time : 0;
	                    $shipment->link 			= $shipment_parse->Tracking_link;
	                    $shipment->total_chargers 	= (!empty($shipment_parse->Total_shipment_chargers)) ? (float)$shipment_parse->Total_shipment_chargers : 0;
	                    $shipment->status 			= 1;
	                    $shipment->awb 				= $shipment_parse->AWB;
	                    $shipment->invoice 			= $shipment_parse->Invoice;
	                    $shipment->list 			= $shipment_parse->Packing_list;
	                    $shipment->other 			= $shipment_parse->Other;
	                    $shipment->comment 			= (!empty($shipment_parse->Comment)) ? $shipment_parse->Comment : '';
	                    $shipment->price 			= 0;
	                    $shipment->price_customs 	= 0;
	                    $shipment->profit 			= 0;
	                    $shipment->save(false);
	                }
                    //print_r($cargo);
                }
            }
        }
    }

    private function CURL($url){
    	$user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';
        $options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        // $header['errno']   = $err;
        // $header['errmsg']  = $errmsg;
        // $header['content'] = $content;
        return $content;
    }

    private function cargoFromFindID($name){
    	$country = Country::find()->where(['name'=>$name])->one();
    	if(isset($country->country_id))
    		return $country->country_id;
    	else 
    		return 0;
    }

    private function managerFindID($username){
    	$manager = Manager::find()->where(['username'=>$username])->one();
    	if($username == ''){
    		return 0;
    	}
    	else if(isset($manager->manager_id)){
    		return $manager->manager_id;
    	}
    	else {
    		$manager = new Manager;
    		$manager->username = $username;
    		$manager->role = 0;
    		$manager->status = 0;
    		$manager->password = 'not_pass';
    		$manager->name = 'noname';
            $manager->avatar = ' ';
            $manager->phone = ' ';
            $manager->email = ' ';
            $manager->viber = ' ';
            $manager->telegram = ' ';
    		$manager->save(false);
    		return $manager->manager_id;
    	}
    }

    private function warehouseFindID($name){
    	$warehouse = Stock::find()->where(['name'=>$name])->one();
    	if(isset($warehouse->stock_id))
    		return $warehouse->stock_id;
    	else 
    		return 0;
    }

    private function shipmentTypeFindID($name){
    	$shipmentType = ShipmentType::find()->where(['name'=>$name])->one();
    	if(isset($shipmentType->type_id))
    		return $shipmentType->type_id;
    	else 
    		return 0;
    }

    private function cargoStatusFindID($name){
    	$cargoStatus = CargoStatus::find()->where(['name'=>$name])->one();
    	if(isset($cargoStatus->status_id))
    		return $cargoStatus->status_id;
    	else 
    		return 0;
    }

    private function cargoTypeFindID($name){
    	$cargoType = CargoType::find()->where(['name'=>$name])->one();
    	if(isset($cargoType->type_id))
    		return $cargoType->type_id;
    	else 
    		return 0;
    }

    private function clientSourceFindID($name){
    	$ClientSource = ClientSource::find()->where(['name'=>$name])->one();
    	if(isset($ClientSource->source_id))
    		return $ClientSource->source_id;
    	else 
    		return 0;
    }

    private function paymentBranch($name){
        switch ($name) {
            case 'China':
                return 1;
                break;
            case 'Ukraine':
                return 2;
                break;
            default:
                return 0;
                break;
        }
    }

    private function paymentStatus($name){
        switch ($name) {
            case 'Executed':
                return 1;
                break;
            case 'New':
                return 2;
                break;
            default:
                return 0;
                break;
        }
    }

    private function paymentType($name){
        switch ($name) {
            case 'Income':
                return 1;
                break;
            case 'Outcome':
                return 2;
                break;
            default:
                return 0;
                break;
        }
    }

    private function paymentCurrencyFindID($name){
    	$currency = CurrencyCash::find()->where(['name'=>$name])->one();
    	if(isset($currency->сurrency_id)){
    		return $currency->сurrency_id;
    	}
    	else {
    		return 0;
    	}
    }

    private function paymentSourceFindID($name){
    	$source = PaymentsSource::find()->where(['name'=>$name])->one();
    	if(isset($source->source_id))
    		return $source->source_id;
    	else 
    		return 0;
    }

    private function projectType($name){
        switch ($name) {
            case 'Delivery':
                return 1;
                break;
            case 'Money Transfer':
                return 2;
                break;
            case 'Buiyng':
                return 3;
                break;
            case 'Customs clearance':
                return 4;
                break;
            default:
                return 0;
                break;
        }
    }

    private function outSource($name){
        switch ($name) {
            case 'Card':
                return 1;
                break;
            case 'Cash':
                return 2;
                break;
            case 'TT':
                return 3;
                break;
            default:
                return 0;
                break;
        }
    }

}
