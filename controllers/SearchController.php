<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Cargo;
use app\models\Shipment;
use app\models\Client;
use app\models\Project;
use app\models\Payments;
use app\models\Manager;

/**
 * CargoController implements the CRUD actions for Cargo model.
 */
class SearchController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    
    public function actionIndex()
    {   
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if(!empty($_POST['search']) && !empty($_POST['type'])){
            $search = $_POST['search'];
            $type = $_POST['type'];
            switch ($type) {
                case 1: //clients
                    $manager = Manager::find()->filterWhere(['like', 'username', $search])->one();
                    if (empty($manager->manager_id)){
                        $manager = new Manager;
                        $manager->manager_id = '';
                    }
                    $result = Client::find()->filterWhere(['like', 'client_id', $search])
                    ->orFilterWhere(['like', 'name', $search])
                    ->orFilterWhere(['like', 'phone', $search])
                    ->orFilterWhere(['like', 'email', $search])
                    ->orFilterWhere(['=', 'manager_id', $manager->manager_id])
                    ->all();
                    break;
                case 2: //cargo
                    $manager = Manager::find()->filterWhere(['like', 'username', $search])->one();
                    if (empty($manager->manager_id)){
                        $manager = new Manager;
                        $manager->manager_id = '';
                    }
                    $result = Cargo::find()->filterWhere(['like', 'cargo_id', $search])
                    ->orFilterWhere(['like', 'client_id', $search])
                    ->orFilterWhere(['like', 'project_id', $search])
                    ->orFilterWhere(['like', 'shipment_id', $search])
                    ->orFilterWhere(['like', 'local_track', $search])
                    ->orFilterWhere(['=', 'manager_id', $manager->manager_id])
                    ->all();
                    break;
                case 3: //project
                    $manager = Manager::find()->filterWhere(['like', 'username', $search])->one();
                    if (empty($manager->manager_id)){
                        $manager = new Manager;
                        $manager->manager_id = '';
                    }
                    $result = Project::find()->filterWhere(['like', 'project_id', $search])
                    ->orFilterWhere(['like', 'name', $search])
                    ->orFilterWhere(['=', 'manager_id', $manager->manager_id])
                    ->all();
                    break;
                case 4: //shipment
                    $manager = Manager::find()->filterWhere(['like', 'username', $search])->one();
                    if (empty($manager->manager_id)){
                        $manager = new Manager;
                        $manager->manager_id = '';
                    }
                    $result = Shipment::find()->filterWhere(['like', 'shipment_id', $search])
                    ->orFilterWhere(['=', 'manager_id', $manager->manager_id])
                    ->all();
                    break;
                case 5: //payments
                    $manager = Manager::find()->filterWhere(['like', 'username', $search])->one();
                    if (empty($manager->manager_id)){
                        $manager = new Manager;
                        $manager->manager_id = '';
                    }
                    $result = Payments::find()->filterWhere(['like', 'payment_id', $search])
                    ->orFilterWhere(['=', 'manager_id', $manager->manager_id])
                    ->orFilterWhere(['=', 'client_id', $manager->manager_id])
                    ->all();
                    break;
                
                default:
                    return 'error';
                    break;
            }
            return $result;
        }
        return 'error';
    }

    public function actionSearch()
    {   
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if(!empty($_POST['search']) && !empty($_POST['type'])){
            $search = $_POST['search'];
            $type = $_POST['type'];
            switch ($type) {
                case 1: //clients
                    $manager = Manager::find()->filterWhere(['like', 'username', $search])->one();
                    if (empty($manager->manager_id)){
                        $manager = new Manager;
                        $manager->manager_id = '';
                    }
                    $query = Client::find()->filterWhere(['like', 'client_id', $search])
                    ->orFilterWhere(['like', 'name', $search])
                    ->orFilterWhere(['like', 'phone', $search])
                    ->orFilterWhere(['like', 'email', $search])
                    ->orFilterWhere(['=', 'manager_id', $manager->manager_id]);
                    
                    $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                        'pagination' => [
                            'pageSize' => 500, 
                        ],
                    ]);

                    return $this->renderPartial('/clients/table', [
                        'dataProvider' => $dataProvider,
                        'menu' => '',
                    ]);
                    break;
                case 2: //cargo
                    $manager = Manager::find()->filterWhere(['like', 'username', $search])->one();
                    if (empty($manager->manager_id)){
                        $manager = new Manager;
                        $manager->manager_id = '';
                    }
                    $query = Cargo::find()->filterWhere(['like', 'cargo_id', $search])
                    ->orFilterWhere(['like', 'client_id', $search])
                    ->orFilterWhere(['like', 'project_id', $search])
                    ->orFilterWhere(['like', 'shipment_id', $search])
                    ->orFilterWhere(['like', 'local_track', $search])
                    ->orFilterWhere(['=', 'manager_id', $manager->manager_id]);
                    
                    $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                        'pagination' => [
                            'pageSize' => 500, 
                        ],
                    ]);

                    return $this->renderPartial('/cargo/table', [
                        'dataProvider' => $dataProvider,
                        'menu' => '',
                    ]);
                    break;
                case 3: //project
                    $manager = Manager::find()->filterWhere(['like', 'username', $search])->one();
                    if (empty($manager->manager_id)){
                        $manager = new Manager;
                        $manager->manager_id = '';
                    }
                    $result = Project::find()->filterWhere(['like', 'project_id', $search])
                    ->orFilterWhere(['like', 'name', $search])
                    ->orFilterWhere(['=', 'manager_id', $manager->manager_id])
                    ->all();
                    break;
                case 4: //shipment
                    $manager = Manager::find()->filterWhere(['like', 'username', $search])->one();
                    if (empty($manager->manager_id)){
                        $manager = new Manager;
                        $manager->manager_id = '';
                    }
                    $result = Shipment::find()->filterWhere(['like', 'shipment_id', $search])
                    ->orFilterWhere(['=', 'manager_id', $manager->manager_id])
                    ->all();
                    break;
                case 5: //payments
                    $manager = Manager::find()->filterWhere(['like', 'username', $search])->one();
                    if (empty($manager->manager_id)){
                        $manager = new Manager;
                        $manager->manager_id = '';
                    }
                    $result = Payments::find()->filterWhere(['like', 'payment_id', $search])
                    ->orFilterWhere(['=', 'manager_id', $manager->manager_id])
                    ->orFilterWhere(['=', 'client_id', $manager->manager_id])
                    ->all();
                    break;
                
                default:
                    return 'error';
                    break;
            }
            return $result;
        }
        return 'error';
    }

    
    protected function findModel($id)
    {
        if (($model = Cargo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
