<?php

/* @var $this yii\web\View */
/* @var $this \app\controllers\SiteController */
/* @var $model \app\models\Master */
/* @var $options \app\models\MainPageOption  */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'ПАНЕЛЬ УПРАВЛЕНИЯ';

?>
    <section class="adminOrder container">
        <h1 class="h2 themeCaption text-center">
            ПАНЕЛЬ УПРАВЛЕНИЯ
        </h1>
        <div class="row">
            <?= $orders ?>
            <!-- <div class="col-sm-12 col-md-6">
                <div class="oderCart">
                    <header>
                        <div class="row">
                            <div class="col-xs-6 header_leftSide">
                                <span class="orderCart_numerOrder">
                                    №10423
                                </span>
                                <span class="orderCart_status">
                                    новый
                                </span>
                            </div>
                            <div class="col-xs-6 orderCart_dataTime">
                                <span class="orderCart_data">
                                        20 августа 2018
                                </span>
                                <span class="orderCart_time">
                                    16:20
                                </span>
                            </div>
                        </div>
                    </header>
                    <div class="row orderCart_body">
                        <div class="col-xs-12 col-md-9 orderCart_body_left">
                            <p class="orderCart_body_left_name">
                                Денис Борисов
                                <mark class="orderCart_body_left_name_status">
                                    Basic
                                </mark>
                            </p>
                            <ul class="orderCart_body_left_contact">
                                <li class="phone">
                                    <a href="tel:+3803)2312323">
                                        +38 (033) 231 23 23
                                    </a>
                                </li>
                                <li class="mail">
                                    <a href="mail:">
                                        denis.borisov@gmail.com
                                    </a>
                                </li>
                            </ul>
                        </div> 
                        <div class="col-xs-3 orderCart_body_right">
                            <span class="docType ">
                                pdf / exel
                            </span>
                        </div>
                    </div>  
                    <footer class="row">
                        <div class="col-xs-12 col-md-12 col-lg-7">
                            <ul>
                                <li class="orderCountDetail">
                                    24 товарные позиции  /  3 доп. услуги
                                </li>
                                <li class="oderDeliveryType">
                                    Способ доставки: Авиадоставка
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-md-12 col-lg-5 footerRightSide" >
                            <a href="/" class="adminInfoBTN">
                                Подробнее
                            </a>
                            <a href="/" class="adminResetOrderBTN">
                                Отменить заявку
                            </a>
                        </div>
                    </footer>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                    <div class="oderCart">
                        <header>
                            <div class="row">
                                <div class="col-xs-6 header_leftSide">
                                    <span class="orderCart_numerOrder">
                                        №10423
                                    </span>
                                    <span class="orderCart_status__annulment">
                                        отменен
                                    </span>
                                </div>
                                <div class="col-xs-6 orderCart_dataTime">
                                    <span class="orderCart_data">
                                            20 августа 2018
                                    </span>
                                    <span class="orderCart_time">
                                        16:20
                                    </span>
                                </div>
                            </div>
                        </header>
                        <div class="row orderCart_body">
                            <div class="col-xs-12 col-md-9 orderCart_body_left">
                                <p class="orderCart_body_left_name">
                                    Денис Борисов
                                    <mark class="orderCart_body_left_name_status">
                                        Basic
                                    </mark>
                                </p>
                                <ul class="orderCart_body_left_contact">
                                    <li class="phone">
                                        <a href="tel:+3803)2312323">
                                            +38 (033) 231 23 23
                                        </a>
                                    </li>
                                    <li class="mail">
                                        <a href="mailto: denis.borisov@gmail.com">
                                            denis.borisov@gmail.com
                                        </a>
                                    </li>
                                </ul>
                            </div> 
                            <div class="col-xs-3 orderCart_body_right">
                                <span class="docType ">
                                    pdf / exel
                                </span>
                            </div>
                        </div>  
                        <footer class="row">
                            <div class="col-xs-12 col-md-12 col-lg-7">
                                <ul>
                                    <li class="orderCountDetail">
                                        24 товарные позиции  /  3 доп. услуги
                                    </li>
                                    <li class="oderDeliveryType">
                                        Способ доставки: Морская
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-12 col-lg-5 footerRightSide" >
                                <a href="/" class="adminInfoBTN">
                                    Подробнее
                                </a>
                                <a href="/" class="adminResetOrderBTN">
                                    Отменить заявку
                                </a>
                            </div>
                        </footer>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                        <div class="oderCart oderCart__Success">
                            <header>
                                <div class="row">
                                    <div class="col-xs-6 header_leftSide">
                                        <span class="orderCart_numerOrder">
                                            №10422
                                        </span>
                                        <span class="orderCart_status">
                                            выполнен
                                        </span>
                                    </div>
                                    <div class="col-xs-6 orderCart_dataTime">
                                        <span class="orderCart_data">
                                                20 августа 2018
                                        </span>
                                        <span class="orderCart_time">
                                            14:30
                                        </span>
                                    </div>
                                </div>
                            </header>
                            <div class="row orderCart_body">
                                <div class="col-xs-12 col-md-9 orderCart_body_left">
                                    <p class="orderCart_body_left_name">
                                        Бенджамин Франклин
                                        <mark class="orderCart_body_left_name_status">
                                            VIP
                                        </mark>
                                    </p>
                                    <ul class="orderCart_body_left_contact">
                                        <li class="phone">
                                            <a href="tel:+38032312323">
                                                +38 (022) 222 22 22
                                            </a>
                                        </li>
                                        <li class="mail">
                                            <a href="mailto:vladimnir321@mail.ru">
                                                vladimnir321@mail.ru
                                            </a>
                                        </li>
                                    </ul>
                                </div> 
                                <div class="col-xs-3 orderCart_body_right">
                                    <span class="docType ">
                                        pdf / exel
                                    </span>
                                </div>
                            </div>  
                            <footer class="row">
                                <div class="col-xs-12 col-md-12 col-lg-7">
                                    <ul>
                                        <li class="orderCountDetail">
                                            24 товарные позиции  /  3 доп. услуги
                                        </li>
                                        <li class="oderDeliveryType">
                                            Способ доставки: Авиадоставка
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-md-12 col-lg-5 footerRightSide" >
                                    <a href="/" class="adminInfoBTN">
                                        Подробнее
                                    </a>
                                    <a href="/" class="adminResetOrderBTN">
                                        Отменить заявку
                                    </a>
                                </div>
                            </footer>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                            <div class="oderCart ">
                                <header>
                                    <div class="row">
                                        <div class="col-xs-6 header_leftSide">
                                            <span class="orderCart_numerOrder">
                                                №10420
                                            </span>
                                            <span class="orderCart_status__wait">
                                                отправлено
                                            </span>
                                        </div>
                                        <div class="col-xs-6 orderCart_dataTime">
                                            <span class="orderCart_data">
                                                    20 августа 2018
                                            </span>
                                            <span class="orderCart_time">
                                                16:20
                                            </span>
                                        </div>
                                    </div>
                                </header>
                                <div class="row orderCart_body">
                                    <div class="col-xs-12 col-md-9 orderCart_body_left">
                                        <p class="orderCart_body_left_name">
                                            Дмитрий Белоусов
                                            <mark class="orderCart_body_left_name_status">
                                                Basic
                                            </mark>
                                        </p>
                                        <ul class="orderCart_body_left_contact">
                                            <li class="phone">
                                                <a href="tel:+3803)2312323">
                                                    +38 (033) 231 23 23
                                                </a>
                                            </li>
                                            <li class="mail">
                                                <a href="mail:">
                                                    denis.borisov@gmail.com
                                                </a>
                                            </li>
                                        </ul>
                                    </div> 
                                    <div class="col-xs-3 orderCart_body_right">
                                        <span class="docType ">
                                            pdf / exel
                                        </span>
                                    </div>
                                </div>  
                                <footer class="row">
                                    <div class="col-xs-12 col-md-12 col-lg-7">
                                        <ul>
                                            <li class="orderCountDetail">
                                                44 товарные позиции  /  3 доп. услуги
                                            </li>
                                            <li class="oderDeliveryType">
                                                Способ доставки: Авиадоставка
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-12 col-md-12 col-lg-5 footerRightSide" >
                                        <a href="/" class="adminInfoBTN">
                                            Подробнее
                                        </a>
                                        <a href="/" class="adminResetOrderBTN">
                                            Отменить заявку
                                        </a>
                                    </div>
                                </footer>
                            </div>
                        </div>
                          <div class="col-sm-12 col-md-6">
                <div class="oderCart">
                    <header>
                        <div class="row">
                            <div class="col-xs-6 header_leftSide">
                                <span class="orderCart_numerOrder">
                                    №10423
                                </span>
                                <span class="orderCart_status">
                                    новый
                                </span>
                            </div>
                            <div class="col-xs-6 orderCart_dataTime">
                                <span class="orderCart_data">
                                        20 августа 2018
                                </span>
                                <span class="orderCart_time">
                                    16:20
                                </span>
                            </div>
                        </div>
                    </header>
                    <div class="row orderCart_body">
                        <div class="col-xs-12 col-md-9 orderCart_body_left">
                            <p class="orderCart_body_left_name">
                                Денис Борисов
                                <mark class="orderCart_body_left_name_status">
                                    Basic
                                </mark>
                            </p>
                            <ul class="orderCart_body_left_contact">
                                <li class="phone">
                                    <a href="tel:+3803)2312323">
                                        +38 (033) 231 23 23
                                    </a>
                                </li>
                                <li class="mail">
                                    <a href="mail:">
                                        denis.borisov@gmail.com
                                    </a>
                                </li>
                            </ul>
                        </div> 
                        <div class="col-xs-3 orderCart_body_right">
                            <span class="docType ">
                                pdf / exel
                            </span>
                        </div>
                    </div> 
                    <footer class="row">
                        <div class="col-xs-12 col-md-12 col-lg-7">
                            <ul>
                                <li class="orderCountDetail">
                                    24 товарные позиции  /  3 доп. услуги
                                </li>
                                <li class="oderDeliveryType">
                                    Способ доставки: Авиадоставка
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-md-12 col-lg-5 footerRightSide" >
                            <a href="/" class="adminInfoBTN">
                                Подробнее
                            </a>
                            <a href="/" class="adminResetOrderBTN">
                                Отменить заявку
                            </a>
                        </div>
                    </footer>
                </div>
            </div>
              <div class="col-sm-12 col-md-6">
                <div class="oderCart">
                    <header>
                        <div class="row">
                            <div class="col-xs-6 header_leftSide">
                                <span class="orderCart_numerOrder">
                                    №10423
                                </span>
                                <span class="orderCart_status">
                                    новый
                                </span>
                            </div>
                            <div class="col-xs-6 orderCart_dataTime">
                                <span class="orderCart_data">
                                        20 августа 2018
                                </span>
                                <span class="orderCart_time">
                                    16:20
                                </span>
                            </div>
                        </div>
                    </header>
                    <div class="row orderCart_body">
                        <div class="col-xs-12 col-md-9 orderCart_body_left">
                            <p class="orderCart_body_left_name">
                                Денис Борисов
                                <mark class="orderCart_body_left_name_status">
                                    Basic
                                </mark>
                            </p>
                            <ul class="orderCart_body_left_contact">
                                <li class="phone">
                                    <a href="tel:+3803)2312323">
                                        +38 (033) 231 23 23
                                    </a>
                                </li>
                                <li class="mail">
                                    <a href="mail:">
                                        denis.borisov@gmail.com
                                    </a>
                                </li>
                            </ul>
                        </div> 
                        <div class="col-xs-12 col-md-3 orderCart_body_right">
                            <span class="docType ">
                                pdf / exel
                            </span>
                        </div>
                    </div> 
                    <footer class="row">
                        <div class="col-xs-12 col-md-12 col-lg-7">
                            <ul>
                                <li class="orderCountDetail">
                                    24 товарные позиции  /  3 доп. услуги
                                </li>
                                <li class="oderDeliveryType">
                                    Способ доставки: Авиадоставка
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-md-12 col-lg-5 footerRightSide" >
                            <a href="/" class="adminInfoBTN">
                                Подробнее
                            </a>
                            <a href="/" class="adminResetOrderBTN">
                                Отменить заявку
                            </a>
                        </div>
                    </footer>
                </div>
            </div> -->
            
        </div>
    </section>