<?php 
    use yii\helpers\Html; 
	use yii\widgets\Breadcrumbs;
?>

<?=$this->render('/layouts/header')?>
    <?= Breadcrumbs::widget([
        'homeLink' => ['label' => 'Панель управления', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= $content ?>
<?=$this->render('/layouts/footer')?>