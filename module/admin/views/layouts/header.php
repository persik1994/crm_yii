<?php 
    use yii\helpers\Html; 
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="/web/css/native.css">
    <link rel="stylesheet" href="/web/css/main_admin.css">
    <title><?= Html::encode($this->title) ?> - ANTEGI GROUP</title>
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="<?= Yii::$app->params['keywords'] ?>" name="keywords">
    <meta content="<?= Yii::$app->params['description'] ?>" name="description">
    <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
    <meta property="og:title" content="<?= Html::encode($this->title) ?>">
    <meta property="og:image" content="<?= Yii::$app->params['seo_image'] ?>">
    <meta property="og:site_name" content="<?= Html::encode($this->title) ?>">
    <meta property="og:description" content="<?= Yii::$app->params['description'] ?>">
    
    <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="<?= Html::encode($this->title) ?>">
    <meta name="twitter:title" content="<?= Html::encode($this->title) ?>">
    <meta name="twitter:description" content="<?= Yii::$app->params['description'] ?>">
    <meta name="twitter:image" content="<?= Yii::$app->params['seo_image'] ?>">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
</head>
<body>
    <header class="container-fluid" id="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12  col-md-3">
                            <a href="/admin">
                                <h1 class="logoText h6">
                                </h1>
                            </a>
                        </div><!--end col-md-3 -->
                        <nav class="col-xs-12 col-sm-12 col-md-9">
                            <div class="mobilBar">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <ul class="nav" id="nav">
                                <li>
                                    <a href="#" class="active dorball-alarm-header">
                                        <i class="fa fa-bell" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="/admin" class="active">
                                        <i class="fa fa-user-circle" aria-hidden="true"></i> 
                                        ADMIN - <?=Yii::$app->user->identity->first?>
                                    </a>
                                </li>
                               
                                <li>
                                    <a href="/logout">
                                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                                        Выход
                                    </a>
                                </li>
                            </ul> 
                        </nav><!-- end nav -->
                    </div>
                </div><!--end col-xs-12 -->
            </div>
        </div>
    </header>
    <div class="pod-menu">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul>
                        <li><a href="/admin">Панель управления</a></li>
                        <li><a href="/admin/order/create">Создать доставку</a></li>
                        <li><a href="/admin/users">Клиенты</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>