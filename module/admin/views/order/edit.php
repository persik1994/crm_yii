<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = '№'.$order->id.' - РЕДАКТИРОВАНИЕ ЗАКАЗА';

?>

<section class="AdminEditOrder container">
        <div class="oderCart">
            <header>
                <div class="row">
                    <div class="col-xs-12 col-md-6 header_leftSide">
                        <span class="orderCart_numerOrder">
                            №<?=$order->id?>
                        </span>
                        <span class="<?= $status->color ?>">
                            <?= $status->name ?>
                        </span>
                    </div>
                    <div class="col-xs-12 col-md-6 orderCart_dataTime">
                        <span class="orderCart_data">
                                <?= $date ?>
                        </span>
                        <span class="orderCart_time">
                            <?= $time ?>
                        </span>
                    </div>
                </div>
            </header>
            <div class="row orderCart_body">
                <div class="col-xs-12 col-md-4 col-lg-3 orderCart_body_left">
                    <p class="orderCart_body_left_name">
                        <?= $user->first ?>
                        <mark class="orderCart_body_left_name_status">
                            <?= $user_type->name ?>
                        </mark>
                    </p>
                    <ul class="orderCart_body_left_contact">
                        <li class="phone">
                            <a href="tel:+3803)2312323">
                                <?= $user->phone ?>
                            </a>
                        </li>
                        <li class="mail">
                            <a href="mail:">
                                <?= $user->email ?>
                            </a>
                        </li>
                    </ul>
                </div> <!-- end leftSide-->
                <div class="col-xs-12 deliverySide col-md-5 col-lg-4">
                    <ul>
                        <li class="orderCountDetail">
                            <?= $order_positions_count ?> товарные позиции  /  <?= $order_service_count ?> доп. услуги
                        </li>
                        <li class="oderDeliveryType">
                            Способ доставки: <?= $delivery->name ?>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-3 col-lg-5 orderCart_body_right">
                    <span class="docType ">
                        Версия для печати
                    </span>
                </div>
            </div>  <!-- end body -->
        </div> <!-- oderCart -->
    <form action="/admin/order/submitedit/<?=$order->id?>" method="POST" id="formEdit" enctype="multipart/form-data">
        <p class="textBold">
            Заказ
        </p>

        <div class="border_theme"></div>

        <p class="textPale">
            Товарные позиции
        </p>
        <div class="wrapTable">
            <table class="tableProduct">
                <tr class="textTableTH">
                    <th style="width: 35%">
                        Товарная позиция
                    </th>
                    <th style="width: 7%">
                        Кол-во
                    </th>
                    <th style="width: 47%">
                        Детали и пожелания
                    </th  >
                    <th style="width: 20%"></th>
                </tr>
                <?= $allPositions ?>
            </table>
        </div>
        <p class="textPale">
            <br>
            <?=($allService != '') ? 'Дополнительные услуги' : ''?>
        </p>
        <div class="wrapTable">
            <table class="tableProduct tableCastom">
                <?= $allService ?>
            </table>
        </div>

        <p class="textPale">
            История
        </p>
        <div class="wrapTable">
            <table class="tableProduct">
                <tr class="textTableTH table-order-history">
                    <th>
                        Дата
                    </th>
                    <th>
                        Комментарий
                    </th>
                    <th>
                        Статус
                    </th>
                    <th>
                        Клиент уведомлен
                    </th>
                    <th>
                        Файл
                    </th>
                    <th>
                        Администратор
                    </th>
                </tr>
                <?= $allOrderHistory ?>
            </table>
        </div>

        <p class="textSmall">
            <br>
            Добавить в историю 
            <br>
        </p>
        <div class="row editOption">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-md-4 col-lg-3">
                        <label class="AG_labelXL" for="status">
                            Статус заказа
                        </label>
                    </div>
                    <div class="col-xs-12 col-md-8 col-lg-9">
                        <?= Html::dropDownList('status', $order->status, ArrayHelper::map($allStatus, 'id', 'name'), [
                            'class'=>'AG_SelectXL',
                            'id'=>'status',
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-md-4 col-lg-3">
                        <label class="AG_labelXL" for="status">
                            Местонахождение заказа
                        </label>
                    </div>
                    <div class="col-xs-12 col-md-8 col-lg-9">
                        <?= Html::dropDownList('status', $order->status, ArrayHelper::map($allLocation, 'id', 'name'), [
                            'class'=>'AG_SelectXL',
                            'id'=>'status',
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-10 col-md-4 col-lg-3">
                        <label class="AG_labelXL" for="SendMail">
                            Уведомить покупателя
                        </label>
                    </div>
                    <div class="col-xs-1 col-md-8 col-lg-9">
                        <input type="checkbox" id="SendMail" name="admin-options-2">
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-10 col-md-4 col-lg-3">
                        <label class="AG_labelXL" for="SendFile">
                            Вложить файл
                        </label>
                    </div>
                    <div class="col-xs-1 col-md-8 col-lg-9">
                        <input type="file" id="SendFile" name="admin-options-1" >
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-md-4 col-lg-3">
                        <label class="AG_labelXL" for="comments">
                            Комментарий
                        </label>
                    </div>
                    <div class="col-xs-12 col-md-8 col-lg-9">
                        <br>
                       <textarea class="AG_textArea" name="admin-options-3" id="comments" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <!-- <button class="AG_btn_addEdit ">
                    Добавить
                </button> -->
            </div>
        </div>    <!-- end row-->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <button class="submitBTN" style="width: 100%;margin-top: 25px;margin-bottom: 25px;">
                    ВНЕСТИ ИЗМЕНЕНИЯ
                </button>
            </div>
        </div>  
    </form>   
    </section>