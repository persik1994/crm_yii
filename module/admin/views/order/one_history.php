<tr id="tableTRedit" class="table-order-history">
    <td>
        <?= date('d.m.Y H:i:s', $history->date) ?>
    </td>
    <td>
        <?= ($status->id == 3) ? '<b>'.(int)$history->comment.'$</b> - выставлен счет для оплаты.' : $history->comment ?>
    </td>
    <td class="<?= $status->color ?>">
        <?= $status->name ?>
    </td>
    <td>
        <?= $history->notification ?>
    </td>
    <td>
        <?= ($history->file != 'Нет') ? '<a href="/uploads/images/'.$history->file.'" style="color:blue;" target="_blank">Открыть</a>' : $history->file ?>
    </td>
    <td>
        <?= (empty($history->admin_id)) ? 'Комментарий пользователя' : $history->admin_id ?>
    </td>
</tr>