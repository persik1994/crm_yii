
<div class="col-sm-12 col-md-6">
    <div class="oderCart <?= ($order->status == 3)? 'oderCart__Success':'' ?>" id="OrderBlock<?= $order->id ?>" style="height: auto;">
        <header>
            <div class="row">
                <div class="col-xs-12 header_leftSide">
                    <!-- <span class="<?= $status->color ?>">
                        <?= $status->name ?>
                    </span> -->
                    <span class="orderCart_numerOrder">
                       Tracking: <a href="<?= $order_options->track_url ?>" target="_blank" class="np_track"><?= $order_options->track ?></a>
                    </span>
                    <span class="orderCart_data" style="float: right;">
                            CR-код: <?= $order_options->cr_code ?>
                    </span>
                </div>
            </div>
        </header>
        <div class="row orderCart_body">
            <div class="col-xs-12 col-md-9 orderCart_body_left">
                <p class="orderCart_body_left_name">
                    <?= $user->first ?>
                </p>
                <ul class="orderCart_body_left_contact">
                    <li class="phone">
                        <a href="tel:<?= $user->phone ?>">
                            <?= $user->phone ?>
                        </a>
                    </li>
                    <li class="mail">
                        <a href="mail:">
                            <?= $user->email ?>
                        </a>
                    </li>
                </ul>
                <p class="orderCart_body_left_name">
                    Снято со счета: <?= $order->money ?>$
                </p>
            </div>
            <div class="col-xs-3 orderCart_body_right">
                <a href="/admin/order/edit/<?= $order->id ?>" class="adminInfoBTN adminInfoBTN_NEW">
                    Подробнее
                </a>
            </div>
            <!-- <div class="col-xs-3 orderCart_body_right">
                <span class="docType ">
                    pdf / exel
                </span>
            </div> -->
        </div>
        <footer class="row">
            <div class="col-xs-12 col-md-12 col-lg-6">
                <ul>
                    <li class="oderDeliveryType">
                        Статус: <span class="<?= $status->color ?>"><?= $status->name ?></span>
                    </li>
                    <li class="oderDeliveryType">
                        Местопололожение: <?= (isset($locate_now)) ? '<img src="'.$locate_now->icon.'" alt="'.$locate_now->name.'">' : '' ?>
                    </li>
                    <li class="oderDeliveryType">
                        Маршрут: <img src="<?= $locate_start->icon ?>" alt="<?= $locate_start->name ?>"> 
                                 <img src="<?= $delivery->icon ?>" alt="<?= $delivery->name ?>"> 
                                 <img src="<?= $locate_finish->icon ?>" alt="<?= $locate_finish->name ?>">
                    </li>
                    <li class="orderCountDetail">
                        <p style="overflow: hidden;">Описание: <?= $order_options->description ?></p>
                    </li>
                    <li class="oderDeliveryType">
                        Локальный трек: <a href="https://novaposhta.ua/en/tracking/?cargo_number=<?= $order_options->track_local ?>" target="_blank" class="np_track"><?= $order_options->track_local ?></a>
                    </li>
                    <li class="oderDeliveryType">
                        Заметка пользователя: <?= $order_options->note ?>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-6">
                <ul>
                    <li class="orderCountDetail">
                        Создано: <?=$date_create?>
                    </li>
                    <li class="orderCountDetail">
                        Доставлено: <?=$date_finish?>
                    </li>
                    <li class="orderCountDetail">
                        кг: <?= $order_options->weight ?>
                    </li>
                    <li class="orderCountDetail">
                        м/куб: <?= $order_options->volume ?>
                    </li>
                </ul>
            </div>
        </footer>
    </div>
</div>