<?php

/* @var $this yii\web\View */
/* @var $this \app\controllers\SiteController */
/* @var $model \app\models\Master */
/* @var $options \app\models\MainPageOption  */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\ArrayHelper;

$this->title = 'СОЗДАНИЕ ДОСТАВКИ';

?>
    <section class="container sectionEditOrder">
        <p class="h2 text-center slimText">
                СОЗДАНИЕ ДОСТАВКИ
        </p>
        <form action="/admin/order/create" method="POST" id="formOrder">
            <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(),[]) ?>
            <!-- oder 1 -->
            <div class="row" id="order1">
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    <input type="text" name="track-number" placeholder="Tracking">
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    <input type="text" name="track-number-url" placeholder="Tracking Ссылка">
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    <input type="text" name="cr-code" placeholder="CR-код">
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    <input type="text" name="track-local" placeholder="Локальный трекинг">
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    <input type="text" name="weight" placeholder="кг">
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    <input type="text" name="volume" placeholder="м/куб">
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <input type="text" name="description" placeholder="Опишите детали груза">
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <p class="text-left hParam">
                                Выберите клиента: <mark>*</mark>
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-3">
                            <label class="AG_labelXL" for="status">
                                Клиент
                            </label>
                        </div>
                        <div class="col-xs-12 col-md-8 col-lg-9">
                            <?= Html::dropDownList('user', $order->status, ArrayHelper::map($users, 'id', 'first'), [
                                'class'=>'AG_SelectXL',
                                'id'=>'status',
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <p class="text-left hParam">
                                Выберите способ доставки: <mark>*</mark>
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-3">
                            <label class="AG_labelXL" for="status">
                                Способ доставки
                            </label>
                        </div>
                        <div class="col-xs-12 col-md-8 col-lg-9">
                            <?= Html::dropDownList('delivery', $order->status, ArrayHelper::map($allDelivery, 'id', 'name'), [
                                'class'=>'AG_SelectXL',
                                'id'=>'status',
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <p class="text-left hParam">
                                Выберите статус заказа: <mark>*</mark>
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-3">
                            <label class="AG_labelXL" for="status">
                                Статус заказа
                            </label>
                        </div>
                        <div class="col-xs-12 col-md-8 col-lg-9">
                            <?= Html::dropDownList('status', $order->status, ArrayHelper::map($allStatus, 'id', 'name'), [
                                'class'=>'AG_SelectXL',
                                'id'=>'status',
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <p class="text-left hParam">
                                Выберите местонахождение заказа: <mark>*</mark>
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-3">
                            <label class="AG_labelXL" for="status">
                                Местонахождение заказа
                            </label>
                        </div>
                        <div class="col-xs-12 col-md-8 col-lg-9">
                            <?= Html::dropDownList('locate', $order->status, $location, [
                                'class'=>'AG_SelectXL',
                                'id'=>'locate',
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <p class="text-left hParam">
                                Укажите из какой страны отправка: <mark>*</mark>
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-3">
                            <label class="AG_labelXL" for="status">
                                Отправка из страны
                            </label>
                        </div>
                        <div class="col-xs-12 col-md-8 col-lg-9">
                            <?= Html::dropDownList('locate_start', $order->status, ArrayHelper::map($allLocation, 'id', 'name'), [
                                'class'=>'AG_SelectXL',
                                'id'=>'status',
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <p class="text-left hParam">
                                Укажите в какую страну отправка: <mark>*</mark>
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-3">
                            <label class="AG_labelXL" for="status">
                                Отправка в страну
                            </label>
                        </div>
                        <div class="col-xs-12 col-md-8 col-lg-9">
                            <?= Html::dropDownList('locate_finish', $order->status, ArrayHelper::map($allLocation, 'id', 'name'), [
                                'class'=>'AG_SelectXL',
                                'id'=>'status',
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <button class="submitBTN">
                        СОЗДАТЬ ДОСТАВКУ
                    </button>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <button class="resetAllBTN" type="reset">
                        Отменить всё
                    </button>
                </div>
            </div>         
        </form>
    </section>