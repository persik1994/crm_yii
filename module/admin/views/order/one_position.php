<tr id="tableTRedit-<?= $position->id ?>">
    <td>
        <input type="url" class="linkProduct" name="url<?= $position->id ?>" readonly value="<?= $position->url ?>">
    </td>
    <td>
        <input class="textBold" name="count<?= $position->id ?>" type="number" readonly value="<?= $position->count ?>">
    </td>
    <td>
        <input type="text"  class="textBold" name="description<?= $position->id ?>" readonly value="<?= $position->description ?>">
    </td>
    <td>
        <button class="formBTNdelet" data-form-id="tableTRedit-<?= $position->id ?>">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </button>

        <button class="formBTNedit" data-form-id="tableTRedit-<?= $position->id ?>">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </button>
    </td>
</tr>