<?php

namespace app\module\admin\controllers;

use Yii;
use yii\web\Controller;
use app\models\Order;
use app\models\OrderPosition;
use app\models\OrderService;
use app\models\OrderHistory;
use app\models\OrderOptions;
use app\models\Users;
use app\models\TypeUser;
use app\models\TypeDelivery;
use app\models\TypeService;
use app\models\TypeStatusOrder;
use app\models\AdminOrderService;
use app\models\UploadForm;
use app\models\SendEmail;
use app\models\TypeLocation;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

class OrderController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/login']);
        }
        if(Yii::$app->user->identity->role != 9)
            return $this->redirect('/index');

        if (Yii::$app->request->post('track-number')){
            $order = new Order;
            $order->user_id = (int)Yii::$app->request->post('user');
            $order->admin_id = Yii::$app->user->identity->id;
            $order->type_delivery = Yii::$app->request->post('delivery');
            $order->money = 1;
            $order->status = (int)Yii::$app->request->post('status');
            $order->create_time = time();
            if($order->save()){
                $order_option = new OrderOptions;
                $order_option->order_id = $order->id;
                $order_option->track = Yii::$app->request->post('track-number');
                $order_option->track_url = Yii::$app->request->post('track-number-url');
                $order_option->track_local = Yii::$app->request->post('track-local');
                $order_option->cr_code = Yii::$app->request->post('cr-code');
                $order_option->description = Yii::$app->request->post('description');
                $order_option->locate_now = Yii::$app->request->post('locate');
                $order_option->locate_start = Yii::$app->request->post('locate_start');
                $order_option->locate_finish = Yii::$app->request->post('locate_finish');
                $order_option->weight = Yii::$app->request->post('weight');
                $order_option->volume = Yii::$app->request->post('volume');
                $order_option->date_delivered = 0;
                $order_option->note = '';
                //print_r($order_option->errors);
                if($order_option->save()){
                    return $this->redirect('/admin');
                }
                else{
                    $order->delete();
                }
            }
        }

        $order = new Order;
        $users = Users::find()->all();
        $allStatus = TypeStatusOrder::AdminTypes();
        $allDelivery = TypeDelivery::DeliveryList();
        ////location////
        $allLocation = TypeLocation::AdminLocations();
        $array_location = ArrayHelper::map($allLocation, 'id', 'name');
        array_unshift($array_location, "Проверяется логистикой");
        ////end location////
        return $this->render('/order/create', [
            'order' => $order,
            'users'=>$users,
            'allStatus'=>$allStatus,
            'allLocation'=>$allLocation,
            'allDelivery'=>$allDelivery,
            'location' => $array_location,
        ]);
    }
    public function actionEdit($id)
    {
        $order = Order::findOne((int)$id);
        $status = TypeStatusOrder::findOne($order->status);
        $date = Order::LoadDateOrder($order->create_time);
        $time = date('H:i', $order->create_time);
        $user = Users::findOne($order->user_id);
        $user_type = TypeUser::findOne($user->type_id);
        $order_positions_count = OrderPosition::find()->where(['order_id'=>$order->id])->count();
        $order_service_count = OrderService::find()->where(['order_id'=>$order->id])->count();
        $delivery = TypeDelivery::findOne($order->type_delivery);
        $allPositions = $this->AllPositionsEdit($order->id);
        $allOrderHistory = $this->AllOrderHistory($order->id);
        $allService = $this->AllServiceEdit($order->id);
        $allStatus = TypeStatusOrder::AdminTypes();
        $allLocation = TypeLocation::AdminLocations();
        return $this->render('/order/edit', [
            'order'=>$order,
            'status'=>$status,
            'date'=>$date,
            'time'=>$time,
            'user'=>$user,
            'user_type'=>$user_type,
            'order_positions_count'=>$order_positions_count,
            'order_service_count'=>$order_service_count,
            'delivery'=>$delivery,
            'allPositions'=>$allPositions,
            'allOrderHistory'=>$allOrderHistory,
            'allService'=>$allService,
            'allStatus'=>$allStatus,
            'allLocation'=>$allLocation,
        ]);
    }

    public function actionSubmitedit($id)
    {
        $order_id = (int)$id;
        $order = Order::findOne((int)$order_id);
        $AllPositions = OrderPosition::find()->where(['order_id'=>$order_id])->all();
        foreach ($AllPositions as $position) {
            if(!empty(Yii::$app->request->post('url'.$position->id)) &&
                !empty(Yii::$app->request->post('count'.$position->id)) &&
                !empty(Yii::$app->request->post('description'.$position->id))
            ){
                $position->url = Yii::$app->request->post('url'.$position->id);
                $position->count = Yii::$app->request->post('count'.$position->id);
                $position->description = Yii::$app->request->post('description'.$position->id);
                $position->update();
            }
            else {
                $position->delete();
            }
        }

        $AllService = OrderService::find()->where(['order_id'=>$order_id])->all();
        foreach ($AllService as $service) {
            if(empty(Yii::$app->request->post('service'.$service->id))){
                $service->delete();
            }
        }

        if(!empty(Yii::$app->request->post('status')) && $order->status != Yii::$app->request->post('status')){
            $order->status = Yii::$app->request->post('status');
            $order->update();
        }

        if( Yii::$app->request->post('admin-options-2') ||
            Yii::$app->request->post('admin-options-3')
        ){
            $orderHistory = new OrderHistory;
            $orderHistory->order_id = $order->id;
            $orderHistory->admin_id = Yii::$app->user->identity->id;
            $orderHistory->date = time();
            $orderHistory->status_id = (int)Yii::$app->request->post('status');
            $orderHistory->comment = Yii::$app->request->post('admin-options-3');

            if(Yii::$app->request->post('admin-options-2') && 
               Yii::$app->request->post('admin-options-2') == 'on'){
                $orderHistory->notification = 1;
                $status = TypeStatusOrder::findOne($orderHistory->status_id);
                SendEmail::SetOrderStatus($status, $order->id);
            }
            else{
                $orderHistory->notification = 0;
            }
            //завантаження файлу///
                $model = new UploadForm();
                $model->file = UploadedFile::getInstanceByName('admin-options-1');
                $orderHistory->file = $model->upload($orderHistory->order_id.'_'.time());
            /////кінець/////
            $orderHistory->save();
        }

        return $this->redirect(['/admin/order/edit/'.$order_id]);
    }

    // public function actionCancelorder($id){
    //     $order = Order::findOne((int)$id);
    //     if(!empty($order)){
    //         $order->status = 4;
    //         $order->update();
    //     }
    //     return $this->redirect(['/admin#OrderBlock'.$order->id]);
    // }

    public function AllPositionsEdit($order_id){
        $html = '';
        $AllPositions = OrderPosition::find()->where(['order_id'=>$order_id])->all();
        foreach ($AllPositions as $position) {
            $html .= Yii::$app->controller->renderPartial('/order/one_position', [
                'position'=>$position,
            ]);
        }
        return $html;
    }

    public function AllOrderHistory($order_id){
        $html = '';
        $OrderHistories = OrderHistory::find()->where(['order_id'=>$order_id])->orderBy('id DESC')->all();
        foreach ($OrderHistories as $history) {
            $status = TypeStatusOrder::findOne($history->status_id);

            if($history->notification == 1)
                $history->notification = 'Да';
            else
                $history->notification = 'Нет';
            
            if($history->file == '')
                $history->file = 'Нет';
            
            $admin = Users::findOne($history->admin_id);
            $history->admin_id = $admin->first;

            $html .= Yii::$app->controller->renderPartial('/order/one_history', [
                'history'=>$history,
                'status'=>$status,
            ]);
        }
        return $html;
    }

    public function AllServiceEdit($order_id){
        $html = '';
        $AllService = OrderService::find()->where(['order_id'=>$order_id])->all();
        foreach ($AllService as $service) {
            $service_type = TypeService::findOne($service->type_id);
            $html .= Yii::$app->controller->renderPartial('/order/one_service', [
                'service'=>$service,
                'service_type'=>$service_type,
            ]);
        }
        return $html;
    }
}
