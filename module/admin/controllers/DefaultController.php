<?php

namespace app\module\admin\controllers;

use Yii;
use yii\web\Controller;
use app\models\Order;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/login']);
        }
    	if(Yii::$app->user->identity->role != 9)
    		return $this->redirect('/index');
    	
    	$orders = Order::AdminOrderOneShow();
        return $this->render('index',['orders'=>$orders]);
    }
}
