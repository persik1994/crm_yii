Highcharts.chart('infographic', {
    credits: {
        enabled: false
    },
    chart: {
        zoomType: 'xy',
        spacingLeft: -9,
        spacingRight: 0
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },

    xAxis: [{
        categories: ['Dec 13', 'Jan 14', 'Feb 14', 'Mar 14', 'Apr 14', 'May 14', 'Jun 14', 'Jul 14', 'Aug 14', 'Sep 14', 'Oct 14', 'Nov 14', 'Dec 14', 'Jan 15', 'Feb 15', 'Mar 15', 'Apr 15', 'May 15', 'Jun 15', 'Jul 15', 'Aug 15', 'Sep 15', 'Oct 15', 'Nov 15', 'Dec 15', 'Jan 16', 'Feb 16', 'Mar 16', 'Apr 16', 'May 16', 'Jun 16', 'Jul 16', 'Aug 16', 'Sept 16', 'Oct 16', 'Nov 16', 'Dec 16', 'Jan 17', 'Feb 17', 'Mar 17', 'Apr 17', 'YTD', '1 Year', '3 Yr Ann', 'S.I. Ann.*'],
        crosshair: true,
        minorTickInterval: 1,
        labels: {
            rotation: -90,
            style: {
                fontSize: '10.5px'
            }
        }
    }],
    yAxis: [{ // Primary yAxis
        min: 0,
        labels: {
            format: '{value}',
            style: {
                color: 'black'
            }
        },
        title: {
            text: 'Percentage Growth',
            style: {
                color: 'black'
            }
        }
    }, { // Secondary yAxis

        //linkedTo: 0,
        title: {
            text: 'Monthly Performance',
            style: {
                color: 'black'
            }
        },
        labels: {
            format: '{value}',
            style: {
                color: '#black'
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        x: 100,
        verticalAlign: 'top',
        y: 80,
        floating: true,
        backgroundColor: '#FFFFFF'
    },
    series: [{
        name: 'Percentage Growth',
        type: 'column',
        color: '#686868',
        yAxis: 1,
        data: [0.68, 0.25, 2.14, 2.28, 1.07, 0.82, 0.60, -0.26, 0.84, 0.06, 0.36, 0.05, -1.56, 0.06, 3.11, 0.14, -0.05, 0.13, -1.81, 1.36, -1.29, -1.87, 2.68, -0.24, -0.92, -2.31, -1.85, 6.098379519, 2.738364211, -0.89, 1.2, 2.06, 1.29, -0.57, 0.45, -3.52, 2.04, 1.48, 1.05, 1.88, 1.16, 5.69, 7.76, 4.75, 6.12],
        tooltip: {
            valueSuffix: ''
        }

    }, {
        name: 'Monthly Performance',
        type: 'spline',
        color: '#26A091',
        data: [0.68, 0.93, 3.09, 5.44, 6.57, 7.44, 8.08, 7.80, 8.71, 8.77, 9.16, 9.22, 7.52, 7.58, 10.93, 11.08, 11.03, 11.17, 9.16, 10.64, 9.21, 7.17, 10.04, 9.78, 8.77, 6.26, 4.29, 10.65, 13.68, 12.67, 14.02, 16.37, 17.87, 17.2, 17.73, 13.59, 15.91, 17.62, 18.85, 21.09, {
            y: 22.5,
            dataLabels: {

                enabled: true,
                style: {
                    color: '#D3A809',
                    fontSize: '20px',
                    fontFamily: 'Exo2-Regular',
                    fontWeight: 'bold'
                },
                formatter: function() {
                    return this.y + '%';
                }
            }
        }],
        tooltip: {
            valueSuffix: ''
        }
    }]
});
