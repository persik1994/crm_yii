$(document).ready(function() {
    "use strict";

    // preloader

    var preloader_timer = setInterval(function() {
        if (document.readyState === "complete") {
            clearInterval(preloader_timer);
            //setTimeout(function() {
                $(".overlay").fadeOut(500);
            //}, 5000);
        }  
    }, 1000);

    // Insert fields
    $('#add-client').on("click", function(e) {
        e.preventDefault();
        var curLink = $(this).parents('.add-new-item').find('.add-new-item-button-inputs-temp');
        var cont = `
        <div class="add-new-item-button-inputs">

            <div class="add-client-form-line">
                <div class="input-addclient-wrap select-input">
                    <select class="client_messanger_type" name="manager" onblur="this.style.color = '#000'">
                        <option value="" disabled selected class="placeholder-select">-Select-</option>
                        <option value="1" class="option">Skype</option>
                        <option value="2">Telegram</option>
                        <option value="3">Viber</option>
                        <option value="4">Wechat</option>
                    </select>
                </div>
            </div>

            <div class="add-client-form-line">
                <div class="input-addclient-wrap ">
                    <input type="text" placeholder="" class="client_messanger_number">
                </div>
            </div>

        </div>
        `;


        curLink.append(cont);
    });

    $('#add-route').on("click", function(e) {
        e.preventDefault();
        var curLink = $(this).parents('.add-new-item').find('.add-new-item-button-inputs-temp');
        var cont = `
        <div class="add-new-item-button-inputs">

            <div class="add-client-form-line">
                <div class="input-addclient-wrap select-input">
                    <select class="route-from" name="manager" onblur="this.style.color = '#000'">
                        <option value="" disabled selected class="placeholder-select">-Select-</option>
                        <option value="1" class="option">China</option>
                        <option value="2">Hong Kong</option>
                        <option value="3">Poland</option>
                        <option value="4">USA</option>
                        <option value="5">Ukraine</option>
                    </select>
                </div>
            </div>

            <div class="add-client-form-line">
                <div class="input-addclient-wrap select-input">
                    <select class="route-to" name="manager" onblur="this.style.color = '#000'">
                        <option value="" disabled selected class="placeholder-select">-Select-</option>
                        <option value="1" class="option">China</option>
                        <option value="2">Hong Kong</option>
                        <option value="3">Poland</option>
                        <option value="4">USA</option>
                        <option value="5">Ukraine</option>
                    </select>
                </div>
            </div>

        </div>
        `;


        curLink.append(cont);
    });

    $('#add-pricing').on("click", function(e) {
        var add_pricing_count = 11;
        e.preventDefault();
        var curLink = $(this).parents('.add-new-item').find('.add-new-item-button-inputs-temp');
        var cont = `
        <div class="add-new-item-button-inputs">

            <div class="add-client-form-line">
                <div class="input-addclient-wrap select-input">
                    <select class="pricing-type pricing-type-`+add_money_off_count+`" name="manager" onblur="this.style.color = '#000'">
                        <option value="" disabled selected class="placeholder-select">-Select-</option>
                        <option value="1" class="option">General</option>
                        <option value="2">Electronics</option>
                        <option value="3">Brands</option>
                    </select>
                </div>
            </div>

            <div class="add-client-form-line">
                <div class="input-addclient-wrap select-input">
                    <select class="pricing-measure pricing-measure-`+add_money_off_count+`" name="manager" onblur="this.style.color = '#000'">
                        <option value="" disabled selected class="placeholder-select">-Select-</option>
                        <option value="1" class="option">usd/kg</option>
                        <option value="2">usd/pcs</option>
                        <option value="3">usd/m3</option>
                    </select>
                </div>
            </div>

            <div class="add-client-form-line">
                <div class="input-addclient-wrap">
                    <input type="text" placeholder="0.00" class="pricing-tariff pricing-tariff-`+add_money_off_count+`">
                </div>
            </div>

            <div class="add-client-form-line">
                <div class="input-addclient-wrap">
                    <a class="submit-add-pricing" data-id="`+$('.project_id_hide').val()+`" data-count="`+add_money_off_count+`">Save</a>
                </div>
            </div>

        </div>
        `;


        curLink.append(cont);
    });

    var add_money_off_count = 11;
    var currentDate = new Date();

    var date = currentDate.getDate();
    var month = currentDate.getMonth(); //Be careful! January is 0 not 1
    var year = currentDate.getFullYear();

    var dateString = date + "." +(month + 1) + "." + year;

    $('#add-money-off').on("click", function(e) {
        e.preventDefault();
        var curLink = $(this).parents('.add-new-item').find('.add-new-item-button-inputs-temp');
        var cont = `
        <div class="add-new-item-button-inputs">

            <div class="add-client-form-line">
                <div class="input-addclient-wrap">
                    <input type="text" class="dropdown-item dropdown-search-input" id="form-choose-date-`+add_money_off_count+`" placeholder="Date" autocomplete="off" value="`+dateString+`" disabled>
                </div>
            </div>

            <div class="add-client-form-line">
                <div class="input-addclient-wrap">
                    <input type="text" placeholder="0.00" class="money-off-amount-`+add_money_off_count+`">
                </div>
            </div>

            <div class="add-client-form-line">
                <div class="input-addclient-wrap">
                    <input type="text" placeholder="Description" class="money-off-description-`+add_money_off_count+`">
                </div>
            </div>

            <div class="add-client-form-line">
                <div class="input-addclient-wrap">
                    <a class="submit-withdraw" data-id="`+$('.project_id_hide').val()+`" data-count="`+add_money_off_count+`" style="display:none;">Withdraw</a>
                </div>
            </div>

        </div>
        <script>
            $("#form-choose-date-`+add_money_off_count+`").datetimepicker({
                format: 'd-M-Y',
                monthNames: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
            });
        </script>
        `;

        add_money_off_count++;

        curLink.append(cont);
    });

    $(document).on('click', '.submit-withdraw', function () {
        $.ajax({ 
            async: false,
            type: 'POST', 
            url: '/payments/withdraw/' + $(this).attr("data-id"), 
            data: {
                date:   $('#form-choose-date-'+$(this).attr("data-count")).val(),
                amount:   $('.money-off-amount-'+$(this).attr("data-count")).val(),
                description: $('.money-off-description-'+$(this).attr("data-count")).val(),
            }, 
            success: function (data) { 
                if(data == 'create' || data == ''){
                    //location.reload();
                }else{
                    alert(data);
                }
                
            }
        });
    });

    // $(document).on('click', '.submit-withdraw', function () {
    //     $.ajax({ 
    //         type: 'POST', 
    //         url: '/payments/withdraw/' + $(this).attr("data-id"), 
    //         data: {
    //             date:   $('#form-choose-date-'+$(this).attr("data-count")).val(),
    //             amount:   $('.money-off-amount-'+$(this).attr("data-count")).val(),
    //             description: $('.money-off-description-'+$(this).attr("data-count")).val(),
    //         }, 
    //         success: function (data) { 
    //             if(data == 'create'){
    //                 location.reload();
    //             }else{
    //                 alert(data);
    //             }
                
    //         }
    //     });
    // });

    $(document).on('click', '.submit-add-pricing', function () {
        $.ajax({ 
            type: 'POST', 
            url: '/projects/pricing/' + $(this).attr("data-id"), 
            data: {
                type:       $('.pricing-type-'+$(this).attr("data-count")).val(),
                measure:    $('.pricing-measure-'+$(this).attr("data-count")).val(),
                tariff:     $('.pricing-tariff-'+$(this).attr("data-count")).val(),
            }, 
            success: function (data) { 
                if(data == 'create'){
                    location.reload();
                }else{
                    alert(data);
                }
                
            }
        });
    });


    // Preview image/file
    var reader = new FileReader();
    var reader1 = new FileReader();

    $('.close-preview').on("click", function() {
        $('#get-img').val('');
        $('.gen-img .ava-name').text('');
        $('.gen-img .ava-size').text('');
        $("#preview_avatar").attr("src", "");
        $("#preview_avatar_1").attr("src", "");
        $('.gen-img').css('height', '0');
        $('.close-preview').hide();
        $('.add-client-form-line-file-mt').css('margin-top', '0px');
    });

    $("#get-img").change(function(event) {

        $('.close-preview').show();
        var filename = $(this).val().split("\\");
        var fileLen = filename.length - 1;
        $('.gen-img .ava-name').text(filename[fileLen]);

        reader.onload = function() {
            $('.gen-img').css('height', 'auto');
            var preview = $("#preview_avatar");
            preview.attr("src", reader.result);
            $('.photo-base64').val(reader.result);
        }

        reader.readAsDataURL(event.target.files[0]);
        var size = event.target.files[0].size / 1000;

        $('.gen-img .ava-size').text(size.toFixed(2) + ' KB');
        $('.add-client-form-line-file-mt').css('margin-top', '20px');

    });


    $("#get-invoice").change(function(event) {
        var filename = $(this).val();
        $('.gen-invoice').text(filename);
    });

    $("#get-of").change(function(event) {
        var filename = $(this).val();
        $('.gen-of').text(filename);
    });

    $("#get-awb").change(function(event) {
        var filename = $(this).val();
        $('.gen-awb').text(filename);
    });
    $("#get-list").change(function(event) {
        var filename = $(this).val();
        $('.gen-list').text(filename);
    });
    $("#get-other").change(function(event) {
        var filename = $(this).val();
        $('.gen-other').text(filename);
    });

    // Option CSS
    $('option').each(function() {
        $(this).addClass("option");
    });

    // Open date Choose
    $(".dashboard-choose-date").on("click", function() {
        if ($(this).find("ul").is(":visible")) {
            $(this).find("ul").fadeOut(500);
        } else {
            $(this).find("ul").fadeIn(500);
        }
    });

    $(document).on("mouseup", function(e) {
        if ($(".dashboard-choose-date").has(e.target).length === 0) {
            $(".dashboard-choose-date").find("ul").fadeOut(500);
        }
        if ($(".tasks-popup").has(e.target).length === 0 && $(".xdsoft_datetimepicker").has(e.target).length === 0) {
            $(".tasks-popup").fadeOut(500);
        }
        if ($(".notif").has(e.target).length === 0) {
            $(".notif").fadeOut(500);
        }
        if ($(".user-set").has(e.target).length === 0) {
            $(".user-set").fadeOut(500);
        }
    });

    // Open countries/info
    $("#country-click a").on("click", function() {
        if ($("#sm-countries").is(":visible")) {
            $("#sm-countries").slideUp(500);
            $(this).find('span').css('transform', 'rotate(0deg)');
        } else {
            $("#sm-countries").slideDown(500);
            $(this).find('span').css('transform', 'rotate(90deg)');
        }
    });

    $("#info-click a").on("click", function() {
        if ($("#sm-info").is(":visible")) {
            $("#sm-info").slideUp(500);
            $(this).find('span').css('transform', 'rotate(0deg)');
        } else {
            $("#sm-info").slideDown(500);
            $(this).find('span').css('transform', 'rotate(-90deg)');
        }
    });

    // Open sm-menu
    $(".hmb-wrap").on("click", function() {
        $(".sm-menu").addClass("menu-opened");
        $(".sm-menu-overlay").fadeIn(500);
        $("html").css('overflow-y', 'hidden');
    })

    $(".close-sm-menu").on("click", function() {
        $(".sm-menu").removeClass("menu-opened");
        $(".sm-menu-overlay").fadeOut(500);
        $("html").css('overflow-y', 'visible');
    });

    // Table click

    var index = 0;

    // $(".table tbody tr").each(function(e) {
    //     $(this).attr("id", "order-" + e);
    // });
    // $(".detail-info-wrap").find('.detail-info').each(function(e) {
    //     $(this).attr("id", "order-" + e);
    // });

    //$(".table tbody .cargo-row").on("click", function() {
    $(document).on('click', '.table tbody .cargo-row .eye-click-view-row', function () {
        var cur_id = $(this).attr("data-key");
        var re, s;
        s = cur_id;
        re = /\D+/ig;
        index = s.replace(re, '');
        if(!$('.detail-info').hasClass("detail-visibe")){
            //$('.detail-info').fadeOut(1000).removeClass("detail-visibe");
            $(".detail-info-wrap").find('.detail-info').fadeIn(700).addClass("detail-visibe");
            $('.detail-info-top-line').animate({
                opacity: 1
            }, 700);
            $('.detail-info-top-line').css({
                "display": "flex"
            });
        }

        $.ajax({ 
            type: 'GET', 
            url: '/cargo/view/' + $(this).attr("data-key"), 
            data: {}, 
            dataType: 'json',
            success: function (data) { 
                $('.detail-info-manager').text(data.manager_id);
                $('.detail-info-weight').text(data.weight);
                $('.detail-info-volume').text(data.volume);
                $('.detail-info-cartons').text(data.cartons);
                $('.detail-info-inside').text(data.count_inside);
                $('.detail-info-description').text(data.description);
                $('.detail-info-date_add').text(data.date_add);
                $('.detail-info-date_received').text(data.date_received);
                $('.detail-info-given_to_client').text(data.date_given_to_client);
                $('.detail-info-delivery_time').text(data.delivery_time);
                $('.detail-info-shipment_type').text(data.shipment_type_id);
                $('.detail-info-status').text(data.status_id);
                $('.detail-info-photo').html(data.photos);
                $('.detail-info-invoice').html(data.invoice);
                $('.detail-info-files').html(data.file);
                $('.detail-info-warehouse').text(data.warehouse_id);
                $('.detail-info-local_tracking').text(data.local_track);
                $('.detail-info-local_delivery_price').text(data.local_delivery_price);
                $('.detail-info-volume_weight').text(data.volume_weight);
                $('.detail-info-type').text(data.type_id);
                $('.detail-info-from').text(data.from_id);
                $('.detail-info-to').text(data.to_id);

                if(data.client_id != ''){
                    $('.detail-info-client_id').text('88 - ' + data.client_id);
                }else{
                    $('.detail-info-client_id').text('');
                }
                if(data.shipment_id != ''){
                    $('.detail-info-shipment').text('SH - ' + data.shipment_id);
                }else{
                    $('.detail-info-shipment').text('');
                }
                if(data.cargo_id != ''){
                    $('.detail-info-cargo_id').text('CC - ' + data.cargo_id);
                }else{
                    $('.detail-info-cargo_id').text('');
                }
                if(data.project_id != ''){
                    $('.detail-info-project').text('PJ - ' + data.project_id);
                }else{
                    $('.detail-info-project').text('');
                }
                if(data.type_special_id != ''){
                    $('.detail-info-special').text(data.type_special_id);
                }else{
                    $('.detail-info-special').text('');
                }
                //////
                $('.cargo-detail-info-url-edit').attr("href", '/cargo/edit/' + data.cargo_id + '?' + window.location.search.replace( '?', ''));
                $('.delete-post-url').attr("data-id", data.cargo_id);
                ////buttons navigations
                $('#slide-position-left').attr("data-id", data.cargo_id);
                $('#slide-position-right').attr("data-id", data.cargo_id);
            }
        });
    });

    $(document).on('click', '.table tbody .client-row .eye-click-view-row', function () {
        var cur_id = $(this).attr("data-key");
        var re, s;
        s = cur_id;
        re = /\D+/ig;
        index = s.replace(re, '');

        $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
        $('.detail-info-top-line').animate({
            opacity: 1
        }, 700);
        $('.detail-info-top-line').css({
            "display": "flex"
        });
        $(".detail-info-wrap").find('.detail-info').fadeIn(700).addClass("detail-visibe");

        $('.client-detail-info-url-edit').attr("href", '/clients/edit/' + $(this).attr("data-key"));
        // $('.client-detail-info-url-delete').attr("href", '/clients/delete/' + $(this).attr("data-key"));

        $.ajax({ 
            type: 'GET', 
            url: '/clients/view/' + $(this).attr("data-key"), 
            data: {}, 
            dataType: 'json',
            success: function (data) { 
                $('.detail-info-manager').text(data.client.manager_id);
                $('.detail-info-name').text(data.client.name);
                $('.detail-info-phone').text(data.client.phone);
                $('.detail-info-email').text(data.client.email);
                // $('.detail-info-messenger').text();
                $('.detail-info-source').text(data.client.source_id);
                $('.detail-info-client-status').text(data.client.status_id);
                $('.detail-info-status').text(data.client.activation);
                $('.detail-info-comment').text(data.client.comment);
                $('.detail-info-balance').text(data.client.balance);
                $('.detail-info-balance_total').text(data.client.balance_total);
                $('.detail-info-date_add').text(data.client.date_add);
                $('.detail-info-weight').text(data.weight);
                if(data.client.client_id != ''){
                    $('.detail-info-id').text('88 - ' + data.client.client_id);
                }else{
                    $('.detail-info-id').text('');
                }
                ///////
                $('.client_money_all').html('');
                $('.client_money_all_block').css({'display':'none'});
                if(data.transfers != 0){
                    $('.client_money_all_block').css({'display':'block'});
                    $(data.transfers).each(function(index,val) {
                        if(val.payment_type == 1){
                            var css_cash_balance = 'color:green;';
                        }else{
                            var css_cash_balance = 'color:red;';
                        }
                        $('.client_money_all').html(
                            $('.client_money_all').html() + 
                            '<div class="detail-info-list-row">' +
                                '<div class="detail-info-list-column">'+val.date_payment+'</div>' +
                                '<div class="detail-info-list-column" style="'+css_cash_balance+'">'+val.cash_usd+'</div>' +
                                '<div class="detail-info-list-column">'+val.project_id+'</div>' +
                                '<div class="detail-info-list-column">'+val.balance+'</div>' +
                                '<div class="detail-info-list-column">'+val.comment+'</div>' +
                            '</div>'
                        );
                    });
                }
                //////
                $('.client-detail-info-url-edit').attr("href", '/clients/edit/' + data.client.client_id + '?' + window.location.search.replace( '?', ''));
                $('.delete-post-url').attr("data-id", data.client.client_id);
                ////buttons navigations
                $('#slide-position-left').attr("data-id", data.client.client_id);
                $('#slide-position-right').attr("data-id", data.client.client_id);
                console.log(data);
            }
        });
    });

    $(document).on('click', '.table tbody .client-balance-row .eye-click-view-row', function () {
        var cur_id = $(this).attr("data-key");
        var re, s;
        s = cur_id;
        re = /\D+/ig;
        index = s.replace(re, '');

        $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
        $('.detail-info-top-line').animate({
            opacity: 1
        }, 700);
        $('.detail-info-top-line').css({
            "display": "flex"
        });
        $(".detail-info-wrap").find('.detail-info').fadeIn(700).addClass("detail-visibe");

        $('.client-detail-info-url-edit').attr("href", '/payments/editbalance/' + $(this).attr("data-key"));
        // $('.client-detail-info-url-delete').attr("href", '/clients/delete/' + $(this).attr("data-key"));

        $.ajax({ 
            type: 'GET', 
            url: '/clients/viewbalance/' + $(this).attr("data-key"), 
            data: {}, 
            dataType: 'json',
            success: function (data) { 
                if(data.client.client_id != ''){
                    $('.detail-info-id').text('88 - ' + data.client.client_id);
                }else{
                    $('.detail-info-id').text('');
                }
                $('.detail-info-name').text(data.client.name);
                $('.detail-info-manager').text(data.client.manager_id);
                
                $('.detail-info-balance').text(data.client.balance);
                $('.detail-info-balance_total').text(data.client.balance_total);

                //////

                $('.client_money_all').html('');
                $('.client_money_all_block').css({'display':'none'});
                if(data.transfers != 0){
                    $('.client_money_all_block').css({'display':'block'});
                    $(data.transfers).each(function(index,val) {
                        if(val.payment_type == 1){
                            var css_cash_balance = 'color:green;';
                        }else{
                            var css_cash_balance = 'color:red;';
                        }
                        $('.client_money_all').html(
                            $('.client_money_all').html() + 
                            '<div class="detail-info-list-row">' +
                                '<div class="detail-info-list-column">'+val.date_payment+'</div>' +
                                '<div class="detail-info-list-column" style="'+css_cash_balance+'">'+val.cash_usd+'</div>' +
                                '<div class="detail-info-list-column">'+val.project_id+'</div>' +
                                '<div class="detail-info-list-column">'+val.balance+'</div>' +
                                '<div class="detail-info-list-column">'+val.comment+'</div>' +
                            '</div>'
                        );
                    });
                }
                
                //////
                $('.client-detail-info-url-edit').attr("href", '/payments/editbalance/' + data.client.client_id + '?' + window.location.search.replace( '?', ''));
                $('.delete-post-url').attr("data-id", data.client.client_id);
                ////buttons navigations
                $('#slide-position-left').attr("data-id", data.client.client_id);
                $('#slide-position-right').attr("data-id", data.client.client_id);
            }
        });
    });


    $(document).on('click', '.table tbody .shipment-row .eye-click-view-row', function () {
        var cur_id = $(this).attr("data-key");
        var re, s;
        s = cur_id;
        re = /\D+/ig;
        index = s.replace(re, '');

        $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
        $('.detail-info-top-line').animate({
            opacity: 1
        }, 700);
        $('.detail-info-top-line').css({
            "display": "flex"
        });
        $(".detail-info-wrap").find('.detail-info').fadeIn(700).addClass("detail-visibe");

        $('.shipment-detail-info-url-edit').attr("href", '/shipments/edit/' + $(this).attr("data-key"));

        $.ajax({ 
            type: 'GET', 
            url: '/shipments/view/' + $(this).attr("data-key"), 
            data: {}, 
            dataType: 'json',
            success: function (data) { 
                $('.detail-info-id').text(data.shipment_id);
                $('.detail-info-name').text(data.name);
                $('.detail-info-manager').text(data.manager_id);
                $('.detail-info-from').text(data.from_id);
                $('.detail-info-to').text(data.to_id);
                $('.detail-info-status').text(data.status_id);
                $('.detail-info-type').text(data.type_id);
                $('.detail-info-date').text(data.date_created);
                $('.detail-info-date_received').text(data.date_received);
                $('.detail-info-tracking').text(data.link);
                if (data.link.length > 5){
                    var linkSplit = data.link.split('-');
                    $('.detail-info-tracking-link').html('<a href="http://www.silkwaywest.com/pages/awb.php?awb='+linkSplit[1]+'&pfx='+linkSplit[0]+'" target="_blank">link</a>');
                }else{
                    $('.detail-info-tracking-link').html('');
                }
                $('.detail-info-time').text(data.time);
                $('.detail-info-weight').text(data.weight);
                $('.detail-info-volume').text(data.volume);
                $('.detail-info-cartons').text(data.cartons);
                $('.detail-info-comment').text(data.comment);
                $('.detail-info-checkpoint').text(data.checkpoint);
                $('.detail-info-price').text(data.price);
                $('.detail-info-price_customs').text(data.price_customs);
                $('.detail-info-profit').text(data.profit);
                $('.detail-info-awb').html(data.awb);
                $('.detail-info-invoice').html(data.invoice);
                $('.detail-info-packing_list').html(data.list);
                $('.detail-info-other').html(data.other);
                //////
                $('.shipment-detail-info-url-edit').attr("href", '/shipments/edit/' + data.shipment_id + '?' + window.location.search.replace( '?', ''));
                $('.delete-post-url').attr("data-id", data.shipment_id);
                ////buttons navigations
                $('#slide-position-left').attr("data-id", data.shipment_id);
                $('#slide-position-right').attr("data-id", data.shipment_id);
            }
        });
    });

    $(document).on('click', '.table tbody .payment-row .eye-click-view-row', function () {
        var cur_id = $(this).attr("data-key");
        var re, s;
        s = cur_id;
        re = /\D+/ig;
        index = s.replace(re, '');

        $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
        $('.detail-info-top-line').animate({
            opacity: 1
        }, 700);
        $('.detail-info-top-line').css({
            "display": "flex"
        });
        $(".detail-info-wrap").find('.detail-info').fadeIn(700).addClass("detail-visibe");

        $('.payment-detail-info-url-edit').attr("href", '/payments/edit/' + $(this).attr("data-key"));

        $.ajax({ 
            type: 'GET', 
            url: '/payments/view/' + $(this).attr("data-key"), 
            data: {}, 
            dataType: 'json',
            success: function (data) { 
                $('.detail-info-manager').text(data.payment.manager_id);
                $('.detail-info-date').text(data.payment.date_payment);
                $('.detail-info-type').text(data.payment.payment_type);
                $('.detail-info-source').text(data.payment.source_id);
                $('.detail-info-outsource').text(data.payment.out_source_id);
                $('.detail-info-payment_status').text(data.payment.payment_status);
                $('.detail-info-status').text(data.payment.status);
                $('.detail-info-comment').text(data.payment.comment);
                $('.detail-info-cash_usd').text(data.payment.cash_usd);
                $('.detail-info-cash_fuct').text(data.payment.cash_fact);
                $('.detail-info-currency').text(data.payment.cash_fact_currency);
                $('.detail-info-branch').text(data.payment.branch_id);
                $('.detail-info-client').text(data.payment.client_id);
                if(data.payment.payment_id != ''){
                    $('.detail-info-id').text('PAY - ' + data.payment.payment_id);
                }else{
                    $('.detail-info-id').text('');
                }
                ///////
                $('.client_money_all').html('');
                $('.client_money_all_block').css({'display':'none'});
                if(data.transfers != 0){
                    $('.client_money_all_block').css({'display':'block'});
                    $(data.transfers).each(function(index,val) {
                        if(val.payment_type == 1){
                            var css_cash_balance = 'color:green;';
                        }else{
                            var css_cash_balance = 'color:red;';
                        }
                        $('.client_money_all').html(
                            $('.client_money_all').html() + 
                            '<div class="detail-info-list-row">' +
                                '<div class="detail-info-list-column">'+val.date_payment+'</div>' +
                                '<div class="detail-info-list-column" style="'+css_cash_balance+'">'+val.cash_usd+'</div>' +
                                '<div class="detail-info-list-column">'+val.project_id+'</div>' +
                                '<div class="detail-info-list-column">'+val.balance+'</div>' +
                                '<div class="detail-info-list-column">'+val.comment+'</div>' +
                            '</div>'
                        );
                    });
                }
                ////////
                $('.payment-detail-info-url-edit').attr("href", '/payments/edit/' + data.payment.payment_id + '?' + window.location.search.replace( '?', ''));
                $('.delete-post-url').attr("data-id", data.payment.payment_id);
                ////buttons navigations
                $('#slide-position-left').attr("data-id", data.payment.payment_id);
                $('#slide-position-right').attr("data-id", data.payment.payment_id);
            }
        });
    });

    $(document).on('click', '.table tbody .project-row .eye-click-view-row', function () {
        var cur_id = $(this).attr("data-key");
        var re, s;
        s = cur_id;
        re = /\D+/ig;
        index = s.replace(re, '');

        $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
        $('.detail-info-top-line').animate({
            opacity: 1
        }, 700);
        $('.detail-info-top-line').css({
            "display": "flex"
        });
        $(".detail-info-wrap").find('.detail-info').fadeIn(700).addClass("detail-visibe");

        $('.shipment-detail-info-url-edit').attr("href", '/projects/edit/' + $(this).attr("data-key"));

        $.ajax({ 
            type: 'GET', 
            url: '/projects/view/' + $(this).attr("data-key"), 
            data: {}, 
            dataType: 'json',
            success: function (data) { 
                $('.detail-info-id').text(data.project.project_id);
                $('.detail-info-manager').text(data.project.manager_id);
                $('.detail-info-client').text(data.project.client_id);
                $('.detail-info-status').text(data.project.status);
                $('.detail-info-name').text(data.project.name);
                $('.detail-info-type').text(data.project.project_type);
                $('.detail-info-delivery').text(data.project.delivery_type);
                $('.detail-info-comment').text(data.project.comment);
                $('.detail-info-date').text(data.project.date_create);
                $('.detail-info-date_closed').text(data.project.date_close);
                $('.detail-info-total').text(data.project.total);
                $('.detail-info-profit').text(data.project.profit);
                $('.detail-info-insurance_sum').text(data.project.client_id);

                $('.detail-info-payed').text(data.project.payed);
                $('.detail-info-must_pay').text(data.project.must_pay);
                $('.detail-info-chargers').text(data.project.chargers);
                $('.detail-info-bonuse').text(data.project.plan_bonuse);
                $('.detail-info-cost').text(data.project.cargo_cost);
                $('.detail-info-insurance').text(data.project.insurance);
                $('.detail-info-insurance_sum').text(data.project.insurance_sum);

                if(data.project.project_id != ''){
                    $('.detail-info-id').text('PJ - ' + data.project.project_id);
                }else{
                    $('.detail-info-id').text('');
                }
                if(data.project.buying_id != ''){
                    $('.detail-info-buying').text('BL - ' + data.project.buying_id);
                }else{
                    $('.detail-info-buying').text('');
                }

                $('.project_cargoes_all').html('');
                $('.project_cargoes_all_block').css({'display':'none'});
                if(data.cargos != 0){
                    $('.project_cargoes_all_block').css({'display':'block'});
                    $(data.cargos).each(function(index,val) {
                        $('.project_cargoes_all').html(
                            $('.project_cargoes_all').html() + 
                            '<div class="detail-info-list-row">' +
                                '<div class="detail-info-list-column">CC-'+val.cargo_id+'</div>' +
                                '<div class="detail-info-list-column">'+val.weight+'</div>' +
                                '<div class="detail-info-list-column">'+val.volume+'</div>' +
                                '<div class="detail-info-list-column">'+val.volume_weight+'</div>' +
                                '<div class="detail-info-list-column">'+val.cartons+'</div>' +
                                '<div class="detail-info-list-column">'+val.local_delivery_price+'¥, '+val.local_expenses+'</div>' +
                            '</div>'
                        );
                    });
                }

                ////////
                $('.project-detail-info-url-edit').attr("href", '/projects/edit/' + data.project.project_id + '?' + window.location.search.replace( '?', ''));
                $('.delete-post-url').attr("data-id", data.project.project_id);
                ////buttons navigations
                $('#slide-position-left').attr("data-id", data.project.project_id);
                $('#slide-position-right').attr("data-id", data.project.project_id);
            }
        });
    });

    $(document).on('click', '.table tbody .buying-row .eye-click-view-row', function () {
        var cur_id = $(this).attr("data-key");
        var re, s;
        s = cur_id;
        re = /\D+/ig;
        index = s.replace(re, '');

        $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
        $('.detail-info-top-line').animate({
            opacity: 1
        }, 700);
        $('.detail-info-top-line').css({
            "display": "flex"
        });
        $(".detail-info-wrap").find('.detail-info').fadeIn(700).addClass("detail-visibe");

        $('.shipment-detail-info-url-edit').attr("href", '/buying/edit/' + $(this).attr("data-key"));

        $.ajax({ 
            type: 'GET', 
            url: '/buying/view/' + $(this).attr("data-key"), 
            data: {}, 
            dataType: 'json',
            success: function (data) { 
                $('.detail-info-id').text('BL-'+data.buying_id);
                $('.detail-info-date-create').text(data.date_create);
                $('.detail-info-client').text(data.client_id);
                $('.detail-info-manater').text(data.manager_id);
                $('.detail-info-manater-buying').text(data.manager_buy_id);
                $('.detail-info-project').text(data.project_id);
                $('.detail-info-status-id').text(data.status_id);
                $('.detail-info-total').text(data.total);
                $('.detail-info-name').text(data.name);
                $('.detail-info-comment').text(data.comment);
                $('.detail-info-file-manager').html(data.date_file);
                $('.detail-info-file-client').html(data.date_file_two);
                $('.detail-info-status').text(data.status);
                ////////
                $('.project-detail-info-url-edit').attr("href", '/buying/edit/' + data.buying_id + '?' + window.location.search.replace( '?', ''));
                $('.delete-post-url').attr("data-id", data.buying_id);
                ////buttons navigations
                $('#slide-position-left').attr("data-id", data.buying_id);
                $('#slide-position-right').attr("data-id", data.buying_id);
            }
        });
    });

    $("#close-detail-info").on('click', function() {
        $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
        $('.detail-info-top-line').animate({
            opacity: 0
        }, 700);
        $('.detail-info-top-line').css({
            "display": "none"
        });
    });
    
    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".detail-info"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            if($('.detail-info').hasClass("detail-visibe")){
                $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
                $('.detail-info-top-line').animate({
                    opacity: 0
                }, 700);
                $('.detail-info-top-line').css({
                    "display": "none"
                });
            }
        }
    });

    // $("#slide-left").on("click", function() {

    //     if (index == 0) {
    //         index = $('.detail-info').length - 1;
    //     } else {
    //         index--;
    //     }

    //     $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
    //     $(".detail-info-wrap").find('.detail-info[id = order-' + index + ']').fadeIn(700).addClass("detail-visibe");
    // });

    // $("#slide-right").on("click", function() {

    //     if (index == $('.detail-info').length - 1) {
    //         index = 0;
    //     } else {
    //         index++;
    //     }

    //     $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
    //     $(".detail-info-wrap").find('.detail-info[id = order-' + index + ']').fadeIn(700).addClass("detail-visibe");
    // });

    // Graphic



    // Task click
    $('.task-for-day ul li label .checkmark-task').on("click", function() {
        $(this).toggleClass('task-checked');
        $(this).parent('.custom-input').find('.checkmark-text p').toggleClass('custom-input-checked');
    });

    // $(".tasks-popup .tasks-popup-task .tasks-popup-task-check").on("click", function() {
    //     $(this).toggleClass('tasks-popup-task-check-checked');
    //     $(this).parent('.tasks-popup-task').find('p').toggleClass('line-through');

    // });

    // Date picker
    $(".add-task-tag-term-click").datetimepicker({
        format: 'd-M-Y H:i',
        monthNames: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
    });

    $("#form-choose-date-time").datetimepicker({
        format: 'd-M-Y H:i',
        monthNames: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
    });

    $("#date").datetimepicker({
        format: 'd-M-Y H:i',
        monthNames: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
    });

    $("#date-sm").datetimepicker({
        format: 'd M H:i',
        monthNames: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
    });

    $("#form-choose-date").datetimepicker({
        format: 'd-M-Y',
        monthNames: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
    });

    $("#form-choose-date-1").datetimepicker({
        format: 'd-M-Y',
        monthNames: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
    });

    $("#form-choose-date-2").datetimepicker({
        format: 'd-M-Y',
        monthNames: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
    });

    $("#form-choose-date-3").datetimepicker({
        format: 'd-M-Y',
        monthNames: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
    });

    $("#form-choose-date-4").datetimepicker({
        format: 'd-M-Y',
        monthNames: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
    });


    $(".add-task-tag-term a").on("click", function(e) {
        e.preventDefault();
    });

    // Tag picker
    $("#tag-choose").on("click", function() {
        if ($('.tag-choose ul').is(':visible')) {
            $('.tag-choose ul').fadeOut(500);
        } else {
            $('.tag-choose ul').fadeIn(500);
        }
    });

    $('.tag-choose ul li').on("click", function() {
        var color = $(this).css('background-color');
        $('#tag-choose').css('background-color', color);
    });

    $("#task-terms-popup-pick").on('click', function() {
        if ($('.tag-choose-popup ul').is(':visible')) {
            $('.tag-choose-popup ul').fadeOut(500);
        } else {
            $('.tag-choose-popup ul').fadeIn(500);
        }
    });

    $('.tag-choose-popup ul li').on("click", function() {
        var color = $(this).css('background-color');
        $('#task-terms-popup-pick').addClass('tag-picked');
        $('#task-terms-popup-pick').css({
            'background': color,
            'color': '#fff',
            'line-height': '22px'
        });
    });

    $("#task-terms-popup-pick-sm").on('click', function() {
        if ($('.tag-choose-popup-sm ul').is(':visible')) {
            $('.tag-choose-popup-sm ul').fadeOut(500);
        } else {
            $('.tag-choose-popup-sm ul').fadeIn(500);
        }
    });

    $('.tag-choose-popup-sm ul li').on("click", function() {
        var color = $(this).css('background-color');
        $('#task-terms-popup-pick-sm').addClass('tag-picked');
        $('#task-terms-popup-pick-sm').css({
            'background': color,
            'color': '#fff',
            'line-height': '22px'
        });
    });

    ////drop-menu

    $(".dropdown-menu-block").hover(function(){
        $('.'+$(this).attr("data-dropdown_class")).show();
    },function(){
        $('.'+$(this).attr("data-dropdown_class")).hide();
    });

    // Search AJAX
    $(".search-form").click(function(){
        $(".tasks-popup").fadeOut(500);
        $(".notif").fadeOut(500);
        $('.user-set').fadeOut(500);
        $(".search-popup, .popups-overlay").fadeIn(500);
        $('.search-popup-title-desktop').hide();
        if($('.search-block-html').html() == ''){
            $('.search-block-html').html('Please, enter at least 3 characters!');
        }
    });
    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".search-popup"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            $(".search-popup, .popups-overlay").fadeOut(500); // скрываем его
        }
    });
    $('input[name=search-type]').change(function() {
        runSearch();
    });
    $("#open-search").keyup(function(){
        runSearch();
    });

    function runSearch(){
        if($("#open-search").val().length >= 3){
            $('.search-popup-title-desktop').show();
            $('.search-block-html').html('');
            $.ajax({ 
                type: 'POST', 
                url: '/search/index', 
                data: {
                    type:   $('input[name=search-type]:checked').val(),
                    search: $('#open-search').val(),
                }, 
                dataType: 'json',
                success: function (data) { 
                    $('.search-block-html').html('');
                    if($('input[name=search-type]:checked').val() == 1){
                        $('.search-block-html').append('<table>');
                        $('.search-block-html table').append('<tr><td>CLIENT ID</td><td>BALANCE</td><td>NAME</td><td>EMAIL</td><td>PHONE</td></tr>');
                        var len = data.length;
                        for(var obj=0;obj<len;obj++ ){
                            $('.search-block-html table').append('<tr><td><a href="/clients/edit/'+data[obj].client_id+'">'+data[obj].client_id+'</a></td><td>'+data[obj].balance+'</td><td>'+data[obj].name+'</td><td>'+data[obj].email+'</td><td>'+data[obj].phone_code+''+data[obj].phone+'</td></tr>');
                        }
                        $('.search-block-html').append('</table>');
                        $('.search-popup-title-desktop').hide();
                    }
                    else if($('input[name=search-type]:checked').val() == 2){
                        $('.search-block-html').append('<table>');
                        $('.search-block-html table').append('<tr><td>CARGO ID</td><td>CLIENT ID</td><td>PROJECT ID</td><td>SHIPMENT ID</td><td>DESCRIPTION</td><td>LOCAL TRACK</td></tr>');
                        var len = data.length;
                        for(var obj=0;obj<len;obj++ ){
                            $('.search-block-html table').append('<tr><td><a href="/cargo/edit/'+data[obj].cargo_id+'">'+data[obj].cargo_id+'</a></td><td>'+data[obj].client_id+'</td><td>'+data[obj].project_id+'</td><td>'+data[obj].shipment_id+'</td><td>'+data[obj].description+'</td><td>'+data[obj].local_track+'</td></tr>');
                        }
                        $('.search-block-html').append('</table>');
                        $('.search-popup-title-desktop').hide();
                    }
                    else if($('input[name=search-type]:checked').val() == 3){
                        $('.search-block-html').append('<table>');
                        $('.search-block-html table').append('<tr><td>PROJECT ID</td><td>CLIENT ID</td><td>NAME</td><td>COMMENT</td></tr>');
                        var len = data.length;
                        for(var obj=0;obj<len;obj++ ){
                            $('.search-block-html table').append('<tr><td><a href="/projects/edit/'+data[obj].project_id+'">'+data[obj].project_id+'</a></td><td>'+data[obj].client_id+'</td><td>'+data[obj].name+'</td><td>'+data[obj].comment+'</td></tr>');
                        }
                        $('.search-block-html').append('</table>');
                        $('.search-popup-title-desktop').hide();
                    }

                    else if($('input[name=search-type]:checked').val() == 4){
                        $('.search-block-html').append('<table>');
                        $('.search-block-html table').append('<tr><td>SHIPMENT ID</td><td>COMMENT</td></tr>');
                        var len = data.length;
                        for(var obj=0;obj<len;obj++ ){
                            $('.search-block-html table').append('<tr><td><a href="/shipments/edit/'+data[obj].shipment_id+'">'+data[obj].shipment_id+'</a></td><td>'+data[obj].comment+'</td></tr>');
                        }
                        $('.search-block-html').append('</table>');
                        $('.search-popup-title-desktop').hide();
                    }

                    else if($('input[name=search-type]:checked').val() == 5){
                        $('.search-block-html').append('<table>');
                        $('.search-block-html table').append('<tr><td>PAYMENT ID</td><td>CLIENT ID</td><td>STATUS</td><td>COMMENT</td></tr>');
                        var len = data.length;
                        for(var obj=0;obj<len;obj++ ){
                            if(data[obj].status == 1){
                                status = 'Approved';
                            }
                            else{
                                status = 'No approved';
                            }
                            $('.search-block-html table').append('<tr><td><a href="/payments/edit/'+data[obj].payment_id+'">'+data[obj].payment_id+'</a></td><td>'+data[obj].client_id+'</td><td>'+status+'</td><td>'+data[obj].comment+'</td></tr>');
                        }
                        $('.search-block-html').append('</table>');
                        $('.search-popup-title-desktop').hide();
                    }
                    console.log(data);
                }
            });
            

        } 
        else{
            $('.search-popup-title-desktop').hide();
            $('.search-block-html').html('Please, enter at least 3 characters!');
        }
    }
    // $("#open-search").on("focus", function() {
    //     $(".tasks-popup").fadeOut(500);
    //     $(".notif").fadeOut(500);
    //     $('.user-set').fadeOut(500);
    //     $(".search-popup, .popups-overlay").fadeIn(500);
    // });

    // $(".search-form input").on("blur", function() {
    //     $(".search-popup, .popups-overlay").fadeOut(500);
    // });

    $("#open-search-sm").on("focus", function() {
        $(".tasks-popup-sm").fadeOut(500);
        $(".notif-sm").fadeOut(500);
        $('.user-set-sm').fadeOut(500);
        $(".search-popup-sm").fadeIn(500);
    });

    $("#open-search-sm").on("blur", function() {
        $(".search-popup-sm").fadeOut(500);
    });

    // Tasks open
    $('#tasks-popup-open').on('click', function(e) {
        $(".notif, .user-set").fadeOut(500);
        if ($('.tasks-popup').is(':visible') && $(".tasks-popup").has(e.target).length === 0) {
            $('.tasks-popup').fadeOut(500);
        } else {
            $('.tasks-popup').fadeIn(500);
        }
    });

    $('#tasks-popup-open-sm').on('click', function(e) {
        $(".notif-sm, .user-set-sm").fadeOut(500);
        if ($('.tasks-popup-sm').is(':visible') && $(".tasks-popup-sm").has(e.target).length === 0) {
            $('.tasks-popup-sm').fadeOut(500);
        } else {
            $('.tasks-popup-sm').fadeIn(500);
        }
    });

    // Notif open
    $('#notif-open').on('click', function(e) {
        $(".tasks-popup, .user-set").fadeOut(500);
        if ($('.notif').is(':visible') && $(".notif").has(e.target).length === 0) {
            $('.notif').fadeOut(500);
        } else {
            $('.notif').fadeIn(500);
        }
    });

    $('#notif-open-sm').on('click', function(e) {
        $(".tasks-popup-sm, .user-set-sm").fadeOut(500);
        if ($('.notif-sm').is(':visible') && $(".notif-sm").has(e.target).length === 0) {
            $('.notif-sm').fadeOut(500);
        } else {
            $('.notif-sm').fadeIn(500);
        }
    });

    // user set open
    $('#user-set-open').on('click', function(e) {
        $(".tasks-popup, .notif").fadeOut(500);
        if ($('.user-set').is(':visible') && $(".user-set").has(e.target).length === 0) {
            $('.user-set').fadeOut(500);
        } else {
            $('.user-set').fadeIn(500);
        }
    });

    $('#user-set-open-sm').on('click', function(e) {
        $(".tasks-popup-sm, .notif-sm").fadeOut(500);
        if ($('.user-set-sm').is(':visible') && $(".user-set-sm").has(e.target).length === 0) {
            $('.user-set-sm').fadeOut(500);
        } else {
            $('.user-set-sm').fadeIn(500);
        }
    });

    /**
     * @function removeClass
     * @description remove class from Dom element/s
     * @param {Object} elem - Dom element
     * @param {String} className - class name to remove
     *
     **/
    function removeClass(elem, className) {
        let l = elem.length;

        if (l == undefined) {
            _removeClass(elem, className);
        } else {
            let i = l - 1;

            while (i >= 0) {
                _removeClass(elem[i], className);
                i--;
            }
        }
    }

    /**
     * @function _removeClass
     * @description internal method to remove class from Dom element
     * @param {Object} elem - Dom element
     * @param {String} newClass - class name to remove
     *
     **/
    function _removeClass(elem, newClass) {
        if (elem.classList) {
            elem.classList.remove(newClass);
        } else {
            let exp = '(^|\\b)' + newClass.split(' ').join('|') + '(\\b|$)';
            elem.className = elem.className.replace(new RegExp(exp, 'gi'), ' ');
        }
    }

    /**
     * @function addClass
     * @description add class to Dom element
     * @param {Object} elem - Dom element
     * @param {String} className - class name to add
     *
     **/
    function addClass(elem, className) {
        let l = elem.length;

        if (l == undefined) {
            _addClass(elem, className);
        } else {
            let i = l - 1;

            while (i >= 0) {
                _addClass(elem[i], className);
                i--;
            }
        }
    }


    /**
     * @function _addClass
     * @description internal method add class to Dom element
     * @param {Object} elem - Dom element
     * @param {String} newClass - class name to add
     *
     **/
    function _addClass(elem, newClass) {
        if (elem.classList) {
            elem.classList.add(newClass);
        } else {
            elem.className += ' ' + className;
        }
    }


    let loaderDashoffsetTotal = 502;
    let preloader = document.querySelector('.preloader');
    let preloaderOuter = preloader.querySelector('.outer');
    let logo = preloader.querySelector('.logo');
    let loaded = 0;
    let total = 10;

    function onProgress() {
        let percentLoaded = Math.round((loaded / total) * 100);
        let calc = (loaderDashoffsetTotal / 100);
        let percent = Math.round(calc * percentLoaded);
        let offset = loaderDashoffsetTotal - percent;
        preloaderOuter.style.strokeDashoffset = offset + 'px';
    }

    function init() {
        let startLength = loaderDashoffsetTotal + 'px';
        preloaderOuter.style.strokeDashoffset = startLength;
        preloaderOuter.style.opacity = 1;

        setTimeout(() => {
            let newLength = (loaderDashoffsetTotal) + 'px';
            preloaderOuter.style.strokeDashoffset = newLength;
            addClass(preloaderOuter, 'loading');
            loadImages();


        }, 500);
    }

    init();

    function loadImages() {

        load();


    }

    function load() {


        loaded++;
        onProgress();

        if (loaded == total) {
            setTimeout(() => {
                onDone();
            }, 1000);
        } else {
            setTimeout(() => {
                load();
            }, 100);
        }

    }

    function onDone() {
        addClass(preloader, 'out');
        removeClass(logo, 'fade-in');
        addClass(logo, 'fade-out');

        setTimeout(() => {
            loaded = 0;
            removeClass(preloader, 'out');
            addClass(logo, 'fade-in');
            removeClass(logo, 'fade-out');
            preloaderOuter.style.strokeDashoffset = loaderDashoffsetTotal + 'px';
            removeClass(preloaderOuter, 'loading');

            init();


        }, 1000);
    }

    $(".tasks-popup-controls-form-submit").click(function(){
        $.ajax({ 
            type: 'POST', 
            url: '/task/create', 
            data: {
                name: $('.tasks-popup-controls-form-input').val(),
                date: $('.tasks-popup-controls-form-date').val(),
            }, 
            success: function (data) { 
                if(data == 'error'){
                    alert('An error occurred while creating the task!');
                }else if(data == 1){
                    $('.dashboard-task-input').val('');
                    $('.tasks-popup-controls-form-input').val('');
                    loadTask();
                    loadTaskDashboard();
                }
            }
        });
    });

    /* For Task manager on main page */
     $(".task-submit").click(function(){
        $.ajax({ 
            type: 'POST', 
            url: '/task/create', 
            data: {
                name: $('.task-input').val(),
                date: $('.add-task-tag-term-click').val(),
            }, 
            success: function (data) { 
                if(data == 'error'){
                    alert('An error occurred while creating the task!');
                }else if(data == 1){
                    $('.dashboard-task-input').val('');
                    $('.tasks-popup-controls-form-input').val('');
                    loadTask();
                    loadTaskDashboard();
                }
            }
        });
    });   

    function loadTaskDashboard(){
        $.ajax({ 
            type: 'GET', 
            url: '/task/index?type=1', 
            data: {}, 
            success: function (data) { 
                $('.tasks-block-all-dashboard').html(data);
            }
        });
    }  
    /////////

    function loadTask(){
        $.ajax({ 
            type: 'GET', 
            url: '/task/index', 
            data: {}, 
            success: function (data) { 
                $('.tasks-block-all').html(data);
            }
        });
    }
    loadTask();
    loadTaskDashboard();

    $(document).on('click', '.tasks-popup-task-check', function () {
        $(this).toggleClass('tasks-popup-task-check-checked');
        $(this).parent('.tasks-popup-task').find('p').toggleClass('line-through');
        $.ajax({ 
            type: 'POST', 
            url: '/task/status', 
            data: {
                id: $(this).attr("data-id"),
            }, 
            success: function (data) { 
                loadTask(); 
                loadTaskDashboard();              
            }
        });
    });

    $('.dropdown-serach-click').click(function(){
        $('.search-filter-table').focus();
    });

    $(document).on('click', '#slide-position-right', function () {
        var btn_slide_right = $(this).attr("data-id");
        var btn_slide_right_count = 0;
        $('.eye-click-view-row').each(function() {
            if(btn_slide_right == $(this).attr('data-key')){
                btn_slide_right_count = 1;
            }
            else if(btn_slide_right_count == 1){
                $('.eye-click-view-row[data-key="'+$(this).attr('data-key')+'"]').trigger('click');
                btn_slide_right_count = 0;
            }
        });
    });

    $(document).on('click', '#slide-position-left', function () {
        var btn_slide_left = $(this).attr("data-id");
        var btn_slide_left_previous = 0;
        $('.eye-click-view-row').each(function() {
            if(btn_slide_left == $(this).attr('data-key')){
                $('.eye-click-view-row[data-key="'+btn_slide_left_previous+'"]').trigger('click');
            }
            btn_slide_left_previous = $(this).attr('data-key');
        });
    });

});
