<?php
require_once 'phpQuery.php';

$parse = new Parse;
$parse->PartsukraineCategory(1);
//echo $curlexit['content'];
//print_r($curlexit);

$arrayMenu = file_get_contents('json/arrayMenu.json');
echo $arrayMenu;

Class Parse
{
    public $menu_up_all = array();
    public $menu_down = array();
    public $productsAll = array();
    private $partsukraine = 'http://partsukraine.com/';
    private $partsukraine_cut = 'http://partsukraine.com';
    //private $kopylbros = 'http://kopylbros.com/catalogs';
    //private $useragent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36';

    public function PartsukraineCategory($type){
        switch ($type) {
            case 1:
                $this->parseMenu($this->partsukraine);
                $this->parseMenuDown($this->partsukraine);
                $this->parseAllArrayCategoryPage();
                ///запись меню в файл/////
                $arrayMenu = fopen('json/arrayMenu.json', 'w'); 
                fputs($arrayMenu, json_encode($this->menu_up_all)); 
                fclose($arrayMenu); 
                ////файл закрыт/////
                return json_encode($this->menu_up_all);
                break;
            
            case 2:
                $kopylbrosURL = $this->kopylbros;
                return $this->get_web_page($kopylbrosURL);
                break;

            case 3:
                //$this->parseProductPage($this->partsukraine_cut.'/catalog/gloves/52933/');
                break;
        }
    }

    private function parseMenu($url){
        $site = file_get_contents($url);
        $html = phpQuery::newDocument($site);
        foreach ($html->find('#g_menu-normal > .menu_dropdown') as $key => $ptn_menu_up){
            $ptn_menu_up = pq($ptn_menu_up);
            $href_class = str_replace("')", "", str_replace("main_menu_open('", "", $ptn_menu_up->find('a')->attr('onclick')));
            $this->menu_up_all[] = array(
                'id' => $key+1,
                'name' => $ptn_menu_up->find('a')->text(),
                'href_class' => $href_class,
            );
        }
        return $this->menu_up_all;
    }

    private function parseMenuDown($url){
        $site = file_get_contents($url);
        $html = phpQuery::newDocument($site);
        foreach ($html->find('.g_menu-opened') as $key => $ptn_menu_down){
            $ptn_menu_down = pq($ptn_menu_down);
            foreach ($ptn_menu_down->find('.opened_sections > li > a') as $key2 => $opened_sections_li){
                //$opened_sections = pq($opened_sections);
                //foreach ($opened_sections->find('li > a') as $opened_sections_li){
                    $opened_sections_li = pq($opened_sections_li);
                    $opened_sections_li_pod = $opened_sections_li->find('ul')->attr('class');
                    if($opened_sections_li_pod != 'opened_subsections'){ 
                        $this->menu_up_all[$key]['categories'][] = array(
                            'id' => (($key+1)*100)+$key2,
                            'href' => $this->partsukraine_cut.$opened_sections_li->attr('href'),
                            'name' => $opened_sections_li->find('span')->text(),
                        ); 
                    }
                //}
            }
        }
        return $this->menu_up_all;
    }

    private function countCatalogPage($url){
        $site = file_get_contents($url);
        $html = phpQuery::newDocument($site);
        $count_pages = 1;
        ///count pages////
        foreach ($html->find('.modern-page-navigation a') as $pages){
            $pages = pq($pages);
            if(preg_replace('~[^0-9]+~','',$pages->text()) > $count_pages){
                $count_pages = preg_replace('~[^0-9]+~','',$pages->text());
            }
        }
        ////end count///
        return $count_pages;
    }

    private function parseCatalogPage($url, $productsAll, $category_id){
        $site = file_get_contents($url);
        $html = phpQuery::newDocument($site);
        ///parse link product////
        foreach ($html->find('.g_productcard') as $product){
            $product = pq($product);
            $product_id = preg_replace('~[^0-9]+~','',$product->attr('href'));
            $productsAll[$product_id] = array(
                'id' => $product_id,
                'category_id' => $category_id,
                'href' => $this->partsukraine_cut.$product->attr('href'),
            ); 
        }
        ////end parse link product///
        return $productsAll;
    }

    private function foreachCatalogPage($url, $category_id){
        $productsAll = array();
        $countCatalogPage = $this->countCatalogPage($url);
        //if($countCatalogPage > 1){
        //    $countCatalogPage = 1;
        //}
        for ($page=1; $page <= $countCatalogPage; $page++) { 
            $productsAll = $this->parseCatalogPage($url.'?PAGEN_1='.$page, $productsAll, $category_id);
        }
        return $productsAll;
    }

    private function parseAllArrayCategoryPage(){
        foreach ($this->menu_up_all as $count_categories => $categories) {
            foreach ($categories['categories'] as $count_category => $category) {
                //echo $category['name'].','.$category['href'].'<br>';
                $this->menu_up_all[$count_categories]['categories'][$count_category]['products'] = $this->foreachCatalogPage($category['href'], (($count_categories+1)*100)+$count_category);

            }
        }
        return '';
    }

    private function parseProductPage($url){
        $site = file_get_contents($url);
        $html = phpQuery::newDocument($site);
        ///parse  product////
            //$title = $html->find('h1')->text();
            //$title = $html->find('.catalog-product-price  catalog-product-price-UAH > .text_price')->text();
            //$box_specs = $html->find('.box_specs')->html();
            //$box_description = $html->find('.box_description')->html();
        // foreach ($html->find('.g_productcard') as $product){
        //     $product = pq($product);
        $product = array(
            'title' => $html->find('h1')->text(),
            'price' => $html->find('.catalog-product-price-UAH > .text_price')->text(),
            'options' => $html->find('.box_specs')->html(),
            'description' => $html->find('.box_description')->html().$html->find('.box_extdescription')->html(),
        );
        var_export($product);
        return '';
    }

    // private function get_web_page($url)
    // {
    //     $user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

    //     $options = array(

    //         CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
    //         CURLOPT_POST           =>false,        //set to GET
    //         CURLOPT_USERAGENT      => $this->useragent, //set user agent
    //         CURLOPT_COOKIEFILE     =>"cookie/kopylbros.txt", //set cookie file
    //         CURLOPT_COOKIEJAR      =>"cookie/kopylbros.txt", //set cookie jar
    //         CURLOPT_RETURNTRANSFER => true,     // return web page
    //         CURLOPT_HEADER         => false,    // don't return headers
    //         CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    //         CURLOPT_ENCODING       => "",       // handle all encodings
    //         CURLOPT_AUTOREFERER    => true,     // set referer on redirect
    //         CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
    //         CURLOPT_TIMEOUT        => 120,      // timeout on response
    //         CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
    //     );

    //     $ch      = curl_init( $url );
    //     curl_setopt_array( $ch, $options );
    //     $content = curl_exec( $ch );
    //     $err     = curl_errno( $ch );
    //     $errmsg  = curl_error( $ch );
    //     $header  = curl_getinfo( $ch );
    //     curl_close( $ch );

    //     $header['errno']   = $err;
    //     $header['errmsg']  = $errmsg;
    //     $header['content'] = $content;
    //     return $header;
    // }
}