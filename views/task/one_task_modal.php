<div class="tasks-popup-task">
    <span class="tasks-popup-task-check <?=($task->status == 1) ? 'tasks-popup-task-check-checked' : ''?>" data-id="<?=$task->task_id?>"></span>
    <p class="<?=($task->status == 1) ? 'line-through' : ''?>"><?=$client?><?=$task->description?></p>
    <div class="tasks-popup-task-tag-time-wrap">
        <div class="tasks-popup-task-tag" style="background-color: <?=$tag?>;"></div>
        <div class="tasks-popup-task-time"><i class="far fa-clock"></i><?=date('d.m.Y H:i', $task->date_end)?></div>
    </div>
    <!-- <div class="edit-task">
        <a><i class="fas fa-pen"></i></a>
    </div> -->
</div>