<div class="options">
    <div class="choose-country d-sm-none d-md-none d-lg-none d-xl-none" id="country-click">
        <a href="#" class="choose-country-click">Choose Buying lists <span><i class="fas fa-caret-right"></i></span></a>
        <ul id="sm-countries">
            <li><a href="/buying/create"><i class="fa fa-plus" aria-hidden="true"></i> Add buying</a></li>
            <li><a href="/buying/index"><i class="fa fa-bars" aria-hidden="true"></i> All buying lists</a></li>
        </ul>
    </div>

    <ul id="countries">
        <li><a href="/buying/create" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i> Add buying</a></li>
        <li><a href="/buying/index"><i class="fa fa-bars" aria-hidden="true"></i> All buying lists</a></li>
    </ul>
</div>