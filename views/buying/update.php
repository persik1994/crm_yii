<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Edit Buying list');
?>

    <?=$menu?>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">Edit Buying list</div>
            </div>

            <div class="add-client-form-wrap">
                <form>

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Manager <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option value="<?=$manager->manager_id?>" <?= ($manager->manager_id == $model->manager_id) ? 'selected' : '' ?>><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Manager buyer<span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager-buyer selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option value="<?=$manager->manager_id?>" <?= ($manager->manager_id == $model->manager_buy_id) ? 'selected' : '' ?>><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Buying code (BL)</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" disabled="disabled" class="input-code"  value="BL-<?= (!empty($model->buying_id)) ? $model->buying_id : '' ?>">
                                </div>
                            </div>
                            
                            <div class="add-client-form-line">
                                <label>Buying date create</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date" class="input-date-create" value="<?= (!empty($model->date_create)) ? date('d-M-Y',$model->date_create) : '' ?>" autocomplete="off">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Buying status<span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-status" name="manager" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php for ($type_count=1; $type_count <= 6; $type_count++){ ?>
                                            <option value="<?=$type_count?>" <?= ($type_count == $model->status_id) ? 'selected' : '' ?>><?=$model->status($type_count)?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Buying name</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-name" value="<?=(!empty($model->name)) ? $model->name : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Comment</label>
                                <div class="input-addclient-wrap">
                                    <textarea name="name" class="input-comment"><?=(!empty($model->comment)) ? $model->comment : '' ?></textarea>
                                </div>
                            </div>

                            <div class="add-client-form-line add-client-form-line-file">
                                <label>Buying files</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                    <input type="file" placeholder="" class="file" id='upload-files' multiple="true">
                                    <textarea class="photo-base64" style="display: none;"></textarea>
                                </div>
                            </div>
                            <link href="/web/css/jquery.uploadPreviewer.css" rel="stylesheet" />
                            <script src="/web/js/jquery.uploadPreviewer.js"></script>
                            <script type="text/javascript">
                              $(document).ready(function() {
                                myUploadInput = $("#upload-files").uploadPreviewer();
                              });
                            </script>

                            <!-- <div class="add-client-form-line add-client-form-line-file">
                                <label>Buying file</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <input type="file" placeholder="" class="file" id="get-other-file">
                                    <textarea class="other-base64" style="display: none;"></textarea>
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                </div>
                            </div> -->
                            <?php if(empty($files_manager)){ ?>
                                <div class="get-other-file load-file"><img src="" id='preview_other' alt="">
                                    <span class="ava-name"></span>
                                    <span class="ava-size"></span>
                                    <span class="close-preview-other"><i class="fas fa-times"></i></span>
                                </div>
                            <?php } else { foreach ($files_manager as $file) { ?>
                                <div class="get-other-file load-file" style="height: auto;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhE..." id="preview_other" alt=""><i class="far fa-file"></i>
                                    <span class="ava-name"><a href="<?=$file->url?>" target="_blank"><?=str_replace('/uploads/files/', '', $file->url)?> <span style="color:#000;">Date:<?=date('d.m.Y H:i',$file->date)?>, <?=(!empty($file->description) ? 'Status - '.$file->description : '')?></span></a></span>
                                    <span class="ava-size"></span>
                                    <span class="close-preview-other" style="display: inline;"><a href="/buying/filesdelete?image_id=<?=$file->image_id?>"><i class="fas fa-times"></i></a></span>
                                </div>
                            <?php } } ?>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Client <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-client selectpicker" name="manager" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($clients as $client) { ?>
                                            <option value="<?=$client->client_id?>" <?= ($client->client_id == $model->client_id) ? 'selected' : '' ?>>88-<?=$client->client_id?> - <?=$client->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line select-project-block">
                                <label>Project</label>
                                <div class="input-addclient-wrap select-input select-input-project">
                                    <select class="select-project selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($projects as $project) { ?>
                                            <option value="<?=$project->project_id?>" <?= ($project->project_id == $model->project_id) ? 'selected' : '' ?>>PJ-<?=$project->project_id?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Total Summ USD</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-total" value="<?= (!empty($model->total)) ? $model->total : 0 ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line add-client-form-line-checkbox">
                                <label>
                                    <input type="checkbox" name="status" class="input-status" <?= ($model->status == 1) ? 'checked' : '' ?>> 
                                    <p>Approved</p>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="bottom-part row no-fw-row">
                        <div class="add-new-form-submit-buttons col-12">
                            <div class="add-new-form-submit-buttons-wrap col-4">
                                <div class="add-new-form-submit-buttons-wrap-width">
                                    <a class="submit-form" >Submit <img src="/web/img/preloader.gif" style="display: none;"></a>
                                    <a href="#" class="reset-form">Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        $('.mob-menu-Buying > a').addClass('active');

        $( ".select-client" ).change(function() {
            select_client();
        });

        function select_client(){
            $.ajax({ 
                url: '/cargo/clientsearch/'+$('.select-client option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data != 0){
                        $(".select-manager select").val(data);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });

            $.ajax({ 
                url: '/projects/projectselect/'+$('.select-client option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data != ''){
                        $('.select-project').remove();
                        $('.select-input-project').html(data);
                    }
                }
            });
        }

        $('.submit-form').click(function(){
            if(this.hasAttribute("disabled")){

            }
            else{
                //блок кнопки и прелоадер
                $(this).find('img').css('display','');
                $(this).attr('disabled', 'true');
                ///
                if ($(".input-status").is(":checked")) {
                    var input_status = 1;
                }
                else {
                    var input_status = 0;
                }

                var formData = new FormData();
                formData.append('manager',       $('.select-manager option:selected').val());
                formData.append('manager-buyer', $('.select-manager-buyer option:selected').val());
                formData.append('status_id',     $('.select-status option:selected').val());
                formData.append('client',        $('.select-client option:selected').val());
                formData.append('project',       $('.select-project option:selected').val());

                formData.append('name',              $('.input-name').val());
                formData.append('comment',           $('.input-comment').val());
                formData.append('total',             $('.input-total').val());
                formData.append('date-create',       $('.input-date-create').val());

                formData.append('status',  input_status);
                // Attach file
                //formData.append('file', document.querySelector('#get-other-file').files[0]);

                var photosAll = document.querySelector('#upload-files').files;
                for (var photosCount = 0; photosCount < photosAll.length; photosCount++) {
                    formData.append('file[]', photosAll[photosCount]);
                }

                $.ajax({ 
                    url: '/buying/edit/<?= (!empty($model->buying_id)) ? $model->buying_id : '0' ?>',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data) { 
                        if (data == 1) {
                            window.location.replace("/buying/index");
                        }
                    }
                });
            }
        });


        var other = new FileReader();

        $('.close-preview-other').on("click", function() {
            $('#get-other-file').val('');
            $('.get-other-file .ava-name').text('');
            $('.get-other-file .ava-size').text('');
            $("#preview_other").attr("src", "");
            $('.get-other-file').css('height', '0');
            $('.close-preview-other').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-other-file").change(function(event) {

            $('.close-preview-other').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-other-file .ava-name').text(filename[fileLen]);

            other.onload = function() {
                $('.get-other-file').css('height', 'auto');
                var preview = $("#preview_other");
                preview.attr("src", other.result);
                $('.other-base64').val(other.result);
            }

            other.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-other-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });
        
        // $(document).keyup(function(e) {
        //     if(e.which == 13){ //enter press
        //         $('.submit-form').trigger('click');
        //     }
        //     if (e.key === "Escape") { 
        //         $('.reset-form').trigger('click');
        //     }
        // });
        $('.reset-form').click(function(){
            location.reload();
        });
    </script>
