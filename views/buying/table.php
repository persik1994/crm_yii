<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Project;
use app\models\CustomTable;
use app\models\BuyingList;
use app\models\Images;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Buying lists');
//$this->params['breadcrumbs'][] = $this->title;
?>

    <?=$menu?>

    <style type="text/css">
        .table-bordered tbody{
          display:block;
          overflow:auto;
          height:200px;
          width:100%;
          overflow-x: hidden;
        }
        .table-bordered thead tr{
          display:block;
        }
    </style>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">

                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        All Buying lists * <?= BuyingList::SearchShowBtn() ?>
                    </div>
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                    <div class="add-new">
                        <div class="dropdown dropdown-table custom-table-div" style="">
                            <a class="custom-table-menu" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-cog"></i> Custom table
                            </a>
                            <div class="dropdown-menu dropdown-menu-table custom-table-checked" aria-labelledby="dropdownMenuLink">
                                <a class="custom-table-btn"><i class="fas fa-save"></i> Save setting</a><Br>
                                <?= CustomTable::menuOptions('buying', 'buying') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-wrap project-wrap">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        // 'rowOptions'=>function($model){
                        //     return ['class' => 'buying-row contextmenu-row', 'url-edit' => '/buying/edit/'.$model->buying_id, 'url-delete' => '/buying/delete/'.$model->buying_id];
                        // },
                        'rowOptions'=>function($model){
                            return ['class' => ($model->status_id == 5 || $model->status_id == 6)?'buying-row contextmenu-row payments-row-approve':'buying-row contextmenu-row', 'url-edit' => '/buying/edit/'.$model->buying_id, 'url-delete' => '/buying/delete/'.$model->buying_id];
                        },
                        'columns' => [
                            [
                                'attribute' => 'check',
                                'label' => '<input type="checkbox" class="checkbox-table-all" value="">',
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                "format" => 'raw',
                                'value' => function ($data) {
                                    return '<input type="checkbox" class="checkbox-table" value="'.$data->buying_id.'"> <i class="far fa-eye eye-click-view-row" title="View" data-key="'.$data->buying_id.'"></i><a href="/buying/edit/'.$data->buying_id.'" class="table-edit-btn"><i class="fas fa-pencil-alt"></i></a>'; 
                                },
                            ],
                            [
                                'attribute' => 'buying_id',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['buying_id'], 'buying_id', 'text', 'Write buying id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','buying_id')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','buying_id')],
                                'value' => function ($data) {
                                    return (!empty($data->buying_id)) ? 'BL-'.$data->buying_id : '';
                                },
                            ],
                            [
                                'attribute' => 'date_create',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['date_create'], 'date_create', 'date', 'Select date create'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','date_create')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','date_create')],
                                'value' => function ($data) {
                                    return (!empty($data->date_create)) ? date('d.m.Y',$data->date_create) : ''; 
                                },
                            ],
                            [
                                'attribute' => 'client_id',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['client_id'], 'client_id', 'text', 'Write client id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','client_id')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','client_id')],
                                'value' => function ($data) {
                                    return (!empty($data->client_id)) ? '88-'.$data->client_id : '';
                                },
                            ],
                            [
                                'attribute' => 'manager_id',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['manager_id'], 'manager_id', 'select', 'Select Manager'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','manager_id').CustomTable::widthTable(140)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','manager_id').CustomTable::widthTable(140)],
                                'value' => function ($data) {
                                    return $data->manager->username ?? '';
                                },
                            ],
                            [
                                'attribute' => 'manager_buy_id',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['manager_buy_id'], 'manager_buy_id', 'select', 'Select Manager'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','manager_buy_id').CustomTable::widthTable(140)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','manager_buy_id').CustomTable::widthTable(140)],
                                'value' => function ($data) {
                                    return $data->managerbuying->username ?? '';
                                },
                            ],
                            [
                                'attribute' => 'project_id',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['project_id'], 'project_id', 'text', 'Write project id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','manager_id').CustomTable::widthTable(140)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','manager_id').CustomTable::widthTable(140)],
                                'value' => function ($data) {
                                    return (!empty($data->project_id)) ? 'PJ-'.$data->project_id : '';
                                },
                            ],
                            [
                                'attribute' => 'status_id',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['status_id'], 'status_id', 'select', 'Select status'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','status_id').CustomTable::widthTable(120)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','status_id').CustomTable::widthTable(120)],
                                'value' => function ($data) {
                                    return BuyingList::status($data->status_id ?? '');
                                },
                            ],
                            [
                                'attribute' => 'total',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['total'], 'total', 'text', 'Write total'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','total').CustomTable::widthTable(140)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','total').CustomTable::widthTable(140)],
                                'value' => function ($data) {
                                    return $data->total ?? '0';
                                },
                            ],
                            [
                                'attribute' => 'name',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['name'], 'name', 'text', 'Write name'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','name').CustomTable::widthTable(200)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','name').CustomTable::widthTable(200)],
                                'value' => function ($data) {
                                    return $data->name ?? '';
                                },
                            ],
                            [
                                'attribute' => 'date_file',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['date_file'], 'date_file', 'date', 'Select date'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','date_file').CustomTable::widthTable(150)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','date_file').CustomTable::widthTable(150)],
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $all_files = '';
                                    $files_manager = Images::findAllImages(1,$data->buying_id);
                                    if(isset($files_manager)){
                                        foreach ($files_manager as $file) {
                                            $all_files .= '<a href="'.$file->url.'" target="_blank"><i class="fas fa-camera-retro"></i></a>';
                                        }
                                    }
                                    return $all_files; 
                                },
                            ],

                            [
                                'attribute' => 'date_file_two',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['date_file_two'], 'date_file_two', 'date', 'Select date'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','date_file_two').CustomTable::widthTable(150)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','date_file_two').CustomTable::widthTable(150)],
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return (!empty($data->file_two) && !empty($data->date_file_two)) ? '<a href="'.$data->file_two.'" target="_blank"><i class="fas fa-camera-retro"></i></a> '.date('d.m.Y',$data->date_file_two) : ''; 
                                },
                            ],
                            [
                                'attribute' => 'comment',
                                'label' => BuyingList::projectDropMenu(BuyingList::attributeLabels()['comment'], 'comment', 'text', 'Write comment'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('buying','buying','comment').CustomTable::widthTable(600)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('buying','buying','comment').CustomTable::widthTable(600)],
                                'value' => function ($data) {
                                    return $data->comment ?? '';
                                },
                            ],
                        ],
                    ]); ?>
                </div>

                <div class="detail-info-top-line">
                    <div class="detail-info-controls">
                        <a href="#" id="slide-position-left"><i class="fas fa-chevron-left"></i></a>
                        <a href="#" id="slide-position-right"><i class="fas fa-chevron-right"></i></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="" class="project-detail-info-url-edit"><i class="fas fa-pencil-alt"></i>Edit</a>
                            <a data-id="" class="delete-post-url" style="display: <?=(Yii::$app->user->identity->role >= 9)?';':'none;'?>"><i class="far fa-trash-alt"></i>Delete</a>
                            <a href="#" id="close-detail-info"><i class="fas fa-times"></i></a>
                        </div>
                    </div>
                </div>

                <div class="detail-info-wrap">
                    <div class="detail-info">
                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Buying code</div>
                                <div class="detail-info-list-column detail-info-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Date Create</div>
                                <div class="detail-info-list-column detail-info-date-create"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client</div>
                                <div class="detail-info-list-column detail-info-client"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manater"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager Buyer</div>
                                <div class="detail-info-list-column detail-info-manater-buying"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project</div>
                                <div class="detail-info-list-column detail-info-project"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Status</div>
                                <div class="detail-info-list-column detail-info-status-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Total</div>
                                <div class="detail-info-list-column detail-info-total"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Buying name</div>
                                <div class="detail-info-list-column detail-info-name"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Comment</div>
                                <div class="detail-info-list-column detail-info-comment"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Buying list approved</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">File Manager</div>
                                <div class="detail-info-list-column detail-info-file-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">File Client</div>
                                <div class="detail-info-list-column detail-info-file-client"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing">
                </div>
                <div class="col-6 summary">
                </div>
            </div>
        </div>
    </section>
    <nav id="context-menu" class="context-menu">
        <ul class="context-menu__items">
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="View"><i class="fa fa-eye"></i> View</a>
          </li>
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="Edit"><i class="fa fa-edit"></i> Edit</a>
          </li>
          <?php if(Yii::$app->user->identity->role >= 9){ ?>
              <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="Delete"><i class="fa fa-times"></i> Delete</a>
              </li>
          <?php } ?>
        </ul>
    </nav>

<script src="/web/js/contecst_menu.js"></script>
<script type="text/javascript">

    setInterval(function() {
        tableHeight();
    }, 500);

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        summ_height = window_height - ($('.all-cargos-title').height() + $('header').height() + $('.options').height() + 50);
        if(summ_height < 500){
            summ_height = 500;
        }
        $('.table-wrap').css({'max-height' : summ_height+'px'});
        $('.table-bordered').css({'max-height' : summ_height+'px'});
        $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
    }

    $('.mob-menu-Buying > a').addClass('active');

    $('.summ_bottom').html($('.summary').html());
    $('.summary').html('');


    $(document).on('keyup', '.dropdown-search-input', function (event) {
        if(event.keyCode==13) {
            runSearch($(this).attr("data-type"),$(this).val());
        }
    });

    $(document).on('click', '.search-menu-click', function () {
        runSearch($(this).attr("data-type"),$(this).attr("data-id"));
    });

    $(document).on('change', '#form-choose-date', function () {
        runSearch($(this).attr("data-type"),$(this).val());
    });

    function runSearch(type, value){
        $.ajax({ 
            type: 'POST', 
            url: '/buying/search?'+type+'='+value+'&sort=<?=Yii::$app->request->get('sort')?>', 
            success: function (data) { 
                $('.all-cargos-content').html(data);
            }
        });
    };
</script>
<script type="text/javascript">
    // $(window).resize(function() {
    //     tableHeight();
    // });

    // tableHeight();

    // function tableHeight(){
    //     window_height = $(window).height();
    //     body_height = $('body').height();
    //     table = $('.table-wrap').height();
    //     summ_height = window_height - (body_height - table) - 15;
    //     $('.table-wrap').css({'max-height' : summ_height+'px'});
    // }

    $(".delete-post-url").click(function() {
        var delete_text = confirm('Do you really want to delete buying list?');
        if(delete_text) window.location.replace('/buying/delete/'+$(this).attr("data-id"));
    });

    $('.custom-table-btn').click(function() {
        var sListType = '';
        $('.custom-table-checked input').each(function () {
            if(this.checked == "1"){
                sListType += $(this).val() + ",";
            }
        });
        if(sListType != ''){
            $.ajax({ 
                type: 'GET', 
                url: '/buying/customtable?page=buying&type='+sListType, 
                success: function (data) { 
                    location.reload();
                }
            });
        }
    });

    $(document).on('click', '.checkbox-table-all', function () {
        $('.checkbox-table').not(this).prop('checked', this.checked);
        allCargosSelectEdits(1);
    });

    $(document).on('click', '.checkbox-table', function () {
        if(this.checked == "1"){
            $('.buying-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
        }else{
            $('.buying-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
        }
        allCargosSelectEdits();
    });

    function allCargosSelectEdits(type = 0){
        var sList = '';
        ///////
        $('.checkbox-table').each(function () {
            if(this.checked == "1"){
                if(type == 1){
                    $('.buying-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
                }
                sList += $(this).val() + ",";
            }else if(type == 1){
                $('.buying-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
            }
        });
    }
</script>
