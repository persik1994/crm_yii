<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Director panel');
?>

<style>
  .progress-bar {
        background-color: #009688;
  }

</style>

<div class="row no-fw-row row-padding">
  <div class="col-md-12">
     <div class="dashboard-title">Director panel / Sales managers</div>
      <div class="row selectsaleadm">
        <p style="font-size: 17px; font-weight: 500; margin-right: 10px;">Select period:</p>
        <div class="dropdown">
          <a class="btn btn-secondary dropdown-toggle select-date-year-now" data-id="<?=date('Y',$now_date)?>" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?=date('Y',$now_date)?>
          </a>

          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <?php for ($year=2018; $year <= date('Y', time()); $year++) { ?>
              <a class="dropdown-item select-date-year" data-id="<?=$year?>"><?=$year?></a>
            <?php } ?>
          </div>
        </div>
        <div class="dropdown">
          <a class="btn btn-secondary dropdown-toggle select-date-month-now" data-id="<?=date('F',$now_date)?>" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?=date('F',$now_date)?>
          </a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <?php for ($month=1; $month <= 12; $month++) { ?>
              <a class="dropdown-item select-date-month" data-id="<?=$month?>"><?=date('F',strtotime('02.'.$month.'.2019'))?></a>
            <?php } ?>
          </div>
        </div>
      </div>

<?php if($plans != '') { ?>
    <table class="table adminsaletable" style="overflow-x: scroll;">
      <thead>
        <tr>
          <th scope="col" style="width: 180px;min-width: 180px;max-width: 180px;">Manager</th>
          <th scope="col">Amount $</th> <!-- весь профит в проектах за етот месяц -->
          <th scope="col">Plan</th> <!-- какой выставил профит на етот месяц -->
          <th scope="col">%</th> <!-- процент между Amount и Plan -->
          
          <th scope="col">Fix salary (грн)</th>
          <th scope="col">Fines $</th>
          <th scope="col">Profit bonus %</th>
          <th scope="col">Profit bonus $</th>
          <th scope="col">Total $</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          ///
          $amount_all = 0;
          $plan_all = 0;
          //////air
          $plan_air_all = 0;
          $fact_air_all = 0;
          //////salary
          $salary_all = 0;
          $bonus_all = 0;
          $fines_all = 0;
          $profit_bonus_all = 0;
          $profit_all = 0;
          $total_all = 0;
        ?>
        <?php foreach ($plans as $plan) { ?>
          <?php 
            ////plan
            $amount_all += $plan->amount_money;
            $plan_all += $plan->plan_money;
            /////air
            $plan_air_all += $plan->plan_air;
            $fact_air_all += $plan->fact_air;
            //////salary
            $salary_all += $plan->fix_salary;
            $bonus_all += $plan->bonus;
            $fines_all += $plan->fines;
            $profit_bonus_all += ($plan->profit/100)*$plan->bonus_percent;
            $profit_all += $plan->profit;
            $total_all += $plan->total;

            ///////plan
            if($plan->amount_money == 0 || $plan->plan_money == 0)
              $percent_money = 0;
            else
              $percent_money = (int)(($plan->amount_money*100)/$plan->plan_money);
            ///////air
            if($plan->fact_air == 0 || $plan->plan_air == 0)
              $percent_air_kg = 0;
            else
              $percent_air_kg = (int)(($plan->fact_air*100)/$plan->plan_air);
          ?>
          <tr>
            <th scope="row" style="width: 180px;min-width: 180px;max-width: 180px;">
              <a class="btn-edit-plan" href="/manager/editplanmanager/<?=$plan->plan_id?>" title="Edit plan this manager">
              <i class="fas fa-pencil-alt"></i></a> 
              <?=$plan->manager->username?>
            </th>
            <th scope="row"><?=$plan->profit?> $</th>
            <th scope="row"><?=($plan->profit/100)*$plan->bonus_percent?> $</th>
            <td>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: <?=$percent_money?>%" aria-valuenow="<?=$percent_money?>" aria-valuemin="0" aria-valuemax="100"><?=$percent_money?>%</div>
              </div>
            </td>
            <td><?=$plan->fix_salary?> (грн)</td>
            <!-- <td scope="col"><?=$plan->bonus?> $</td> -->
            <td><?=$plan->fines?> $</td>
            <td scope="col"><?=$plan->bonus_percent?> %</td>
            <td scope="row"><?=($plan->profit/100)*$plan->bonus_percent?> $</td>
            <!-- <td scope="row"><?=$plan->profit?> $</td> -->
            <td scope="row"><?=$plan->total?> $</td>
          </tr>
        <?php } ?>

          <?php
              ///////plan
              if($amount_all == 0 || $plan_all == 0)
                $percent_money_all = 0;
              else
                $percent_money_all = (int)(($amount_all*100)/$plan_all);
              /////air
              if($fact_air_all == 0 || $plan_air_all == 0)
                $percent_air_kg_all = 0;
              else
                $percent_air_kg_all = (int)(($fact_air_all*100)/$plan_air_all);
          ?>
          <tr style="border-top: 2px solid;">
            <th scope="row" style="width: 180px;min-width: 180px;max-width: 180px;">Total</th>
            <th scope="row"><?=$profit_all?> $</th>
            <th scope="row"><?=$plan_all?> $</th>
            <th>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: <?=$percent_money_all?>%" aria-valuenow="<?=$percent_money_all?>" aria-valuemin="0" aria-valuemax="100"><?=$percent_money_all?>%</div>
              </div>
            </th>
            <th scope="row"><?=$salary_all?> (грн)</th>
            <th><?=$fines_all?> $</td>
            <th scope="row"></th>
            <th scope="row"><?=$profit_bonus_all?> $</th>
            <th scope="row"><?=$total_all?> $</th>
          </tr>
      </tbody>
    </table>
  <?php }else { ?>
    <a href="/manager/createplans/<?=$now_date?>" class="btn-create-plans">Create this month plan</a>
  <?php } ?>
</div>
</div>

<script type="text/javascript">
  $('.select-date-year').click(function(){
    window.location.href = "/manager/manageradmin/"+parseInt((new Date($(this).attr("data-id")+'-'+$('.select-date-month-now').attr("data-id")+'-15').getTime() / 1000).toFixed(0));
  });
  $('.select-date-month').click(function(){
    window.location.href = "/manager/manageradmin/"+parseInt((new Date($('.select-date-year-now').attr("data-id")+'-'+$(this).attr("data-id")+'-15').getTime() / 1000).toFixed(0));
  });
</script>