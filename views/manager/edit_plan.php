<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Edit month plan');
?>

	<section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">Edit month plan</div>
            </div>

            <div class="add-client-form-wrap">
                <form action="/manager/editplanmanager/<?=$plan->plan_id?>" method="post">

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Manager</label>
                                <div class="input-addclient-wrap select-input">
                                    <?=$plan->manager->username?> (<?=date('F, Y',$plan->date)?>)
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Plan $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" name="plan_money" value="<?= (!empty($plan->plan_money)) ? $plan->plan_money : 0 ?>">
                                </div>
                            </div>
                            
                            <!-- <div class="add-client-form-line">
                                <label>Plan Air, kg</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0" name="plan_air" value="<?= (!empty($plan->plan_air)) ? $plan->plan_air : 0 ?>">
                                </div>
                            </div> -->

                            <div class="add-client-form-line">
                                <label>Fix salary (грн)</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" name="fix_salary" value="<?= (!empty($plan->fix_salary)) ? $plan->fix_salary : 0 ?>">
                                </div>
                            </div>

                            <!-- <div class="add-client-form-line">
                                <label>Fix salary $</label>
                                <div class="input-addclient-wrap">
                                   <?=$plan->fix_salary?> / <?=$currency?> = <?=(int)($plan->fix_salary / $currency)?>$
                                </div>
                            </div> -->

                            <div class="add-client-form-line">
                                <label>Fines $</label>
                                <div class="input-addclient-wrap">
                                    <input disabled type="text" placeholder="0.00" name="fines" value="<?= (!empty($plan->fines)) ? $plan->fines : 0 ?>">
                                </div>
                            </div>

                            <!-- <div class="add-client-form-line">
                                <label>Bonus $</label>
                                <div class="input-addclient-wrap">
                                    <input disabled type="text" placeholder="0.00" name="bonus" value="<?= (!empty($plan->bonus)) ? $plan->bonus : 0 ?>">
                                </div>
                            </div> -->

                            <div class="add-client-form-line">
                                <label>Profit Bonus %</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0" name="bonus_percent" value="<?= (!empty($plan->bonus_percent)) ? $plan->bonus_percent : 0 ?>">
                                    <input type="hidden" name="now_date" value="<?= (!empty($plan->date)) ? $plan->date : 0 ?>">
                                </div>
                            </div>

                            <div class="add-new-form-submit-buttons col-12">
                                <div class="add-new-form-submit-buttons-wrap col-4">
                                    <div class="add-new-form-submit-buttons-wrap-width">
                                        <a class="submit-form" >Save</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="add-new-section row no-fw-row" style="">
                            <div class="col-lg-4 col-12">
                                <div class="add-new-item add-new-item-mw">
                                    <div class="add-new-item-title">Bonuses</div>
                                    <div class="add-new-item-top-line">
                                        <span>Amount $</span>
                                        <span>Description</span>
                                    </div>

                                    <div class="add-new-item-button-inputs-temp">
                                        <?php foreach ($bonuses as $bonus) { 
                                            echo '<div class="add-new-item-button-inputs">
                                                <div class="add-client-form-line">
                                                    <div class="input-addclient-wrap">
                                                        <input type="text" value="'.$bonus->amount.'" placeholder="0.00">
                                                    </div>
                                                </div>
                                                <div class="add-client-form-line">
                                                    <div class="input-addclient-wrap">
                                                        <input type="text" value="'.$bonus->description.'">
                                                    </div>
                                                </div>
                                            </div>';
                                        } ?>
                                    </div>

                                    <div class="add-new-item-button">
                                        <a href="#" id='add-bonus'><i class="fas fa-plus"></i> Add New</a>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="add-new-section row no-fw-row" style="">
                            <div class="col-lg-4 col-12">
                                <div class="add-new-item add-new-item-mw">
                                    <div class="add-new-item-title">Fines</div>
                                    <div class="add-new-item-top-line">
                                        <span>Amount $</span>
                                        <span>Description</span>
                                    </div>

                                    <div class="add-new-item-button-inputs-temp">
                                        <?php foreach ($fines as $fine) { 
                                            echo '<div class="add-new-item-button-inputs">
                                                <div class="add-client-form-line">
                                                    <div class="input-addclient-wrap">
                                                        <input type="text" value="-'.$fine->amount.'" placeholder="0.00">
                                                    </div>
                                                </div>
                                                <div class="add-client-form-line">
                                                    <div class="input-addclient-wrap">
                                                        <input type="text" value="'.$fine->description.'">
                                                    </div>
                                                </div>
                                            </div>';
                                        } ?>
                                    </div>

                                    <div class="add-new-item-button">
                                        <a href="#" id='add-fine'><i class="fas fa-plus"></i> Add New</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="add-new-section row no-fw-row" style="">
                            <div class="col-lg-4 col-12">
                                <div class="add-new-item add-new-item-mw">
                                    <div class="add-new-item-title">Paid salary</div>
                                    <div class="add-new-item-top-line">
                                        <span>Amount $</span>
                                        <span>Description</span>
                                    </div>

                                    <div class="add-new-item-button-inputs-temp">
                                        <?php foreach ($salary_all as $salary) { 
                                            echo '<div class="add-new-item-button-inputs">
                                                <div class="add-client-form-line">
                                                    <div class="input-addclient-wrap">
                                                        <input type="text" value="-'.$salary->amount.'" placeholder="0.00">
                                                    </div>
                                                </div>
                                                <div class="add-client-form-line">
                                                    <div class="input-addclient-wrap">
                                                        <input type="text" value="'.$salary->description.'">
                                                    </div>
                                                </div>
                                            </div>';
                                        } ?>
                                    </div>

                                    <div class="add-new-item-button">
                                        <a href="#" id='add-salary'><i class="fas fa-plus"></i> Add New</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        $('.submit-form').click(function(){
            $("form").submit();
        });

        var add_bonus_count = 111;
        var add_fine_count = 111;
        var add_salary_count = 111;

        $('#add-bonus').on("click", function(e) {
            e.preventDefault();
            var curLink = $(this).parents('.add-new-item').find('.add-new-item-button-inputs-temp');
            var cont = `
            <div class="add-new-item-button-inputs">
                <div class="add-client-form-line">
                    <div class="input-addclient-wrap">
                        <input type="text" placeholder="0.00" class="money-amount-`+add_bonus_count+`">
                    </div>
                </div>
                <div class="add-client-form-line">
                    <div class="input-addclient-wrap">
                        <input type="text" placeholder="Description" class="money-description-`+add_bonus_count+`">
                    </div>
                </div>
                <div class="add-client-form-line">
                    <div class="input-addclient-wrap">
                        <a class="submit-director-new" data-type="1" data-count="`+add_bonus_count+`">Add bonus</a>
                    </div>
                </div>
            </div>`;
            add_bonus_count++;
            curLink.append(cont);
        });

        $('#add-fine').on("click", function(e) {
            e.preventDefault();
            var curLink = $(this).parents('.add-new-item').find('.add-new-item-button-inputs-temp');
            var cont = `
            <div class="add-new-item-button-inputs">
                <div class="add-client-form-line">
                    <div class="input-addclient-wrap">
                        <input type="text" placeholder="0.00" class="money-amount-`+add_fine_count+`">
                    </div>
                </div>
                <div class="add-client-form-line">
                    <div class="input-addclient-wrap">
                        <input type="text" placeholder="Description" class="money-description-`+add_fine_count+`">
                    </div>
                </div>
                <div class="add-client-form-line">
                    <div class="input-addclient-wrap">
                        <a class="submit-director-new" data-type="2" data-count="`+add_bonus_count+`">Add fine</a>
                    </div>
                </div>
            </div>`;
            add_fine_count++;
            curLink.append(cont);
        });

        $('#add-salary').on("click", function(e) {
            e.preventDefault();
            var curLink = $(this).parents('.add-new-item').find('.add-new-item-button-inputs-temp');
            var cont = `
            <div class="add-new-item-button-inputs">
                <div class="add-client-form-line">
                    <div class="input-addclient-wrap">
                        <input type="text" placeholder="0.00" class="money-amount-`+add_salary_count+`">
                    </div>
                </div>
                <div class="add-client-form-line">
                    <div class="input-addclient-wrap">
                        <input type="text" placeholder="Description" class="money-description-`+add_salary_count+`">
                    </div>
                </div>
                <div class="add-client-form-line">
                    <div class="input-addclient-wrap">
                        <a class="submit-director-new" data-type="3" data-count="`+add_bonus_count+`">Add paid</a>
                    </div>
                </div>
            </div>`;
            add_salary_count++;
            curLink.append(cont);
        });

        $(document).on('click', '.submit-director-new', function () {
            $.ajax({ 
                type: 'POST', 
                url: '/manager/addplanmovement/<?=$plan->plan_id?>', 
                data: {
                    type:   $(this).attr("data-type"),
                    amount:   $('.money-amount-'+$(this).attr("data-count")).val(),
                    description: $('.money-description-'+$(this).attr("data-count")).val(),
                }, 
                success: function (data) { 
                    if(data == 'create'){
                        location.reload();
                    }else{
                        alert(data);
                    }
                    
                }
            });
        });
    </script>
