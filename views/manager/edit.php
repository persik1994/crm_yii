<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Edit profile');
?>

    <section class="dashboard-content">
        <div class="container-fluid">
            <div class="add-client-form-wrap">
                <form action="/manager/edit" class='row no-fw-row' method="POST" enctype="multipart/form-data">
                    <div class="col-lg-4 col-md-12">
                        <div class="setting-title">Profile settings</div>

                        <div class="add-client-form-line">
                            <label>Username</label>
                            <div class="input-addclient-wrap border-span">
                                <input type="text" placeholder="Username" disabled value="<?=Yii::$app->user->identity->username?>" >
                            </div>
                        </div>

                        <div class="add-client-form-line">
                            <label>Name</label>
                            <div class="input-addclient-wrap border-span">
                                <input type="text" placeholder="Name" value="<?=$user->name?>" name="name">
                            </div>
                        </div>

                        <div class="add-client-form-line">
                            <label>Email</label>
                            <div class="input-addclient-wrap border-span">
                                <input type="email" placeholder="info@gmail.com" value="<?=$user->email?>" name="email">
                                <span class="input-icon"><i class="far fa-envelope"></i></span>
                            </div>
                        </div>

                        <div class="add-client-form-line add-client-form-line-mt">
                            <label>Phone</label>
                            <div class="input-addclient-wrap">
                                <input type="hidden" name="phone-code" class="phone-code">
                                <input type="tel" id='choose-phone' placeholder="000000000" value="<?=$user->phone?>" name="phone">
                            </div>
                        </div>

                        <div class="add-client-form-line">
                            <label>Viber</label>
                            <div class="input-addclient-wrap border-span">
                                <input type="text" placeholder="+380500000000" value="<?=$user->viber?>" name="viber">
                            </div>
                        </div>

                        <div class="add-client-form-line add-client-form-line-file">
                            <label>Main photo</label>
                            <div class="input-addclient-wrap border-span">
                                <input type="" placeholder="Select File">
                                <input type="file" placeholder="" class="file" id='get-img' name="avatar-file">
                                <span class="input-icon"><i class="fas fa-upload"></i></span>
                            </div>
                        </div>
                        <?php if(!empty(Yii::$app->user->identity->avatar)){ ?>
                            <div style="text-align: right;padding-right: 85px;">
                                <img style="width: 100px;" src="<?=Yii::$app->user->identity->avatar?>">
                            </div>
                        <?php } ?>
                        

                        <div class="gen-img"><img src="" id='preview_avatar' alt="">
                            <span class="ava-name"></span>
                            <span class="ava-size"></span>
                            <span class="close-preview"><i class="fas fa-times"></i></span>
                        </div>

                        <div class="pf-set-button">
                            <input type="submit" name="" value="Change my info">
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-12">
                        <div class="setting-title">Password settings</div>
                        <div class="add-client-form-line">
                            <label>Current Password</label>
                            <div class="input-addclient-wrap">
                                <input type="password" placeholder="Current Password" name="current_pass">
                            </div>
                        </div>

                        <div class="add-client-form-line">
                            <label>New password</label>
                            <div class="input-addclient-wrap">
                                <input type="password" placeholder="New password" name="new_pass">
                            </div>
                        </div>

                        <div class="add-client-form-line">
                            <label>Confirm new password</label>
                            <div class="input-addclient-wrap">
                                <input type="password" placeholder="Confirm new password" name="repeat_pass">
                            </div>
                        </div>

                        <div class="pf-set-button">
                            <input type="submit" name="" value="Change password">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <div class="setting-title">Notification settings</div>
                        <div class="add-client-form-line add-client-form-line-checkbox">
                            <label class="edit-profile-notification-label">
                                <input type="checkbox" value="<?=$notification_option->telegram ?? ''?>" <?=(!empty($notification_option->telegram) && $notification_option->telegram > 0) ? 'checked' : ''?> name="notification_telegram">
                                <p>Telegram</p>
                                <span></span>
                            </label>
                        </div>
                        <div class="add-client-form-line">
                            <label>Telegram username</label>
                            <div class="input-addclient-wrap border-span">
                                <input type="text" placeholder="@Telegram" value="<?=$user->telegram?>" name="telegram">
                            </div>
                        </div>
                        <div class="info-telegram-token">
                            *** Для регистрации в боте найдите его по имени "@office_inta_group_bot" или по <a href="tg://resolve?domain=office_inta_group_bot">ссылке</a>. После чего наберите команду /start в чате.
                        </div>

                        <div class="add-client-form-line add-client-form-line-checkbox">
                            <label class="edit-profile-notification-label">
                                <input type="checkbox" value="<?=$notification_option->email ?? ''?>" <?=(!empty($notification_option->email) && $notification_option->email > 0) ? 'checked' : ''?> name="notification_email">
                                <p>E-mail</p>
                                <span></span>
                            </label>
                        </div>

                        <div class="add-client-form-line add-client-form-line-checkbox">
                            <label class="edit-profile-notification-label">
                                <input type="checkbox" value="<?=$notification_option->sms ?? ''?>" <?=(!empty($notification_option->sms) && $notification_option->sms > 0) ? 'checked' : ''?> name="notification_sms">
                                <p>SMS</p>
                                <span></span>
                            </label>
                        </div>

                        <div class="pf-set-button pf-set-button-lf">
                            <input type="submit" name="" value="Change notification">
                        </div>

                    </div>
                    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                </form>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        var phone_code = '';
        setInterval(function() {
            if($('.selected-dial-code').text() != phone_code){
                phone_code = $('.phone-code').val();
                $('.phone-code').val($('.selected-dial-code').text());
            }
        }, 500); // каждую секунду
        $('input[type=checkbox]').click(function(){
            if($(this).attr("checked") == 'checked') { 
                $(this).val(0);
                $(this).removeAttr("checked","");
            }else{
                $(this).val(1);
                $(this).attr("checked","checked");
            }
        });
    </script>
<?=$error?>
   