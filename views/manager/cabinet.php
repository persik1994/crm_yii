<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Pesonal cabinet');
?>

<style>
  .progress-bar {
        background-color: #009688;
  }
</style>

<div class="row no-fw-row row-padding">
  <div class="col-md-12">
    <div class="dashboard-title"><?=$user->name?> / Pesonal cabinet</div>
    <div class="row selectsaleadm">
      <p style="font-size: 17px; font-weight: 500; margin-right: 10px;">Select period:</p>
      <div class="dropdown">
        <a class="btn btn-secondary dropdown-toggle select-date-year-now" data-id="<?=date('Y',$now_date)?>" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?=date('Y',$now_date)?>
        </a>

        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <?php for ($year=2018; $year <= date('Y', time()); $year++) { ?>
            <a class="dropdown-item select-date-year" data-id="<?=$year?>"><?=$year?></a>
          <?php } ?>
        </div>
      </div>
      <div class="dropdown">
        <a class="btn btn-secondary dropdown-toggle select-date-month-now" data-id="<?=date('F',$now_date)?>" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?=date('F',$now_date)?>
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <?php for ($month=1; $month <= 12; $month++) { ?>
            <a class="dropdown-item select-date-month" data-id="<?=$month?>"><?=date('F',strtotime('02.'.$month.'.2019'))?></a>
          <?php } ?>
        </div>
      </div>
    </div>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Profit</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Salary</a>
      </li>
    </ul>

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
    <div class="col-md-12">
      <table class="table adminsaletable adminsaletable-personal-profit">
        <thead>
          <tr>
            <th scope="col" colspan="6">Mounth profit</th>
          </tr>
          <tr>
            <th scope="col">Plan</th>
            <th scope="col">Amount Plan</th>
            <th scope="col">% ready</th>
            <th scope="col">Plan Air, kg</th>
            <th scope="col">Fact Air, kg</th>
            <th scope="col">% Air ready, kg</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row"><?=$plan->plan_money?> $</th>
            <td><?=$plan->amount_money?> $</td>
            <td>
              <?php
                if($plan->amount_money == 0 || $plan->plan_money == 0)
                  $percent_money = 0;
                else
                  $percent_money = (int)(($plan->amount_money*100)/$plan->plan_money);
              ?>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: <?=$percent_money?>%" aria-valuenow="<?=$percent_money?>" aria-valuemin="0" aria-valuemax="100"><?=$percent_money?>%</div>
              </div>
            </td>
            <td><?=$plan->plan_air?> kg</td>
            <td><?=$plan->fact_air?> kg</td>
            <td>
              <?php
                if($plan->fact_air == 0 || $plan->plan_air == 0)
                  $percent_air_kg = 0;
                else
                  $percent_air_kg = (int)(($plan->fact_air*100)/$plan->plan_air);
              ?>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: <?=$percent_air_kg?>%" aria-valuenow="<?=$percent_air_kg?>" aria-valuemin="0" aria-valuemax="100"><?=$percent_air_kg?>%</div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="col-md-12">
      <table class="table adminsaletable">
        <thead>
          <tr>
            <th scope="col" colspan="2">Mounth salary</th>
          </tr>
          <tr>
            <th scope="col">Type</th>
            <th scope="col">Amount</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">Fix salary</th>
            <td><?=$plan->fix_salary?> грн</td>
          </tr>
          <tr>
            <th scope="row">Bonus</th>
            <td><?=$plan->bonus?> $</td>
          </tr>
          <tr>
            <th scope="row">Fines</th>
            <td><?=$plan->fines?> $</td>
          </tr>
          <tr>
            <th scope="row">Profit</th>
            <td><?=$plan->profit?> $</td>
          </tr>
          <tr>
            <th scope="row">Profit Bonus %</th>
            <td><?=$plan->bonus_percent?> %</td>
          </tr>
          <tr>
            <th scope="row">Profit Bonus $</th>
            <td><?=($plan->profit/100)*$plan->bonus_percent?> $</td>
          </tr>
          <tr>
            <th scope="row">Total</th>
            <td><?=$plan->total?> $</td>
          </tr>
          <tr>
            <th scope="row"></th>
            <td></td>
          </tr>
          <tr>
            <th scope="row">Payed</th>
            <td><?=$payed?> $</td>
          </tr>
          <tr>
            <th scope="row">Total to payed</th>
            <td><?=$plan->total-$payed?> $</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  </div>
</div>

<script type="text/javascript">
  $('.select-date-year').click(function(){
    window.location.href = "/manager/cabinet/"+parseInt((new Date($(this).attr("data-id")+'-'+$('.select-date-month-now').attr("data-id")+'-15').getTime() / 1000).toFixed(0));
  });
  $('.select-date-month').click(function(){
    window.location.href = "/manager/cabinet/"+parseInt((new Date($('.select-date-year-now').attr("data-id")+'-'+$(this).attr("data-id")+'-15').getTime() / 1000).toFixed(0));
  });
</script>