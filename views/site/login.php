<?php
  use yii\helpers\Html;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login | INTA.GROUP</title>

    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Login to the CPM Office" name="description">
    <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
    <meta property="og:title" content="Login | INTA.GROUP">
    <meta property="og:image" content="/web/img/logo_black.jpg">
    <meta property="og:site_name" content="Login | INTA.GROUP">
    <meta property="og:description" content="Login to the CPM Office">
    
    <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="Login | INTA.GROUP">
    <meta name="twitter:title" content="Login | INTA.GROUP">
    <meta name="twitter:description" content="Login to the CPM Office">
    <meta name="twitter:image" content="/web/img/logo_black.jpg">

    <link rel="stylesheet" href="/web/css/main_2.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
</head>

<body>
    <div class="overlay flex cac vac ">
        <div>
            <div class="loader preloader flex vac">
                <svg width="200" height="200">
                    <circle class="background" cx="90" cy="90" r="80" transform="rotate(-90, 100, 90)" />
                    <circle class="outer" cx="90" cy="90" r="80" transform="rotate(-90, 100, 90)" />
                </svg>
                <span class="circle-background"></span>
                <span class="logo animated fade-in"></span>
            </div>
        </div>
    </div>
    <header class="login-page">
        <div class="container">
            <div class="content-wrap row">

                <div class="logo col-lg-7 col-md-12">
                    <img src="/web/img/logo.png" alt="">
                </div>

                <div class="login-page-form-wrap col-lg-5 col-md-12">
                    <div class="login-page-form-title">Войдите для доступа<br />к CrM INTA</div>
                    <form action="/login" method="post">
                        <div class="input-wrap">
                            <span><i class="fas fa-user"></i></span>
                            <input type="text" name="Login[username]" placeholder="Логин" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Логин'">
                        </div>
                        <div class="input-wrap">
                            <span><i class="fas fa-lock"></i></span>
                            <input type="password" name="Login[password]" placeholder="Пароль" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Пароль'">
                            <!-- <div class="forget-pass">
                                <a href="#">Забыли пароль?</a>
                            </div> -->
                        </div>
                        <div class="input-wrap-checkbox">
                            <label for="remember" class="custom-input">
                                <input type="checkbox" id='remember'>
                                <span class="checkmark"></span>
                                 Не запоминать
                            </label>
                        </div>
                        <div class="input-wrap-button">
                            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                            <input type="submit" name="submit" value="войти">
                        </div>
                    </form>

                    <div class="markets-links">
                        <a href="#"><img src="/web/img/appstore.jpg" alt="#"></a>
                        <a href="#"><img src="/web/img/google-play.jpg" alt="#"></a>
                    </div>

                </div>

                <div class="login-page-footer">
                    © 2018 «inta.group»
                </div>

            </div>
        </div>

    </header>
    <script defer src="https://use.fontawesome.com/releases/v5.5.0/js/all.js" integrity="sha384-GqVMZRt5Gn7tB9D9q7ONtcp4gtHIUEW/yG7h98J7IpE3kpi+srfFyyB/04OV6pG0" crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/highcharts.src.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="/web/libs/datetimepicker-master/jquery.datetimepicker.full.min.js"></script>
    <script src="/web/libs/phone/build/js/intlTelInput.min.js"></script>
    <script type="text/javascript">
        var input = document.querySelector("#choose-phone");
        window.intlTelInput(input, {
            separateDialCode: true,
        });
    </script>
    <!-- <script src="/web/js/main_1.js"></script> -->
    <script src="/web/js/main_4.js"></script>
    <script src="/web/js/chart.js"></script>
</body>

</html>
<?php $this->endPage() ?>