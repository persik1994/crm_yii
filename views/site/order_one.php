<?php
    use app\models\Orders;
    $orderController = new Orders;
    if($order->shipment_type == 'Post'){
        $order->shipment_type = 'Air+Freight';
    }
?>

<tr>
    <td class="way">
        <i class="sprite <?= $orderController->location($order->location_from) ?>" title="<?= $order->location_from ?>"></i>
        <i class="sprite <?= $orderController->type_shipment($order->shipment_type) ?>" title="<?= $order->shipment_type ?>"></i>
        <i class="sprite <?= $orderController->location($order->location_to) ?>" title="<?= $order->location_to ?>"></i>
    </td>
    <td>
        <?= $order->code_cc ?>
    </td>
    <td>
        <?= $order->date_received ?>
    </td>
    <td>
        <?= $order->warehouse ?>
    </td>
    <td>
        <?= $order->status ?>
    </td>
    <td>
        <?= $order->weight ?>
    </td>
    <td>
        <?= $order->volume ?>
    </td>
    <td>
        <?= $order->cartons ?>
    </td>
    <td>
        <?= $order->description ?>
    </td>
    <td>
        <?= $order->local_tracking ?>
    </td>
</tr>