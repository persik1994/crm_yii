<?php
    $this->title = Yii::t('app', 'Dashboard');
?>


    <section class="dashboard-content">
        <div class="container-fluid">
            <div class="row no-fw-row row-padding">

                <div class="col-sm-6">
                    <div class="dashboard-title">Dashboard</div>
                </div>

                <div class="col-sm-6 dashboard-choose-date-align">
                    <div class="dashboard-choose-date">
                        <i class="far fa-calendar-alt"></i> This Month <i class="fas fa-sort-down"></i>
                        <ul>
                            <li><a href="#">Today</a></li>
                            <li><a href="#">Yesterday</a></li>
                            <li><a href="#">Last 7 days</a></li>
                            <li><a href="#">Last 30 days</a></li>
                            <li><a href="#">This Month</a></li>
                            <li><a href="#">Last Month</a></li>
                        </ul>
                    </div>
                </div>


                <div class="col-xl-3 col-lg-6 col-sm-6">
                    <div class="dashboard-option">
                        <div class="dashboard-option-title">Sales plan</div>
                        <div class="dashboard-option-numbers">
                            <?php
                              $percent_money = 0;
                              $style_plan = '<style>.dashboard-option-progress-bar::before{width: 0%;}</style>';
                            ///////plan
                            if(isset($plan) && $plan->amount_money != 0 && $plan->plan_money != 0){
                              $percent_money = (int)(($plan->amount_money*100)/$plan->plan_money);
                              if($percent_money > 87)
                                  $percent = 87;
                              else
                                  $percent = $percent_money;
                              $style_plan = '<style>.dashboard-option-progress-bar::before{width: '.$percent.'%;}</style>';
                            }
                            echo $style_plan;
                            ?>    
                            <div class="dashboard-option-percents"><?=$percent_money?>%</div>
                            <div class="dashboard-option-percents-count">$<?=$plan->plan_money ?? '0.00'?></div>
                        </div>
                        <div class="dashboard-option-progress-bar"></div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 col-sm-6">
                    <div class="dashboard-option">
                        <div class="dashboard-option-title">Current sales</div>
                        <div class="dashboard-option-numbers">
                            <div class="dashboard-option-percents percents-blue" style="visibility: hidden;"></div>
                            <div class="dashboard-option-percents-count">$<?=$current_sales?></div>
                        </div>
                        <div class="dashboard-option-button">
                            <a href="/payments/create"><span>+</span> add payments </a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 col-sm-6">
                    <div class="dashboard-option">
                        <div class="dashboard-option-title">Clients</div>
                        <div class="dashboard-option-numbers">
                            <div class="dashboard-option-percents percents-red" style="visibility: hidden;"></div>
                            <div class="dashboard-option-percents-count"><?=$current_clients?></div>
                        </div>
                        <div class="dashboard-option-button">
                            <a href="/clients/create"><span>+</span> add client </a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 col-sm-6">
                    <div class="dashboard-option">
                        <div class="dashboard-option-title">Cargos/Kg</div>
                        <div class="dashboard-option-numbers">
                            <div class="dashboard-option-percents percents-purple" style="visibility: hidden;"></div>
                            <div class="dashboard-option-percents-count"><?=$current_cargos?>/<?=$current_cargos_kg?>kg</div>
                        </div>
                        <div class="dashboard-option-button">
                            <a href="/cargo/create"><span>+</span> add cargo </a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-8">
                    <!-- <div class="infographic-wrap">
                        <div class="infographic"></div>
                        <div id="infographic">
                        </div>
                    </div> -->
                    <div class="dashboard-title">Cargos</div>
                    <div class="dashboard-table-wrap" style="margin-top: 15px;max-height: 100%;">
                        <table class="table table-striped table-bordered table-resizable custom-tab" >
                            <thead>
                                <tr>
                                    <th style="width: 5px;"></th>
                                    <th>Cargo code</th>
                                    <th>Cargo Date received</th>
                                    <th>Client code</th>
                                    <th>Manager</th>
                                    <th>Project</th>
                                    <th>Shipment number</th>
                                    <th>Cargo shipment type</th>
                                    <th>Cargo type</th>
                                    <th>Cargo status</th>
                                    <th>Cargo weight</th>
                                    <th>Cargo volume</th>
                                    <th>Cargo volume weight</th>
                                    <th>Cargo cartons</th>
                                    <th>Qty inside</th>
                                    <th>Cargo description</th>
                                    <th>Local Tracking</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?= $all_table_rows ?>
                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="col-xl-4 "> 
                    <div class="dashboard-title">Clients</div>
                    <div class="dashboard-table-wrap" style="margin-top: 15px;max-height: 100%;">
                        <table class="table table-striped table-bordered table-resizable custom-tab" >
                            <thead>
                                <tr>
                                    <th>Manager</th>
                                    <th>Client code</th>
                                    <th>Client balance</th>
                                    <th>Client date add</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Client Messenger</th>
                                    <th>Pricing</th>
                                    <th>Route</th>
                                    <th>Request source</th>
                                    <th>Activate client</th>
                                    <th>Comment</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?= $all_table_rows_clients ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="detail-info-top-line">

                    <div class="detail-info-controls">
                        <a href="#" id="slide-left"><img src="/web/img/arr-left.png" alt="#"></a>
                        <a href="#" id="slide-right"><img src="/web/img/arr-right.png" alt="#"></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="/cargo/edit/" class="cargo-detail-info-url-edit"><img src="/web/img/pencil.png" alt="">Edit</a>
                            <!-- <a href="#"><img src="/web/img/duplicate-icon.png" alt="">Duplicate</a> -->
                            <a href="#" id="close-detail-info"><img src="/web/img/close-icon.png" alt=""></a>
                        </div>
                    </div>

                </div>

                <div class="detail-info-wrap">

                    <div class="detail-info">

                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo weight</div>
                                <div class="detail-info-list-column detail-info-weight"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo volume</div>
                                <div class="detail-info-list-column detail-info-volume"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo cartons</div>
                                <div class="detail-info-list-column detail-info-cartons"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Qty inside</div>
                                <div class="detail-info-list-column detail-info-inside"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo description</div>
                                <div class="detail-info-list-column detail-info-description"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo date received</div>
                                <div class="detail-info-list-column detail-info-date_received"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo given to client</div>
                                <div class="detail-info-list-column detail-info-given_to_client"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo delivery time</div>
                                <div class="detail-info-list-column detail-info-delivery_time"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo shipment type</div>
                                <div class="detail-info-list-column detail-info-shipment_type"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo status</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo photo</div>
                                <div class="detail-info-list-column type-img detail-info-photo"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo invoice</div>
                                <div class="detail-info-list-column detail-info-invoice"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Other files</div>
                                <div class="detail-info-list-column detail-info-files"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo code</div>
                                <div class="detail-info-list-column detail-info-cargo_id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo warehouse</div>
                                <div class="detail-info-list-column detail-info-warehouse"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Local Tracking</div>
                                <div class="detail-info-list-column detail-info-local_tracking"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Local delivery price</div>
                                <div class="detail-info-list-column detail-info-local_delivery_price"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo volume weight</div>
                                <div class="detail-info-list-column detail-info-volume_weight"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client code</div>
                                <div class="detail-info-list-column detail-info-client_id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment</div>
                                <div class="detail-info-list-column detail-info-shipment"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo type</div>
                                <div class="detail-info-list-column detail-info-type"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">From</div>
                                <div class="detail-info-list-column detail-info-from"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">To</div>
                                <div class="detail-info-list-column detail-info-to"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project</div>
                                <div class="detail-info-list-column detail-info-project"></div>
                            </div>
                        </div>
                    </div>
                </div>
    </section>

    <script type="text/javascript">
        $('.mob-menu-Dasboard > a').addClass('active');
    </script>