<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Пополнение баланса';
// $this->params['breadcrumbs'][] = $this->title;
?>
<section class="container sectionEditOrder">
    <p class="h2 text-center slimText">
        ПОПОЛНЕНИЕ БАЛАНСА
    </p>
    <div class="row" id="order1">
        <form id="login-form" class="form-horizontal" action="/money" method="post">
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div style="font-size: 30px;font-weight: 100;">
                    ВАШ БАЛАНС - 
                    <span style="font-weight: 400;"><?=$user->money?></span>
                    $
                </div>  
            </div>
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <input type="text" required="" name="money" placeholder="Сумма - 100$" value="" id="money">  
            </div>
            <div class="col-xs-12 col-sm-12 col-lg-12"> 
                <button class="submitBTN" style="width: 100%;cursor: not-allowed;" disabled="disabled">
                    ПОПОЛНИТЬ
                </button>
            </div>
        </form>
    </div>
</section>