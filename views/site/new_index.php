<style type="text/css">
    .tasks-popup-task p {
        color: #CACACA !important;
    }
</style>

<section class="dashboard-content">
        <div class="container-fluid">
            <div class="row no-fw-row row-padding">

                <div class="col-xl-3">
                    <div class="dashboard-title dashboard-title-pb">Dashboard</div>

                    <div class="second-dashboard-item-pesron">
                        <div class="second-dashboard-item-pesron-image">
                            <img src="<?=(!empty(Yii::$app->user->identity->avatar)) ? Yii::$app->user->identity->avatar : '/web/img/comment_2.png'?>" alt="#">
                        </div>
                        <div class="second-dashboard-item-pesron-info">
                            <div class="second-dashboard-item-pesron-info-name"><?=Yii::$app->user->identity->name?></div>
                            <div class="second-dashboard-item-pesron-info-position"><!-- Sales manager --></div>
                        </div>
                    </div>

                    <div class="second-dashboard-item">
                        <div class="second-dashboard-item-icon">
                            <i class="fas fa-chart-line"></i>
                        </div>

                        <div class="second-dashboard-item-text">
                            <div class="second-dashboard-item-text-title">Sales plan</div>
                            <div class="second-dashboard-item-text-price">
                                <?php
                                    $percent_money = 0;
                                    $style_plan = '<style>.second-dashboard-item-text-bar::before{width: 0%;}</style>';
                                    ///////plan
                                    if(isset($plan) && $plan->amount_money != 0 && $plan->plan_money != 0){
                                      $percent_money = (int)(($plan->amount_money*100)/$plan->plan_money);
                                      if($percent_money > 87)
                                          $percent = 87;
                                      else
                                          $percent = $percent_money;
                                      $style_plan = '<style>.second-dashboard-item-text-bar::before{width: '.$percent.'%;}</style>';
                                    }
                                    echo $style_plan;
                                ?>   
                                <span>$<?=$plan->plan_money ?? '0.00'?></span>
                                <div><?=$percent_money?>%</div>
                            </div>
                            <div class="second-dashboard-item-text-bar"></div>
                        </div>

                    </div>

                    <div class="second-dashboard-item">

                        <div class="second-dashboard-item-icon">
                            <i class="fas fa-money-bill-alt"></i>
                        </div>

                        <div class="second-dashboard-item-text">
                            <div class="second-dashboard-item-text-title">Current sales</div>
                            <div class="second-dashboard-item-text-price">
                                <span>$<?=$current_sales?></span>
                            </div>
                        </div>

                    </div>

                    <div class="second-dashboard-item">

                        <div class="second-dashboard-item-icon">
                            <i class="fas fa-users"></i>
                        </div>

                        <div class="second-dashboard-item-text">
                            <div class="second-dashboard-item-text-title">Clients</div>
                            <div class="second-dashboard-item-text-price">
                                <span><?=$current_clients?></span>
                            </div>
                        </div>

                    </div>

                    <div class="second-dashboard-item">

                        <div class="second-dashboard-item-icon">
                            <i class="fas fa-balance-scale"></i>
                        </div>

                        <div class="second-dashboard-item-text">
                            <div class="second-dashboard-item-text-title">Cargos/Kg</div>
                            <div class="second-dashboard-item-text-price">
                                <span><?=$current_cargos?>/<?=$current_cargos_kg?>kg</span>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="col-xl-6 task-for-day-padding task-for-day-no-pd dashboard-pl">
                    <div class="dashboard-title dashboard-title-pb">Tasks for Day</div>
                    <div class="task-for-day task-for-day-2">
                        <div class="task-for-day-title">Tasks for the day</div>

<!--                         <ul>

                            <li>

                                <label for="task" class="custom-input task-label">
                                    <input type="checkbox" id='task'>
                                    <span class="checkmark-task"></span>
                                    <span class="checkmark-text">
                                        <p>Call uncle Sam</p>
                                        <span class='task-term'><i class="far fa-clock"></i> 27 Nov</span>
                                        <span class="task-tag"></span>
                                    </span>
                                </label>

                            </li>

                        </ul> -->

                        <ul class="tasks-block-all-dashboard"></ul>

                        <div class="add-task">
                            <div class="add-form">
                                <form action="#">
                                    <input type="text" placeholder="Enter task text" class="task-input dashboard-task-input">
                                    <a class="task-submit"><span>+</span>add task</a>
                                </form>
                            </div>
                            <div class="add-task-tag-term">
                                <a href="#"><i class="far fa-clock"></i> <input type="text" placeholder="Add term" class="add-task-tag-term-click"></a>
                                <!-- <a id='tag-choose'><i class="fas fa-tag"></i> Add tag
                                    <div class="tag-choose">
                                        <ul>
                                            <li></li>
                                            <li style="background-color: blue;"></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                        </ul>
                                    </div>
                                </a> -->
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-xl-3 dashboard-pl">
                    <div class="dashboard-title dashboard-title-pb">Last Events</div>
                    <div class="notif-visible">
                        <div class="notif-message">
                            <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                            <div class="notif-message-text">
                                <span>cc-1330</span> status has been charged: <strong>Signed</strong>
                                <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                            </div>
                        </div>
                        <div class="notif-message">
                            <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                            <div class="notif-message-text">
                                <span>cc-1330</span> status has been charged: <strong>Signed</strong>
                                <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                            </div>
                        </div>
                        <div class="notif-message">
                            <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                            <div class="notif-message-text">
                                <span>cc-1330</span> status has been charged: <strong>Aproved</strong>
                                <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                            </div>
                        </div>
                        <div class="notif-message">
                            <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                            <div class="notif-message-text">
                                <span>cc-1330</span> status has been charged: <strong>Aproved</strong>
                                <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                            </div>
                        </div>
                        <div class="notif-message">
                            <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                            <div class="notif-message-text">
                                <span>cc-1330</span> status has been charged: <strong>Aproved</strong>
                                <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                            </div>
                        </div>
                        <div class="notif-message">
                            <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                            <div class="notif-message-text">
                                <span>cc-1330</span> status has been charged: <strong>Aproved</strong>
                                <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                            </div>
                        </div>
                        <div class="notif-message notif-message-mb">
                            <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                            <div class="notif-message-text">
                                <span>cc-1330</span> status has been charged: <strong>Aproved</strong>
                                <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                            </div>
                        </div>

                    </div>
                </div>

                
    </section>