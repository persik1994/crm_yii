<div class="options">
    <div class="choose-country d-sm-none d-md-none d-lg-none d-xl-none" id="country-click">
        <a href="#" class="choose-country-click">Choose Clients <span><i class="fas fa-caret-right"></i></span></a>
        <ul id="sm-countries">
            <li><a href="/clients/create"><i class="fa fa-plus" aria-hidden="true"></i> Add Client</a></li>
            <li><a href="/clients/index"><i class="fa fa-users" aria-hidden="true"></i> All Clients</a></li>
            <li><a href="/clients/active"><i class="fa fa-check" aria-hidden="true"></i> Active Clients</a></li>
            <li><a href="/clients/leads"><i class="fa fa-spinner" aria-hidden="true"></i> Leads Clients</a></li>
            <li><a href="/clients/archive"><i class="fa fa-archive"></i> Archive Clients</a></li>
        </ul>
    </div>

    <ul id="countries">
        <li><a href="/clients/create" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i> Add Client</a></li>
        <li><a href="/clients/index"><i class="fa fa-users" aria-hidden="true"></i> All Clients</a></li>
        <li><a href="/clients/active"><i class="fa fa-check" aria-hidden="true"></i> Active Clients</a></li>
        <li><a href="/clients/leads"><i class="fa fa-spinner" aria-hidden="true"></i> Leads Clients</a></li>
        <li><a href="/clients/archive"><i class="fa fa-archive"></i> Archive Clients</a></li>
    </ul>
</div>