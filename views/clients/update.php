<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Edit Client');
?>

	<div class="options">
        <div class="choose-country d-sm-none d-md-none d-lg-none d-xl-none" id="country-click">
            <a href="#" class="choose-country-click">Choose Clients <span><i class="fas fa-caret-right"></i></span></a>
            <ul id="sm-countries">
                <li><a href="/clients/create"><i class="fa fa-plus" aria-hidden="true"></i> Add Client</a></li>
                <li><a href="/clients/index"><i class="fa fa-users" aria-hidden="true"></i> All Clients</a></li>
                <li><a href="/clients/active"><i class="fa fa-check" aria-hidden="true"></i> Active Clients</a></li>
                <li><a href="/clients/leads"><i class="fa fa-spinner" aria-hidden="true"></i> Leads Clients</a></li>
            </ul>
        </div>

        <ul id="countries">
            <li><a href="/clients/create" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i> Add Client</a></li>
            <li><a href="/clients/index"><i class="fa fa-users" aria-hidden="true"></i> All Clients</a></li>
            <li><a href="/clients/active"><i class="fa fa-check" aria-hidden="true"></i> Active Clients</a></li>
            <li><a href="/clients/leads"><i class="fa fa-spinner" aria-hidden="true"></i> Leads Clients</a></li>
        </ul>
    </div>

	<section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">Edit Client</div>
            </div>

            <div class="add-client-form-wrap update-client-form">
                <form>

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Manager <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager selectpicker" name="manager-input" onblur="this.style.color = '#000'" <?=(Yii::$app->user->identity->role < 9 && Yii::$app->user->identity->role != 2) ? 'disabled' : ''?> data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option <?= ($manager->manager_id == $model->manager_id) ? 'selected' : '' ?> value="<?=$manager->manager_id?>"><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Client date add <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date" class="input-date-add" <?= (!empty($model->date_add)) ? 'value="'.date('d-M-Y',$model->date_add).'"' : '' ?> autocomplete="off" name="date-input">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Request source <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-source" name="source-input" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($sources as $source) { ?>
                                        	<option <?= ($source->source_id == $model->source_id) ? 'selected' : '' ?> value="<?=$source->source_id?>"><?=$source->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Client status</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-source" name="source-input" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($statuses as $status) { ?>
                                            <option <?= ($status->status_id == $model->status_id) ? 'selected' : '' ?> value="<?=$status->status_id?>"><?=$status->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line add-client-form-line-checkbox">
                                <label>
                                    <input type="checkbox" name="status" class="input-status" <?= ($model->activation == 1) ? 'checked' : '' ?>>
                                    <p>Activate client</p>
                                    <span></span>
                                </label>
                            </div>

                            <div class="add-client-form-line">
                                <label>Client code</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" disabled="disabled" value="<?= (!empty($model->client_id)) ? '88-'.$model->client_id : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Name</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-name" value="<?= (!empty($model->name)) ? $model->name : '' ?>">
                                    <div class="hint">First Name</div>
                                </div>
                            </div>

                            <div class="add-client-form-line add-client-form-line-mt">
                                <label>Phone <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="tel" id='choose-phone' value="<?= (!empty($model->phone)) ? $model->phone : '' ?>" name="phone" class="contacts">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Email <span>*</span></label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="email" placeholder="" class="input-email contacts" value="<?= (!empty($model->email)) ? $model->email : '' ?>" name="email">
                                    <span class="input-icon"><i class="far fa-envelope"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Comment</label>
                                <div class="input-addclient-wrap">
                                    <textarea name="name" class="input-comment" placeholder="Comment task"><?= (!empty($model->comment)) ? $model->comment : '' ?></textarea>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-4 offset-lg-1 col-md-12">
                            <div class="add-client-form-line mb">
                                <label>Next Contact Date</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date-time" class="input-date_next" <?= (!empty($model->date_next_connect)) ? 'value="'.date('d.m.Y H:i',$model->date_next_connect).'"' : '' ?> autocomplete="off">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Description</label>
                                <div class="input-addclient-wrap">
                                    <textarea name="name" class="input-description" placeholder="Comment task"></textarea>
                                </div>
                            </div>
                            <div class="add-new-form-submit-buttons-wrap-width" style="display: inline;">
                                <a class="submit-form-task" style="width: 100% !important;">Call customer back <img src="/web/img/preloader.gif" style="display: none;"></a>
                            </div>
                        </div>
                    </div>

                    <div class="add-new-section row no-fw-row">
                        <div class="col-lg-4 col-12">
                            <div class="bottom-part-title">
                                Client Balance
                            </div>
                            <div class="add-client-form-line">
                                <label>Balance now $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00$" disabled  value="<?= (!empty($model->balance)) ? $model->balance.'$' : '0' ?>"> 
                                </div>
                            </div>
                            <div class="add-client-form-line">
                                <label>Money for all time $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00$" disabled  value="<?= (!empty($model->balance_total)) ? $model->balance_total.'$' : '0' ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="add-new-section row no-fw-row">
                        <div class="col-lg-4 col-12">
                            <div class="bottom-part-title">
                                Client Weight
                            </div>
                            <div class="add-client-form-line">
                                <label>Total Weight Kg.</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="" disabled  value="<?= (!empty($allWeight)) ? $allWeight : '0' ?>"> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="add-new-section row no-fw-row">
                        <div class="col-xl-4 col-12">

                            <div class="add-new-item">
                                <div class="add-new-item-title">Client Messanger</div>
                                <div class="add-new-item-top-line">
                                    <span>Client Messanger</span>
                                    <span>Client Messanger</span>
                                </div>
                                <?php if(isset($messenger->messenger_id)){ ?>
                                    <div class="add-new-item-button-inputs-temp">
                                    	<div class="add-new-item-button-inputs">
    							            <div class="add-client-form-line">
    							                <div class="input-addclient-wrap select-input">
    							                    <select class="client_messanger_type" name="manager" onblur="this.style.color = '#000'" style="color: rgb(0, 0, 0);">
    							                        <option value="" selected class="placeholder-select">-Select-</option>
    							                        <option <?= ($messenger->messenger_id == 1) ? 'selected' : '' ?> value="1" class="option">Skype</option>
    							                        <option <?= ($messenger->messenger_id == 2) ? 'selected' : '' ?> value="2">Telegram</option>
    							                        <option <?= ($messenger->messenger_id == 3) ? 'selected' : '' ?> value="3">Viber</option>
    							                        <option <?= ($messenger->messenger_id == 4) ? 'selected' : '' ?> value="4">Wechat</option>
    							                    </select>
    							                </div>
    							            </div>
    							            <div class="add-client-form-line">
    							                <div class="input-addclient-wrap ">
    							                    <input type="text" placeholder="" class="client_messanger_number" value="<?= (!empty($messenger->number)) ? $messenger->number : '' ?>">
    							                </div>
    							            </div>
    							        </div>
                                    </div>
                                <?php } ?>
                                <div class="add-new-item-button">
                                    <a href="#" id='add-client'><i class="fas fa-plus"></i> Add New</a>
                                </div>
                            </div>

                            <div class="add-new-item">
                                <div class="add-new-item-title">Route</div>
                                <div class="add-new-item-top-line">
                                    <span>Delivery from</span>
                                    <span>Delivery to</span>
                                </div>
                                <?php if(isset($route->from_id) && isset($route->to_id)){ ?>
                                    <div class="add-new-item-button-inputs-temp">
                                    	<div class="add-new-item-button-inputs">
    							            <div class="add-client-form-line">
    							                <div class="input-addclient-wrap select-input">
    							                    <select class="route-from" name="manager" onblur="this.style.color = '#000'" style="color: rgb(0, 0, 0);">
    							                        <option value="" selected class="placeholder-select">-Select-</option>
    							                        <option <?= ($route->from_id == 1) ? 'selected' : '' ?> value="1" class="option">China</option>
    							                        <option <?= ($route->from_id == 2) ? 'selected' : '' ?> value="2">Hong Kong</option>
    							                        <option <?= ($route->from_id == 3) ? 'selected' : '' ?> value="3">Poland</option>
    							                        <option <?= ($route->from_id == 4) ? 'selected' : '' ?> value="4">USA</option>
    							                        <option <?= ($route->from_id == 5) ? 'selected' : '' ?> value="5">Ukraine</option>
    							                    </select>
    							                </div>
    							            </div>
    							            <div class="add-client-form-line">
    							                <div class="input-addclient-wrap select-input">
    							                    <select class="route-to" name="manager" onblur="this.style.color = '#000'">
    							                        <option value="" selected class="placeholder-select">-Select-</option>
    							                        <option <?= ($route->to_id == 1) ? 'selected' : '' ?> value="1" class="option">China</option>
    							                        <option <?= ($route->to_id == 2) ? 'selected' : '' ?> value="2">Hong Kong</option>
    							                        <option <?= ($route->to_id == 3) ? 'selected' : '' ?> value="3">Poland</option>
    							                        <option <?= ($route->to_id == 4) ? 'selected' : '' ?> value="4">USA</option>
    							                        <option <?= ($route->to_id == 5) ? 'selected' : '' ?> value="5">Ukraine</option>
    							                    </select>
    							                </div>
    							            </div>
    							        </div>
                                    </div>
                                <?php } ?>
                                <div class="add-new-item-button">
                                    <a href="#" id='add-route'><i class="fas fa-plus"></i> Add New</a>
                                </div>
                            </div>

                            <div class="add-new-item add-new-item-mw">
                                <div class="add-new-item-title">Pricing</div>
                                <div class="add-new-item-top-line">
                                    <span>Cargo Type</span>
                                    <span><span class="req">*</span>Tariff measure</span>
                                    <span>Tariff</span>
                                    <span></span>
                                </div>
                                <?php if(isset($tariff->type_cargo_id) && isset($tariff->tariff_id)){ ?>
                                    <div class="add-new-item-button-inputs-temp">
                                    	<div class="add-new-item-button-inputs">
    							            <div class="add-client-form-line">
    							                <div class="input-addclient-wrap select-input">
    							                    <select class="pricing-type" name="manager" onblur="this.style.color = '#000'" style="color: rgb(0, 0, 0);">
    							                        <option value="" selected class="placeholder-select">-Select-</option>
    							                        <option <?= ($tariff->type_cargo_id == 1) ? 'selected' : '' ?> value="1" class="option">General</option>
    							                        <option <?= ($tariff->type_cargo_id == 2) ? 'selected' : '' ?> value="2">Electronics</option>
    							                        <option <?= ($tariff->type_cargo_id == 3) ? 'selected' : '' ?> value="3">Brands</option>
    							                    </select>
    							                </div>
    							            </div>
    							            <div class="add-client-form-line">
    							                <div class="input-addclient-wrap select-input">
    							                    <select class="pricing-measure" name="manager" onblur="this.style.color = '#000'">
    							                        <option value="" selected class="placeholder-select">-Select-</option>
    							                        <option <?= ($tariff->tariff_id == 1) ? 'selected' : '' ?> value="1" class="option">usd/kg</option>
    							                        <option <?= ($tariff->tariff_id == 2) ? 'selected' : '' ?> value="2">usd/pcs</option>
    							                        <option <?= ($tariff->tariff_id == 3) ? 'selected' : '' ?> value="3">usd/m3</option>
    							                    </select>
    							                </div>
    							            </div>
    							            <div class="add-client-form-line">
    							                <div class="input-addclient-wrap">
    							                    <input type="text" placeholder="0.00" class="pricing-tariff" value="<?= (!empty($tariff->number)) ? $tariff->number : '' ?>">
    							                </div>
    							            </div>
                                            <div class="add-client-form-line">
                                                <div class="input-addclient-wrap">
                                                    
                                                </div>
                                            </div>
    							        </div>
                                    </div>
                                <?php } ?>
                                <div class="add-new-item-button">
                                    <a href="#" id='add-pricing'><i class="fas fa-plus"></i> Add New</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-12">
                            <div class="add-new-section row no-fw-row">
                                <div class="col-lg-8 col-12">
                                    <div class="add-new-item-title" style="font-size: 15px;margin-bottom: 10px;">Money Transfer Income</div>
                                    <table class="table table-hover" style="width: 100% !important;border-right: 1px solid #dee2e6;">
                                      <thead>
                                        <tr>
                                            <th scope="col">Date</th>
                                            <th scope="col">Paid $</th>
                                            <th scope="col">Project</th>
                                            <?=(Yii::$app->user->identity->role >= 10) ? '<th scope="col">Balance after</th>' : ''; ?>
                                            <th scope="col">Comment</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php foreach ($transfers as $transfer) { ?>
                                            <tr>
                                                <td><?= date('d.m.Y',$transfer->date_payment) ?></td>
                                                <td style="<?= ($transfer->payment_type == 1) ? 'color: green;' : 'color: red;' ?>"><?= ($transfer->payment_type == 1) ? '+'.$transfer->cash_usd : '-'.$transfer->cash_usd ?></td>
                                                <td style="white-space: initial;overflow: auto;"><?= (!empty($transfer->project_id)) ? '<a target="_blank" href="/projects/edit/'.$transfer->project_id.'">PRJ-'.$transfer->project_id.'</a>' : '' ?></td>
                                                <?=(Yii::$app->user->identity->role >= 10) ? '<td style="white-space: initial;overflow: auto;">'. $transfer->balance .'</td>' : ''; ?>
                                                <td style="white-space: initial;overflow: auto;"><?= $transfer->comment ?></td>
                                                
                                            </tr>
                                        <?php } ?> 
                                      </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="bottom-part row no-fw-row">
                        <div class="bottom-part-title">
                            Local delivery Ukraine
                        </div>

                        <div class="col-lg-4 col-12">
                            <div class="add-client-form-line">
                                <label>Courier service</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-courier" name="manager" onblur="this.style.color = '#000'" >
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($deliveries as $delivery) { ?>
                                        	<option <?= (isset($delivery_now) && $delivery->courier_id == $delivery_now->courier_id) ? 'selected' : '' ?> value="<?=$delivery->courier_id?>"><?=$delivery->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Receiver</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="" class="delivery-receiver" value="<?= (isset($delivery_now)) ? $delivery_now->receiver : '' ?>" >
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Mobile No.</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="" class="delivery-phone" value="<?= (isset($delivery_now)) ? $delivery_now->mobile : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>State</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="delivery-state" name="manager" onblur="this.style.color = '#000'" >
                                        <option value="0">-Select-</option>
                                        <?php foreach ($states as $state) { ?>
                                            <option value="<?=$state->id?>" <?=(isset($delivery_now->state) && $delivery_now->state == (int)$state->id) ? 'selected' : '' ?>><?=$state->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="add-client-form-line">
                                <label>City</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="delivery-city" name="manager" onblur="this.style.color = '#000'">
                                        <option value="0">Select state</option>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Warehouse No. / Address</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="" class="delivery-address" value="<?= (isset($delivery_now)) ? $delivery_now->address : '' ?>">
                                </div>
                            </div>

                        </div>

                        <div class="add-new-form-submit-buttons col-12">
                            <div class="add-new-form-submit-buttons-wrap col-4">
                                <div class="add-new-form-submit-buttons-wrap-width">
                                    <button  class="submit-form">Save <img src="/web/img/preloader.gif" style="display: none;"></button>
                                    <a href="#" class="reset-form">Reset</a>
                                </div>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        //RULES FORM
        $(".update-client-form form").validate({
            groups: {
                contacts: "email phone"
            },
            rules: {
              "manager-input": {
                  required: true
              },
              "date-input": {
                  required: true
              },
              'source-input': {
                  required: true
              },
              'email': {
                  require_from_group: [1, '.contacts'],
              },
              'phone': {
                  require_from_group: [1, '.contacts'],
              },
            },
            messages: {
              "manager-input": {
                  required: "Please select manager",
              },
              "date-input": {
                  required: "Please select date",
              },
              "source-input": {
                  required: "Please select source",
              },
              'email': {
                  require_from_group: 'Please enter email or phone',
              },
              'phone': {
                  require_from_group: 'Please enter email or phone',
              },
            },
            submitHandler: function(form) {
              submitForm();
            }
        });
    
    </script>

    <script type="text/javascript">
    	function submitForm(){
                //блок кнопки и прелоадер
                $('.submit-form').find('img').css('display','');
                $('.submit-form').attr('disabled', 'true');
                ///
        		if ($(".input-status").is(":checked")) {
    			    var input_status = 1;
    			}
    			else {
    			    var input_status = 0;
    			}

        		$.ajax({ 
    	            type: 'POST', 
    	            url: '/clients/edit/<?= $model->client_id ?>', 
    	            data:{
    		            manager: 	$('.select-manager option:selected').val(), 
    	            	date_add: 	$('.input-date-add').val(), 
    	            	source: 	$('.select-source option:selected').val(), 
                        status_id:  $('.status-select option:selected').val(),
    	            	status: 	input_status,//$('.input-status').val(), 
    	            	name: 		$('.input-name').val(),
    	            	//phone_code: $('.selected-dial-code').text(), 
    	            	phone: 		$('.selected-dial-code').text()+''+$('#choose-phone').val(), 
    	            	email: 		$('.input-email').val(), 
    	            	comment: 	$('.input-comment').val(), 
    	            	date_next: 	$('.input-date_next').val(), 

    	            	delivery_courier: 	$('.select-courier option:selected').val(), 
    	            	delivery_receiver: 	$('.delivery-receiver').val(), 
    	            	delivery_phone: 	$('.delivery-phone').val(), 
                        delivery_state:     $('.delivery-state option:selected').val(), 
    	            	delivery_city: 		$('.delivery-city option:selected').val(), 
    	            	delivery_address: 	$('.delivery-address').val(), 

    	            	client_messanger_type: 		$('.client_messanger_type option:selected').val(),
    	            	client_messanger_number: 	$('.client_messanger_number').val(), 

    	            	route_from: $('.route-from option:selected').val(),
    	            	route_to: 	$('.route-to option:selected').val(),

    	            	pricing_type: 		$('.pricing-type option:selected').val(),
    	            	pricing_measure: 	$('.pricing-measure option:selected').val(),
    	            	pricing_tariff: 	$('.pricing-tariff').val(),
    	            	'<?= \yii::$app->request->csrfParam?>': '<?= \yii::$app->request->csrfToken ?>'
    		        },
    	            //dataType: 'json',
    	            success: function (data) { 
    	                console.log(data);
    	                if (data == 1) {
    	                	window.location.replace("/clients/index?sort=<?=Yii::$app->request->get('sort')?>");
    	                }
    	            }
    	        });
    	}

        $('.submit-form-task').click(function(){
            if($('.input-date_next').val() != ''){
                submitFormTask();
            }else{
                alert('Please enter "Next Contact Date"!');
            }
        });

        function submitFormTask(){
                //блок кнопки и прелоадер
                $('.submit-form-task').find('img').css('display','');
                $('.submit-form-task').attr('disabled', 'true');
                ///
                $.ajax({ 
                    type: 'POST', 
                    url: '/clients/tasknew/<?= $model->client_id ?>', 
                    data:{
                        date:  $('.input-date_next').val(), 
                        description:  $('.input-description').val(), 
                        '<?= \yii::$app->request->csrfParam?>': '<?= \yii::$app->request->csrfToken ?>'
                    },
                    success: function (data) { 
                        if (data == 1) {
                            window.location.replace("/clients/index?sort=<?=Yii::$app->request->get('sort')?>");
                        }else{
                            alert('Error save call back!');
                        }
                    }
                });
        }

        $('.mob-menu-Clients > a').addClass('active');

        $('.delivery-state').change(function(){
            changeState();
        });
        changeState();
        function changeState(){
            $.ajax({ 
                type: 'POST', 
                url: '/clients/city/' + $('.delivery-state').val(), 
                data: {
                    city:   <?= $delivery->city ?? 0 ?>,
                }, 
                success: function (data) { 
                    $('.delivery-city').html(data);
                }
            });
        }
        
        // $(document).keyup(function(e) {
        //     if(e.which == 13){ //enter press
        //         $('.submit-form').trigger('click');
        //     }
        //     if (e.key === "Escape") { 
        //         $('.reset-form').trigger('click');
        //     }
        // });
        $('.reset-form').click(function(){
            location.reload();
        });
    </script>
