<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Client;
use app\models\CustomTable;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Clients';
?>

    <?=$menu?>

    <style type="text/css">
    	.table-bordered tbody{
		  display:block;
		  overflow:auto;
		  height:200px;
		  width:100%;
		  overflow-x: hidden;
		}
		.table-bordered thead tr{
		  display:block;
		}
    </style>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        All Clients * <?= Client::SearchShowBtn() ?>
                    </div>
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                    <div class="add-new">
                        <div class="dropdown dropdown-table custom-table-div" style="">
                        	<a class="custom-table-menu" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                      <i class="fas fa-cog"></i> Custom table
		                    </a>
		                    <div class="dropdown-menu dropdown-menu-table custom-table-checked" aria-labelledby="dropdownMenuLink">
							    <a class="custom-table-btn"><i class="fas fa-save"></i> Save setting</a><Br>
		                    	<?= CustomTable::menuOptions('client', 'client') ?>
		                    </div>
                        </div>
                    </div>
                </div>
				<div class="table-wrap clent-wrap">
				    <?= GridView::widget([
				        'dataProvider' => $dataProvider,
				        'rowOptions'=>function($model){
					        return ['class' => 'client-row contextmenu-row', 'url-edit' => '/clients/edit/'.$model->client_id, 'url-delete' => '/clients/delete/'.$model->client_id];
					    },
				        'columns' => [
				            [
				            	'attribute' => 'check',
					            'label' => '<input type="checkbox" class="checkbox-table-all" value="">',
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
					            "format" => 'raw',
					            'value' => function ($data) {
					                return '<input type="checkbox" class="checkbox-table" value="'.$data->client_id.'"> <i class="far fa-eye eye-click-view-row" title="View" data-key="'.$data->client_id.'"></i><a href="/clients/edit/'.$data->client_id.'" class="table-edit-btn"><i class="fas fa-pencil-alt"></i></a>'; 
					            },
					        ],
				            [
				            	'attribute' => 'client_id',
					            'label' => Client::clientDropMenu(Client::attributeLabels()['client_id'], 'client_id', 'text', 'Write client id'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('client','client','client_id')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('client','client','client_id')],
					            'value' => function ($data) {
					                return (!empty($data->client_id)) ? '88-'.$data->client_id : '';
					            },
					        ],
				            [
				            	'attribute' => 'manager_id',
					            'label' => Client::clientDropMenu(Client::attributeLabels()['manager_id'], 'manager_id', 'select', 'Select Manager'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('client','client','manager_id').CustomTable::widthTable(140)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('client','client','manager_id').CustomTable::widthTable(140)],
					            'value' => function ($data) {
					                return $data->manager->username ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'balance',
					            'label' => Client::clientDropMenu(Client::attributeLabels()['balance'], 'balance', 'text', 'Write balance'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('client','client','balance')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('client','client','balance')],
					            'value' => function ($data) {
					                return (!empty($data->balance)) ? round($data->balance) : '0';
					            },
					        ],
				            [
				            	'attribute' => 'date_add',
					            'label' => Client::clientDropMenu(Client::attributeLabels()['date_add'], 'date_add', 'date', 'Select date'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('client','client','date_add')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('client','client','date_add')],
					            'value' => function ($data) {
					                return (!empty($data->date_add)) ? date('d.m.Y',$data->date_add) : ''; 
					            },
					        ],
				            [
				            	'attribute' => 'name',
					            'label' => Client::clientDropMenu(Client::attributeLabels()['name'], 'name', 'text', 'Write client name'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('client','client','name').CustomTable::widthTable(220)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('client','client','name').CustomTable::widthTable(220)],
					            'value' => function ($data) {
					                return $data->name ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'phone',
					            'label' => Client::clientDropMenu(Client::attributeLabels()['phone'], 'phone', 'text', 'Write phone'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('client','client','phone').CustomTable::widthTable(150)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('client','client','phone').CustomTable::widthTable(150)],
					            'value' => function ($data) {
					                return $data->phone ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'email',
					            'label' => Client::clientDropMenu(Client::attributeLabels()['email'], 'email', 'text', 'Write email'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('client','client','email').CustomTable::widthTable(200)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('client','client','email').CustomTable::widthTable(200)],
					            'value' => function ($data) {
					                return $data->email ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'source_id',
					            'label' => Client::clientDropMenu(Client::attributeLabels()['source_id'], 'source_id', 'select', 'Select source'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('client','client','source_id').CustomTable::widthTable(150)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('client','client','source_id').CustomTable::widthTable(150)],
					            'value' => function ($data) {
					                return $data->source->name ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'status_id',
					            'label' => Client::clientDropMenu(Client::attributeLabels()['status_id'], 'status_id', 'select', 'Select status'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('client','client','status_id').CustomTable::widthTable(150)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('client','client','status_id').CustomTable::widthTable(150)],
					            'value' => function ($data) {
					                return $data->clientstatus->name ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'activation',
					            'label' => Client::clientDropMenu(Client::attributeLabels()['activation'], 'activation', 'select', 'Select status'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('client','client','activation')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('client','client','activation')],
					            'value' => function ($data) {
					            	if($data->activation > 0)
					                	return 'Active';
					                else
					                	return 'No active';
					            },
					        ],
				            [
				            	'attribute' => 'comment',
					            'label' => Client::clientDropMenu(Client::attributeLabels()['comment'], 'comment', 'text', 'Write comment'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('client','client','comment').CustomTable::widthTable(600)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('client','client','comment').CustomTable::widthTable(600).'overflow-x: auto;'],
					            'value' => function ($data) {
					            	return $data->comment;
					            },
					        ],
				        ],
				    ]); ?>
				</div>

			    <div class="detail-info-top-line">
                    <div class="detail-info-controls">
                        <a href="#" id="slide-position-left"><i class="fas fa-chevron-left"></i></a>
                        <a href="#" id="slide-position-right"><i class="fas fa-chevron-right"></i></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="" class="client-detail-info-url-edit"><i class="fas fa-pencil-alt"></i>Edit</a>
                            <a data-id="" class="delete-post-url" style="display: <?=(Yii::$app->user->identity->role >= 9)?';':'none;'?>"><i class="far fa-trash-alt"></i>Delete</a>
                            <a href="#" id="close-detail-info"><i class="fas fa-times"></i></a>
                        </div>
                    </div>
                </div>

                <div class="detail-info-wrap">

                    <div class="detail-info">

                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client date add</div>
                                <div class="detail-info-list-column detail-info-date_add"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Request source</div>
                                <div class="detail-info-list-column detail-info-source"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client status</div>
                                <div class="detail-info-list-column detail-info-client-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Activate client</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client code</div>
                                <div class="detail-info-list-column detail-info-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Name</div>
                                <div class="detail-info-list-column detail-info-name"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Phone</div>
                                <div class="detail-info-list-column detail-info-phone"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Email</div>
                                <div class="detail-info-list-column detail-info-email"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Comment</div>
                                <div class="detail-info-list-column detail-info-comment"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Balance now $</div>
                                <div class="detail-info-list-column detail-info-balance"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Money for all time $</div>
                                <div class="detail-info-list-column detail-info-balance_total"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client all weight (kg.)</div>
                                <div class="detail-info-list-column detail-info-weight"></div>
                            </div>
                        </div>
                        <div class="detail-info-list-wrap client_money_all_block">
                            <label>Client Money Transfers</label>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Date</div>
                                <div class="detail-info-list-column column-grey">Paid $</div>
                                <div class="detail-info-list-column column-grey">Project</div>
                                <div class="detail-info-list-column column-grey">Balance</div>
                                <div class="detail-info-list-column column-grey">Comment</div>
                            </div>
                            <div class="client_money_all">
                                
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing summ_bottom">
                </div>
                <div class="col-6 summary">
                    <!-- <img src="/web/img/list-icon.png" alt="#"> <a href="#">Show Summary</a> -->
                </div>
            </div>
        </div>
    </section>

    <nav id="context-menu" class="context-menu">
	    <ul class="context-menu__items">
	      <li class="context-menu__item">
	        <a href="#" class="context-menu__link" data-action="View"><i class="fa fa-eye"></i> View</a>
	      </li>
	      <li class="context-menu__item">
	        <a href="#" class="context-menu__link" data-action="Edit"><i class="fa fa-edit"></i> Edit</a>
	      </li>
	      <?php if(Yii::$app->user->identity->role >= 9){ ?>
		      <li class="context-menu__item">
		        <a href="#" class="context-menu__link" data-action="Delete"><i class="fa fa-times"></i> Delete</a>
		      </li>
		  <?php } ?>
	    </ul>
	</nav>

<script src="/web/js/contecst_menu.js"></script>
<script type="text/javascript">
	
    setInterval(function() {
        tableHeight();
    }, 500);

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        summ_height = window_height - ($('.all-cargos-title').height() + $('header').height() + $('.options').height() + 50);
        if(summ_height < 500){
            summ_height = 500;
        }
        $('.table-wrap').css({'max-height' : summ_height+'px'});
        $('.table-bordered').css({'max-height' : summ_height+'px'});
        $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
    }

    $('.mob-menu-Clients > a').addClass('active');

    $('.summ_bottom').html($('.summary').html());
    $('.summary').html('');


	$(document).on('keyup', '.dropdown-search-input', function (event) {
        if(event.keyCode==13) {
			runSearch($(this).attr("data-type"),$(this).val());
		}
    });

	$(document).on('click', '.search-menu-click', function () {
        runSearch($(this).attr("data-type"),$(this).attr("data-id"));
    });

	$(document).on('change', '#form-choose-date', function () {
        runSearch($(this).attr("data-type"),$(this).val());
    });

    function runSearch(type, value){
        $.ajax({ 
            type: 'POST', 
            url: '/clients/search?'+type+'='+value+'&sort=<?=Yii::$app->request->get('sort')?>', 
            success: function (data) { 
                $('.all-cargos-content').html(data);
            }
        });
    };

    $(".delete-post-url").click(function() {
        var delete_text = confirm('Do you really want to delete client?');
        if(delete_text) window.location.replace('/clients/delete/'+$(this).attr("data-id"));
    });

	$('.custom-table-btn').click(function() {
		var sListType = '';
		$('.custom-table-checked input').each(function () {
			if(this.checked == "1"){
		    	sListType += $(this).val() + ",";
		    }
		});
		if(sListType != ''){
			$.ajax({ 
                type: 'GET', 
                url: '/clients/customtable?page=client&type='+sListType, 
                success: function (data) { 
                    location.reload();
                }
            });
		}
	});

	$(document).on('click', '.checkbox-table-all', function () {
        $('.checkbox-table').not(this).prop('checked', this.checked);
    	allCargosSelectEdits(1);
    });

    $(document).on('click', '.checkbox-table', function () {
    	if(this.checked == "1"){
			$('.client-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
		}else{
			$('.client-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
		}
        allCargosSelectEdits();
    });

	function allCargosSelectEdits(type = 0){
		var sList = '';
		///////
		$('.checkbox-table').each(function () {
			if(this.checked == "1"){
				if(type == 1){
					$('.client-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
				}
		    	sList += $(this).val() + ",";
		    }else if(type == 1){
				$('.client-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
		    }
		});
	}
</script>