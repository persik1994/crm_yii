<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Clients');
//$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="options">
        <div class="choose-country d-sm-none d-md-none d-lg-none d-xl-none" id="country-click">
            <a href="#" class="choose-country-click">Choose Clients <span><i class="fas fa-caret-right"></i></span></a>
            <ul id="sm-countries">
                <li><a href="/clients/create"><i class="fa fa-plus" aria-hidden="true"></i> Add Client</a></li>
                <li><a href="/clients/index"><i class="fa fa-users" aria-hidden="true"></i> All Clients</a></li>
                <li><a href="/clients/active"><i class="fa fa-check" aria-hidden="true"></i> Active Clients</a></li>
                <li><a href="/clients/leads"><i class="fa fa-spinner" aria-hidden="true"></i> Leads Clients</a></li>
            </ul>
        </div>

        <ul id="countries">
            <li><a href="/clients/create" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i> Add Client</a></li>
            <li><a href="/clients/index"><i class="fa fa-users" aria-hidden="true"></i> All Clients</a></li>
            <li><a href="/clients/active"><i class="fa fa-check" aria-hidden="true"></i> Active Clients</a></li>
            <li><a href="/clients/leads"><i class="fa fa-spinner" aria-hidden="true"></i> Leads Clients</a></li>
        </ul>
    </div>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">

                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        All Clients *
                    </div>
                    <!-- <div class="sorting">
                        <div class="sorting-title">sorting</div>
                        <div class="picked-sorting">Clients date recelved <i class="fas fa-caret-down"></i> <a href="#"><img src="/web/img/cancel-icon.png" alt="#"></a></div>
                    </div> -->
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                    <div class="add-new">
                        <!-- <div class="ad-search">
                            <a href="#"><i class="fas fa-search"></i></a>
                        </div>
                        <div class="ad-button">
                            <a href="#"><i class="fas fa-plus"></i> Add</a>
                        </div>
                        <div class="ad-menu">
                            <a href="#"><i class="fas fa-bars"></i></a>
                        </div> -->
                    </div>
                </div>

                <div class="table-wrap">
                    <table class="table table-striped table-bordered table-resizable custom-tab">
                        <thead>
                                <tr>
                                    <!-- <th class="low-w"><i class="far fa-eye"></i></th>
                                    <th class="low-w"><input type="checkbox"></th> -->
                                    <th>Manager</th>
                                    <th>Client code</th>
                                    <th>Client balance</th>
                                    <th>Client date add</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Client Messenger</th>
                                    <th>Pricing</th>
                                    <th>Route</th>
                                    <th>Request source</th>
                                    <!-- <th>Request number</th> -->
                                    <th>Activate client</th>
                                    <!-- <th>Lead number</th> -->
                                    <th>Comment</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?= $all_table_rows ?>
                            </tbody>
                    </table>
                </div>

                <div class="detail-info-top-line">

                    <div class="detail-info-controls">
                        <a href="#" id="slide-left"><i class="fas fa-chevron-left"></i></a>
                        <a href="#" id="slide-right"><i class="fas fa-chevron-right"></i></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="/clients/edit/" class="client-detail-info-url-edit"><i class="fas fa-pen"></i>Edit</a>
                            <a href="#" id="close-detail-info"><i class="fas fa-times"></i></a>
                        </div>
                    </div>

                </div>

                <div class="detail-info-wrap">

                    <div class="detail-info">

                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Name</div>
                                <div class="detail-info-list-column detail-info-name"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Phone</div>
                                <div class="detail-info-list-column detail-info-phone"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Email</div>
                                <div class="detail-info-list-column detail-info-email"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client Messenger</div>
                                <div class="detail-info-list-column detail-info-messenger"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Request source</div>
                                <div class="detail-info-list-column detail-info-source"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Activate client</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client code</div>
                                <div class="detail-info-list-column detail-info-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Comment</div>
                                <div class="detail-info-list-column detail-info-comment"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client balance $</div>
                                <div class="detail-info-list-column detail-info-balance"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client balance total $</div>
                                <div class="detail-info-list-column detail-info-balance_total"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client date add</div>
                                <div class="detail-info-list-column detail-info-date_add"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing">
                    Showing <?=$clientsCount?> of <?=$clientsCount?>
                </div>
                <div class="col-6 summary">
                    <!-- <img src="/web/img/list-icon.png" alt="#"> <a href="#">Show Summary</a> -->
                </div>
            </div>
        </div>
    </section>

<script type="text/javascript">
    $(window).resize(function() {
        tableHeight();
    });

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        body_height = $('body').height();
        table = $('.table-wrap').height();
        summ_height = window_height - (body_height - table) - 15;
        $('.table-wrap').css({'max-height' : summ_height+'px'});
    }

    $('.mob-menu-Clients > a').addClass('active');

    /////////////////////////////////////////////////

    $(".table tbody .client-row").on("click", function() {
        var cur_id = $(this).attr("data-client_id");
        var re, s;
        s = cur_id;
        re = /\D+/ig;
        index = s.replace(re, '');

        $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
        $('.detail-info-top-line').animate({
            opacity: 1
        }, 700);
        $('.detail-info-top-line').css({
            "display": "flex"
        });
        $(".detail-info-wrap").find('.detail-info').fadeIn(700).addClass("detail-visibe");

        $('.client-detail-info-url-edit').attr("href", '/clients/edit/' + $(this).attr("data-client_id"));
        $('.client-detail-info-url-delete').attr("href", '/clients/delete/' + $(this).attr("data-client_id"));

        $.ajax({ 
            type: 'GET', 
            url: '/clients/view/' + $(this).attr("data-client_id"), 
            data: {}, 
            dataType: 'json',
            success: function (data) { 
                $('.detail-info-manager').text(data.manager_id);
                $('.detail-info-name').text(data.name);
                $('.detail-info-phone').text(data.phone);
                $('.detail-info-email').text(data.email);
                $('.detail-info-messenger').text();
                $('.detail-info-source').text(data.source_id);
                $('.detail-info-status').text(data.activation);
                $('.detail-info-id').text('88-' + data.client_id);
                $('.detail-info-comment').text(data.comment);
                $('.detail-info-balance').text(data.balance);
                $('.detail-info-balance_total').text(data.balance_total);
                $('.detail-info-date_add').text(data.date_add);
                console.log(data);
            }
        });
    });
</script>
