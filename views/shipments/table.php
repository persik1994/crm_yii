<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Shipment;
use app\models\CustomTable;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Shipments');
//$this->params['breadcrumbs'][] = $this->title;
?>

    <?=$menu?>

    <style type="text/css">
        .table-bordered tbody{
          display:block;
          overflow:auto;
          height:200px;
          width:100%;
          overflow-x: hidden;
        }
        .table-bordered thead tr{
          display:block;
        }
    </style>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">

                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        All Shipments * <?= Shipment::SearchShowBtn() ?>
                    </div>
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                    <div class="add-new">
                        <div class="dropdown dropdown-table custom-table-div" style="">
                            <a class="custom-table-menu" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-cog"></i> Custom table
                            </a>
                            <div class="dropdown-menu dropdown-menu-table custom-table-checked" aria-labelledby="dropdownMenuLink">
                                <a class="custom-table-btn"><i class="fas fa-save"></i> Save setting</a><Br>
                                <?= CustomTable::menuOptions('shipment', 'shipment') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-wrap shipment-wrap">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'rowOptions'=>function($model){
                            return ['class' => 'shipment-row contextmenu-row', 'url-edit' => '/shipments/edit/'.$model->shipment_id, 'url-delete' => '/shipments/delete/'.$model->shipment_id];
                        },
                        'columns' => [
                            [
                                'attribute' => 'check',
                                'label' => '<input type="checkbox" class="checkbox-table-all" value="">',
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                "format" => 'raw',
                                'value' => function ($data) {
                                    return '<input type="checkbox" class="checkbox-table" value="'.$data->shipment_id.'"> <i class="far fa-eye eye-click-view-row" title="View" data-key="'.$data->shipment_id.'"></i><a href="/shipments/edit/'.$data->shipment_id.'" class="table-edit-btn"><i class="fas fa-pencil-alt"></i></a>'; 
                                },
                            ],
                            [
                                'attribute' => 'shipment_id',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['shipment_id'], 'shipment_id', 'text', 'Write shipment id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','shipment_id').CustomTable::widthTable(150)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','shipment_id').'overflow-x: auto;'.CustomTable::widthTable(150)],
                                'value' => function ($data) {
                                        return ((!empty($data->shipment_id)) ? 'SH-'.$data->shipment_id : '').((!empty($data->name)) ? '-'.$data->name : '');
                                },
                            ],
                            [
                                'attribute' => 'date_created',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['date_created'], 'date_created', 'date', 'Select date'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','date_created')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','date_created')],
                                'value' => function ($data) {
                                    return (!empty($data->date_created)) ? date('d.m.Y',$data->date_created) : ''; 
                                },
                            ],
                            [
                                'attribute' => 'from_id',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['from_id'], 'from_id', 'select', 'Select from'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','from_id').CustomTable::widthTable(130)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','from_id').CustomTable::widthTable(130)],
                                'value' => function ($data) {
                                    return $data->from->name ?? '';
                                },
                            ],
                            [
                                'attribute' => 'to_id',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['to_id'], 'to_id', 'select', 'Select to'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','to_id').CustomTable::widthTable(130)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','to_id').CustomTable::widthTable(130)],
                                'value' => function ($data) {
                                    return $data->to->name ?? '';
                                },
                            ],
                            [
                                'attribute' => 'manager_id',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['manager_id'], 'manager_id', 'select', 'Select Manager'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','manager_id').CustomTable::widthTable(140)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','manager_id').CustomTable::widthTable(140)],
                                'value' => function ($data) {
                                    return $data->manager->username ?? '';
                                },
                            ],
                            [
                                'attribute' => 'status_id',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['status_id'], 'status_id', 'select', 'Select status'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','status_id').CustomTable::widthTable(120)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','status_id').CustomTable::widthTable(120)],
                                'value' => function ($data) {
                                    return $data->cargoStatus->name ?? '';
                                },
                            ],
                            [
                                'attribute' => 'checkpoint',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['checkpoint'], 'checkpoint', 'select', 'Select checkpoint'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','checkpoint').CustomTable::widthTable(150)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','checkpoint').CustomTable::widthTable(150)],
                                'value' => function ($data) {
                                    return $data->warehouse->name ?? '';
                                },
                            ],
                            [
                                'attribute' => 'type_id',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['type_id'], 'type_id', 'select', 'Select type'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','type_id')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','type_id')],
                                'value' => function ($data) {
                                    return $data->type->name ?? '';
                                },
                            ],
                            [
                                'attribute' => 'weight',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['weight'], 'weight', 'text', 'Write weight'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','weight')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','weight')],
                                'value' => function ($data) {
                                    return $data->weight ?? '';
                                },
                            ],
                            [
                                'attribute' => 'volume',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['volume'], 'volume', 'text', 'Write volume'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','volume')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','volume')],
                                'value' => function ($data) {
                                    return $data->volume ?? '';
                                },
                            ],
                            [
                                'attribute' => 'cartons',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['cartons'], 'cartons', 'text', 'Write cartons'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','cartons')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','cartons')],
                                'value' => function ($data) {
                                    return $data->cartons ?? '';
                                },
                            ],
                            // [
                            //     'attribute' => 'name',
                            //     'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['name'], 'name', 'text', 'Write name'),
                            //     'encodeLabel' => false,
                            //     'class' => 'yii\grid\DataColumn',
                            //     'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','name').CustomTable::widthTable(200)],
                            //     'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','name').CustomTable::widthTable(200).'overflow-x: auto;'],
                            //     'value' => function ($data) {
                            //         return $data->name ?? '';
                            //     },
                            // ],
                            [
                                'attribute' => 'comment',
                                'label' => Shipment::shipmentDropMenu(Shipment::attributeLabels()['comment'], 'comment', 'text', 'Write comment'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','comment').CustomTable::widthTable(600)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('shipment','shipment','comment').CustomTable::widthTable(600).'overflow-x: auto;'],
                                'value' => function ($data) {
                                    return $data->comment ?? '';
                                },
                            ],
                        ],
                    ]); ?>
                </div>

                <div class="detail-info-top-line">
                    <div class="detail-info-controls">
                        <a href="#" id="slide-position-left"><i class="fas fa-chevron-left"></i></a>
                        <a href="#" id="slide-position-right"><i class="fas fa-chevron-right"></i></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="" class="shipment-detail-info-url-edit"><i class="fas fa-pencil-alt"></i>Edit</a>
                            <a data-id="" class="delete-post-url" style="display: <?=(Yii::$app->user->identity->role >= 9)?';':'none;'?>"><i class="far fa-trash-alt"></i>Delete</a>
                            <a href="#" id="close-detail-info"><i class="fas fa-times"></i></a>
                        </div>
                    </div>
                </div>

                <div class="detail-info-wrap">
                    <div class="detail-info">
                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment ID</div>
                                <div class="detail-info-list-column detail-info-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment Name</div>
                                <div class="detail-info-list-column detail-info-name"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">From</div>
                                <div class="detail-info-list-column detail-info-from"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">To</div>
                                <div class="detail-info-list-column detail-info-to"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment Type</div>
                                <div class="detail-info-list-column detail-info-type"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment Status</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment Checkpoint</div>
                                <div class="detail-info-list-column detail-info-checkpoint"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment Weight</div>
                                <div class="detail-info-list-column detail-info-weight"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment Volume</div>
                                <div class="detail-info-list-column detail-info-volume"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cartons Quantity</div>
                                <div class="detail-info-list-column detail-info-cartons"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Comment</div>
                                <div class="detail-info-list-column detail-info-comment"></div>
                            </div>

                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment created</div>
                                <div class="detail-info-list-column detail-info-date"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment received</div>
                                <div class="detail-info-list-column detail-info-date_received"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment time</div>
                                <div class="detail-info-list-column detail-info-time"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Tracking code</div>
                                <div class="detail-info-list-column detail-info-tracking"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Tracking link</div>
                                <div class="detail-info-list-column detail-info-tracking-link"></div>
                            </div>

                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">AWB</div>
                                <div class="detail-info-list-column detail-info-awb"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Invoice</div>
                                <div class="detail-info-list-column detail-info-invoice"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Packing list</div>
                                <div class="detail-info-list-column detail-info-packing_list"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Other</div>
                                <div class="detail-info-list-column detail-info-other"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing">
                </div>
                <div class="col-6 summary">
                    <!-- <img src="/web/img/list-icon.png" alt="#"> <a href="#">Show Summary</a> -->
                </div>
            </div>
        </div>
    </section>
    <nav id="context-menu" class="context-menu">
        <ul class="context-menu__items">
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="View"><i class="fa fa-eye"></i> View</a>
          </li>
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="Edit"><i class="fa fa-edit"></i> Edit</a>
          </li>
          <?php if(Yii::$app->user->identity->role >= 9){ ?>
              <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="Delete"><i class="fa fa-times"></i> Delete</a>
              </li>
          <?php } ?>
        </ul>
    </nav>

<script src="/web/js/contecst_menu.js"></script>
<script type="text/javascript">

    setInterval(function() {
        tableHeight();
    }, 500);

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        summ_height = window_height - ($('.all-cargos-title').height() + $('header').height() + $('.options').height() + 50);
        if(summ_height < 500){
            summ_height = 500;
        }
        $('.table-wrap').css({'max-height' : summ_height+'px'});
        $('.table-bordered').css({'max-height' : summ_height+'px'});
        $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
    }

    $('.mob-menu-Shipments > a').addClass('active');

    $('.summ_bottom').html($('.summary').html());
    $('.summary').html('');


    $(document).on('keyup', '.dropdown-search-input', function (event) {
        if(event.keyCode==13) {
            runSearch($(this).attr("data-type"),$(this).val());
        }
    });

    $(document).on('click', '.search-menu-click', function () {
        runSearch($(this).attr("data-type"),$(this).attr("data-id"));
    });

    $(document).on('change', '#form-choose-date', function () {
        runSearch($(this).attr("data-type"),$(this).val());
    });

    function runSearch(type, value){
        $.ajax({ 
            type: 'POST', 
            url: '/shipments/search?'+type+'='+value+'&sort=<?=Yii::$app->request->get('sort')?>', 
            success: function (data) { 
                $('.all-cargos-content').html(data);
            }
        });
    };

    $(".delete-post-url").click(function() {
        var delete_text = confirm('Do you really want to delete shipment?');
        if(delete_text) window.location.replace('/shipments/delete/'+$(this).attr("data-id"));
    });

    $('.custom-table-btn').click(function() {
        var sListType = '';
        $('.custom-table-checked input').each(function () {
            if(this.checked == "1"){
                sListType += $(this).val() + ",";
            }
        });
        if(sListType != ''){
            $.ajax({ 
                type: 'GET', 
                url: '/shipments/customtable?page=shipment&type='+sListType, 
                success: function (data) { 
                    location.reload();
                }
            });
        }
    });

    $(document).on('click', '.checkbox-table-all', function () {
        $('.checkbox-table').not(this).prop('checked', this.checked);
        allCargosSelectEdits(1);
    });

    $(document).on('click', '.checkbox-table', function () {
        if(this.checked == "1"){
            $('.shipment-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
        }else{
            $('.shipment-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
        }
        allCargosSelectEdits();
    });

    function allCargosSelectEdits(type = 0){
        var sList = '';
        ///////
        $('.checkbox-table').each(function () {
            if(this.checked == "1"){
                if(type == 1){
                    $('.shipment-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
                }
                sList += $(this).val() + ",";
            }else if(type == 1){
                $('.shipment-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
            }
        });
    }
</script>
