<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Shipments');
//$this->params['breadcrumbs'][] = $this->title;
?>

    <?=$menu?>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">

                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        All Shipments *
                    </div>
                    <!-- <div class="sorting">
                        <div class="sorting-title">sorting</div>
                        <div class="picked-sorting">Shipments date recelved <i class="fas fa-caret-down"></i> <a href="#"><img src="/web/img/cancel-icon.png" alt="#"></a></div>
                    </div> -->
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                    <div class="add-new">
                        <!-- <div class="ad-search">
                            <a href="#"><i class="fas fa-search"></i></a>
                        </div>
                        <div class="ad-button">
                            <a href="#"><i class="fas fa-plus"></i> Add</a>
                        </div>
                        <div class="ad-menu">
                            <a href="#"><i class="fas fa-bars"></i></a>
                        </div> -->
                    </div>
                </div>

                <div class="table-wrap">
                    <table class="table table-striped table-bordered table-resizable custom-tab">
                        <thead>
                                <tr>
                                    <th>Shipment ID</th>
                                    <th>Shipment created</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Shipment Status</th>
                                    <th>Shipment Checkpoint</th>
                                    <th>Shipment Type</th>
                                    <th>Shipment Weight</th>
                                    <th>Shipment Volume</th>
                                    <th>Cartons Quantity</th>
                                    <th>Comment</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?= $all_table_rows ?>
                            </tbody>
                    </table>
                </div>

                <div class="detail-info-top-line">

                    <div class="detail-info-controls">
                        <a href="#" id="slide-left"><img src="/web/img/arr-left.png" alt="#"></a>
                        <a href="#" id="slide-right"><img src="/web/img/arr-right.png" alt="#"></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="/projects/edit/" class="projects-detail-info-url-edit"><img src="/web/img/pencil.png" alt="">Edit</a>
                            <!-- <a href="#"><img src="/web/img/duplicate-icon.png" alt="">Duplicate</a> -->
                            <a href="#" id="close-detail-info"><img src="/web/img/close-icon.png" alt=""></a>
                        </div>
                    </div>

                </div>

                <div class="detail-info-wrap">

                    <div class="detail-info">

                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment ID</div>
                                <div class="detail-info-list-column detail-info-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">From</div>
                                <div class="detail-info-list-column detail-info-from"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">To</div>
                                <div class="detail-info-list-column detail-info-to"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment Status</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment Type</div>
                                <div class="detail-info-list-column detail-info-type"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment created</div>
                                <div class="detail-info-list-column detail-info-date"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment received</div>
                                <div class="detail-info-list-column detail-info-date_received"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Tracking link</div>
                                <div class="detail-info-list-column detail-info-tracking"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment time</div>
                                <div class="detail-info-list-column detail-info-time"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment Weight</div>
                                <div class="detail-info-list-column detail-info-weight"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment Volume</div>
                                <div class="detail-info-list-column detail-info-volume"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cartons Quantity</div>
                                <div class="detail-info-list-column detail-info-cartons"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Comment</div>
                                <div class="detail-info-list-column detail-info-comment"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment Checkpoint</div>
                                <div class="detail-info-list-column detail-info-checkpoint"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Price</div>
                                <div class="detail-info-list-column detail-info-price"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Сustoms Price</div>
                                <div class="detail-info-list-column detail-info-price_customs"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Profit</div>
                                <div class="detail-info-list-column detail-info-profit"></div>
                            </div>

                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">AWB</div>
                                <div class="detail-info-list-column detail-info-awb"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Invoice</div>
                                <div class="detail-info-list-column detail-info-invoice"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Packing list</div>
                                <div class="detail-info-list-column detail-info-packing_list"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Other</div>
                                <div class="detail-info-list-column detail-info-other"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing">
                    Showing <?=$shipmentsCount?> of <?=$shipmentsCount?>
                </div>
                <div class="col-6 summary">
                    <!-- <img src="/web/img/list-icon.png" alt="#"> <a href="#">Show Summary</a> -->
                </div>
            </div>
        </div>
    </section>

<script type="text/javascript">
    $(window).resize(function() {
        tableHeight();
    });

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        body_height = $('body').height();
        table = $('.table-wrap').height();
        summ_height = window_height - (body_height - table) - 15;
        $('.table-wrap').css({'max-height' : summ_height+'px'});
    }

    $('.mob-menu-Shipments > a').addClass('active');

    /////////////////////////////////////////////////

    $(".table tbody .shipments-row").on("click", function() {
        var cur_id = $(this).attr("data-shipment_id");
        var re, s;
        s = cur_id;
        re = /\D+/ig;
        index = s.replace(re, '');

        $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
        $('.detail-info-top-line').animate({
            opacity: 1
        }, 700);
        $('.detail-info-top-line').css({
            "display": "flex"
        });
        $(".detail-info-wrap").find('.detail-info').fadeIn(700).addClass("detail-visibe");
        
        $('.projects-detail-info-url-edit').attr("href", '/shipments/edit/' + $(this).attr("data-shipment_id"));

        $.ajax({ 
            type: 'GET', 
            url: '/shipments/view/' + $(this).attr("data-shipment_id"), 
            data: {}, 
            dataType: 'json',
            success: function (data) { 
                $('.detail-info-id').text(data.shipment_id);
                $('.detail-info-manager').text(data.manager_id);
                $('.detail-info-from').text(data.from_id);
                $('.detail-info-to').text(data.to_id);
                $('.detail-info-status').text(data.status_id);
                $('.detail-info-type').text(data.type_id);
                $('.detail-info-date').text(data.date_created);
                $('.detail-info-date_received').text(data.date_received);
                $('.detail-info-tracking').text(data.link);
                $('.detail-info-time').text(data.time);
                $('.detail-info-weight').text(data.weight);
                $('.detail-info-volume').text(data.volume);
                $('.detail-info-cartons').text(data.cartons);
                $('.detail-info-comment').text(data.comment);
                $('.detail-info-checkpoint').text(data.checkpoint);
                $('.detail-info-price').text(data.price);
                $('.detail-info-price_customs').text(data.price_customs);
                $('.detail-info-profit').text(data.profit);
                $('.detail-info-awb').html(data.awb);
                $('.detail-info-invoice').html(data.invoice);
                $('.detail-info-packing_list').html(data.list);
                $('.detail-info-other').html(data.other);

                console.log(data);
            }
        });
    });
</script>
