<div class="options">
        <div class="choose-country d-sm-none d-md-none d-lg-none d-xl-none" id="country-click">
            <a href="#" class="choose-country-click">Shipments <span><i class="fas fa-caret-right"></i></span></a>
            <ul id="sm-countries">
                <li><a href="/shipments/index"><i class="fa fa-list" aria-hidden="true"></i>All Shipments</a></li>
                <li><a href="/shipments/filter/5"><i class="fa fa-filter" aria-hidden="true"></i>Export process</a></li>
                <li><a href="/shipments/filter/6"><i class="fa fa-filter" aria-hidden="true"></i>In transit</a></li>
                <li><a href="/shipments/filter/7"><i class="fa fa-filter" aria-hidden="true"></i>Import process</a></li>
                <li><a href="/shipments/filter/11"><i class="fa fa-filter" aria-hidden="true"></i>Delivered</a></li>
                <li><a href="/shipments/create" class=""><i class="fa fa-plus" aria-hidden="true"></i>Add Shipment</a></li>
            </ul>
        </div>

        <ul id="countries">
            <!-- <li class="dropdown-menu-block" data-dropdown_class="dropdown-menu-china">
                <a class="dropdown-menu-china-btn"><img src="/web/img/chine-flag.png" alt="">China Mainland Warehouse</a>
                <div class="dropdown-menu dropdown-menu-china" x-placement="top-start" style="display: none;">
                  <a class="dropdown-item" href="/shipments/warehouse/35"><i class="fa fa-list" aria-hidden="true"></i>Shenzhen Warehouse</a>
                  <a class="dropdown-item" href="/shipments/warehouse/12"><i class="fa fa-list" aria-hidden="true"></i>Guangzhou Warehouse</a>
                  <a class="dropdown-item" href="/shipments/warehouse/40"><i class="fa fa-list" aria-hidden="true"></i>Beijing Warehouse</a>
                  <a class="dropdown-item" href="/shipments/warehouse/39"><i class="fa fa-list" aria-hidden="true"></i>Shanghai Warehouse</a>
                  <a class="dropdown-item" href="/shipments/warehouse/10"><i class="fa fa-list" aria-hidden="true"></i>Foshan1 Warehouse</a>
                  <a class="dropdown-item" href="/shipments/warehouse/11"><i class="fa fa-list" aria-hidden="true"></i>Foshan2 Warehouse</a>
                </div>
            </li>

            <li><a href="/shipments/warehouse/13"><img src="/web/img/hongkong-flag.png" alt="">Hong Kong Warehouse</a></li>
            <li><a href="/shipments/warehouse/37"><img src="/web/img/usa-flag.png" alt="">USA Warehouse</a></li>
            <li><a href="/shipments/warehouse/41"><img src="/web/img/poland-flag.png" alt="">Chelm Warehouse</a></li>
            <li><a href="/shipments/warehouse/32"><img src="/web/img/ukraine-flag.png" alt="">Kiev Warehouse</a></li> -->
        </ul>

        <ul id="cargo-info">
            <!-- <li class="dropdown-menu-block" data-dropdown_class="dropdown-menu-cargos">
                <a class="dropdown-menu-cargos-btn"><i class="fa fa-list" aria-hidden="true"></i>Shipments</a>
                <div class="dropdown-menu dropdown-menu-cargos" x-placement="top-start" style="display: none;">
                  <a class="dropdown-item" href="/shipments/index"><i class="fa fa-list" aria-hidden="true"></i>All Shipments</a>
                  <a class="dropdown-item" href="/shipments/filter/11"><i class="fa fa-filter" aria-hidden="true"></i>Delivered</a>
                  <a class="dropdown-item" href="/shipments/filter/5"><i class="fa fa-filter" aria-hidden="true"></i>Export process</a>
                  <a class="dropdown-item" href="/shipments/filter/6"><i class="fa fa-filter" aria-hidden="true"></i>In transit</a>
                  <a class="dropdown-item" href="/shipments/filter/7"><i class="fa fa-filter" aria-hidden="true"></i>Import process</a>
                </div>
            </li> -->
            <li><a href="/shipments/index"><i class="fa fa-list" aria-hidden="true"></i>All Shipments</a></li>
            <li><a href="/shipments/filter/5"><i class="fa fa-filter" aria-hidden="true"></i>Export process</a></li>
            <li><a href="/shipments/filter/6"><i class="fa fa-filter" aria-hidden="true"></i>In transit</a></li>
            <li><a href="/shipments/filter/7"><i class="fa fa-filter" aria-hidden="true"></i>Import process</a></li>
            <li><a href="/shipments/filter/11"><i class="fa fa-filter" aria-hidden="true"></i>Delivered</a></li>
            <li><a href="/shipments/create" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i>Add Shipment</a></li>
        </ul>
    </div>