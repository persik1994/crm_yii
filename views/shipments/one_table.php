<tr class="shipments-row" data-shipment_id="<?= (!empty($shipment->shipment_id)) ? $shipment->shipment_id : '' ?>">
    <td><?= (!empty($shipment->shipment_id)) ? 'SH-'.$shipment->shipment_id : '' ?></td>
    <td><?= (!empty($shipment->date_created)) ? date('d.m.Y', $shipment->date_created) : '' ?></td>
    <td><?= (!empty($shipment->from->name)) ? $shipment->from->name : '' ?></td>
    <td><?= (!empty($shipment->to->name)) ? $shipment->to->name : '' ?></td>
    <td><?= (!empty($shipment->cargoStatus->name)) ? $shipment->cargoStatus->name : '' ?></td>
    <td><?= (!empty($shipment->warehouse->name)) ? $shipment->warehouse->name : '' ?></td>
    <td><?= (!empty($shipment->type->name)) ? $shipment->type->name : '' ?></td>
    <td><?= (!empty($shipment->weight)) ? $shipment->weight : '0.00' ?></td>
    <td><?= (!empty($shipment->volume)) ? $shipment->volume : '0.00' ?></td>
    <td><?= (!empty($shipment->cartons)) ? $shipment->cartons : '0.00' ?></td>
    <td class="table-description"><?= (!empty($shipment->comment)) ? $shipment->comment : '' ?></td>
</tr>