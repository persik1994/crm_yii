<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Create Shipment');
?>

	<?=$menu?>

	<section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">Create Shipment</div>
            </div>

            <div class="add-client-form-wrap create-shipment-form">
                <form>

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Manager</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager selectpicker" name="manager" onblur="this.style.color = '#000'" <?=(Yii::$app->user->identity->role < 1) ? 'disabled' : ''?> data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option value="<?=$manager->manager_id?>" <?=(Yii::$app->user->identity->role < 9 && $manager->username == Yii::$app->user->identity->username) ? 'selected' : ''?>><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Shipment number</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-number" disabled> 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Shipment name</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-name" placeholder="Shipment name"> 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>From <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-from" name="select-from" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($countries as $country) { ?>
                                        	<option value="<?=$country->country_id?>"><?=$country->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>To <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-to" name="select-to" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($countries as $country) { ?>
                                        	<option value="<?=$country->country_id?>"><?=$country->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Shipment Type <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-type" name="select-type" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($shipment_types as $type) { ?>
                                        	<option value="<?=$type->type_id?>"><?=$type->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Shipment Status <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-status" name="select-status" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($statuses as $status) { ?>
                                        	<option value="<?=$status->status_id?>"><?=$status->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Shipment Checkpoint</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-stock selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($stocks as $stock) { ?>
                                        	<option value="<?=$stock->stock_id?>"><?=$stock->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Shipment Weight</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-weight">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Shipment Volume</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-volume">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cartons Quantity</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-cartons">
                                </div>
                            </div>
                            
                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Comment</label>
                                <div class="input-addclient-wrap">
                                    <textarea name="name" class="input-comment"> </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Shipment created <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date" class="input-created" value="<?=date('d-M-Y',time())?>" autocomplete="off" name="input-created">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Shipment received</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date-1" class="input-received" autocomplete="off">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Shipment time</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="" class="input-time" disabled="">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Tracking link</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="http://" class="input-link">
                                </div>
                            </div>

                            <div class="add-client-form-line add-client-form-line-file">
                                <label>AWB</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <input type="file" placeholder="" class="file" id="get-awb-file">
                                    <textarea class="awb-base64" style="display: none;"></textarea>
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                </div>
                            </div>

                            <div class="get-awb-file load-file"><img src="" id='preview_awb' alt="">
                                <span class="ava-name"></span>
                                <span class="ava-size"></span>
                                <span class="close-preview-awb"><i class="fas fa-times"></i></span>
                            </div>

                            <hr>

                            <div class="add-client-form-line add-client-form-line-file">
                                <label>Invoice</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <input type="file" placeholder="" class="file" id="get-invoice-file">
                                    <textarea class="invoice-base64" style="display: none;"></textarea>
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                </div>
                            </div>

                            <div class="get-invoice-file load-file"><img src="" id='preview_invoice' alt="">
                                <span class="ava-name"></span>
                                <span class="ava-size"></span>
                                <span class="close-preview-invoice"><i class="fas fa-times"></i></span>
                            </div>

                            <hr>

                            <div class="add-client-form-line add-client-form-line-file">
                                <label>Packing list</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <input type="file" placeholder="" class="file" id="get-packing-file">
                                    <textarea class="packing-base64" style="display: none;"></textarea>
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                </div>
                            </div>

                            <div class="get-packing-file load-file"><img src="" id='preview_packing' alt="">
                                <span class="ava-name"></span>
                                <span class="ava-size"></span>
                                <span class="close-preview-packing"><i class="fas fa-times"></i></span>
                            </div>

                            <hr>

                            <div class="add-client-form-line add-client-form-line-file">
                                <label>Other file</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <input type="file" placeholder="" class="file" id="get-other-file">
                                    <textarea class="other-base64" style="display: none;"></textarea>
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                </div>
                            </div>

                            <div class="get-other-file load-file"><img src="" id='preview_other' alt="">
                                <span class="ava-name"></span>
                                <span class="ava-size"></span>
                                <span class="close-preview-other"><i class="fas fa-times"></i></span>
                            </div>
                            <hr>

                        </div>
                    </div>

                    <div>
                        <div class="col-4">
                            <div class="bottom-part-title">
                                Shipment Calculation
                            </div>

                            <div class="col-12 shipment-calc calculation_columns">
                                <?=$calculation_columns?>
                            </div>
                        </div>
                    </div>

                    <div class="bottom-part row no-fw-row">
                    	

                        <!-- <div class="col-lg-4 col-12">
                            <div class="calculation_columns">
                                <?=$calculation_columns?>
                            </div>
	                    </div> -->

                        <div class="add-new-form-submit-buttons col-12">
                            <div class="add-new-form-submit-buttons-wrap col-4">
                                <div class="add-new-form-submit-buttons-wrap-width">
                                    <button  class="submit-form">Submit <img src="/web/img/preloader.gif" style="display: none;"></button>
                                    <a href="#" class="reset-form">Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        //RULES FORM
        $(".create-shipment-form form").validate({
          // Specify validation rules
          rules: {
            "select-from": {
                required: true
            },
            "select-to": {
                required: true
            },
            'select-type': {
                required: true
            },
            'select-status': {
                required: true
            },
            "input-created": {
                required: true
            },
          },
          // Specify validation error messages
          messages: {
            "select-from": {
                required: "Please select",
            },
            "select-to": {
                required: "Please select",
            },
            "select-type": {
                required: "Please select type",
            },
            "select-status": {
                required: "Please select status",
            },
            "input-created": {
                required: "Please select date",
            },
          },
          // Make sure the form is submitted to the destination defined
          // in the "action" attribute of the form when valid
          submitHandler: function(form) {
            submitForm();
          }
        });
    
    </script>

    <script type="text/javascript">
        function submitForm(){
                //блок кнопки и прелоадер
                $('.submit-form').find('img').css('display','');
                $('.submit-form').attr('disabled', 'true');
                ///
                var formData = new FormData();
                formData.append('manager',      $('.select-manager option:selected').val());
                formData.append('from',         $('.select-from option:selected').val());
                formData.append('to',           $('.select-to option:selected').val());
                formData.append('type',         $('.select-type option:selected').val());
                formData.append('status',       $('.select-status option:selected').val());
                formData.append('stock',        $('.select-stock option:selected').val());

                formData.append('number',          $('.input-number').val());
                formData.append('weight',          $('.input-weight').val());
                formData.append('volume',          $('.input-volume').val());
                formData.append('cartons',         $('.input-cartons').val());
                formData.append('created',         $('.input-created').val());
                formData.append('received',        $('.input-received').val());
                formData.append('time',            $('.input-time').val());
                formData.append('link',            $('.input-link').val());
                formData.append('name',            $('.input-name').val());
                formData.append('comment',         $('.input-comment').val());
                // Attach file
                formData.append('awb', document.querySelector('#get-awb-file').files[0]); 
                formData.append('invoice', document.querySelector('#get-invoice-file').files[0]);
                formData.append('list', document.querySelector('#get-packing-file').files[0]);
                formData.append('other', document.querySelector('#get-other-file').files[0]);

                $('.shipment-calculation-input').each(function() {
                    formData.append('calculation-'+$(this).attr('data-id'), $(this).val());
                });

                $.ajax({ 
                    url: '/shipments/create',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data) { 
                        console.log(data);
                        if (data == 1) {
                            window.location.replace("/shipments/index");
                        }
                    }
                });
        }

        $('.mob-menu-Shipments > a').addClass('active');

        var awb = new FileReader();
        var packing = new FileReader();
        var invoice = new FileReader();
        var other = new FileReader();

        $('.close-preview-awb').on("click", function() {
            $('#get-awb-file').val('');
            $('.get-awb-file .ava-name').text('');
            $('.get-awb-file .ava-size').text('');
            $("#preview_awb").attr("src", "");
            $('.get-awb-file').css('height', '0');
            $('.close-preview-awb').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-awb-file").change(function(event) {

            $('.close-preview-awb').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-awb-file .ava-name').text(filename[fileLen]);

            awb.onload = function() {
                $('.get-awb-file').css('height', 'auto');
                var preview = $("#preview_awb");
                preview.attr("src", awb.result);
                $('.awb-base64').val(awb.result);
            }

            awb.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-awb-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });

        $('.close-preview-invoice').on("click", function() {
            $('#get-invoice-file').val('');
            $('.get-invoice-file .ava-name').text('');
            $('.get-invoice-file .ava-size').text('');
            $("#preview_invoice").attr("src", "");
            $('.get-invoice-file').css('height', '0');
            $('.close-preview-invoice').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-invoice-file").change(function(event) {

            $('.close-preview-invoice').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-invoice-file .ava-name').text(filename[fileLen]);

            invoice.onload = function() {
                $('.get-invoice-file').css('height', 'auto');
                var preview = $("#preview_invoice");
                preview.attr("src", invoice.result);
                $('.invoice-base64').val(invoice.result);
            }

            invoice.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-invoice-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });

        $('.close-preview-other').on("click", function() {
            $('#get-other-file').val('');
            $('.get-other-file .ava-name').text('');
            $('.get-other-file .ava-size').text('');
            $("#preview_other").attr("src", "");
            $('.get-other-file').css('height', '0');
            $('.close-preview-other').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-other-file").change(function(event) {

            $('.close-preview-other').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-other-file .ava-name').text(filename[fileLen]);

            other.onload = function() {
                $('.get-other-file').css('height', 'auto');
                var preview = $("#preview_other");
                preview.attr("src", other.result);
                $('.other-base64').val(other.result);
            }

            other.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-other-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });

        $('.close-preview-packing').on("click", function() {
            $('#get-packing-file').val('');
            $('.get-packing-file .ava-name').text('');
            $('.get-packing-file .ava-size').text('');
            $("#preview_packing").attr("src", "");
            $('.get-packing-file').css('height', '0');
            $('.close-preview-packing').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-packing-file").change(function(event) {

            $('.close-preview-packing').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-packing-file .ava-name').text(filename[fileLen]);

            packing.onload = function() {
                $('.get-packing-file').css('height', 'auto');
                var preview = $("#preview_packing");
                preview.attr("src", packing.result);
                $('.packing-base64').val(packing.result);
            }

            packing.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-packing-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });

        $( ".select-type" ).change(function() {
            $.ajax({ 
                url: '/shipments/calculationcolumns/'+$('.select-type option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data != ''){
                        $(".calculation_columns").html(data);
                    }
                }
            });
        });
        
        // $(document).keyup(function(e) {
        //     if(e.which == 13){ //enter press
        //         $('.submit-form').trigger('click');
        //     }
        //     if (e.key === "Escape") { 
        //         $('.reset-form').trigger('click');
        //     }
        // });
        $('.reset-form').click(function(){
            location.reload();
        });
    </script>
