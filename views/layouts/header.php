<?php
  use yii\helpers\Html;
  use app\models\Manager;
  use app\models\Task;

  if(!Yii::$app->user->isGuest){
    $user = Manager::findOne(Yii::$app->user->identity->id);
  }

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= Html::encode($this->title) ?> | INTA.GROUP</title>
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="<?= Yii::$app->params['keywords'] ?>" name="keywords">
    <meta content="<?= Yii::$app->params['description'] ?>" name="description">
    <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
    <meta property="og:title" content="<?= Html::encode($this->title) ?>">
    <meta property="og:image" content="<?= Yii::$app->params['seo_image'] ?>">
    <meta property="og:site_name" content="<?= Html::encode($this->title) ?>">
    <meta property="og:description" content="<?= Yii::$app->params['description'] ?>">
    
    <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="<?= Html::encode($this->title) ?>">
    <meta name="twitter:title" content="<?= Html::encode($this->title) ?>">
    <meta name="twitter:description" content="<?= Yii::$app->params['description'] ?>">
    <meta name="twitter:image" content="<?= Yii::$app->params['seo_image'] ?>">

    <link rel="stylesheet" href="/web/css/main_2.css">
    <link rel="stylesheet" href="/web/css/bogdan.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="/web/libs/datetimepicker-master/jquery.datetimepicker.css">
    <link rel="stylesheet" href="/web/libs/phone/build/css/intlTelInput.min.css">

    <link rel="shortcut icon" href="/web/img/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css" />

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
    <script src="/web/js/jquery.validate.min.js"></script>
    <script src="/web/js/additional-methods.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
    <?php $this->head(); ?>
</head>

<body class="pb">
<?php if(!Yii::$app->user->isGuest){ ?>
    <div class="overlay flex cac vac ">
        <div>
            <div class="loader preloader flex vac">
                <svg width="200" height="200">
                    <circle class="background" cx="90" cy="90" r="80" transform="rotate(-90, 100, 90)" />
                    <circle class="outer" cx="90" cy="90" r="80" transform="rotate(-90, 100, 90)" />
                </svg>
                <span class="circle-background"></span>
                <span class="logo animated fade-in"></span>
            </div>
        </div>
    </div>

    <div class="sm-menu-overlay"></div>
    <div class="popups-overlay"></div>
    <div class="sm-menu">
        <div class="close-sm-menu"><i class="fas fa-times"></i></div>
        <div class="sm-logo"><a href="/"><img src="/web/img/logo-mini.png" alt="#"></a></div>
        <div class="user-info sm-users-info">
            <a href="#" class="circle" id="tasks-popup-open-sm"><img src="/web/img/checked-icon.png" alt="#">
                <?php 
                    $count_tasks = Task::find()->where(['manager_id'=>Yii::$app->user->identity->id, 'status'=>0])->count();
                    if(!empty($count_tasks)){ 
                ?>
                    <span class="notifications-counter"><?=$count_tasks?></span>
                <?php } ?>
            </a>
            <a href="#" class="circle" id="notif-open-sm"><img src="/web/img/bell-icon.png" alt="#"><span class="notifications-counter">12</span></a>
            <a href="#" class="circle user" id="user-set-open-sm"><img src="<?=(!empty(Yii::$app->user->identity->avatar)) ? Yii::$app->user->identity->avatar : '/web/img/comment_2.png'?>" alt="" class="avatar-circle"></a>
            <div class="tasks-popup tasks-popup-sm">
                <div class="tasks-popup-title">Tasks manager</div>

                <div class="tasks-popup-task">
                    <span class="tasks-popup-task-check"></span>
                    <p>Call uncle Sam</p>
                    <div class="tasks-popup-task-tag-time-wrap">
                        <div class="tasks-popup-task-tag"></div>
                        <div class="tasks-popup-task-time"><i class="far fa-clock"></i> 27 Nov</div>
                    </div>
                    <div class="edit-task">
                        <a href="#"><i class="fas fa-pen"></i></a>
                    </div>
                </div>

                <div class="tasks-popup-task">
                    <span class="tasks-popup-task-check"></span>
                    <p>
                        Миссия выполнима или не
                        выполнима как кому угодно
                    </p>
                    <div class="tasks-popup-task-tag-time-wrap">
                        <div class="tasks-popup-task-tag"></div>
                        <div class="tasks-popup-task-time"><i class="far fa-clock"></i> 27 Nov</div>
                    </div>
                    <div class="edit-task">
                        <a href="#"><i class="fas fa-pen"></i></a>
                    </div>
                </div>

                <div class="tasks-popup-controls">
                    <div class="tasks-popup-controls-form">
                        <form action="#">
                            <input type="text" class="tasks-popup-controls-form-input-sm">
                            <a class="tasks-popup-controls-form-submit-sm"><i class="fas fa-plus"></i></a>
                        </form>
                    </div>
                    <div class="task-terms-popup">
                        <div><i class="far fa-clock"></i> <input type="text" placeholder="Add term" id='date-sm' class="tasks-popup-controls-form-date-sm"></div>
                        <!-- <div><i class="fas fa-tag"></i>
                            <div id='task-terms-popup-pick-sm'>Add tag
                                <div class="tag-choose-popup tag-choose-popup-sm">
                                    <ul>
                                        <li></li>
                                        <li style="background-color: blue;"></li>
                                    </ul>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>

            <div class="notif notif-sm">
                <div class="notif-title">Notifications</div>
                <div class="notif-message">
                    <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                    <div class="notif-message-text">
                        <span>cc-1330</span> status has been charged: <strong>Signed</strong>
                        <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                    </div>
                </div>
                <div class="notif-message">
                    <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                    <div class="notif-message-text">
                        <span>cc-1330</span> status has been charged: <strong>Signed</strong>
                        <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                    </div>
                </div>
                <div class="notif-message">
                    <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                    <div class="notif-message-text">
                        <span>cc-1330</span> status has been charged: <strong>Signed</strong>
                        <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                    </div>
                </div>

                <div class="lines"></div>
                <ul>
                    <li><a href="#"><img src="/web/img/watch.png" alt="#"> Notification history</a></li>
                    <li><a href=""><img src="/web/img/settings.png" alt="#"> Setting notifications</a></li>
                </ul>
            </div>

            <div class="user-set user-set-sm">
                <div class="user-name-wrap">
                    <div class="user-img">
                        <img src="<?=(!empty(Yii::$app->user->identity->avatar)) ? Yii::$app->user->identity->avatar : '/web/img/comment_2.png'?>" alt="#" class="avatar-circle">
                    </div>
                    <div class="user-name">
                        <span><?=$user->username?></span>
                        <!-- <span><?=$user->role?></span> -->
                    </div>
                </div>
                <ul>
                    <?= (Yii::$app->user->identity->role >= 10 || Yii::$app->user->identity->id == 34)? '<li><a href="/manager/manageradmin">Director panel</a></li>' : '' ?>
                    <li><a href="/manager/cabinet">My cabinet</a></li>
                    <li><a href="/manager/edit">Setting profile</a></li>
                    <li><a href="/logout">Exit</a></li>

                </ul>
            </div>

            <div class="search-popup search-popup-sm">
                <div class="search-popup-title">
                    <img src="/web/img/1_inYwyq37FdvRPLRphTqwBA (1) (1).gif" alt="#"> Search starting...
                </div>
                <div class="search-popup-tags">
                    <div class="search-popup-tags-title">Search</div>
                    <div class="search-popup-tags-links">
                        <ul>
                            <li><a href="#">Clients</a></li>
                            <li><a href="#">Cargos</a></li>
                            <li><a href="#">Payments</a></li>
                            <li><a href="#">Tasks</a></li>
                        </ul>
                    </div>
                </div>
            </div>


        </div>
        <div class="search-form sm-search" style="display: none !important;">
            <form action="#">
                <input type="text" id='open-search-sm' placeholder="Global search" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Global search'">
                <input type="submit" value="" class="submit_search">
                <span><i class='fas fa-search'></i></span>
            </form>
        </div>
        <ul>
            <?php if(Yii::$app->user->identity->role > 5){ ?>
                <li class="mob-menu-Dasboard"><a href="/">
                        <div><img src="/web/img/gauge-icon.png" alt="#"></div>Dasboard
                    </a>
                </li>
                <li class="mob-menu-Clients"><a href="/clients/index">
                        <div><img src="/web/img/clients-icon.png" alt="#"></div>Clients
                    </a>
                </li>
                <li class="mob-menu-Cargo"><a href="/cargo/index">
                        <div><img src="/web/img/van-icon.png" alt="#"></div>Cargo
                    </a>
                </li>
                <li class="mob-menu-Projects"><a href="/projects/index">
                        <div><img src="/web/img/projects-icon.png" alt="#">
                        </div>Projects
                    </a>
                </li>
                <li class="mob-menu-Buying"><a href="/buying/index">
                        <div><img src="/web/img/buying-icon.png" alt="#">
                        </div>Buying
                    </a>
                </li>
                <li class="mob-menu-Shipments"><a href="/shipments/index">
                        <div><img src="/web/img/shipment_ico.png" alt="#">
                        </div>Shipments
                    </a>
                </li>
                <li class="mob-menu-Payments"><a href="/payments/index">
                        <div><img src="/web/img/money-icon.png" alt="#"></div>Payments
                    </a>
                </li>
            <?php } ?>
            <?php if(Yii::$app->user->identity->role > 2 && Yii::$app->user->identity->role <= 5){ ?>
                <li class="mob-menu-Cargo"><a href="/cargo/index">
                        <div><img src="/web/img/van-icon.png" alt="#"></div>Cargo
                    </a>
                </li>
                <?php if(Yii::$app->user->identity->id != 50){ ?>
                <li class="mob-menu-Shipments"><a href="/shipments/index">
                        <div><img src="/web/img/shipment_ico.png" alt="#">
                        </div>Shipments
                    </a>
                </li>
                <?php } ?>
                <?php if(Yii::$app->user->identity->id == 42){ ?>
                <li class="mob-menu-Buying"><a href="/buying/index">
                        <div><img src="/web/img/buying-icon.png" alt="#">
                        </div>Buying
                    </a>
                </li>
                <?php } ?>
            <?php } ?>
            <?php if(Yii::$app->user->identity->role == 2){ ?>
                <li class="mob-menu-Clients"><a href="/clients/index">
                        <div><img src="/web/img/clients-icon.png" alt="#"></div>Clients
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>

    <header class="all-cargos">
        <div class="container-fluid">
            <div class="menu-line row">
                <div class="logo-mini col-xl-1 col-lg-2 col-sm-2 col-3"><a href="/"><img src="/web/img/logo-mini.png" alt="#"></a></div>
                <nav class="main-menu col-6 col-xl-7 d-none d-xl-flex">
                    <ul>
                        <?php if(Yii::$app->user->identity->role > 5){ ?>
                        <li class="mob-menu-Dasboard"><a href="/"><img src="/web/img/gauge-icon.png" alt="#">Dasboard</a></li>
                        <li class="mob-menu-Clients"><a href="/clients/index"><img src="/web/img/clients-icon.png" alt="#">Clients</a></li>
                        <li class="mob-menu-Cargo"><a href="/cargo/index"><img src="/web/img/van-icon.png" alt="#">Cargo</a></li>
                        <li class="mob-menu-Projects"><a href="/projects/index"><img src="/web/img/projects-icon.png" alt="#">Projects</a></li>
                        <li class="mob-menu-Buying"><a href="/buying/index"><img src="/web/img/buying-icon.png" alt="#">Buying</a></li>
                        <li class="mob-menu-Shipments"><a href="/shipments/index"><img src="/web/img/shipment_ico.png" alt="#">Shipments</a></li>
                        <li class="mob-menu-Payments"><a href="/payments/index"><img src="/web/img/money-icon.png" alt="#">Payments</a></li>
                        <?php } ?>
                        <?php if(Yii::$app->user->identity->role > 2 && Yii::$app->user->identity->role <= 5){ ?>
                            <li class="mob-menu-Cargo"><a href="/cargo/index"><img src="/web/img/van-icon.png" alt="#">Cargo</a></li>
                            <?php if(Yii::$app->user->identity->id != 50){ ?>
                                <li class="mob-menu-Shipments"><a href="/shipments/index"><img src="/web/img/shipment_ico.png" alt="#">Shipments</a></li>
                            <?php } ?>
                            <?php if(Yii::$app->user->identity->id == 42){ ?>
                                <li class="mob-menu-Buying"><a href="/buying/index"><img src="/web/img/buying-icon.png" alt="#">Buying</a></li>
                            <?php } ?>
                        <?php } ?>
                        <?php if(Yii::$app->user->identity->role == 2){ ?>
                            <li class="mob-menu-Clients"><a href="/clients/index"><img src="/web/img/clients-icon.png" alt="#">Clients</a></li>
                        <?php } ?>
                    </ul>
                </nav>

                <div class="col-xl-2 col-lg-8 col-sm-8 col-7 block-search-block">
                    <?php if(Yii::$app->user->identity->role > 5){ ?>
                        <div class="search-form">
                            <form action="#">
                                <input type="text" id="open-search" placeholder="Global search" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Global search'">
                                <input type="submit" value="" class="submit_search">
                                <span><i class='fas fa-search'></i></span>
                            </form>
                        </div>
                        <div class="search-popup">
                            <div class="search-popup-title search-popup-title-desktop">
                                <img src="/web/img/1_inYwyq37FdvRPLRphTqwBA (1) (1).gif" alt="#"> Search starting...
                            </div>
                            <div class="search-block-html"></div>
                            <div class="search-popup-tags">
                                <!-- <div class="search-popup-tags-title">Search type</div> -->
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                  <label class="btn btn-secondary active">
                                    <input type="radio" name="search-type" id="option1" autocomplete="off" checked value="1"> Clients
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="search-type" id="option2" autocomplete="off" value="2"> Cargo
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="search-type" id="option3" autocomplete="off" value="3"> Projects
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="search-type" id="option4" autocomplete="off" value="4"> Shipments
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="search-type" id="option5" autocomplete="off" value="5"> Payments
                                  </label>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>


                <div class="user-info d-none d-xl-flex circle-searck-block">
                    <div class="circle-wrap">
                        <a href="#" class="circle search-form">
                            <i class="fas fa-search"></i>
                        </a>
                        <?php if(Yii::$app->user->identity->role > 5){ ?>
                            <div style="display: none;">
                                <form action="#">
                                    <input type="text" id="open-search" placeholder="Global search" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Global search'">
                                    <input type="submit" value="" class="submit_search">
                                    <span><i class='fas fa-search'></i></span>
                                </form>
                            </div>
                            <div class="search-popup">
                                <div class="search-popup-title search-popup-title-desktop">
                                    <input type="text" id="open-search" placeholder="Global search" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Global search'">
                                </div>
                                <div class="search-block-html"></div>
                                <div class="search-popup-tags">
                                    <!-- <div class="search-popup-tags-title">Search type</div> -->
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                      <label class="btn btn-secondary active">
                                        <input type="radio" name="search-type" id="option1" autocomplete="off" checked value="1"> Clients
                                      </label>
                                      <label class="btn btn-secondary">
                                        <input type="radio" name="search-type" id="option2" autocomplete="off" value="2"> Cargo
                                      </label>
                                      <label class="btn btn-secondary">
                                        <input type="radio" name="search-type" id="option3" autocomplete="off" value="3"> Projects
                                      </label>
                                      <label class="btn btn-secondary">
                                        <input type="radio" name="search-type" id="option4" autocomplete="off" value="4"> Shipments
                                      </label>
                                      <label class="btn btn-secondary">
                                        <input type="radio" name="search-type" id="option5" autocomplete="off" value="5"> Payments
                                      </label>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="circle-wrap">
                        <a href="#" class="circle" id='tasks-popup-open'>
                            <img src="/web/img/checked-icon.png" alt="#">
                            <?php 
                                $count_tasks = Task::find()->where(['manager_id'=>Yii::$app->user->identity->id, 'status'=>0])->count();
                                if(!empty($count_tasks)){ 
                            ?>
                                <span class="notifications-counter"><?=$count_tasks?></span>
                            <?php } ?>
                        </a>
                        <div class="tasks-popup">
                            <div class="tasks-popup-title">Tasks manager</div>

                            <div class="tasks-block-all"></div>

                            <div class="tasks-popup-controls">
                                <div class="tasks-popup-controls-form">
                                    <!-- Форма в header task manager -->
                                    <form action="#">
                                        <input type="text" class="tasks-popup-controls-form-input">
                                        <a class="tasks-popup-controls-form-submit"><i class="fas fa-plus"></i></a>
                                    </form>
                                </div>
                                <div class="task-terms-popup">
                                    <div><i class="far fa-clock"></i> <input type="text" placeholder="Add term" id="date" class="tasks-popup-controls-form-date"></div>
                                    <!-- <div><i class="fas fa-tag"></i>
                                        <div id='task-terms-popup-pick'>Add tag
                                            <div class="tag-choose-popup">
                                                <ul>
                                                    <li></li>
                                                    <li style="background-color: blue;"></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="circle-wrap">
                        <a href="#" class="circle" id='notif-open'><img src="/web/img/bell-icon.png" alt="#">
                            <span class="notifications-counter">12</span>
                        </a>
                        <div class="notif">
                            <div class="notif-title">Notifications</div>
                            <div class="notif-message">
                                <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                                <div class="notif-message-text">
                                    <span>cc-1330</span> status has been charged: <strong>Signed</strong>
                                    <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                                </div>
                            </div>
                            <div class="notif-message">
                                <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                                <div class="notif-message-text">
                                    <span>cc-1330</span> status has been charged: <strong>Signed</strong>
                                    <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                                </div>
                            </div>
                            <div class="notif-message">
                                <div class="notif-message-icon"><img src="/web/img/arrow.png" alt="#"></div>
                                <div class="notif-message-text">
                                    <span>cc-1330</span> status has been charged: <strong>Signed</strong>
                                    <div><span><i class="far fa-clock"></i> 12:00</span><span>24.10.2018</span></div>
                                </div>
                            </div>

                            <div class="lines"></div>
                            <ul>
                                <li><a href="#"><img src="/web/img/watch.png" alt="#"> Notification history</a></li>
                                <li><a href=""><img src="/web/img/settings.png" alt="#"> Setting notifications</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="circle-wrap">

                        <a href="#" class="circle user" id='user-set-open'><img src="<?=(!empty(Yii::$app->user->identity->avatar)) ? Yii::$app->user->identity->avatar : '/web/img/comment_2.png'?>" alt="" class="avatar-circle"></a>

                        <div class="user-set">
                            <div class="user-name-wrap">
                                <div class="user-img">
                                    <img src="<?=(!empty(Yii::$app->user->identity->avatar)) ? Yii::$app->user->identity->avatar : '/web/img/comment_2.png'?>" class="avatar-circle" alt="#">
                                </div>
                                <div class="user-name">
                                    <span><?=$user->username?></span>
                                    <!-- <span><?=$user->role?></span> -->
                                </div>
                            </div>
                            <ul>
                                <?= (Yii::$app->user->identity->role >= 10 || Yii::$app->user->identity->id == 34)? '<li><a href="/manager/manageradmin">Director panel</a></li>' : '' ?>
                                <li><a href="/manager/cabinet">My cabinet</a></li>
                                <li><a href="/manager/edit">Setting profile</a></li>
                                <li><a href="/logout">Exit</a></li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="hmb-icon d-xl-none col-2">
                    <div class="hmb-wrap">
                        <div class="hmb"></div>
                    </div>
                </div>

            </div>

        </div>
    </header>
<?php } ?>        