<?php
  use yii\helpers\Html;
?>
    <script defer src="https://use.fontawesome.com/releases/v5.5.0/js/all.js" integrity="sha384-GqVMZRt5Gn7tB9D9q7ONtcp4gtHIUEW/yG7h98J7IpE3kpi+srfFyyB/04OV6pG0" crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/highcharts.src.js"></script>
    <script src="/web/libs/datetimepicker-master/jquery.datetimepicker.full.min.js"></script>
    <script src="/web/libs/phone/build/js/intlTelInput.min.js"></script>

    <script type="text/javascript">
        var input = document.querySelector("#choose-phone");
        window.intlTelInput(input, {
            separateDialCode: true,
        });
    </script>
    <!-- <script src="/web/js/main_1.js"></script> -->

    <script src="/web/js/main_4.js"></script>
    <script src="/web/js/chart.js"></script>

    <?php if(Yii::$app->session->getFlash('success')){ ?>
        <script type="text/javascript">
            alert('<?= Yii::$app->session->getFlash('success') ?>');
        </script>
    <?php } ?>
</body>

</html>
<?php $this->endPage() ?>