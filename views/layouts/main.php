<?php 
    use yii\helpers\Html; 
	use yii\widgets\Breadcrumbs;
?>
<?=$this->render('/layouts/header')?>
	<?php $this->beginBody(); ?>
		    <?= Breadcrumbs::widget([
		        'homeLink' => ['label' => 'Главная', 'url' => '/'],
		        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		    ]) ?>
		    <?= $content ?>
	<?php $this->endBody(); ?>
<?=$this->render('/layouts/footer')?>