<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Edit Project');

$cargo_weight = 0;
$cargo_volume = 0;
$cargo_volume_weight = 0;
$cargo_cartons = 0;
$cargo_expenses = 0;
?>

	<div class="options">
        <div class="choose-country d-sm-none d-md-none d-lg-none d-xl-none" id="country-click">
            <a href="#" class="choose-country-click">Choose Projects <span><i class="fas fa-caret-right"></i></span></a>
            <ul id="sm-countries">
                <li><a href="/projects/create"><i class="fa fa-plus" aria-hidden="true"></i> Add Project</a></li>
	            <li><a href="/projects/index"><i class="fa fa-bars" aria-hidden="true"></i> All Projects</a></li>
	            <!-- <li><a href="/projects/profit"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Total month profit</a></li> -->
            </ul>
        </div>

        <ul id="countries">
            <li><a href="/projects/create" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i> Add Project</a></li>
            <li><a href="/projects/index"><i class="fa fa-bars" aria-hidden="true"></i> All Projects</a></li>
            <!-- <li><a href="/projects/profit"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Total month profit</a></li> -->
        </ul>
    </div>

	<section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">Edit Project</div>
            </div>

            <div class="add-client-form-wrap update-project-form">
                <form>

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Manager <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager selectpicker" name="select-manager" onblur="this.style.color = '#000'" <?=(Yii::$app->user->identity->role < 9) ? 'disabled' : ''?> data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option <?= ($manager->manager_id == $model->manager_id) ? 'selected' : '' ?> value="<?=$manager->manager_id?>"><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project name <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-name" name="input-name" value="<?= (!empty($model->name)) ? $model->name : '' ?>"> 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project number</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-number" disabled value="<?= (!empty($model->project_id)) ? 'PJ-'.$model->project_id : '' ?>"> 
                                    <input type="hidden" class="project_id_hide" disabled value="<?= (!empty($model->project_id)) ? $model->project_id : '' ?>"> 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project created <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date" class="input-created" name="input-created" value="<?= (!empty($model->date_create)) ? date('d-M-Y',$model->date_create) : '' ?>" autocomplete="off">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project closed</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date-1" class="input-closed" name="input-closed" value="<?= (!empty($model->date_close)) ? date('d-M-Y',$model->date_close) : '' ?>" autocomplete="off">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project Type <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-type" name="select-type" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php for ($type_count=1; $type_count <= 4; $type_count++){ ?>
                                        	<option <?= ($type_count == $model->project_type) ? 'selected' : '' ?> value="<?=$type_count?>"><?=$model->type($type_count)?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line div-buying" style="display: none;">
                                <label>Buying list <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-buying selectpicker" name="select-buying" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($buyings as $buying) { ?>
                                            <option value="<?=$buying->buying_id?>" <?= ($buying->buying_id == $model->buying_id) ? 'selected' : '' ?>>BL-<?=$buying->buying_id?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <!-- <div class="add-client-form-line div-delivery_type" style="display: none;">
                                <label>Delivery type <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-delivery_type" name="manager" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($delivery_types as $delivery_type) { ?>
                                            <option <?= ($delivery_type->type_id == $model->delivery_type) ? 'selected' : '' ?> value="<?=$delivery_type->type_id?>"><?=$delivery_type->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> -->

                            <div class="add-client-form-line">
                                <label>Client <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-client selectpicker" name="select-client" data-live-search="true" <?=($model->payed == 0) ? '' : 'disabled' ?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?=(!empty($model->client_id) ? '<option value="'.$client->client_id.'" selected class="placeholder-select">88-'.$model->client_id.'</option>' : '')?>
                                        <?php foreach ($clients as $client) { ?>
                                        	<option <?= ($client->client_id == $model->client_id) ? 'selected' : '' ?> value="<?=$client->client_id?>">88-<?=$client->client_id?> - <?=$client->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Comment</label>
                                <div class="input-addclient-wrap">
                                    <textarea name="name" class="input-comment"><?= (!empty($model->comment)) ? $model->comment : '' ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                        	<div class="add-client-form-line">
                                <label>Total to pay USD <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-total" name="input-total" value="<?= (!empty($model->total)) ? $model->total : '' ?>">
                                </div>
                            </div>

                        	<div class="add-client-form-line">
                                <label>Profit USD</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-profit" value="<?= (!empty($model->profit)) ? $model->profit : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line add-client-form-line-checkbox">
                                <label>
                                    <input type="checkbox" name="status" class="input-status" <?= ($model->status == 1) ? 'checked' : '' ?>> 
                                    <p>Project approved</p>
                                    <span></span>
                                </label>
                            </div>

                            <div class="add-client-form-line">
                                <label>Payed $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-payed" value="<?= (!empty($model->payed)) ? $model->payed : '' ?>" disabled>
                                    <input type="hidden" placeholder="0.00" class="input-payed-hide" value="<?= (!empty($model->payed)) ? $model->payed : '' ?>" disabled>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Must pay $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-must_pay" value="<?= (!empty($model->must_pay)) ? (round($model->must_pay) > 0) ? '+'.round($model->must_pay) : round($model->must_pay) : 0 ?>" disabled>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Chargers $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-chargers" name="input-chargers" value="<?= (!empty($model->chargers)) ? $model->chargers : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Plan bonuse $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-plan_bonuse" name="input-plan_bonuse" value="<?= (!empty($model->plan_bonuse)) ? $model->plan_bonuse : '' ?>" disabled>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-4">

                            <div class="add-new-section row no-fw-row" style="">
                                <div class="col-lg-12 col-12">
                                    <div class="add-new-item add-new-item-mw">
                                        <div class="add-new-item-title">Pricing</div>
                                        <div class="add-new-item-top-line">
                                            <span>Cargo Type</span>
                                            <span>Tariff measure</span>
                                            <span>Tariff</span>
                                            <!-- <span></span> -->
                                        </div>
                                        <div class="add-new-item-button-inputs-temp">
                                            <?php foreach ($tariffs as $tariff) { ?>
                                                <div class="add-new-item-button-inputs">
                                                    <div class="add-client-form-line">
                                                        <div class="input-addclient-wrap select-input">
                                                            <select class="pricing-type pricing-type-<?=$tariff->id?>" name="manager" onblur="this.style.color = '#000'" style="color: rgb(0, 0, 0);" <?=($tariff->number == 0) ? '' : 'disabled'?>>
                                                                <option value="" selected class="placeholder-select">-Select-</option>
                                                                <option <?= ($tariff->type_cargo_id == 1) ? 'selected' : '' ?> value="1" class="option">General</option>
                                                                <option <?= ($tariff->type_cargo_id == 2) ? 'selected' : '' ?> value="2">Electronics</option>
                                                                <option <?= ($tariff->type_cargo_id == 3) ? 'selected' : '' ?> value="3">Brands</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="add-client-form-line">
                                                        <div class="input-addclient-wrap select-input">
                                                            <select class="pricing-measure pricing-measure-<?=$tariff->id?>" name="manager" onblur="this.style.color = '#000'"  <?=($tariff->number == 0) ? '' : 'disabled'?>>
                                                                <option value="" selected class="placeholder-select">-Select-</option>
                                                                <option <?= ($tariff->tariff_id == 1) ? 'selected' : '' ?> value="1" class="option">usd/kg</option>
                                                                <option <?= ($tariff->tariff_id == 2) ? 'selected' : '' ?> value="2">usd/pcs</option>
                                                                <option <?= ($tariff->tariff_id == 3) ? 'selected' : '' ?> value="3">usd/m3</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="add-client-form-line">
                                                        <div class="input-addclient-wrap">
                                                            <input type="text" placeholder="0.00" class="pricing-tariff pricing-tariff-<?=$tariff->id?>" value="<?= (!empty($tariff->number)) ? $tariff->number : '' ?>" <?=($tariff->number == 0) ? '' : 'disabled'?>>
                                                        </div>
                                                    </div>
                                                    <?php if($tariff->number == 0){ ?>
                                                        <div class="add-client-form-line" style="display:none;">
                                                            <div class="input-addclient-wrap">
                                                                <a class="submit-add-pricing" data-id="<?=$model->project_id?>" data-count="<?=$tariff->id?>">Save</a>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <!-- <div class="add-client-form-line">
                                                        <div class="input-addclient-wrap">
                                                            <a class="submit-add-pricing" style="background-color: #ff6565;" href="/projects/tariffdelete/<?=$tariff->id?>">Delete</a>
                                                        </div>
                                                    </div> -->
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <!-- <div class="add-new-item-button">
                                            <a href="#" id='add-pricing'><i class="fas fa-plus"></i> Add New</a>
                                        </div> -->
                                    </div>
                                </div>
                            </div>

                            <div class="add-new-section row no-fw-row">
                                <div class="col-lg-12 col-12 block-cargo-insurance">
                                    <div class="bottom-part-title">
                                        Cargo insurance
                                    </div>
                                    <div class="add-client-form-line">
                                        <label>Cargo cost $</label>
                                        <div class="input-addclient-wrap">
                                            <input type="text" placeholder="0.00" class="input-cargo_cost" value="<?= (!empty($model->cargo_cost)) ? $model->cargo_cost : '' ?>"> 
                                        </div>
                                    </div>
                                    <div class="add-client-form-line">
                                        <label>Insurance %</label>
                                        <div class="input-addclient-wrap">
                                            <input type="text" placeholder="0.00" class="input-insurance" value="<?= (!empty($model->insurance)) ? $model->insurance : '' ?>">
                                        </div>
                                    </div>
                                    <div class="add-client-form-line">
                                        <label>Insurance sum $</label>
                                        <div class="input-addclient-wrap">
                                            <input type="text" placeholder="0.00" class="input-insurance_sum" value="<?= (!empty($model->insurance_sum)) ? $model->insurance_sum : '' ?>" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                        </div>
                        <div class="col-8">
                            <div class="add-new-section row no-fw-row client_balance_block" style="display: none;">
                                <div class="col-lg-6 col-12">
                                    <div class="bottom-part-title">
                                        Balance
                                    </div>
                                    <div class="add-client-form-line">
                                        <label>Client Balance</label>
                                        <div class="input-addclient-wrap client_balance">
                                            <?=$balance?>$
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="add-new-section row no-fw-row money-movement-block" style="display: none;">
                                <div class="col-lg-12 col-12">
                                    <div class="add-new-item add-new-item-mw">
                                        <div class="add-new-item-title">Money Movement</div>
                                        <div class="add-new-item-top-line">
                                            <span>Data transaction</span>
                                            <span>Amount $</span>
                                            <span>Description</span>
                                            <span></span>
                                        </div>

                                        <div class="add-new-item-button-inputs-temp">
                                            <?php foreach ($withdraws as $withdraw) { ?>
                                                <?= 
                                                    '<div class="add-new-item-button-inputs">
                                                        <div class="add-client-form-line">
                                                            <div class="input-addclient-wrap">
                                                                <input type="text" class="dropdown-item dropdown-search-input" value="'.date('d.m.Y',$withdraw->date_payment).'">
                                                            </div>
                                                        </div>

                                                        <div class="add-client-form-line">
                                                            <div class="input-addclient-wrap">
                                                                <input type="text" value="-'.$withdraw->cash_usd.'">
                                                            </div>
                                                        </div>

                                                        <div class="add-client-form-line">
                                                            <div class="input-addclient-wrap">
                                                                <input type="text" value="'.$withdraw->comment.'">
                                                            </div>
                                                        </div>
                                                        <div class="add-client-form-line">
                                                            <div class="input-addclient-wrap">
                                                            </div>
                                                        </div>
                                                    </div>' 
                                                ?>
                                            <?php } ?>
                                        </div>

                                        <div class="add-new-item-button">
                                            <a href="#" id='add-money-off'><i class="fas fa-plus"></i> Add New</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="col-12">
                            <div class="bottom-part row no-fw-row">
                                <div class="add-new-form-submit-buttons col-12">
                                    <div class="add-new-form-submit-buttons-wrap col-4">
                                        <div class="add-new-form-submit-buttons-wrap-width">
                                            <button  class="submit-form">Save <img src="/web/img/preloader.gif" style="display: none;"></button>
                                            <a href="#" class="reset-form">Reset</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12" style="padding:25px 50px;">
                            <div class="add-new-section row no-fw-row" style="<?= ($cargos_count == 0) ? 'display: none;' : '' ?>">
                                <div class="add-new-item add-new-item-mw">
                                    <div class="add-new-item-title">Project Cargoes</div>
                                </div>
                            </div>
                            <div class="project-cargos" style="<?= ($cargos_count == 0) ? 'display: none;' : '' ?>">
                                <div class="row project-cargos-title">
                                    <div class="col-2">
                                        CC-code
                                    </div>
                                    <div class="col-1">
                                        Type
                                    </div>
                                    <div class="col-2">
                                        Weight
                                    </div>
                                    <div class="col-2">
                                        Volume
                                    </div>
                                    <div class="col-2">
                                        Volume Weight
                                    </div>
                                    <div class="col-2">
                                        Cartons
                                    </div>
                                    <div class="col-1">
                                        Expenses
                                    </div>
                                </div>
                                <?php foreach ($cargos as $cargo) { ?>
                                    <div class="row project-cargos-cargo">    
                                        <div class="col-2">
                                            <a href="/cargo/edit/<?=$cargo->cargo_id?>" target="_blank"><?= 'CC-'.$cargo->cargo_id ?></a>
                                        </div>
                                        <div class="col-1">
                                            <?= $cargo->type->name ?>
                                        </div>
                                        <div class="col-2">
                                            <?= $cargo->weight ?>
                                        </div>
                                        <div class="col-2">
                                            <?= $cargo->volume ?>
                                        </div>
                                        <div class="col-2">
                                            <?= $cargo->volume_weight ?>
                                        </div>
                                        <div class="col-2">
                                            <?= $cargo->cartons ?>
                                        </div>
                                        <div class="col-1">
                                            <?= $cargo->local_delivery_price ?>¥. <?= $cargo->local_expenses ?>
                                        </div>
                                    </div>
                                        <?php
                                            $cargo_weight += $cargo->weight;
                                            $cargo_volume += $cargo->volume;
                                            $cargo_volume_weight += $cargo->volume_weight;
                                            $cargo_cartons += $cargo->cartons;
                                            $cargo_expenses += $cargo->local_delivery_price;
                                        ?>
                                <?php } ?> 
                                <!-- Сумирование -->
                                <div class="row project-cargos-title">
                                    <div class="col-2">
                                        Total
                                    </div>
                                    <div class="col-1"></div>
                                    <div class="col-2">
                                        <?= $cargo_weight ?>
                                    </div>
                                    <div class="col-2">
                                        <?= $cargo_volume ?>
                                    </div>
                                    <div class="col-2">
                                        <?= $cargo_volume_weight ?>
                                    </div>
                                    <div class="col-2">
                                        <?= $cargo_cartons ?>
                                    </div>
                                    <div class="col-1">
                                        <?= $cargo_expenses ?>¥
                                    </div>
                                </div>
                            </div>
                        </div>

                </form>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        //RULES FORM
        $(".update-project-form form").validate({
            rules: {
              "select-manager": {
                  required: true
              },
              "input-name": {
                  required: true
              },
              'input-created': {
                  required: true
              },
              'select-type': {
                  required: true
              },
              'select-client': {
                  required: true
              },
              'input-total': {
                  required: true,
                  //min: 1
              },
            },
            messages: {
              "select-manager": {
                  required: "Please select manager",
              },
              "input-name": {
                  required: "Please enter project name",
              },
              "input-created": {
                  required: "Please select date created",
              },
              "select-type": {
                  required: "Please select type",
              },
              "select-client": {
                  required: "Please select client",
              },
              "input-total": {
                  required: "Please enter total to pay",
                  min:      "Please enter total to pay",
              },
              "select-buying": {
                  required: "Please select buying list",
                  min: "Please select buying list",
              },
              "input-closed":{
                required: "Please select date closed",
              },
            },
            submitHandler: function(form) {
              submitForm();
            }
        });
    </script>

    <script type="text/javascript">
    	function submitForm(){
            window.allWithdrawStatus = 0;
            //блок кнопки и прелоадер
            $('.submit-form').find('img').css('display','');
            $('.submit-form').attr('disabled', 'true');
            ///
            if ($(".input-status").is(":checked")) {
                var input_status = 1;
            }
            else {
                var input_status = 0;
            }
            var statuswithdrow = 0;
            // $('.submit-withdraw').each(function() {
            //     if($(this).parents().eq(2).find('[placeholder="Description"]').val() == ''){
            //         $(this).parents().eq(2).find('[placeholder="Description"]').css({'border': '1px solid red'});
            //         statuswithdrow = 1;
            //     }
            // });
            if(statuswithdrow == 0){
                $.ajax({ 
                    type: 'POST', 
                    url: '/projects/edit/<?=$model->project_id?>', 
                    data:{
                        manager:        $('.select-manager option:selected').val(), 
                        type:           $('.select-type option:selected').val(), 
                        buying:         $('.select-buying option:selected').val(), 
                        client:         $('.select-client option:selected').val(),
                        delivery_type:  $('.select-delivery_type option:selected').val(),

                        name:           $('.input-name').val(), 
                        number:         $('.input-number').val(), 
                        created:        $('.input-created').val(), 
                        closed:         $('.input-closed').val(), 
                        comment:        $('.input-comment').val(), 
                        total:          $('.input-total').val(), 
                        profit:         $('.input-profit').val(),
                        status:         input_status,


                        payed:          $('.input-payed').val(), 
                        must_pay:       $('.input-must_pay').val(), 
                        chargers:       $('.input-chargers').val(), 
                        plan_bonuse:    $('.input-plan_bonuse').val(), 
                        cargo_cost:     $('.input-cargo_cost').val(), 
                        insurance:      $('.input-insurance').val(), 
                        insurance_sum:  $('.input-insurance_sum').val(),

                        '<?= \yii::$app->request->csrfParam?>': '<?= \yii::$app->request->csrfToken ?>'
                    },
                    //dataType: 'json',
                    success: function (data) { 
                        $('.submit-withdraw').each(function() {
                            if(statuswithdrow == 0){
                                $(this).trigger("click");
                            }
                        });
                        $('.submit-add-pricing').each(function() {
                            if(statuswithdrow == 0){
                                $(this).trigger("click");
                            }
                        });
                        if (window.allWithdrawStatus == 0) { //data == 1 && 
                            window.location.replace("/projects/index");
                        }else{
                          $('.submit-form').find('img').css('display','none');
                          $('.submit-form').removeAttr("disabled");
                          //window.location.replace("/projects/index");
                        }
                    }
                });
            }else{
                alert('Please enter Description to Money Movement!');
                $('.submit-form').find('img').css('display','none');
                $('.submit-form').removeAttr("disabled");
            }
        }

        $('.select-type').change(function() {
            selectType(1);
        });
        selectType();

        function selectType(chenge = 0){
            if($('.select-type option:selected').val() == 1){
                //$('.div-delivery_type').css({'display':''});
                $('.div-buying').css({'display':'none'});
                $(".select-buying option:selected").prop("selected", false);
                /////validate
                $(".select-buying").rules("remove");
                //$('.input-chargers').removeAttr("disabled");
                // $('.input-plan_bonuse').removeAttr("disabled");
                /////////новшиство
                $(".input-total").attr('disabled', 'true');
                if(chenge == 1){
                    $(".input-total").val(0);
                }
                $(".input-chargers").attr('disabled', 'true');
                $(".input-profit").attr('disabled', 'true');
                $('.block-cargo-insurance').css({'display':''});
            }
            else if($('.select-type option:selected').val() == 2){
                //$('.div-delivery_type').css({'display':'none'});
                $('.div-buying').css({'display':'none'});
                $('.select-delivery_type').val(0);
                $('.div-buying').css({'display':'none'});
                $(".select-buying option:selected").prop("selected", false);
                ////validate
                $(".input-chargers").val('');
                $(".input-chargers").attr('disabled', 'true');
                // $(".input-plan_bonuse").val('');
                // $(".input-plan_bonuse").attr('disabled', 'true');
                $(".select-buying").rules("remove");
                $('.input-chargers').val($('.input-total').val() - $('.input-profit').val());
                /////////новшиство
                $('.input-total').removeAttr("disabled");
                $('.input-chargers').removeAttr("disabled");
                $('.input-profit').removeAttr("disabled");
                $('.block-cargo-insurance').css({'display':'none'});
            }
            else if($('.select-type option:selected').val() == 3){
                //$('.div-delivery_type').css({'display':'none'});
                $('.div-buying').css({'display':''});
                $('.select-delivery_type').val(0);
                /////validate
                $(".select-buying").rules("add",{
                    required: true,
                    min: 1
                });
                $('.input-chargers').removeAttr("disabled");
                // $('.input-plan_bonuse').removeAttr("disabled");
                /////////новшиство
                $('.input-total').removeAttr("disabled");
                $('.input-chargers').removeAttr("disabled");
                $('.input-profit').removeAttr("disabled");
                $('.block-cargo-insurance').css({'display':'none'});
            }
            else{
                //$('.div-delivery_type').css({'display':'none'});
                $('.select-delivery_type').val(0);
                $('.div-buying').css({'display':'none'});
                $(".select-buying option:selected").prop("selected", false);
                /////validate
                $(".select-buying").rules("remove");
                $('.input-chargers').removeAttr("disabled");
                // $('.input-plan_bonuse').removeAttr("disabled");
                /////////новшиство
                $('.input-total').removeAttr("disabled");
                $('.input-chargers').removeAttr("disabled");
                $('.input-profit').removeAttr("disabled");
                $('.block-cargo-insurance').css({'display':'none'});
            }
            //$('.selectpicker').selectpicker('refresh');
        }

        $('.input-status').change(function() {
            if ($(".input-status").is(":checked")) {
                $(".input-closed").rules("add",{
                    required: true
                });
            }else{
                $(".input-closed").rules("remove");
            }
        });

        $('.mob-menu-Projects > a').addClass('active');

        $('.input-cargo_cost').change(function(){
            if($('.input-cargo_cost').val() > 0 && $('.input-insurance').val() > 0){
                $('.input-insurance_sum').val(($('.input-cargo_cost').val()/100)*$('.input-insurance').val());
            }
        });
        $('.input-insurance').change(function(){
            if($('.input-cargo_cost').val() > 0 && $('.input-insurance').val() > 0){
                $('.input-insurance_sum').val(($('.input-cargo_cost').val()/100)*$('.input-insurance').val());
            }
        });

        $( ".select-client" ).change(function() {
            select_client();
        });
        select_client();

        function select_client(){
            $.ajax({ 
                url: '/projects/clientsearch/'+$('.select-client option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data.client_id > 0){
                        $('.client_balance').text(data.balance+'$');
                        $(".select-manager select").val(data.manager_id);
                        $('.selectpicker').selectpicker('refresh');
                        $('.client_balance_block').css({'display':'block'});
                        $('.money-movement-block').css({'display':'block'});
                    }else{
                        $('.client_balance_block').text('0$');
                        $('.client_balance_block').css({'display':'none'});
                        $('.money-movement-block').css({'display':'none'});
                    }
                }
            });
        }
        
        // $(document).keyup(function(e) {
        //     if(e.which == 13){ //enter press
        //         $('.submit-form').trigger('click');
        //     }
        //     if (e.key === "Escape") { 
        //         $('.reset-form').trigger('click');
        //     }
        // });
        $('.reset-form').click(function(){
            location.reload();
        });
        
        ////chargers math
        $('.input-total').change(function(){
            if($('.select-type option:selected').val() == 2){
                $('.input-chargers').val($('.input-total').val() - $('.input-profit').val());
            }
        });

        $('.input-profit').change(function(){
            if($('.select-type option:selected').val() == 2){
                $('.input-chargers').val($('.input-total').val() - $('.input-profit').val());
            }
        });

        setInterval(function() {
                summ = 0;
                for (var i = 11; i < 30; i++) {
                    if($('.money-off-amount-'+i).val() > 0){
                        summ += parseFloat($('.money-off-amount-'+i).val());
                    }
                }
                //paid
                $('.input-payed').val(summ + parseFloat($('.input-payed-hide').val()));
                ///must
                $('.input-must_pay').val(-1*(parseFloat($('.input-total').val()) - parseFloat($('.input-payed').val())));
                if($('.input-must_pay').val() > 0){
                    $('.input-must_pay').val('+'+$('.input-must_pay').val())
                }
        }, 1000); // каждую секунду
    </script>
