<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Create Project');
?>

	<div class="options">
        <div class="choose-country d-sm-none d-md-none d-lg-none d-xl-none" id="country-click">
            <a href="#" class="choose-country-click">Choose Projects <span><i class="fas fa-caret-right"></i></span></a>
            <ul id="sm-countries">
                <li><a href="/projects/create"><i class="fa fa-plus" aria-hidden="true"></i> Add Project</a></li>
	            <li><a href="/projects/index"><i class="fa fa-bars" aria-hidden="true"></i> All Projects</a></li>
	            <!-- <li><a href="/projects/profit"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Total month profit</a></li> -->
            </ul>
        </div>

        <ul id="countries">
            <li><a href="/projects/create" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i> Add Project</a></li>
            <li><a href="/projects/index"><i class="fa fa-bars" aria-hidden="true"></i> All Projects</a></li>
            <!-- <li><a href="/projects/profit"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Total month profit</a></li> -->
        </ul>
    </div>

	<section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">Create Project</div>
            </div>

            <div class="add-client-form-wrap create-project-form">
                <form>

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Manager <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager selectpicker" name="select-manager" onblur="this.style.color = '#000'" <?=(Yii::$app->user->identity->role < 9) ? 'disabled' : ''?> data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option value="<?=$manager->manager_id?>" <?=(Yii::$app->user->identity->role < 9 && $manager->username == Yii::$app->user->identity->username) ? 'selected' : ''?>><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project name <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-name" name="input-name"> 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project number</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-number" disabled> 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project created <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date" class="input-created" value="<?=date('d-M-Y',time())?>" autocomplete="off" name="input-created">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <!-- <div class="add-client-form-line">
                                <label>Project closed</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date-1" class="input-closed" name="input-closed" autocomplete="off">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div> -->

                            <div class="add-client-form-line">
                                <label>Project Type <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-type" name="select-type" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php for ($type_count=1; $type_count <= 4; $type_count++){ ?>
                                        	<option value="<?=$type_count?>"><?=$model->type($type_count)?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line div-buying" style="display: none;">
                                <label>Buying list <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-buying selectpicker" name="select-buying" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($buyings as $buying) { ?>
                                            <option value="<?=$buying->buying_id?>">BL-<?=$buying->buying_id?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <!-- <div class="add-client-form-line div-delivery_type" style="display: none;">
                                <label>Delivery type <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-delivery_type" name="manager" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($delivery_types as $delivery_type) { ?>
                                            <option value="<?=$delivery_type->type_id?>"><?=$delivery_type->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> -->

                            <div class="add-client-form-line">
                                <label>Client <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-client selectpicker" name="select-client" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($clients as $client) { ?>
                                        	<option value="<?=$client->client_id?>">88-<?=$client->client_id?> - <?=$client->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Comment</label>
                                <div class="input-addclient-wrap">
                                    <textarea name="name" class="input-comment"> </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                        	<div class="add-client-form-line">
                                <label>Total to pay $ <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-total" name="input-total">
                                </div>
                            </div>

                        	<div class="add-client-form-line">
                                <label>Profit $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-profit">
                                </div>
                            </div>

                            <!-- <div class="add-client-form-line add-client-form-line-checkbox">
                                <label>
                                    <input type="checkbox" name="status" class="input-status">
                                    <p>Project approved</p>
                                    <span></span>
                                </label>
                            </div> -->

                            <div class="add-client-form-line">
                                <label>Payed $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-payed" disabled>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Must pay $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-must_pay" disabled>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Chargers $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-chargers" name="input-chargers">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Plan bonuse $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-plan_bonuse" name="input-plan_bonuse" disabled>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="add-new-section row no-fw-row">
                        <div class="col-lg-4 col-12">
                            <div class="bottom-part-title">
                                Cargo insurance
                            </div>
                            <div class="add-client-form-line">
                                <label>Cargo cost $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-cargo_cost" > 
                                </div>
                            </div>
                            <div class="add-client-form-line">
                                <label>Insurance %</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-insurance" >
                                </div>
                            </div>
                            <div class="add-client-form-line">
                                <label>Insurance sum $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-insurance_sum" disabled>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bottom-part row no-fw-row">
                        <div class="add-new-form-submit-buttons col-12">
                            <div class="add-new-form-submit-buttons-wrap col-4">
                                <div class="add-new-form-submit-buttons-wrap-width">
                                    <button  class="submit-form">Create <img src="/web/img/preloader.gif" style="display: none;"></button>
                                    <a href="#" class="reset-form">Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        //RULES FORM
        $(".create-project-form form").validate({
            rules: {
              "select-manager": {
                  required: true
              },
              "input-name": {
                  required: true
              },
              'input-created': {
                  required: true
              },
              'select-type': {
                  required: true
              },
              'select-client': {
                  required: true
              },
              'input-total': {
                  required: true
              },
            },
            messages: {
              "select-manager": {
                  required: "Please select manager",
              },
              "input-name": {
                  required: "Please enter project name",
              },
              "input-created": {
                  required: "Please select date created",
              },
              "select-type": {
                  required: "Please select type",
              },
              "select-client": {
                  required: "Please select client",
              },
              "input-total": {
                  required: "Please enter total to pay",
              },
              "select-buying": {
                  required: "Please select buying list",
              },
              "input-closed":{
                required: "Please select date closed",
              },
            },
            submitHandler: function(form) {
              submitForm();
            }
        });
    </script>

    <script type="text/javascript">
        function submitForm(){
            //блок кнопки и прелоадер
            $('.submit-form').find('img').css('display','');
            $('.submit-form').attr('disabled', 'true');
            ///
            if ($(".input-status").is(":checked")) {
                var input_status = 1;
            }
            else {
                var input_status = 0;
            }
    	
        	$.ajax({ 
    	        type: 'POST', 
    	        url: '/projects/create', 
    	        data:{
    		        manager: 		$('.select-manager option:selected').val(), 
    		        type: 			$('.select-type option:selected').val(), 
                    buying:         $('.select-buying option:selected').val(), 
    		        client: 		$('.select-client option:selected').val(),
                    delivery_type:  $('.select-delivery_type option:selected').val(),

    	        	name: 			$('.input-name').val(), 
    	        	number: 		$('.input-number').val(), 
    	        	created: 		$('.input-created').val(), 
    	        	closed: 		$('.input-closed').val(), 
    	        	comment: 		$('.input-comment').val(), 
    	        	total: 			$('.input-total').val(), 
    	        	profit: 		$('.input-profit').val(),
    	        	status: 		input_status,


                    payed:          $('.input-payed').val(), 
                    must_pay:       $('.input-must_pay').val(), 
                    chargers:       $('.input-chargers').val(), 
                    plan_bonuse:    $('.input-plan_bonuse').val(), 
                    cargo_cost:     $('.input-cargo_cost').val(), 
                    insurance:      $('.input-insurance').val(), 
                    insurance_sum:  $('.input-insurance_sum').val(),

    	        	'<?= \yii::$app->request->csrfParam?>': '<?= \yii::$app->request->csrfToken ?>'
    		    },
    	        //dataType: 'json',
    	        success: function (data) { 
    	            console.log(data);
    	            if (data > 0) {
    	            	window.location.replace("/projects/edit/"+data);
    	            }else{
                      alert(data);
                      $('.submit-form').find('img').css('display','none');
                      $('.submit-form').removeAttr("disabled");
                    }
    	        }
    	    });
    	}

        $('.select-type').change(function() {
            selectType();
        });
        selectType();
        
        function selectType(){
            if($('.select-type option:selected').val() == 1){
                //$('.div-delivery_type').css({'display':''});
                $('.div-buying').css({'display':'none'});
                $('.add-new-section').css({'display':''});
                $(".select-buying option:selected").prop("selected", false);
                /////validate
                $(".select-buying").rules("remove");
                $('.input-chargers').removeAttr("disabled");
                // $('.input-plan_bonuse').removeAttr("disabled");
            }
            else if($('.select-type option:selected').val() == 2){
                //$('.div-delivery_type').css({'display':'none'});
                $('.div-buying').css({'display':'none'});
                $('.add-new-section').css({'display':'none'});
                $('.select-delivery_type').val(0);
                $('.div-buying').css({'display':'none'});
                $(".select-buying option:selected").prop("selected", false);
                ////validate
                $(".input-chargers").val('');
                $(".input-chargers").attr('disabled', 'true');
                $(".input-plan_bonuse").val('');
                $(".input-plan_bonuse").attr('disabled', 'true');
                $(".select-buying").rules("remove");
                $('.input-chargers').val($('.input-total').val() - $('.input-profit').val());
            }
            else if($('.select-type option:selected').val() == 3){
                //$('.div-delivery_type').css({'display':'none'});
                $('.div-buying').css({'display':''});
                $('.add-new-section').css({'display':'none'});
                $('.select-delivery_type').val(0);
                /////validate
                $(".select-buying option:selected").prop("selected", false);
                $(".select-buying").rules("add",{
                    required: true
                });
                $('.input-chargers').removeAttr("disabled");
                // $('.input-plan_bonuse').removeAttr("disabled");
            }
            else{
                //$('.div-delivery_type').css({'display':'none'});
                $('.select-delivery_type').val(0);
                $('.add-new-section').css({'display':'none'});
                $('.div-buying').css({'display':'none'});
                $(".select-buying option:selected").prop("selected", false);
                /////validate
                $(".select-buying").rules("remove");
                $('.input-chargers').removeAttr("disabled");
                // $('.input-plan_bonuse').removeAttr("disabled");
            }
            //$('.selectpicker').selectpicker('refresh');
        }

        $('.input-status').change(function() {
            if ($(".input-status").is(":checked")) {
                $(".input-closed").rules("add",{
                    required: true
                });
            }else{
                $(".input-closed").rules("remove");
            }
        });

        $('.mob-menu-Projects > a').addClass('active');
        
        $('.input-cargo_cost').change(function(){
            if($('.input-cargo_cost').val() > 0 && $('.input-insurance').val() > 0){
                $('.input-insurance_sum').val(($('.input-cargo_cost').val()/100)*$('.input-insurance').val());
            }
        });
        $('.input-insurance').change(function(){
            if($('.input-cargo_cost').val() > 0 && $('.input-insurance').val() > 0){
                $('.input-insurance_sum').val(($('.input-cargo_cost').val()/100)*$('.input-insurance').val());
            }
        });

        $( ".select-client" ).change(function() {
            select_client();
        });

        function select_client(){
            $.ajax({ 
                url: '/cargo/clientsearch/'+$('.select-client option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data != 0){
                        $(".select-manager select").val(data);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
        }
        
        // $(document).keyup(function(e) {
        //     if(e.which == 13){ //enter press
        //         $('.submit-form').trigger('click');
        //     }
        //     if (e.key === "Escape") { 
        //         $('.reset-form').trigger('click');
        //     }
        // });
        $('.reset-form').click(function(){
            location.reload();
        });

        ////chargers math
        $('.input-total').change(function(){
            if($('.select-type option:selected').val() == 2){
                $('.input-chargers').val($('.input-total').val() - $('.input-profit').val());
            }
        });

        $('.input-profit').change(function(){
            if($('.select-type option:selected').val() == 2){
                $('.input-chargers').val($('.input-total').val() - $('.input-profit').val());
            }
        });
    </script>
