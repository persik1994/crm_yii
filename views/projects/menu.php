<div class="options">
    <div class="choose-country d-sm-none d-md-none d-lg-none d-xl-none" id="country-click">
        <a href="#" class="choose-country-click">Choose Projects <span><i class="fas fa-caret-right"></i></span></a>
        <ul id="sm-countries">
            <li><a href="/projects/create"><i class="fa fa-plus" aria-hidden="true"></i> Add Project</a></li>
            <li><a href="/projects/index"><i class="fa fa-bars" aria-hidden="true"></i> All Projects</a></li>
            <li><a href="/projects/paid"><i class="fas fa-check-circle"></i> Paid Projects</a></li>
            <li><a href="/projects/nopaid"><i class="fas fa-times-circle"></i> Unpaid Projects</a></li>
            <!-- <li><a href="/projects/profit"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Total month profit</a></li> -->
        </ul>
    </div>

    <ul id="countries">
        <li><a href="/projects/create" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i> Add Project</a></li>
        <li><a href="/projects/index"><i class="fa fa-bars" aria-hidden="true"></i> All Projects</a></li>
        <li><a href="/projects/paid"><i class="fas fa-check-circle"></i> Paid Projects</a></li>
        <li><a href="/projects/nopaid"><i class="fas fa-times-circle"></i> Unpaid Projects</a></li>
        <!-- <li><a href="/projects/profit"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Total month profit</a></li> -->
    </ul>
</div>