<tr class="projects-row" data-project_id="<?= (!empty($project->project_id)) ? $project->project_id : '' ?>">
    
    <td><?= (!empty($project->project_id)) ? 'PRJ-'.$project->project_id : '' ?></td>
    <td><?= (!empty($project->manager->username)) ? $project->manager->username : '' ?></td>
    <td><?= (!empty($project->client->client_id)) ? '88-'.$project->client->client_id : '' ?><?= (!empty($project->client->name)) ? ' - '.$project->client->name : '' ?></td>
    <td><?= (!empty($project->date_create)) ? date('d.m.Y', $project->date_create) : '' ?></td>
    <td><?= (!empty($project->date_close)) ? date('d.m.Y', $project->date_close) : '' ?></td>
    <td><?= (!empty($project->project_type)) ? $project_Obj->type($project->project_type) : '' ?></td>
    <td><?= (!empty($project->shipmentType->name)) ? $project->shipmentType->name : '' ?></td>
    <td><?= (!empty($project->name)) ? $project->name : '' ?></td>
    <td class="table-description"><?= (!empty($project->comment)) ? $project->comment : '' ?></td>
</tr>