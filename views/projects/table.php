<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Project;
use app\models\Payments;
use app\models\CustomTable;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects');
//$this->params['breadcrumbs'][] = $this->title;
?>

    <?=$menu?>

    <style type="text/css">
        .table-bordered tbody{
          display:block;
          overflow:auto;
          height:200px;
          width:100%;
          overflow-x: hidden;
        }
        .table-bordered thead tr{
          display:block;
        }
    </style>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">

                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        All Projects * <?= Project::SearchShowBtn() ?>
                    </div>
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                    <div class="add-new">
                        <div class="dropdown dropdown-table custom-table-div" style="">
                            <a class="custom-table-menu" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-cog"></i> Custom table
                            </a>
                            <div class="dropdown-menu dropdown-menu-table custom-table-checked" aria-labelledby="dropdownMenuLink">
                                <a class="custom-table-btn"><i class="fas fa-save"></i> Save setting</a><Br>
                                <?= CustomTable::menuOptions('project', 'project') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-wrap project-wrap">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'rowOptions'=>function($model){
                            return ['class' => ($model->status == 1)? 'project-row contextmenu-row projects-row-approve':'project-row contextmenu-row', 'url-edit' => '/projects/edit/'.$model->project_id, 'url-delete' => '/projects/delete/'.$model->project_id];
                        },
                        'columns' => [
                            [
                                'attribute' => 'check',
                                'label' => '<input type="checkbox" class="checkbox-table-all" value="">',
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                "format" => 'raw',
                                'value' => function ($data) {
                                    return '<input type="checkbox" class="checkbox-table" value="'.$data->project_id.'"> <i class="far fa-eye eye-click-view-row" title="View" data-key="'.$data->project_id.'"></i><a href="/projects/edit/'.$data->project_id.'" class="table-edit-btn"><i class="fas fa-pencil-alt"></i></a>'; 
                                },
                            ],
                            [
                                'attribute' => 'project_id',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['project_id'], 'project_id', 'text', 'Write project id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','project_id')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','project_id')],
                                'value' => function ($data) {
                                    return (!empty($data->project_id)) ? 'PJ-'.$data->project_id : '';
                                },
                            ],
                            [
                                'attribute' => 'name',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['name'], 'name', 'text', 'Write project name'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','name').CustomTable::widthTable(150)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','name').CustomTable::widthTable(150)],
                                'value' => function ($data) {
                                    return $data->name ?? '';
                                },
                            ],
                            [
                                'attribute' => 'manager_id',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['manager_id'], 'manager_id', 'select', 'Select Manager'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','manager_id').CustomTable::widthTable(140)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','manager_id').CustomTable::widthTable(140)],
                                'value' => function ($data) {
                                    return $data->manager->username ?? '';
                                },
                            ],
                            [
                                'attribute' => 'client_id',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['client_id'], 'client_id', 'text', 'Write client id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','client_id')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','client_id')],
                                'value' => function ($data) {
                                    return (!empty($data->client_id)) ? '88-'.$data->client_id : '';
                                },
                            ],
                            [
                                'attribute' => 'date_create',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['date_create'], 'date_create', 'date', 'Select date create'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','date_create')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','date_create')],
                                'value' => function ($data) {
                                    return (!empty($data->date_create)) ? date('d.m.Y',$data->date_create) : ''; 
                                },
                            ],
                            [
                                'attribute' => 'date_close',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['date_close'], 'date_close', 'date2', 'Select date close'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','date_close')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','date_close')],
                                'value' => function ($data) {
                                    return (!empty($data->date_close)) ? date('d.m.Y',$data->date_close) : ''; 
                                },
                            ],
                            [
                                'attribute' => 'project_type',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['project_type'], 'project_type', 'select', 'Select type'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','project_type').CustomTable::widthTable(120)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','project_type').CustomTable::widthTable(120)],
                                'value' => function ($data) {
                                    return Project::type($data->project_type ?? '');
                                },
                            ],
                            [
                                'attribute' => 'delivery_type',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['delivery_type'], 'delivery_type', 'select', 'Select delivery type'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','delivery_type').'display:none;'],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','delivery_type').'display:none;'],
                                'value' => function ($data) {
                                    return $data->shipmentType->name ?? '';
                                },
                            ],
                            [
                                'attribute' => 'buying_id',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['buying_id'], 'buying_id', 'text', 'Write buying id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','buying_id')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','buying_id')],
                                'value' => function ($data) {
                                    return (!empty($data->buying_id)) ? 'BL-'.$data->buying_id : '';
                                },
                            ],
                            ////////

                            [
                                'attribute' => 'total',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['total'], 'total', 'text', 'Write total'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','total')],
                                'contentOptions' => function ($data) {
                                    return ['style' => CustomTable::rowVisible('project','project','total'), 'class'=>'data-total-'.$data->project_id]; 
                                },
                                'value' => function ($data) {
                                    return (!empty($data->total)) ? round($data->total) : 0;
                                },
                            ],
                            [
                                'attribute' => 'profit',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['profit'], 'profit', 'text', 'Write profit'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','profit')],
                                'contentOptions' => function ($data) {
                                    return ['style' => CustomTable::rowVisible('project','project','profit'), 'class'=>'data-profit-'.$data->project_id]; 
                                },
                                'value' => function ($data) {
                                    return (!empty($data->profit)) ? round($data->profit) : 0;
                                },
                            ],
                            [
                                'attribute' => 'payed',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['payed'], 'payed', 'text', 'Write payed'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','payed')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','payed')],
                                'value' => function ($data) {
                                    return (!empty($data->payed)) ? round($data->payed) : 0;
                                },
                            ],
                            [
                                'attribute' => 'must_pay',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['must_pay'], 'must_pay', 'text', 'Write must pay'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','must_pay')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','must_pay')],
                                'value' => function ($data) {
                                    return (!empty($data->must_pay)) ? (round($data->must_pay) > 0) ? '+'.round($data->must_pay) : round($data->must_pay) : 0;
                                },
                            ],
                            [
                                'attribute' => 'chargers',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['chargers'], 'chargers', 'text', 'Write chargers'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','chargers')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','chargers')],
                                'value' => function ($data) {
                                    return (!empty($data->chargers)) ? (round(-1 * $data->chargers) > 0) ? '+'.round(-1 * $data->chargers) : round(-1 * $data->chargers) : 0;
                                },
                            ],
                            [
                                'attribute' => 'plan_bonuse',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['plan_bonuse'], 'plan_bonuse', 'text', 'Write plan bonuse'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','plan_bonuse')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','plan_bonuse')],
                                'value' => function ($data) {
                                    return (!empty($data->plan_bonuse)) ? round($data->plan_bonuse) : 0;
                                },
                            ],
                            ////////
                            [
                                'attribute' => 'comment',
                                'label' => Project::projectDropMenu(Project::attributeLabels()['comment'], 'comment', 'text', 'Write comment'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('project','project','comment').CustomTable::widthTable(600)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('project','project','comment').CustomTable::widthTable(600)],
                                'value' => function ($data) {
                                    return $data->comment ?? '';
                                },
                            ],
                        ],
                    ]); ?>
                </div>

                <div class="detail-info-top-line">
                    <div class="detail-info-controls">
                        <a href="#" id="slide-position-left"><i class="fas fa-chevron-left"></i></a>
                        <a href="#" id="slide-position-right"><i class="fas fa-chevron-right"></i></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="" class="project-detail-info-url-edit"><i class="fas fa-pencil-alt"></i>Edit</a>
                            <a data-id="" class="delete-post-url" style="display: <?=(Yii::$app->user->identity->role >= 9)?';':'none;'?>"><i class="far fa-trash-alt"></i>Delete</a>
                            <a href="#" id="close-detail-info"><i class="fas fa-times"></i></a>
                        </div>
                    </div>
                </div>

                <div class="detail-info-wrap">
                    <div class="detail-info">
                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project ID</div>
                                <div class="detail-info-list-column detail-info-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project name</div>
                                <div class="detail-info-list-column detail-info-name"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project created</div>
                                <div class="detail-info-list-column detail-info-date"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project closed</div>
                                <div class="detail-info-list-column detail-info-date_closed"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project Type</div>
                                <div class="detail-info-list-column detail-info-type"></div>
                            </div>
                            <div class="detail-info-list-row" style="display: none;">
                                <div class="detail-info-list-column column-grey">Delivery type</div>
                                <div class="detail-info-list-column detail-info-delivery"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Buying ID</div>
                                <div class="detail-info-list-column detail-info-buying"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client</div>
                                <div class="detail-info-list-column detail-info-client"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Comment</div>
                                <div class="detail-info-list-column detail-info-comment"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Total to pay</div>
                                <div class="detail-info-list-column detail-info-total"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Profit</div>
                                <div class="detail-info-list-column detail-info-profit"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project approved</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>

                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Payed $</div>
                                <div class="detail-info-list-column detail-info-payed"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Must pay $</div>
                                <div class="detail-info-list-column detail-info-must_pay"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Chargers $</div>
                                <div class="detail-info-list-column detail-info-chargers"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Plan bonuse $</div>
                                <div class="detail-info-list-column detail-info-bonuse"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo cost $</div>
                                <div class="detail-info-list-column detail-info-cost"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Insurance %</div>
                                <div class="detail-info-list-column detail-info-insurance"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Insurance sum $</div>
                                <div class="detail-info-list-column detail-info-insurance_sum"></div>
                            </div>
                        </div>

                        <div class="detail-info-list-wrap project_cargoes_all_block">
                            <label>Project Cargoes</label>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">CC-code</div>
                                <div class="detail-info-list-column column-grey">Weight</div>
                                <div class="detail-info-list-column column-grey">Volume</div>
                                <div class="detail-info-list-column column-grey">Volume Weight</div>
                                <div class="detail-info-list-column column-grey">Cartons</div>
                                <div class="detail-info-list-column column-grey">Expenses</div>
                            </div>
                            <div class="project_cargoes_all">
                                
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing">
                </div>
                <div class="col-6 summary-selected">
                    <span class="summ-total"></span>
                    <span class="summ-profit"></span>
                </div>
            </div>
        </div>
    </section>
    <nav id="context-menu" class="context-menu">
        <ul class="context-menu__items">
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="View"><i class="fa fa-eye"></i> View</a>
          </li>
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="Edit"><i class="fa fa-edit"></i> Edit</a>
          </li>
          <?php if(Yii::$app->user->identity->role >= 9){ ?>
              <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="Delete"><i class="fa fa-times"></i> Delete</a>
              </li>
          <?php } ?>
        </ul>
    </nav>

<script src="/web/js/contecst_menu.js"></script>
<script type="text/javascript">

    setInterval(function() {
        tableHeight();
    }, 500);

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        summ_height = window_height - ($('.all-cargos-title').height() + $('.all-cargos').height() + $('.options').height() + 50);
        if(summ_height < 500){
            summ_height = 500;
        }
        $('.table-wrap').css({'max-height' : summ_height+'px'});
        $('.table-bordered').css({'max-height' : summ_height+'px'});
        $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
    }

    $('.mob-menu-Projects > a').addClass('active');

    $('.summ_bottom').html($('.summary').html());
    $('.summary').html('');


    $(document).on('keyup', '.dropdown-search-input', function (event) {
        if(event.keyCode==13) {
            runSearch($(this).attr("data-type"),$(this).val());
        }
    });

    $(document).on('click', '.search-menu-click', function () {
        runSearch($(this).attr("data-type"),$(this).attr("data-id"));
    });

    $(document).on('change', '#form-choose-date', function () {
        runSearch($(this).attr("data-type"),$(this).val());
    });

    function runSearch(type, value){
        $.ajax({ 
            type: 'POST', 
            url: '/projects/search?'+type+'='+value+'&sort=<?=Yii::$app->request->get('sort')?>', 
            success: function (data) { 
                $('.all-cargos-content').html(data);
            }
        });
    };

</script>
<script type="text/javascript">
    // $(window).resize(function() {
    //     tableHeight();
    // });

    // tableHeight();

    // function tableHeight(){
    //     window_height = $(window).height();
    //     body_height = $('body').height();
    //     table = $('.table-wrap').height();
    //     summ_height = window_height - (body_height - table) - 15;
    //     $('.table-wrap').css({'max-height' : summ_height+'px'});
    // }

    $('.mob-menu-Projects > a').addClass('active');

    $(".delete-post-url").click(function() {
        var delete_text = confirm('Do you really want to delete project?');
        if(delete_text) window.location.replace('/projects/delete/'+$(this).attr("data-id"));
    });

    $('.custom-table-btn').click(function() {
        var sListType = '';
        $('.custom-table-checked input').each(function () {
            if(this.checked == "1"){
                sListType += $(this).val() + ",";
            }
        });
        if(sListType != ''){
            $.ajax({ 
                type: 'GET', 
                url: '/projects/customtable?page=project&type='+sListType, 
                success: function (data) { 
                    location.reload();
                }
            });
        }
    });

    $(document).on('click', '.checkbox-table-all', function () {
        $('.checkbox-table').not(this).prop('checked', this.checked);
        allCargosSelectEdits(1);
    });

    $(document).on('click', '.checkbox-table', function () {
        if(this.checked == "1"){
            $('.project-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
        }else{
            $('.project-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
        }
        allCargosSelectEdits();
    });

    function allCargosSelectEdits(type = 0){
        var sList = '';
        //////
        var Total = 0;
        var Profit = 0;
        ///////
        $('.checkbox-table').each(function () {
            if(this.checked == "1"){
                if(type == 1){
                    $('.project-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
                }
                sList += $(this).val() + ",";
                ////////
                Total        += parseFloat($('.data-total-'+$(this).val()).text());
                Profit       += parseFloat($('.data-profit-'+$(this).val()).text());
            }else if(type == 1){
                $('.project-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
            }
        });
        /////
        $('.summ-total').text('Total = '+Total.toFixed(2)+';');
        $('.summ-profit').text('Profit = '+Profit.toFixed(2)+';');
    }
</script>
