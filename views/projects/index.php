<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects');
//$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="options">
        <div class="choose-country d-sm-none d-md-none d-lg-none d-xl-none" id="country-click">
            <a href="#" class="choose-country-click">Choose Projects <span><i class="fas fa-caret-right"></i></span></a>
            <ul id="sm-countries">
                <li><a href="/projects/create"><i class="fa fa-plus" aria-hidden="true"></i> Add Project</a></li>
                <li><a href="/projects/index"><i class="fa fa-bars" aria-hidden="true"></i> All Projects</a></li>
                <!-- <li><a href="/projects/profit"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Total month profit</a></li> -->
            </ul>
        </div>

        <ul id="countries">
            <li><a href="/projects/create" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i> Add Project</a></li>
            <li><a href="/projects/index"><i class="fa fa-bars" aria-hidden="true"></i> All Projects</a></li>
            <!-- <li><a href="/projects/profit"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Total month profit</a></li> -->
        </ul>
    </div>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">

                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        All Projects *
                    </div>
                    <!-- <div class="sorting">
                        <div class="sorting-title">sorting</div>
                        <div class="picked-sorting">Projects date recelved <i class="fas fa-caret-down"></i> <a href="#"><img src="/web/img/cancel-icon.png" alt="#"></a></div>
                    </div> -->
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                    <div class="add-new">
                        <!-- <div class="ad-search">
                            <a href="#"><i class="fas fa-search"></i></a>
                        </div>
                        <div class="ad-button">
                            <a href="#"><i class="fas fa-plus"></i> Add</a>
                        </div>
                        <div class="ad-menu">
                            <a href="#"><i class="fas fa-bars"></i></a>
                        </div> -->
                    </div>
                </div>

                <div class="table-wrap">
                    <table class="table table-striped table-bordered table-resizable custom-tab">
                        <thead>
                                <tr>
                                    <th>Project ID</th>
                                    <th>Manager</th>
                                    <th>Client</th>
                                    <th>Project Created</th>
                                    <th>Project Closed</th>
                                    <th>Project Type</th>
                                    <th>Delivery type</th>
                                    <th>Project name</th>
                                    <th>Comments</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?= $all_table_rows ?>
                            </tbody>
                    </table>
                </div>

                <div class="detail-info-top-line">

                    <div class="detail-info-controls">
                        <a href="#" id="slide-left"><img src="/web/img/arr-left.png" alt="#"></a>
                        <a href="#" id="slide-right"><img src="/web/img/arr-right.png" alt="#"></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="/projects/edit/" class="projects-detail-info-url-edit"><img src="/web/img/pencil.png" alt="">Edit</a>
                            <!-- <a href="#"><img src="/web/img/duplicate-icon.png" alt="">Duplicate</a> -->
                            <a href="#" id="close-detail-info"><img src="/web/img/close-icon.png" alt=""></a>
                        </div>
                    </div>

                </div>

                <div class="detail-info-wrap">

                    <div class="detail-info">

                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project ID</div>
                                <div class="detail-info-list-column detail-info-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client</div>
                                <div class="detail-info-list-column detail-info-client"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project approved</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project name</div>
                                <div class="detail-info-list-column detail-info-name"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project Type</div>
                                <div class="detail-info-list-column detail-info-type"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Delivery type</div>
                                <div class="detail-info-list-column detail-info-delivery"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Comment</div>
                                <div class="detail-info-list-column detail-info-comment"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project created</div>
                                <div class="detail-info-list-column detail-info-date"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project closed</div>
                                <div class="detail-info-list-column detail-info-date_closed"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Total to pay</div>
                                <div class="detail-info-list-column detail-info-total"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Profit</div>
                                <div class="detail-info-list-column detail-info-profit"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing">
                    Showing <?=$projectsCount?> of <?=$projectsCount?>
                </div>
                <div class="col-6 summary">
                    <!-- <img src="/web/img/list-icon.png" alt="#"> <a href="#">Show Summary</a> -->
                </div>
            </div>
        </div>
    </section>

<script type="text/javascript">
    $(window).resize(function() {
        tableHeight();
    });

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        body_height = $('body').height();
        table = $('.table-wrap').height();
        summ_height = window_height - (body_height - table) - 15;
        $('.table-wrap').css({'max-height' : summ_height+'px'});
    }

    $('.mob-menu-Projects > a').addClass('active');

    /////////////////////////////////////////////////

    $(".table tbody .projects-row").on("click", function() {
        var cur_id = $(this).attr("data-project_id");
        var re, s;
        s = cur_id;
        re = /\D+/ig;
        index = s.replace(re, '');

        $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
        $('.detail-info-top-line').animate({
            opacity: 1
        }, 700);
        $('.detail-info-top-line').css({
            "display": "flex"
        });
        $(".detail-info-wrap").find('.detail-info').fadeIn(700).addClass("detail-visibe");
        $('.projects-detail-info-url-edit').attr("href", '/projects/edit/' + $(this).attr("data-project_id"));
        $.ajax({ 
            type: 'GET', 
            url: '/projects/view/' + $(this).attr("data-project_id"), 
            data: {}, 
            dataType: 'json',
            success: function (data) { 
                $('.detail-info-id').text(data.project_id);
                $('.detail-info-manager').text(data.manager_id);
                $('.detail-info-client').text(data.client_id);
                $('.detail-info-status').text(data.status);
                $('.detail-info-name').text(data.name);
                $('.detail-info-type').text(data.project_type);
                $('.detail-info-delivery').text(data.delivery_type);
                $('.detail-info-comment').text(data.comment);
                $('.detail-info-date').text(data.date_create);
                $('.detail-info-date_closed').text(data.date_close);
                $('.detail-info-total').text(data.total);
                $('.detail-info-profit').text(data.profit);
                console.log(data);
            }
        });
    });
</script>
