<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'View balance client');
?>

	
    <?=$menu?>

	<section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">View balance client</div>
            </div>

            <div class="add-client-form-wrap update-client-form">
                <form>

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label style="width: auto;">Client</label>
                                <div class="input-addclient-wrap">
                                    <span style="color: #23af8d;"><?= (!empty($model->client_id)) ? '88-'.$model->client_id : '' ?></span> - <?= (!empty($model->name)) ? $model->name : '' ?>
                                </div>
                            </div>
                            <div class="add-client-form-line">
                                <label>Manager <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager selectpicker" name="manager-input" onblur="this.style.color = '#000'" <?=(Yii::$app->user->identity->role < 9 && Yii::$app->user->identity->role != 2) ? 'disabled' : ''?> data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option <?= ($manager->manager_id == $model->manager_id) ? 'selected' : '' ?> value="<?=$manager->manager_id?>"><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="bottom-part-title">
                                Client Balance
                            </div>
                            <div class="add-client-form-line">
                                <label>Balance now $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00$" disabled  value="<?= (!empty($model->balance)) ? $model->balance.'$' : '0' ?>"> 
                                </div>
                            </div>
                            <div class="add-client-form-line">
                                <label>Money for all time $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00$" disabled  value="<?= (!empty($model->balance_total)) ? $model->balance_total.'$' : '0' ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                                <div class="add-new-item-title" style="font-size: 15px;margin-bottom: 10px;">Client Money Transfers</div>
                                <table class="table table-hover" style="width: 100% !important;border-right: 1px solid #dee2e6;">
                                  <thead>
                                    <tr>
                                        <th scope="col">Date</th>
                                        <th scope="col">Paid $</th>
                                        <th scope="col">Project</th>
                                        <?=(Yii::$app->user->identity->role >= 10) ? '<th scope="col">Balance after</th>' : ''; ?>
                                        <th scope="col">Comment</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php foreach ($transfers as $transfer) { ?>
                                        <tr>
                                          <td><?= date('d.m.Y',$transfer->date_payment) ?></td>
                                          <td style="<?= ($transfer->payment_type == 1) ? 'color: green;' : 'color: red;' ?>"><?= ($transfer->payment_type == 1) ? '+'.$transfer->cash_usd : '-'.$transfer->cash_usd ?></td>
                                          <td style="white-space: initial;overflow: auto;"><?= (!empty($transfer->project_id)) ? '<a target="_blank" href="/projects/edit/'.$transfer->project_id.'">PRJ-'.$transfer->project_id.'</a>' : '' ?></td>
                                          <?=(Yii::$app->user->identity->role >= 10) ? '<td style="white-space: initial;overflow: auto;">'. $transfer->balance .'</td>' : ''; ?>
                                          <td style="white-space: initial;overflow: auto;"><?= $transfer->comment ?></td>
                                        </tr>
                                    <?php } ?> 
                                  </tbody>
                                </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    
    <script type="text/javascript">
    	$('.mob-menu-Payments > a').addClass('active');
    </script>
