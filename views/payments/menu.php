<?php
    use app\models\CurrencyCash;
?>
<div class="options">
    <div class="choose-country d-sm-none d-md-none d-lg-none d-xl-none" id="country-click">
        <a href="#" class="choose-country-click">Choose Payments <span><i class="fas fa-caret-right"></i></span></a>
        <ul id="sm-countries">
            <li><a href="/payments/create"><i class="fa fa-plus" aria-hidden="true"></i> Add Payment</a></li>
            <li><a href="/payments/index"><i class="fa fa-bars" aria-hidden="true"></i> All Payments</a></li>
            <li><a href="/payments/approved"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Approved payments</a></li>
            <li><a href="/payments/noapproved"><i class="fa fa-thumbs-down" aria-hidden="true"></i> No Approved payments</a></li>
            <li><a href="/payments/balances"><i class="fas fa-user"></i> Balances</a></li>
            <li><a href="/payments/exchangeshistory"><i class="fas fa-history"></i> Exchanges History</a></li>
            <?php if(Yii::$app->user->identity->role >= 10){ ?>
                <li><a href="/payments/exchanges"><i class="fas fa-money-bill"></i> Exchanges</a></li>
                <li><a href="/payments/costs"><i class="fas fa-hand-holding-usd"></i> All costs</a></li>
                <li><a href="/payments/createcosts"><i class="fa fa-plus" aria-hidden="true"></i> Add cost</a></li>
            <?php } ?>
        </ul>
    </div>

    <ul id="countries">
        <li><a href="/payments/create" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i> Add Payment</a></li>
        <li><a href="/payments/index"><i class="fa fa-bars" aria-hidden="true"></i> All Payments</a></li>
        <li><a href="/payments/approved"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Approved payments</a></li>
        <li><a href="/payments/noapproved"><i class="fa fa-thumbs-down" aria-hidden="true"></i> No Approved payments</a></li>
        <li><a href="/payments/balances"><i class="fas fa-user"></i> Balances</a></li>
        <li><a href="/payments/exchangeshistory"><i class="fas fa-history"></i> Exchanges History</a></li>
        <?php if(Yii::$app->user->identity->role >= 10){ ?>
            <li><a href="/payments/exchanges"><i class="fas fa-money-bill"></i> Exchanges</a></li>
            <li><a href="/payments/costs"><i class="fas fa-hand-holding-usd"></i> All costs</a></li>
            <li><a href="/payments/createcosts" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i> Add cost</a></li>
        <?php } ?>
    </ul>
<div class="menu-pauments-currency">
    <?php $currencyAll = CurrencyCash::find()->all(); ?>
    <a><span>USD/UAH</span> - <?=$currencyAll[2]->currency?></a>
    <a><span>| USD/CNY</span> - <?=$currencyAll[0]->currency?></a>
</div>
</div>