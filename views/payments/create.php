<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Create Payment');
?>

<?=$menu?>

  <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">Add Payment</div>
            </div>

            <div class="add-client-form-wrap create-payment-form">
                <form>

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Manager <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager selectpicker" name="select-manager" onblur="this.style.color = '#000'" <?=(Yii::$app->user->identity->role < 9) ? 'disabled' : ''?> data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option value="<?=$manager->manager_id?>" <?=(Yii::$app->user->identity->role < 9 && $manager->username == Yii::$app->user->identity->username) ? 'selected' : ''?>><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Client Code <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-client  selectpicker" name="select-client" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($clients as $client) { ?>
                                          <option value="<?=$client->client_id?>">88-<?=$client->client_id?> - <?=$client->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Branch <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-branch" name="select-branch" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                          <option value="1">China</option>
                                          <option value="2">Ukraine</option>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Date payment </label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date" class="input-date-add" value="<?=date('d-M-Y',time())?>" autocomplete="off" <?=(Yii::$app->user->identity->role < 10) ? 'disabled' : ''?> >
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <!-- <div class="add-client-form-line">
                                <label>Payment status <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-status" name="select-status" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <option value="1">Executed</option>
                                        <option value="2">New</option>
                                    </select>
                                </div>
                            </div> -->

                            <div class="add-client-form-line">
                                <label>Payment type <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-type" name="select-type" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <option value="1">Income</option>
                                        <option value="2">Outcome</option>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line select-source-block" style="display: none;">
                                <label>Income source <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-source" name="select-source" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($sources as $source) { ?>
                                          <option value="<?=$source->source_id?>"><?=$source->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line select-out_source-block" style="display: none;">
                                <label>Outcome source <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-out_source source_group source_project" name="select-out-source" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <option value="1">Card</option>
                                        <option value="2">Cash</option>
                                        <option value="3">TT</option>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line select-project-block" style="display: none;">
                                <label>Project <span>*</span></label>
                                <div class="input-addclient-wrap select-input select-input-project">
                                    <select class="select-project selectpicker source_project" name="select-project" onblur="this.style.color = '#000'" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($projects as $project) { ?>
                                            <option value="<?=$project->project_id?>">PJ-<?=$project->project_id?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <hr>
                            <div class="add-client-form-line add-client-form-line-file">
                                <label>Invoice</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <input type="file" placeholder="" class="file" id="get-invoice-file">
                                    <textarea class="invoice-base64" style="display: none;"></textarea>
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                </div>
                            </div>

                            <div class="get-invoice-file load-file"><img src="" id='preview_invoice' alt="">
                                <span class="ava-name"></span>
                                <span class="ava-size"></span>
                                <span class="close-preview-invoice"><i class="fas fa-times"></i></span>
                            </div>
                            <hr>
                            <div class="add-client-form-line add-client-form-line-file">
                                <label>Bankbill</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <input type="file" placeholder="" class="file" id="get-other-file">
                                    <textarea class="other-base64" style="display: none;"></textarea>
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                </div>
                            </div>

                            <div class="get-other-file load-file"><img src="" id='preview_other' alt="">
                                <span class="ava-name"></span>
                                <span class="ava-size"></span>
                                <span class="close-preview-other"><i class="fas fa-times"></i></span>
                            </div>

                        </div>

                        <div class="col-lg-4 offset-lg-1 col-md-12"> 

                            <div class="add-client-form-line">
                                <label>Fact paid currency <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-currency" name="select-currency" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($currencies as $currency) { ?>
                                            <option value="<?=$currency->сurrency_id?>" <?= ($currency->сurrency_id == 4) ? 'selected' : '' ?> data-currency="<?=$currency->currency?>"><?=$currency->name?> - <?=$currency->currency?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="add-client-form-line">
                                <label>Fact paid <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-paid-fact" name="input-fact">
                                </div>
                            </div>

                          <div class="add-client-form-line">
                                <label>Paid USD <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-paid" disabled name="input-paid">
                                </div>
                            </div>

                            <div class="add-client-form-line add-client-form-line-checkbox" style="<?=(Yii::$app->user->identity->role < 9) ? 'display:none' : ''?>" >
                                <label>
                                    <input type="checkbox" name="status" class="input-status">
                                    <p>Approved</p>
                                    <span></span>
                                </label>
                            </div>

                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Payment comment</label>
                                <div class="input-addclient-wrap">
                                    <textarea name="name" class="input-comment"> </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="add-new-form-submit-buttons col-12">
                            <div class="add-new-form-submit-buttons-wrap col-4">
                                <div class="add-new-form-submit-buttons-wrap-width">
                                    <button  class="submit-form">Submit <img src="/web/img/preloader.gif" style="display: none;"></button>
                                    <a href="#" class="reset-form">Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        //RULES FORM
        $(".create-payment-form form").validate({
            rules: {
              "select-manager": {
                  required: true
              },
              "select-branch": {
                  required: true
              },
              // 'select-status': {
              //     required: true
              // },
              'select-type': {
                  required: true
              },
              'select-client': {
                  required: true
              },

              'select-currency': {
                  required: true
              },
              'input-fact': {
                  required: true
              },
              'input-paid': {
                  required: true
              },
            },
            messages: {
              "select-manager": {
                  required: "Please select manager",
              },
              "select-branch": {
                  required: "Please select branch",
              },
              // "select-status": {
              //     required: "Please select status",
              // },
              "select-type": {
                  required: "Please select type",
              },
              "select-client": {
                  required: "Please select client",
              },
              "select-out-source": {
                  required: "Please select source or out source",
              },
              "select-source": {
                  required: "Please select source or out source",
              },
              "select-project": {
                  required: 'Please select project',
              },
              "select-currency": {
                  required: "Please select currency",
              },
              "input-fact": {
                  required: "Please enter fact paid",
              },
              "input-paid": {
                  required: "Please enter paid USD",
              },
            },
            submitHandler: function(form) {
              submitForm();
            }
        });
    </script>

    <script type="text/javascript">
      $( ".select-type" ).change(function() {
      if($('.select-type option:selected').val() == 1){
        $('.select-source-block').show();
        $('.select-out_source-block').hide();
                $('.select-project-block').hide();
                $(".select-out_source option:selected").prop("selected", false);
                $(".select-project option:selected").prop("selected", false);
                ////validate
                $(".select-source").rules("add",{
                    required: true
                });
                $(".select-out_source").rules("remove");
                $(".select-project[name=select-project]").rules("remove");
      }
      else if($('.select-type option:selected').val() == 2){
        $('.select-source-block').hide();
        $('.select-out_source-block').show();
                $('.select-project-block').show();
                $(".select-source option:selected").prop("selected", false);
                ////validate
                $(".select-out_source").rules("add",{
                    required: true
                });
                $(".select-project[name=select-project]").rules("add",{
                    required: true
                });
                $(".select-source").rules("remove");
                $('.selectpicker').selectpicker('refresh');
      }
      // else{
      //   $('.select-source-block').hide();
      //   $('.select-out_source-block').hide();
      //           $('.select-project-block').hide();
      //           $(".select-out_source option:selected").prop("selected", false);
      //           $(".select-project option:selected").prop("selected", false);
      //           $(".select-source option:selected").prop("selected", false);
      //           ////validate
      //           $(".select-out_source").rules("remove");
      //           $(".select-project[name=select-project]").rules("remove");
      //           $(".select-source").rules("remove");
      // }
    });

        function submitForm(){
                //блок кнопки и прелоадер
                $('.submit-form').find('img').css('display','');
                $('.submit-form').attr('disabled', 'true');
                ///
                if ($(".input-status").is(":checked")) {
                    var input_status = 1;
                }
                else {
                    var input_status = 0;
                }
                var formData = new FormData();
                formData.append('manager',          $('.select-manager option:selected').val());
                formData.append('branch',           $('.select-branch option:selected').val());
                formData.append('payment_status',   $('.select-status option:selected').val());
                formData.append('payment_type',     $('.select-type option:selected').val());
                formData.append('client_id',        $('.select-client option:selected').val());
                formData.append('paid_currency_id', $('.select-currency option:selected').val());
                formData.append('source',           $('.select-source option:selected').val());
                formData.append('out_source',       $('.select-out_source option:selected').val());
                formData.append('project',          $('.select-project option:selected').val());

                formData.append('date_add',           $('.input-date-add').val());
                formData.append('paid_usd',           $('.input-paid').val());
                formData.append('status',             input_status);
                formData.append('comment',            $('.input-comment').val());
                formData.append('paid_fact',               $('.input-paid-fact').val());
                formData.append('code',               $('.input-code').val());
                formData.append('weight',             $('.input-weight').val());
                formData.append('volume',             $('.input-volume').val());
                formData.append('cartons',            $('.input-cartons').val());
                formData.append('volume_weight',      $('.input-volume-weight').val());
                formData.append('inside',             $('.input-inside').val());
                formData.append('local_tracking',     $('.input-local-tracking').val());
                formData.append('local_price',        $('.input-local-price').val());
                formData.append('description',        $('.input-description').val());
                formData.append('delivery_tracking',  $('.input-delivery-tracking').val());
                // Attach file
                formData.append('invoice', document.querySelector('#get-invoice-file').files[0]);
                formData.append('bankbill', document.querySelector('#get-other-file').files[0]);
                /////csrf
                formData.append('<?= \yii::$app->request->csrfParam?>', '<?= \yii::$app->request->csrfToken ?>');

                $.ajax({ 
                    url: '/payments/create',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data) { 
                        if (data == 1) {
                            window.location.replace("/payments/index");
                        }else{
                          alert(data);
                          $('.submit-form').find('img').css('display','none');
                          $('.submit-form').removeAttr("disabled");
                        }
                    }
                });
        }

        $('.mob-menu-Payments > a').addClass('active');

        $('.input-paid-fact').on('input', function(){
            if($('.select-currency').find('option:selected').attr('data-currency') > 0){
                calc = $('.input-paid-fact').val()/$('.select-currency').find('option:selected').attr('data-currency');
                $('.input-paid').val(calc.toFixed(2));
            }else{
                calc = $('.input-paid-fact').val();
                $('.input-paid').val(calc);
            }
        });

        $(".select-currency").change(function(){ 
            if($('.select-currency').find('option:selected').attr('data-currency') > 0){
                calc = $('.input-paid-fact').val()/$('.select-currency').find('option:selected').attr('data-currency');
                $('.input-paid').val(calc.toFixed(2));
            }else{
                calc = $('.input-paid-fact').val();
                $('.input-paid').val(calc);
            }
        });

        $( ".select-client" ).change(function() {
            select_client();
        });

        function select_client(){
            $.ajax({ 
                url: '/cargo/clientsearch/'+$('.select-client option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data != 0){
                        $(".select-manager select").val(data);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
            $.ajax({ 
                url: '/projects/projectselect/'+$('.select-client option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data != ''){
                        $('.select-project').remove();
                        $('.select-input-project').html(data);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
        }

        var invoice = new FileReader();
        var other = new FileReader();

        $('.close-preview-invoice').on("click", function() {
            $('#get-invoice-file').val('');
            $('.get-invoice-file .ava-name').text('');
            $('.get-invoice-file .ava-size').text('');
            $("#preview_invoice").attr("src", "");
            $('.get-invoice-file').css('height', '0');
            $('.close-preview-invoice').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-invoice-file").change(function(event) {

            $('.close-preview-invoice').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-invoice-file .ava-name').text(filename[fileLen]);

            invoice.onload = function() {
                $('.get-invoice-file').css('height', 'auto');
                var preview = $("#preview_invoice");
                preview.attr("src", invoice.result);
                $('.invoice-base64').val(invoice.result);
            }

            invoice.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-invoice-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });

        $('.close-preview-other').on("click", function() {
            $('#get-other-file').val('');
            $('.get-other-file .ava-name').text('');
            $('.get-other-file .ava-size').text('');
            $("#preview_other").attr("src", "");
            $('.get-other-file').css('height', '0');
            $('.close-preview-other').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-other-file").change(function(event) {

            $('.close-preview-other').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-other-file .ava-name').text(filename[fileLen]);

            other.onload = function() {
                $('.get-other-file').css('height', 'auto');
                var preview = $("#preview_other");
                preview.attr("src", other.result);
                $('.other-base64').val(other.result);
            }

            other.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-other-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });
        
        // $(document).keyup(function(e) {
        //     if(e.which == 13){ //enter press
        //         $('.submit-form').trigger('click');
        //     }
        //     if (e.key === "Escape") { 
        //         $('.reset-form').trigger('click');
        //     }
        // });
        $('.reset-form').click(function(){
            location.reload();
        });
    </script>
