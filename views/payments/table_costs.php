<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Payments;
use app\models\PaymentsCosts;
use app\models\CustomTable;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'All Costs');
//$this->params['breadcrumbs'][] = $this->title;
?>

    <?=$menu?>

    <style type="text/css">
        .table-bordered tbody{
          display:block;
          overflow:auto;
          height:200px;
          width:100%;
          overflow-x: hidden;
        }
        .table-bordered thead tr{
          display:block;
        }
    </style>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">

                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        All Costs * <?= PaymentsCosts::SearchShowBtn() ?>
                    </div>
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                </div>

                <div class="table-wrap payment-wrap">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'rowOptions'=>function($model){
                            return ['class' => ($model->status == 1)?'payment-row contextmenu-row payments-row-approve':'payment-row contextmenu-row', 'url-edit' => '/payments/editcosts/'.$model->payment_id, 'url-delete' => '/payments/delete/'.$model->payment_id];
                        },
                        'columns' => [
                            [
                                'attribute' => 'check',
                                'label' => '<input type="checkbox" class="checkbox-table-all" value="">',
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                "format" => 'raw',
                                'value' => function ($data) {
                                    return '<input type="checkbox" class="checkbox-table" value="'.$data->payment_id.'"> <i class="far fa-eye eye-click-view-row" title="View" data-key="'.$data->payment_id.'"></i><a href="/payments/editcosts/'.$data->payment_id.'" class="table-edit-btn"><i class="fas fa-pencil-alt"></i></a>'; 
                                },
                            ],
                            [
                                'attribute' => 'payment_id',
                                'label' => PaymentsCosts::paymentDropMenu(PaymentsCosts::attributeLabels()['payment_id'], 'payment_id', 'text', 'Write payment id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'value' => function ($data) {
                                    return (!empty($data->payment_id)) ? 'PAY-'.$data->payment_id : '';
                                },
                            ],
                            [
                                'attribute' => 'manager_id',
                                'label' => PaymentsCosts::paymentDropMenu(PaymentsCosts::attributeLabels()['manager_id'], 'manager_id', 'select', 'Select Manager'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::widthTable(140)],
                                'contentOptions' => ['style' => CustomTable::widthTable(140)],
                                'value' => function ($data) {
                                    return $data->manager->username ?? '';
                                },
                            ],
                            [
                                'attribute' => 'date_payment',
                                'label' => PaymentsCosts::paymentDropMenu(PaymentsCosts::attributeLabels()['date_payment'], 'date_payment', 'date', 'Select date'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::widthTable(115)],
                                'contentOptions' => ['style' => CustomTable::widthTable(115)],
                                'value' => function ($data) {
                                    return (!empty($data->date_payment)) ? date('d.m.Y',$data->date_payment) : ''; 
                                },
                            ],
                            [
                                'attribute' => 'cash_usd',
                                'label' => PaymentsCosts::paymentDropMenu(PaymentsCosts::attributeLabels()['cash_usd'], 'cash_usd', 'text', 'Write USD'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'value' => function ($data) {
                                    return $data->cash_usd ?? '';
                                },
                            ],
                            [
                                'attribute' => 'cash_fact',
                                'label' => PaymentsCosts::paymentDropMenu(PaymentsCosts::attributeLabels()['cash_fact'], 'cash_fact', 'text', 'Write cash fact'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'value' => function ($data) {
                                    return $data->cash_fact ?? '';
                                },
                            ],
                            [
                                'attribute' => 'cash_fact_currency',
                                'label' => PaymentsCosts::paymentDropMenu(PaymentsCosts::attributeLabels()['cash_fact_currency'], 'cash_fact_currency', 'select', 'Write fact currency'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::widthTable(140)],
                                'contentOptions' => ['style' => CustomTable::widthTable(140)],
                                'value' => function ($data) {
                                    return $data->currency->name ?? '';
                                },
                            ],
                            [
                                'attribute' => 'comment',
                                'label' => PaymentsCosts::paymentDropMenu(PaymentsCosts::attributeLabels()['comment'], 'comment', 'text', 'Write comment'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::widthTable(600)],
                                'contentOptions' => ['style' => CustomTable::widthTable(600)],
                                'value' => function ($data) {
                                    return $data->comment ?? '';
                                },
                            ],
                        ],
                    ]); ?>
                </div>

                <div class="detail-info-top-line">
                    <div class="detail-info-controls">
                        <a href="#" id="slide-position-left"><i class="fas fa-chevron-left"></i></a>
                        <a href="#" id="slide-position-right"><i class="fas fa-chevron-right"></i></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="" class="payment-detail-info-url-edit"><i class="fas fa-pencil-alt"></i>Edit</a>
                            <a data-id="" class="delete-post-url" style="display: <?=(Yii::$app->user->identity->role >= 9)?';':'none;'?>"><i class="far fa-trash-alt"></i>Delete</a>
                            <a href="#" id="close-detail-info"><i class="fas fa-times"></i></a>
                        </div>
                    </div>
                </div>

                <div class="detail-info-wrap">

                    <div class="detail-info">

                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Payment ID</div>
                                <div class="detail-info-list-column detail-info-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Date payment</div>
                                <div class="detail-info-list-column detail-info-date"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Fact paid currency</div>
                                <div class="detail-info-list-column detail-info-currency"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Fact paid</div>
                                <div class="detail-info-list-column detail-info-cash_fuct"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Paid USD</div>
                                <div class="detail-info-list-column detail-info-cash_usd"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Approved</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Payment comment</div>
                                <div class="detail-info-list-column detail-info-comment"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing">
                </div>
                <div class="col-6 summary">
                </div>
            </div>
        </div>
    </section>
    <nav id="context-menu" class="context-menu">
        <ul class="context-menu__items">
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="View"><i class="fa fa-eye"></i> View</a>
          </li>
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="Edit"><i class="fa fa-edit"></i> Edit</a>
          </li>
          <?php if(Yii::$app->user->identity->role >= 9){ ?>
              <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="Delete"><i class="fa fa-times"></i> Delete</a>
              </li>
          <?php } ?>
        </ul>
    </nav>

<script src="/web/js/contecst_menu.js"></script>
<script type="text/javascript">

    setInterval(function() {
        tableHeight();
    }, 500);

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        summ_height = window_height - ($('.all-cargos-title').height() + $('header').height() + $('.options').height() + 50);
        if(summ_height < 500){
            summ_height = 500;
        }
        $('.table-wrap').css({'max-height' : summ_height+'px'});
        $('.table-bordered').css({'max-height' : summ_height+'px'});
        $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
    }

    $('.mob-menu-Payments > a').addClass('active');

    $('.summ_bottom').html($('.summary').html());
    $('.summary').html('');


    $(document).on('keyup', '.dropdown-search-input', function (event) {
        if(event.keyCode==13) {
            runSearch($(this).attr("data-type"),$(this).val());
        }
    });

    $(document).on('click', '.search-menu-click', function () {
        runSearch($(this).attr("data-type"),$(this).attr("data-id"));
    });

    $(document).on('change', '#form-choose-date', function () {
        runSearch($(this).attr("data-type"),$(this).val());
    });

    function runSearch(type, value){
        $.ajax({ 
            type: 'POST', 
            url: '/payments/search?'+type+'='+value+'&sort=<?=Yii::$app->request->get('sort')?>', 
            success: function (data) { 
                $('.all-cargos-content').html(data);
            }
        });
    };

    $(".delete-post-url").click(function() {
        var delete_text = confirm('Do you really want to delete payment?');
        if(delete_text) window.location.replace('/payments/delete/'+$(this).attr("data-id"));
    });

    $('.custom-table-btn').click(function() {
        var sListType = '';
        $('.custom-table-checked input').each(function () {
            if(this.checked == "1"){
                sListType += $(this).val() + ",";
            }
        });
        if(sListType != ''){
            $.ajax({ 
                type: 'GET', 
                url: '/payments/customtable?page=payment&type='+sListType, 
                success: function (data) { 
                    location.reload();
                }
            });
        }
    });

    $(document).on('click', '.checkbox-table-all', function () {
        $('.checkbox-table').not(this).prop('checked', this.checked);
        allCargosSelectEdits(1);
    });

    $(document).on('click', '.checkbox-table', function () {
        if(this.checked == "1"){
            $('.payment-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
        }else{
            $('.payment-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
        }
        allCargosSelectEdits();
    });

    function allCargosSelectEdits(type = 0){
        var sList = '';
        ///////
        $('.checkbox-table').each(function () {
            if(this.checked == "1"){
                if(type == 1){
                    $('.payment-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
                }
                sList += $(this).val() + ",";
            }else if(type == 1){
                $('.payment-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
            }
        });
    }
</script>
