<?php
  $this->title = $title ?? 'Exchanges';
?>

<?=$menu?>

<section class="all-cargos-content">
    <div class="container-fluid">
        <div class="row no-fw-row">
            <div class="col-sm-6">
                <div class="all-cargos-title">
                    <?=$title ?? 'Exchanges'?> *
                </div>
            </div>
            <div class="col-sm-6 all-cargos-buttons">
                <div class="add-new">
                </div>
            </div>
            <div class="table-wrap">
                <table class="table" style="width: 100% !important;overflow-x: scroll;">
                    <thead>
                      <tr>
                        <th scope="col">Code</th>
                        <th scope="col">Currency</th>
                        <th scope="col">Date edit</th>
                        <th scope="col">Manager edit</th>
                        <th scope="col"></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($exchanges as $exchange) { ?>
                        <tr>
                          <th scope="row"><img src="<?=$exchange->flag?>"><?=$exchange->name?></th>
                          <td>
                              <input type="text" data-id="<?=$exchange->сurrency_id?>" value="<?=$exchange->currency?>" style="display: none;">
                              <span class="exchange-currency" data-id="<?=$exchange->сurrency_id?>"><?=$exchange->currency?></span>
                          </td>
                          <td><?=date('r',$exchange->date ?? '')?></td>
                          <td><?=$exchange->manager->username ?? ''?></td>
                          <td>
                              <span class="edit-exchange" data-id="<?=$exchange->сurrency_id?>"><i class="fas fa-pencil-alt"></i></span>
                              <span class="save-exchange" data-id="<?=$exchange->сurrency_id?>" style="display: none;"><i class="fas fa-check"></i></span>
                              <span class="cancel-exchange" data-id="<?=$exchange->сurrency_id?>" style="display: none;"><i class="fas fa-ban"></i></span>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
  $('.edit-exchange').click(function(){
    if($('input[data-id="'+$(this).attr('data-id')+'"]').css('display') == 'none'){
      $('input[data-id="'+$(this).attr('data-id')+'"]').css('display','block');
      ////////////CSS//////////
      $('.exchange-currency[data-id="'+$(this).attr('data-id')+'"]').css('display','none');
      $('.edit-exchange[data-id="'+$(this).attr('data-id')+'"]').css('display','none');
      $('.cancel-exchange[data-id="'+$(this).attr('data-id')+'"]').css('display','block');
      $('.save-exchange[data-id="'+$(this).attr('data-id')+'"]').css('display','block');
    }
  });

  $('.save-exchange').click(function(){
    $.ajax({ 
        type: 'POST', 
        url: '/payments/exchanges', 
        data: {
          id:    $(this).attr("data-id"),
          money: $('input[data-id="'+$(this).attr('data-id')+'"]').val(),
        }, 
        success: function (data) { 
          window.location.replace('/payments/exchanges');
        }
    });
  });

  $('.cancel-exchange').click(function(){
    $('input[data-id="'+$(this).attr('data-id')+'"]').css('display','none');
    ////////////CSS//////////
    $('.exchange-currency[data-id="'+$(this).attr('data-id')+'"]').css('display','block');
    $('.edit-exchange[data-id="'+$(this).attr('data-id')+'"]').css('display','block');
    $('.cancel-exchange[data-id="'+$(this).attr('data-id')+'"]').css('display','none');
    $('.save-exchange[data-id="'+$(this).attr('data-id')+'"]').css('display','none');
  });

  $('.mob-menu-Payments > a').addClass('active');
</script>

