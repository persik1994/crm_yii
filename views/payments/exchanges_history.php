<?php
  use app\models\CurrencyHistory;

  $this->title = $title ?? 'Exchanges history';
?>

<?=$menu?>

<section class="all-cargos-content">
    <div class="container-fluid">
        <div class="row no-fw-row">
            <div class="col-sm-6">
                <div class="all-cargos-title">
                    <?=$title ?? 'Exchanges history'?> *
                </div>
            </div>
            <div class="col-sm-6 all-cargos-buttons">
                <div class="add-new">
                </div>
            </div>
            <div class="table-wrap" style="max-height: none;">
                <table class="table" style="width: 100% !important;overflow-x: scroll;">
                    <thead>
                      <tr>
                        <th scope="col">Date</th>
                        <th scope="col">UAH</th>
                        <th scope="col">CNY</th>
                        <th scope="col">EUR</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $now_date = date('Y-m-d 23:59', strtotime(date('Y-m-d',time()).''));
                        $period_days = array_reverse(iterator_to_array(new DatePeriod(
                             new DateTime('2018-12-28'),
                             new DateInterval('P1D'),
                             new DateTime($now_date)
                        ))); 
                        foreach ($period_days as $day) {
                          echo "<tr>";
                          echo '<td>'.$day->format('d.m.Y').'</td>';

                          $exchangeUAH = CurrencyHistory::find()->where('date <= '.strtotime($day->format('Y-m-d 23:59')).' AND currency_id = 3')->orderBy(['date' => SORT_DESC])->one();
                          if(isset($exchangeUAH)){
                            echo '<td>'.$exchangeUAH->currency.'</td>';
                          }else{
                            echo '<td></td>';
                          }

                          $exchangeCNY = CurrencyHistory::find()->where('date <= '.strtotime($day->format('Y-m-d 23:59')).' AND currency_id = 1')->orderBy(['date' => SORT_DESC])->one();
                          if(isset($exchangeCNY)){
                            echo '<td>'.$exchangeCNY->currency.'</td>';
                          }else{
                            echo '<td></td>';
                          }

                          $exchangeEUR = CurrencyHistory::find()->where('date <= '.strtotime($day->format('Y-m-d 23:59')).' AND currency_id = 2')->orderBy(['date' => SORT_DESC])->one();
                          if(isset($exchangeEUR)){
                            echo '<td>'.$exchangeEUR->currency.'</td>';
                          }else{
                            echo '<td></td>';
                          }
                          echo '</tr>';
                        }
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
  $('.mob-menu-Payments > a').addClass('active');
</script>

