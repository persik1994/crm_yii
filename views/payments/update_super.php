<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Edit Payment');
?>

<?=$menu?>

	<section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">Edit Payment</div>
            </div>

            <div class="add-client-form-wrap update-payment-form">
                <form>

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Manager <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager selectpicker" name="select-manager" onblur="this.style.color = '#000'" <?=(Yii::$app->user->identity->role < 9 || $model->status == 1) ? 'disabled' : ''?> data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option <?= ($manager->manager_id == $model->manager_id) ? 'selected' : '' ?> value="<?=$manager->manager_id?>"><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Client Code</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-client  selectpicker" name="select-client" data-live-search="true" <?=($model->status == 1) ? 'disabled' : ''?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($clients as $client) { ?>
                                            <option <?= ($client->client_id == $model->client_id) ? 'selected' : '' ?> value="<?=$client->client_id?>">88-<?=$client->client_id?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Branch <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-branch" name="select-branch" onblur="this.style.color = '#000'" <?=($model->status == 1) ? 'disabled' : ''?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        	<option <?= ($model->branch_id == 1) ? 'selected' : '' ?> value="1">China</option>
                                        	<option <?= ($model->branch_id == 2) ? 'selected' : '' ?> value="2">Ukraine</option>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Date payment </label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date" class="input-date-add" value="<?= (!empty($model->date_payment)) ? date('d-M-Y',$model->date_payment) : '' ?>" autocomplete="off" <?=($model->status == 1) ? 'disabled' : ''?>>
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <!-- <div class="add-client-form-line">
                                <label>Payment status <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-status" name="select-status" onblur="this.style.color = '#000'" <?=($model->status == 1) ? 'disabled' : ''?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <option <?= ($model->payment_status == 1) ? 'selected' : '' ?> value="1">Executed</option>
                                        <option <?= ($model->payment_status == 2) ? 'selected' : '' ?> value="2">New</option>
                                    </select>
                                </div>
                            </div> -->

                            <div class="add-client-form-line">
                                <label>Payment type <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-type" name="select-type" onblur="this.style.color = '#000'" <?=($model->status == 1) ? 'disabled' : ''?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <option <?= ($model->payment_type == 1) ? 'selected' : '' ?> value="1">Income</option>
                                        <option <?= ($model->payment_type == 2) ? 'selected' : '' ?> value="2">Outcome</option>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line select-source-block" style="display: none;">
                                <label>Income source <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-source" name="select-source" onblur="this.style.color = '#000'" <?=($model->status == 1) ? 'disabled' : ''?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($sources as $source) { ?>
                                        	<option <?= ($source->source_id == $model->source_id) ? 'selected' : '' ?> value="<?=$source->source_id?>"><?=$source->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line select-out_source-block" style="display: none;">
                                <label>Outcome source <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-out_source" name="select-out-source" onblur="this.style.color = '#000'" <?=($model->status == 1) ? 'disabled' : ''?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <option <?= ($model->out_source_id == 1) ? 'selected' : '' ?> value="1">Card</option>
                                        <option <?= ($model->out_source_id == 2) ? 'selected' : '' ?> value="2">Cash</option>
                                        <option <?= ($model->out_source_id == 3) ? 'selected' : '' ?> value="3">TT</option>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line select-project-block" style="display: none;">
                                <label>Project <span>*</span></label>
                                <div class="input-addclient-wrap select-input select-input-project">
                                    <select class="select-project selectpicker" name="select-project" onblur="this.style.color = '#000'" data-live-search="true" <?=($model->status == 1) ? 'disabled' : ''?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($projects as $project) { ?>
                                            <option <?= ($project->project_id == $model->project_id) ? 'selected' : '' ?> value="<?=$project->project_id?>">PJ-<?=$project->project_id?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <hr>
                            <div class="add-client-form-line add-client-form-line-file">
                                <label>Invoice</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <input type="file" placeholder="" class="file" id="get-invoice-file" <?=($model->status == 1) ? 'disabled' : ''?>>
                                    <textarea class="invoice-base64" style="display: none;"></textarea>
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                </div>
                            </div>
                            <?php if(empty($model->invoice)){ ?>
                                <div class="get-invoice-file load-file"><img src="" id='preview_invoice' alt="">
                                    <span class="ava-name"></span>
                                    <span class="ava-size"></span>
                                    <span class="close-preview-invoice"><i class="fas fa-times"></i></span>
                                </div>
                            <?php } else { ?>
                                <div class="get-invoice-file load-file" style="height: auto;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhE..." id="preview_invoice" alt=""><i class="far fa-file"></i>
                                    <span class="ava-name"><a href="<?=$model->invoice?>" target="_blank"><?=str_replace('/uploads/files/', '', $model->invoice)?></a></span>
                                    <span class="ava-size"></span>
                                    <span class="close-preview-invoice" style="display: inline;"><a href="/payments/filedelete?type=invoice&cargo=<?=$model->payment_id?>"><i class="fas fa-times"></i></a></span>
                                </div>
                            <?php } ?>
                            <hr>
                            <div class="add-client-form-line add-client-form-line-file">
                                <label>Bankbill</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <input type="file" placeholder="" class="file" id="get-other-file" <?=($model->status == 1) ? 'disabled' : ''?>>
                                    <textarea class="other-base64" style="display: none;"></textarea>
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                </div>
                            </div>
                            <?php if(empty($model->bankbill)){ ?>
                                <div class="get-other-file load-file"><img src="" id='preview_other' alt="">
                                    <span class="ava-name"></span>
                                    <span class="ava-size"></span>
                                    <span class="close-preview-other"><i class="fas fa-times"></i></span>
                                </div>
                            <?php } else { ?>
                                <div class="get-other-file load-file" style="height: auto;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhE..." id="preview_other" alt=""><i class="far fa-file"></i>
                                    <span class="ava-name"><a href="<?=$model->bankbill?>" target="_blank"><?=str_replace('/uploads/files/', '', $model->bankbill)?></a></span>
                                    <span class="ava-size"></span>
                                    <span class="close-preview-other" style="display: inline;"><a href="/payments/filedelete?type=bankbill&payment=<?=$model->payment_id?>"><i class="fas fa-times"></i></a></span>
                                </div>
                            <?php } ?>

                        </div>

                        <div class="col-lg-4 offset-lg-1 col-md-12"> 

                            <div class="add-client-form-line">
                                <label>Fact paid currency <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-currency" name="select-currency" onblur="this.style.color = '#000'" <?=($model->status == 1) ? 'disabled' : ''?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($currencies as $currency) { ?>
                                            <option <?= ($currency->сurrency_id == $model->cash_fact_currency) ? 'selected' : '' ?> value="<?=$currency->сurrency_id?>" data-currency="<?=$currency->currency?>"><?=$currency->name?> - <?=$currency->currency?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Fact paid <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-paid-fact" value="<?= (!empty($model->cash_fact)) ? $model->cash_fact : '' ?>" <?=($model->status == 1) ? 'disabled' : ''?> name="input-fact">
                                </div>
                            </div>

                        	<div class="add-client-form-line">
                                <label>Paid USD <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-paid" value="<?= (!empty($model->cash_usd)) ? $model->cash_usd : '' ?>" disabled name="input-paid">
                                </div>
                            </div>

                            <div class="add-client-form-line add-client-form-line-checkbox">
                                <label>
                                    <input type="checkbox" name="status" class="input-status" <?= ($model->status == 1) ? 'checked' : '' ?> > 
                                    <p>Approved</p>
                                    <span></span>
                                </label>
                            </div>

                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Payment comment</label>
                                <div class="input-addclient-wrap">
                                    <textarea name="name" class="input-comment" <?=($model->status == 1) ? 'disabled' : ''?>><?= (!empty($model->comment)) ? $model->comment : '' ?></textarea>
                                </div>
                            </div>
                            
                        </div>
                        <div class="add-new-form-submit-buttons col-12">
                            <div class="add-new-form-submit-buttons-wrap col-4">
                                <div class="add-new-form-submit-buttons-wrap-width">
                                    <button  class="submit-form">Submit <img src="/web/img/preloader.gif" style="display: none;"></button>
                                    <a href="#" class="reset-form">Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="col-lg-6 col-md-12">
                    <div class="add-client-title">About Client</div>
                    <div class="add-new-section row no-fw-row">
                        <div class="col-lg-12 col-12">
                            <div class="bottom-part-title">
                                Client Balance
                            </div>
                            <div class="add-client-form-line">
                                <label>Balance now $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00$" disabled  value="<?= (!empty($balance)) ? round($balance).'$' : '' ?>"> 
                                </div>
                            </div>
                            <div class="add-client-form-line">
                                <label>Money for all time $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00$" disabled  value="<?= (!empty($balance_total)) ? round($balance_total).'$' : '' ?>">
                                </div>
                            </div>
                            <div class="add-new-section row no-fw-row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="add-new-item-title" style="font-size: 15px;margin-bottom: 10px;">Client Money Transfers</div>
                                    <table class="table table-hover" style="width: 100% !important;border-right: 1px solid #dee2e6;">
                                      <thead>
                                        <tr>
                                            <th scope="col">Date</th>
                                            <th scope="col">Paid $</th>
                                            <th scope="col">Project</th>
                                            <?=(Yii::$app->user->identity->role >= 10) ? '<th scope="col">Balance after</th>' : ''; ?>
                                            <th scope="col">Comment</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php foreach ($transfers as $transfer) { ?>
                                            <tr>
                                                <td><?= date('d.m.Y',$transfer->date_payment) ?></td>
                                                <td style="<?= ($transfer->payment_type == 1) ? 'color: green;' : 'color: red;' ?>"><?= ($transfer->payment_type == 1) ? '+'.$transfer->cash_usd : '-'.$transfer->cash_usd ?></td>
                                                <td style="white-space: initial;overflow: auto;"><?= (!empty($transfer->project_id)) ? '<a target="_blank" href="/projects/edit/'.$transfer->project_id.'">PRJ-'.$transfer->project_id.'</a>' : '' ?></td>
                                                <?=(Yii::$app->user->identity->role >= 10) ? '<td style="white-space: initial;overflow: auto;">'. $transfer->balance .'</td>' : ''; ?>
                                                <td style="white-space: initial;overflow: auto;"><?= $transfer->comment ?></td>
                                                
                                            </tr>
                                        <?php } ?> 
                                      </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="add-client-title">About Project</div>
                    <div class="add-new-section row no-fw-row">
                        <div class="col-lg-12 col-12">
                            <div class="add-new-item add-new-item-mw" style="width: 100%;">
                                <div class="add-new-item-title">Project Money Movement</div>
                                <div class="add-new-item-top-line">
                                    <span>Data transaction</span>
                                    <span>Amount $</span>
                                    <span>Description</span>
                                </div>

                                <div class="add-new-item-button-inputs-temp">
                                    <?php $withdraws = (!isset($withdraws)) ? array() : $withdraws;
                                    foreach ($withdraws as $withdraw) { ?>
                                        <?= 
                                            '<div class="add-new-item-button-inputs">
                                                <div class="add-client-form-line">
                                                    <div class="input-addclient-wrap project-cargoes-val">
                                                        '.date('d.m.Y',$withdraw->date_payment).'
                                                    </div>
                                                </div>

                                                <div class="add-client-form-line">
                                                    <div class="input-addclient-wrap project-cargoes-val">
                                                        -'.round($withdraw->cash_usd).'
                                                    </div>
                                                </div>

                                                <div class="add-client-form-line">
                                                    <div class="input-addclient-wrap project-cargoes-val">
                                                        '.$withdraw->comment.'
                                                    </div>
                                                </div>
                                            </div>' 
                                        ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="bottom-part-title">
                            Project Price <a class="show-project-desc">Show project informations</a>
                        </div>

                        <div class="col-lg-12 col-md-12 project-info-all" style="display:none;">
                            <div class="add-client-form-line">
                                <label>Total to pay USD <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" disabled value="<?= (!empty($model_Project->total)) ? $model_Project->total : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Profit USD <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" disabled value="<?= (!empty($model_Project->profit)) ? $model_Project->profit : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line add-client-form-line-checkbox">
                                <label>
                                    <input type="checkbox" name="status" disabled <?= ($model_Project->status == 1) ? 'checked' : '' ?>> 
                                    <p>Project approved</p>
                                    <span></span>
                                </label>
                            </div>

                            <div class="add-client-form-line">
                                <label>Payed $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" disabled value="<?= (!empty($model_Project->payed)) ? $model_Project->payed : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Must pay $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" disabled value="<?= (!empty($model_Project->must_pay)) ? $model_Project->must_pay : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Chargers $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" disabled value="<?= (!empty($model_Project->chargers)) ? $model_Project->chargers : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Plan bonuse $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" disabled value="<?= (!empty($model_Project->plan_bonuse)) ? $model_Project->plan_bonuse : '' ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 project-info-description" style="display:none;">
                            <div class="bottom-part-title">
                                Project Description
                            </div>
                            <div class="add-client-form-line">
                                <label>Manager <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select disabled name="manager" onblur="this.style.color = '#000'" <?=(Yii::$app->user->identity->role < 9) ? 'disabled' : ''?> data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option <?= ($manager->manager_id == $model_Project->manager_id) ? 'selected' : '' ?> value="<?=$manager->manager_id?>"><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project name</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" disabled value="<?= (!empty($model_Project->name)) ? $model_Project->name : '' ?>"> 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project number</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" disabled value="<?= (!empty($model_Project->project_id)) ? 'PJ-'.$model_Project->project_id : '' ?>"> 
                                    <input type="hidden" disabled value="<?= (!empty($model_Project->project_id)) ? $model_Project->project_id : '' ?>"> 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project created</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date" disabled value="<?= (!empty($model_Project->date_create)) ? date('d-M-Y',$model_Project->date_create) : '' ?>" autocomplete="off">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project closed</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date-1" disabled value="<?= (!empty($model_Project->date_close)) ? date('d-M-Y',$model_Project->date_close) : '' ?>" autocomplete="off">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project Type <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select disabled name="manager" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php for ($type_count=1; $type_count <= 4; $type_count++){ ?>
                                            <option <?= ($type_count == $model_Project->project_type) ? 'selected' : '' ?> value="<?=$type_count?>"><?=$model_Project->type($type_count)?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line div-buying" style="display: none;">
                                <label>Buying list</label>
                                <div class="input-addclient-wrap select-input">
                                    <select disabled name="manager" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($buyings as $buying) { ?>
                                            <option value="<?=$buying->buying_id?>" <?= ($buying->buying_id == $model_Project->buying_id) ? 'selected' : '' ?>>BL-<?=$buying->buying_id?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <!-- <div class="add-client-form-line div-delivery_type" style="display: none;">
                                <label>Delivery type <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select disabled name="manager" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($delivery_types as $delivery_type) { ?>
                                            <option <?= ($delivery_type->type_id == $model_Project->delivery_type) ? 'selected' : '' ?> value="<?=$delivery_type->type_id?>"><?=$delivery_type->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> -->

                            <div class="add-client-form-line">
                                <label>Client <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select disabled name="manager" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($clients as $client) { ?>
                                            <option <?= ($client->client_id == $model_Project->client_id) ? 'selected' : '' ?> value="<?=$client->client_id?>">88-<?=$client->client_id?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Comment</label>
                                <div class="input-addclient-wrap">
                                    <textarea name="name" disabled><?= (!empty($model_Project->comment)) ? $model_Project->comment : '' ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-12 project-info-insurance" style="display:none;">
                            <div class="bottom-part-title">
                                Project Insurance
                            </div>
                            <div class="add-client-form-line">
                                <label>Cargo cost $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" disabled value="<?= (!empty($model_Project->cargo_cost)) ? $model_Project->cargo_cost : '' ?>"> 
                                </div>
                            </div>
                            <div class="add-client-form-line">
                                <label>Insurance %</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" disabled value="<?= (!empty($model_Project->insurance)) ? $model_Project->insurance : '' ?>">
                                </div>
                            </div>
                            <div class="add-client-form-line">
                                <label>Insurance sum $</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" disabled value="<?= (!empty($model_Project->insurance_sum)) ? $model_Project->insurance_sum : '' ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        //RULES FORM
        $(".update-payment-form form").validate({
            rules: {
              "select-manager": {
                  required: true
              },
              "select-branch": {
                  required: true
              },
              // 'select-status': {
              //     required: true
              // },
              'select-type': {
                  required: true
              },
              'select-client': {
                  required: true
              },

              'select-currency': {
                  required: true
              },
              'input-fact': {
                  required: true
              },
              'input-paid': {
                  required: true
              },
            },
            messages: {
              "select-manager": {
                  required: "Please select manager",
              },
              "select-client": {
                  required: "Please select client",
              },
              "select-branch": {
                  required: "Please select branch",
              },
              // "select-status": {
              //     required: "Please select status",
              // },
              "select-type": {
                  required: "Please select type",
              },
              "select-out-source": {
                  required: "Please select source or out source",
              },
              "select-source": {
                  required: "Please select source or out source",
              },
              "select-project": {
                  required: 'Please select project',
              },
              "select-currency": {
                  required: "Please select currency",
              },
              "input-fact": {
                  required: "Please enter fact paid",
              },
              "input-paid": {
                  required: "Please enter paid USD",
              },
            },
            submitHandler: function(form) {
              submitForm();
            }
        });
    </script>

    <script type="text/javascript">
        $( ".select-type" ).change(function() {
            type_select_check();
        });
        type_select_check();

        function type_select_check(){
            if($('.select-type option:selected').val() == 1){
                $('.select-source-block').show();
                $('.select-out_source-block').hide();
                $('.select-project-block').hide();
                $(".select-out_source option:selected").prop("selected", false);
                $(".select-project option:selected").prop("selected", false);
                ////validate
                $(".select-source").rules("add",{
                    required: true
                });
                $(".select-out_source").rules("remove");
                $(".select-project[name=select-project]").rules("remove");
            }
            else if($('.select-type option:selected').val() == 2){
                $('.select-source-block').hide();
                $('.select-out_source-block').show();
                $('.select-project-block').show();
                $(".select-source option:selected").prop("selected", false);
                ////validate
                $(".select-out_source").rules("add",{
                    required: true
                });
                $(".select-project[name=select-project]").rules("add",{
                    required: true
                });
                $(".select-source").rules("remove");
                $('.selectpicker').selectpicker('refresh');
            }
            else{
                $('.select-source-block').hide();
                $('.select-out_source-block').hide();
                $('.select-project-block').hide();
                $(".select-out_source option:selected").prop("selected", false);
                $(".select-project option:selected").prop("selected", false);
                $(".select-source option:selected").prop("selected", false);
                ////validate
                $(".select-out_source").rules("remove");
                $(".select-project[name=select-project]").rules("remove");
                $(".select-source").rules("remove");
            }
        }

        function submitForm(){
                //блок кнопки и прелоадер
                $('.submit-form').find('img').css('display','');
                $('.submit-form').attr('disabled', 'true');
                ///
                if ($(".input-status").is(":checked")) {
                    var input_status = 1;
                }
                else {
                    var input_status = 0;
                }
                var formData = new FormData();
                formData.append('manager',          $('.select-manager option:selected').val());
                formData.append('branch',           $('.select-branch option:selected').val());
                formData.append('payment_status',   $('.select-status option:selected').val());
                formData.append('payment_type',     $('.select-type option:selected').val());
                formData.append('client_id',        $('.select-client option:selected').val());
                formData.append('paid_currency_id', $('.select-currency option:selected').val());
                formData.append('source',           $('.select-source option:selected').val());
                formData.append('out_source',       $('.select-out_source option:selected').val());
                formData.append('project',          $('.select-project option:selected').val());

                formData.append('date_add',           $('.input-date-add').val());
                formData.append('paid_usd',           $('.input-paid').val());
                formData.append('status',             input_status);
                formData.append('comment',            $('.input-comment').val());
                formData.append('paid_fact',          $('.input-paid-fact').val());
                formData.append('code',               $('.input-code').val());
                formData.append('weight',             $('.input-weight').val());
                formData.append('volume',             $('.input-volume').val());
                formData.append('cartons',            $('.input-cartons').val());
                formData.append('volume_weight',      $('.input-volume-weight').val());
                formData.append('inside',             $('.input-inside').val());
                formData.append('local_tracking',     $('.input-local-tracking').val());
                formData.append('local_price',        $('.input-local-price').val());
                formData.append('description',        $('.input-description').val());
                formData.append('delivery_tracking',  $('.input-delivery-tracking').val());
                // Attach file
                formData.append('invoice', document.querySelector('#get-invoice-file').files[0]);
                formData.append('bankbill', document.querySelector('#get-other-file').files[0]);
                /////csrf
                formData.append('<?= \yii::$app->request->csrfParam?>', '<?= \yii::$app->request->csrfToken ?>');

                $.ajax({ 
                    url: '/payments/edit/<?= $model->payment_id ?>',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data) { 
                        if (data == 1) {
                            window.location.replace("/payments/index");
                        }else{
                            alert(data);
                            $('.submit-form').find('img').css('display','none');
                            $('.submit-form').removeAttr("disabled");
                        }
                    }
                });
        }

        $('.mob-menu-Payments > a').addClass('active');

        $('.input-paid-fact').on('input', function(){
            if($('.select-currency').find('option:selected').attr('data-currency') > 0){
                calc = $('.input-paid-fact').val()/$('.select-currency').find('option:selected').attr('data-currency');
                $('.input-paid').val(calc.toFixed(2));
            }else{
                calc = $('.input-paid-fact').val();
                $('.input-paid').val(calc);
            }
        });

        $(".select-currency").change(function(){ 
            if($('.select-currency').find('option:selected').attr('data-currency') > 0){
                calc = $('.input-paid-fact').val()/$('.select-currency').find('option:selected').attr('data-currency');
                $('.input-paid').val(calc.toFixed(2));
            }else{
                calc = $('.input-paid-fact').val();
                $('.input-paid').val(calc);
            }
        });

        $( ".select-client" ).change(function() {
            select_client();
        });
        select_client();

        function select_client(){
            $.ajax({ 
                url: '/cargo/clientsearch/'+$('.select-client option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data != 0){
                        $(".select-manager select").val(data);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
            $.ajax({ 
                url: '/projects/projectselect/'+$('.select-client option:selected').val(), 
                type: 'GET',
                data:{
                    payment_id: <?= (isset($model->payment_id)) ? $model->payment_id : 0 ?>, 
                },
                success: function (data) { 
                    if(data != '' && $('.select-project').is(':enabled')){
                        $('.select-project').remove();
                        $('.select-input-project').html(data);
                    }
                }
            });
        }

        var invoice = new FileReader();
        var other = new FileReader();

        $('.close-preview-invoice').on("click", function() {
            $('#get-invoice-file').val('');
            $('.get-invoice-file .ava-name').text('');
            $('.get-invoice-file .ava-size').text('');
            $("#preview_invoice").attr("src", "");
            $('.get-invoice-file').css('height', '0');
            $('.close-preview-invoice').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-invoice-file").change(function(event) {

            $('.close-preview-invoice').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-invoice-file .ava-name').text(filename[fileLen]);

            invoice.onload = function() {
                $('.get-invoice-file').css('height', 'auto');
                var preview = $("#preview_invoice");
                preview.attr("src", invoice.result);
                $('.invoice-base64').val(invoice.result);
            }

            invoice.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-invoice-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });

        $('.close-preview-other').on("click", function() {
            $('#get-other-file').val('');
            $('.get-other-file .ava-name').text('');
            $('.get-other-file .ava-size').text('');
            $("#preview_other").attr("src", "");
            $('.get-other-file').css('height', '0');
            $('.close-preview-other').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-other-file").change(function(event) {

            $('.close-preview-other').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-other-file .ava-name').text(filename[fileLen]);

            other.onload = function() {
                $('.get-other-file').css('height', 'auto');
                var preview = $("#preview_other");
                preview.attr("src", other.result);
                $('.other-base64').val(other.result);
            }

            other.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-other-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });
        
        // $(document).keyup(function(e) {
        //     if(e.which == 13){ //enter press
        //         $('.submit-form').trigger('click');
        //     }
        //     if (e.key === "Escape") { 
        //         $('.reset-form').trigger('click');
        //     }
        // });
        $('.reset-form').click(function(){
            location.reload();
        });

        $('.show-project-desc').click(function(){
            if($('.show-project-desc').text() == 'Show project informations'){
                $('.project-info-all').css({'display':'block'});
                $('.project-info-description').css({'display':'block'});
                $('.project-info-insurance').css({'display':'block'});
                $('.show-project-desc').text('Hide project informations');
            }else{
                $('.project-info-all').css({'display':'none'});
                $('.project-info-description').css({'display':'none'});
                $('.project-info-insurance').css({'display':'none'});
                $('.show-project-desc').text('Show project informations');
            }
        });
    </script>
