<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'All Payments');
//$this->params['breadcrumbs'][] = $this->title;
?>

    <?=$menu?>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">

                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        All Payments *
                    </div>
                    <!-- <div class="sorting">
                        <div class="sorting-title">sorting</div>
                        <div class="picked-sorting">Payment date recelved <i class="fas fa-caret-down"></i> <a href="#"><img src="/web/img/cancel-icon.png" alt="#"></a></div>
                    </div> -->
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                    <div class="add-new">
                        <!-- <div class="ad-search">
                            <a href="#"><i class="fas fa-search"></i></a>
                        </div>
                        <div class="ad-button">
                            <a href="#"><i class="fas fa-plus"></i> Add</a>
                        </div>
                        <div class="ad-menu">
                            <a href="#"><i class="fas fa-bars"></i></a>
                        </div> -->
                    </div>
                </div>

                <div class="table-wrap">
                    <table class="table table-striped table-bordered table-resizable custom-tab">
                        <thead>
                                <tr>
                                    <!-- <th class="low-w"><i class="far fa-eye"></i></th>
                                    <th class="low-w"><input type="checkbox"></th> -->
                                    <th>Payment ID</th>
                                    <th>Manager</th>
                                    <th>Data payment</th>
                                    <th>Payment type</th>
                                    <th>Income source</th>
                                    <th>Outcome source</th>
                                    <th>Invoice</th>
                                    <th>Bankbill</th>
                                    <th>Paid USD</th>
                                    <th>Fact paid</th>
                                    <th>Fact paid currency</th>
                                    <th>Payment comment</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?= $all_table_rows ?>
                            </tbody>
                    </table>
                </div>

                <div class="detail-info-top-line">

                    <div class="detail-info-controls">
                        <a href="#" id="slide-left"><img src="/web/img/arr-left.png" alt="#"></a>
                        <a href="#" id="slide-right"><img src="/web/img/arr-right.png" alt="#"></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="/payments/edit/" class="payment-detail-info-url-edit"><img src="/web/img/pencil.png" alt="">Edit</a>
                            <!-- <a href="#"><img src="/web/img/duplicate-icon.png" alt="">Duplicate</a> -->
                            <a href="#" id="close-detail-info"><img src="/web/img/close-icon.png" alt=""></a>
                        </div>
                    </div>

                </div>

                <div class="detail-info-wrap">

                    <div class="detail-info">

                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Date payment</div>
                                <div class="detail-info-list-column detail-info-date"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Payment type</div>
                                <div class="detail-info-list-column detail-info-type"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Income source</div>
                                <div class="detail-info-list-column detail-info-source"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Outcome source</div>
                                <div class="detail-info-list-column detail-info-outsource"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Payment status</div>
                                <div class="detail-info-list-column detail-info-payment_status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Payment ID</div>
                                <div class="detail-info-list-column detail-info-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Paid USD</div>
                                <div class="detail-info-list-column detail-info-cash_usd"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Approved</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <!-- <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Outcome destination</div>
                                <div class="detail-info-list-column detail-info-date_add"></div>
                            </div> -->
                            <!-- <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Add shipment</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div> -->
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Payment comment</div>
                                <div class="detail-info-list-column detail-info-comment"></div>
                            </div>
                            <!-- <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client balance total</div>
                                <div class="detail-info-list-column detail-info-phone"></div>
                            </div> -->
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Fact paid</div>
                                <div class="detail-info-list-column detail-info-cash_fuct"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Fact paid currency</div>
                                <div class="detail-info-list-column detail-info-currency"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Branch</div>
                                <div class="detail-info-list-column detail-info-branch"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Invoice</div>
                                <div class="detail-info-list-column detail-info-invoice"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Bankbill</div>
                                <div class="detail-info-list-column detail-info-bankbill"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing">
                    Showing <?=$paymentsCount?> of <?=$paymentsCount?>
                </div>
                <div class="col-6 summary">
                    <!-- <img src="/web/img/list-icon.png" alt="#"> <a href="#">Show Summary</a> -->
                </div>
            </div>
        </div>
    </section>

<script type="text/javascript">
    $(window).resize(function() {
        tableHeight();
    });

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        body_height = $('body').height();
        table = $('.table-wrap').height();
        summ_height = window_height - (body_height - table) - 15;
        $('.table-wrap').css({'max-height' : summ_height+'px'});
    }

    $('.mob-menu-Payments > a').addClass('active');

    /////////////////////////////////////////////////

    $(".table tbody .payments-row").on("click", function() {
        var cur_id = $(this).attr("data-payment_id");
        var re, s;
        s = cur_id;
        re = /\D+/ig;
        index = s.replace(re, '');

        $('.detail-info').fadeOut(1000).removeClass("detail-visibe");
        $('.detail-info-top-line').animate({
            opacity: 1
        }, 700);
        $('.detail-info-top-line').css({
            "display": "flex"
        });
        $(".detail-info-wrap").find('.detail-info').fadeIn(700).addClass("detail-visibe");

        $.ajax({ 
            type: 'GET', 
            url: '/payments/view/' + $(this).attr("data-payment_id"), 
            data: {}, 
            dataType: 'json',
            success: function (data) { 
                $('.detail-info-manager').text(data.manager_id);
                $('.detail-info-date').text(data.date_payment);
                $('.detail-info-type').text(data.payment_type);
                $('.detail-info-source').text(data.source_id);
                $('.detail-info-outsource').text(data.out_source_id);
                $('.detail-info-payment_status').text(data.payment_status);
                $('.detail-info-id').text(data.payment_id);
                $('.detail-info-status').text(data.status);
                $('.detail-info-comment').text(data.comment);
                $('.detail-info-cash_usd').text(data.cash_usd);
                $('.detail-info-cash_fuct').text(data.cash_fact);
                $('.detail-info-currency').text(data.cash_fact_currency);
                $('.detail-info-branch').text(data.branch_id);

                $('.payment-detail-info-url-edit').attr("href", '/payments/edit/' + data.payment_id);
                console.log(data);
            }
        });
    });
</script>
