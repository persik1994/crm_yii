<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Payments;
use app\models\CustomTable;
use app\models\Client;
use app\models\Project;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'All Payments');
//$this->params['breadcrumbs'][] = $this->title;
?>

    <?=$menu?>

    <style type="text/css">
        .table-bordered tbody{
          display:block;
          overflow:auto;
          height:200px;
          width:100%;
          overflow-x: hidden;
        }
        .table-bordered thead tr{
          display:block;
        }
    </style>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">

                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        All Payments * <?= Payments::SearchShowBtn() ?>
                    </div>
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                    <div class="add-new">
                        <div class="dropdown dropdown-table custom-table-div" style="">
                            <a class="custom-table-menu" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-cog"></i> Custom table
                            </a>
                            <div class="dropdown-menu dropdown-menu-table custom-table-checked" aria-labelledby="dropdownMenuLink">
                                <a class="custom-table-btn"><i class="fas fa-save"></i> Save setting</a><Br>
                                <?= CustomTable::menuOptions('payment', 'payment') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-wrap payment-wrap">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'rowOptions'=>function($model){
                            $editUrl = 'edit';
                            if($model->payment_type  == 2 && Yii::$app->user->identity->role >= 10){
                                $editUrl = 'editsuper';
                            }
                            return ['class' => ($model->status == 1)? ($model->payment_type == 2) ? 'payment-row contextmenu-row payments-row-approve-out': 'payment-row contextmenu-row payments-row-approve':'payment-row contextmenu-row', 'url-edit' => '/payments/'.$editUrl.'/'.$model->payment_id, 'url-delete' => '/payments/delete/'.$model->payment_id, 'url-approve' => '/payments/approve/'.$model->payment_id];
                        },
                        'columns' => [
                            [
                                'attribute' => 'check',
                                'label' => '<input type="checkbox" class="checkbox-table-all" value="">',
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                "format" => 'raw',
                                'value' => function ($data) {
                                    $editUrl = 'edit';
                                    if($data->payment_type  == 2 && Yii::$app->user->identity->role >= 10){
                                        $editUrl = 'editsuper';
                                    }
                                    return '<input type="checkbox" class="checkbox-table" value="'.$data->payment_id.'"> <i class="far fa-eye eye-click-view-row" title="View" data-key="'.$data->payment_id.'"></i><a href="/payments/'.$editUrl.'/'.$data->payment_id.'" class="table-edit-btn"><i class="fas fa-pencil-alt"></i></a>'; 
                                },
                            ],
                            [
                                'attribute' => 'manager_id',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['manager_id'], 'manager_id', 'select', 'Select Manager'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','manager_id').CustomTable::widthTable(140)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','manager_id').CustomTable::widthTable(140)],
                                'value' => function ($data) {
                                    return $data->manager->username ?? '';
                                },
                            ],
                            [
                                'attribute' => 'client_id',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['client_id'], 'client_id', 'text', 'Write client id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','client_id')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','client_id')],
                                'value' => function ($data) {
                                    return (!empty($data->client_id)) ? '88-'.$data->client_id : '';
                                },
                            ],
                            [
                                'attribute' => 'date_payment',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['date_payment'], 'date_payment', 'date', 'Select date'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','date_payment').CustomTable::widthTable(115)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','date_payment').CustomTable::widthTable(115)],
                                'value' => function ($data) {
                                    return (!empty($data->date_payment)) ? date('d.m.Y',$data->date_payment) : ''; 
                                },
                            ],
                            [
                                'attribute' => 'payment_type',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['payment_type'], 'payment_type', 'select', 'Select type'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','payment_type').CustomTable::widthTable(115)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','payment_type').CustomTable::widthTable(115)],
                                'value' => function ($data) {
                                    return Payments::type($data->payment_type ?? '');
                                },
                            ],[
                                'attribute' => 'cash_usd',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['cash_usd'], 'cash_usd', 'text', 'Write USD'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','cash_usd')],
                                'contentOptions' => function ($data) {
                                    return ['style' => CustomTable::rowVisible('payment','payment','cash_usd'), 'class'=>'payments-td-right data-cash_usd-'.$data->payment_id]; 
                                },
                                'value' => function ($data) {
                                    if($data->payment_type == 1)
                                        return (!empty($data->cash_usd)) ? round($data->cash_usd).' $' : '';
                                    else if($data->payment_type == 2 && empty($data->project_id))
                                        return (!empty($data->cash_usd)) ? '-'.round($data->cash_usd).' $' : '';
                                    else
                                        return '0 $';
                                },
                            ],
                            [
                                'attribute' => 'cash_fact',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['cash_fact'], 'cash_fact', 'text', 'Write cash fact'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','cash_fact')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','cash_fact'),'class'=>'payments-td-right'],
                                'value' => function ($data) {
                                    if($data->payment_type == 1)
                                        return (!empty($data->cash_fact)) ? round($data->cash_fact).' '.((isset($data->currency->flag)) ? $data->currency->code : '') : '';
                                    else if($data->payment_type == 2 && empty($data->project_id))
                                        return (!empty($data->cash_fact)) ? '-'.round($data->cash_fact).' '.((isset($data->currency->name)) ? $data->currency->code : '') : '';
                                    else
                                        return '0 $';
                                },
                            ],
                            [
                                'attribute' => 'client_paid',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['client_paid'], 'client_paid', 'text', 'Write USD'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','client_paid').CustomTable::widthTable(130)],
                                'contentOptions' => function ($data) {
                                    return ['style' => CustomTable::rowVisible('payment','payment','client_paid').CustomTable::widthTable(130), 'class'=>'payments-td-right data-client_paid-'.$data->payment_id]; 
                                },
                                'value' => function ($data) {
                                    //if(!empty(round($data->cash_usd))){
                                        return round(Payments::MathPayMoneyMovementProject($data->project_id)).' $';
                                    //}
                                    //return '0 $';
                                },
                            ],
                            [
                                'attribute' => 'chargers_project',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['chargers_project'], 'chargers_project', 'text', 'Write USD'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','chargers_project').CustomTable::widthTable(120)],
                                'contentOptions' => function ($data) {
                                    return ['style' => CustomTable::rowVisible('payment','payment','chargers_project').CustomTable::widthTable(120), 'class'=>'payments-td-right data-chargers_project-'.$data->payment_id]; 
                                },
                                'value' => function ($data) {
                                    if($data->payment_type == 2 && !empty($data->project_id) && $data->out_source_id < 4){
                                        $project = Project::findOne(['project_id'=>$data->project_id]);
                                        if(isset($project)){
                                            if(round($project->chargers) > 0){
                                                return '-'.round($project->chargers).' $';
                                            }
                                            if($project->chargers == 0 && $project->profit > 0){
                                                return '-'.round($project->total - $project->profit).' $';
                                            }
                                        }
                                    }
                                    return '0 $';
                                },
                            ],
                            [
                                'attribute' => 'project_profit',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['project_profit'], 'project_profit', 'text', 'Write USD'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','project_profit').CustomTable::widthTable(130)],
                                'contentOptions' => function ($data) {
                                    return ['style' => CustomTable::rowVisible('payment','payment','project_profit').CustomTable::widthTable(130), 'class'=>'payments-td-right data-project_profit-'.$data->payment_id]; 
                                },
                                'value' => function ($data) {
                                    if($data->payment_type == 2 && !empty($data->project_id) && $data->out_source_id < 4){
                                        $project = Project::findOne(['project_id'=>$data->project_id]);
                                        if(isset($project)){
                                            if(!empty(round($project->profit))){
                                                return '-'.round($project->profit).' $';
                                            }
                                        }
                                    }
                                    return '0 $';
                                },
                            ],
                            [
                                'attribute' => 'balance',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['balance'], 'balance', 'text', 'Write USD'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                "format" => 'raw',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','balance').CustomTable::widthTable(120)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','balance').CustomTable::widthTable(120),'class'=>'payments-td-right payments-td-balance'],
                                'value' => function ($data) {
                                    // if($data->payment_type == 2 && $data->out_source_id != 4){
                                    //     if(!empty($data->client_id)){
                                    //         $client = Client::findOne(['client_id'=>$data->client_id]);
                                    //     }
                                    //     return (isset($client->balance)) ? round($client->balance).' <span>$</span>' : '0 <span>$</span>';
                                    // }
                                    return (!empty($data->balance)) ? round($data->balance).' <span>$</span>' : '0 <span>$</span>';
                                },
                            ],
                            [
                                'attribute' => 'source_id',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['source_id'], 'source_id', 'select', 'Select source'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','source_id').CustomTable::widthTable(115)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','source_id').CustomTable::widthTable(115)],
                                'value' => function ($data) {
                                    return $data->source->name ?? '';
                                },
                            ],
                            [
                                'attribute' => 'out_source_id',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['out_source_id'], 'out_source_id', 'select', 'Select out source'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','out_source_id').CustomTable::widthTable(150)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','out_source_id').CustomTable::widthTable(150)],
                                'value' => function ($data) {
                                    return Payments::outSource($data->out_source_id ?? '');
                                },
                            ],
                            
                            [
                                'attribute' => 'status',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['status'], 'status', 'select', 'Select status'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','status')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','status'),'class'=>'payment-td-status'],
                                'value' => function ($data) {
                                    if($data->status > 0)
                                        return 'Approved';
                                    else
                                        return 'No Approved';
                                },
                            ],
                            [
                                'attribute' => 'payment_id',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['payment_id'], 'payment_id', 'text', 'Write payment id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','payment_id')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','payment_id')],
                                'value' => function ($data) {
                                    return (!empty($data->payment_id)) ? 'PAY-'.$data->payment_id : '';
                                },
                            ],
                            [
                                'attribute' => 'project_id',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['project_id'], 'project_id', 'text', 'Write project id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','project_id')],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','project_id')],
                                'value' => function ($data) {
                                    return (!empty($data->project_id)) ? 'PJ-'.$data->project_id : '';
                                },
                            ],
                            [
                                'attribute' => 'comment',
                                'label' => Payments::paymentDropMenu(Payments::attributeLabels()['comment'], 'comment', 'text', 'Write comment'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('payment','payment','comment').CustomTable::widthTable(600)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('payment','payment','comment').CustomTable::widthTable(600)],
                                'value' => function ($data) {
                                    return $data->comment ?? '';
                                },
                            ],
                        ],
                    ]); ?>
                </div>

                <div class="detail-info-top-line">
                    <div class="detail-info-controls">
                        <a href="#" id="slide-position-left"><i class="fas fa-chevron-left"></i></a>
                        <a href="#" id="slide-position-right"><i class="fas fa-chevron-right"></i></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="" class="payment-detail-info-url-edit"><i class="fas fa-pencil-alt"></i>Edit</a>
                            <a data-id="" class="delete-post-url" style="display: <?=(Yii::$app->user->identity->role >= 9)?';':'none;'?>"><i class="far fa-trash-alt"></i>Delete</a>
                            <a href="#" id="close-detail-info"><i class="fas fa-times"></i></a>
                        </div>
                    </div>
                </div>

                <div class="detail-info-wrap">

                    <div class="detail-info">

                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Payment ID</div>
                                <div class="detail-info-list-column detail-info-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Branch</div>
                                <div class="detail-info-list-column detail-info-branch"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Date payment</div>
                                <div class="detail-info-list-column detail-info-date"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Payment status</div>
                                <div class="detail-info-list-column detail-info-payment_status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Payment type</div>
                                <div class="detail-info-list-column detail-info-type"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Income source</div>
                                <div class="detail-info-list-column detail-info-source"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Outcome source</div>
                                <div class="detail-info-list-column detail-info-outsource"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client</div>
                                <div class="detail-info-list-column detail-info-client"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Fact paid currency</div>
                                <div class="detail-info-list-column detail-info-currency"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Fact paid</div>
                                <div class="detail-info-list-column detail-info-cash_fuct"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Paid USD</div>
                                <div class="detail-info-list-column detail-info-cash_usd"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Approved</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Payment comment</div>
                                <div class="detail-info-list-column detail-info-comment"></div>
                            </div>

                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Invoice</div>
                                <div class="detail-info-list-column detail-info-invoice"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Bankbill</div>
                                <div class="detail-info-list-column detail-info-bankbill"></div>
                            </div>
                        </div>
                        <div class="detail-info-list-wrap client_money_all_block">
                            <label>Client Money Transfers</label>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Date</div>
                                <div class="detail-info-list-column column-grey">Paid $</div>
                                <div class="detail-info-list-column column-grey">Project</div>
                                <div class="detail-info-list-column column-grey">Balance</div>
                                <div class="detail-info-list-column column-grey">Comment</div>
                            </div>
                            <div class="client_money_all">
                                
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing">
                </div>
                <div class="col-6 summary-selected">
                    <span class="summ-paid"></span>
                    <span class="summ-client"></span>
                    <span class="summ-chargers"></span>
                    <span class="summ-profit"></span>
                </div>
            </div>
        </div>
    </section>
    <nav id="context-menu" class="context-menu">
        <ul class="context-menu__items">
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="View"><i class="fa fa-eye"></i> View</a>
          </li>
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="Edit"><i class="fa fa-edit"></i> Edit</a>
          </li>
          <?php if(Yii::$app->user->identity->role >= 10){ ?>
              <li class="context-menu__item">
                <a href="#" class="context-menu__link context-menu-approve" data-action="Approve"><i class="fas fa-check"></i> Approve</a>
              </li>
              <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="Delete"><i class="fa fa-times"></i> Delete</a>
              </li>
          <?php } ?>
        </ul>
    </nav>

<script src="/web/js/contecst_menu.js"></script>
<script type="text/javascript">

    setInterval(function() {
        tableHeight();
    }, 500);

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        summ_height = window_height - ($('.all-cargos-title').height() + $('header').height() + $('.options').height() + 50);
        if(summ_height < 500){
            summ_height = 500;
        }
        $('.table-wrap').css({'max-height' : summ_height+'px'});
        $('.table-bordered').css({'max-height' : summ_height+'px'});
        $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
    }

    $('.mob-menu-Payments > a').addClass('active');

    $('.summ_bottom').html($('.summary').html());
    $('.summary').html('');


    $(document).on('keyup', '.dropdown-search-input', function (event) {
        if(event.keyCode==13) {
            runSearch($(this).attr("data-type"),$(this).val());
        }
    });

    $(document).on('click', '.search-menu-click', function () {
        runSearch($(this).attr("data-type"),$(this).attr("data-id"));
    });

    $(document).on('change', '#form-choose-date', function () {
        runSearch($(this).attr("data-type"),$(this).val());
    });

    function runSearch(type, value){
        $.ajax({ 
            type: 'POST', 
            url: '/payments/search?'+type+'='+value+'&sort=<?=Yii::$app->request->get('sort')?>', 
            success: function (data) { 
                $('.all-cargos-content').html(data);
            }
        });
    };

    $(".delete-post-url").click(function() {
        var delete_text = confirm('Do you really want to delete payment?');
        if(delete_text) window.location.replace('/payments/delete/'+$(this).attr("data-id"));
    });

    $('.custom-table-btn').click(function() {
        var sListType = '';
        $('.custom-table-checked input').each(function () {
            if(this.checked == "1"){
                sListType += $(this).val() + ",";
            }
        });
        if(sListType != ''){
            $.ajax({ 
                type: 'GET', 
                url: '/payments/customtable?page=payment&type='+sListType, 
                success: function (data) { 
                    location.reload();
                }
            });
        }
    });

    $(document).on('click', '.checkbox-table-all', function () {
        $('.checkbox-table').not(this).prop('checked', this.checked);
        allPaymentsSelect(1);
    });

    $(document).on('click', '.checkbox-table', function () {
        if(this.checked == "1"){
            $('.payment-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
        }else{
            $('.payment-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
        }
        allPaymentsSelect();
    });

    function allPaymentsSelect(type = 0){
        var sList = '';
        //////
        var summPaid = 0;
        var summClientPaid = 0;
        var summChargers = 0;
        var summProfit = 0;
        ///////
        $('.checkbox-table').each(function () {
            if(this.checked == "1"){
                if(type == 1){
                    $('.payment-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
                }
                sList += $(this).val() + ",";
                ////////
                /////
                summPaid        += parseInt($('.data-cash_usd-'+$(this).val()).text());
                summClientPaid  += parseInt($('.data-client_paid-'+$(this).val()).text());
                summChargers    += parseInt($('.data-chargers_project-'+$(this).val()).text());
                summProfit      += parseInt($('.data-project_profit-'+$(this).val()).text());


                if(type == 1){
                    $('.payment-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
                }
                sList += $(this).val() + ",";
            }else if(type == 1){
                $('.payment-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
            }
        });
        $('.summ-paid').text('Paid USD = '+summPaid+';');
        $('.summ-client').text('Client paid = '+summClientPaid+';');
        $('.summ-chargers').text('Chargers = '+summChargers+';');
        $('.summ-profit').text('Profit = '+summProfit+';');
    }
</script>
