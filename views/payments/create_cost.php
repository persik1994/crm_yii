<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Create Cost Payment');
?>

<?=$menu?>

	<section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">Create Cost Payment</div>
            </div>

            <div class="add-client-form-wrap">
                <form>

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Manager <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager selectpicker" name="manager" onblur="this.style.color = '#000'" <?=(Yii::$app->user->identity->role < 9) ? 'disabled' : ''?> data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option value="<?=$manager->manager_id?>" <?=(Yii::$app->user->identity->role < 9 && $manager->username == Yii::$app->user->identity->username) ? 'selected' : ''?>><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Date payment </label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date" class="input-date-add" value="<?=date('d-M-Y',time())?>" autocomplete="off">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-4 offset-lg-1 col-md-12"> 

                            <div class="add-client-form-line">
                                <label>Fact paid currency <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-currency" name="manager" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($currencies as $currency) { ?>
                                            <option value="<?=$currency->сurrency_id?>" data-currency="<?=$currency->currency?>"><?=$currency->name?> - <?=$currency->currency?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="add-client-form-line">
                                <label>Fact paid <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-paid-fact">
                                </div>
                            </div>

                        	<div class="add-client-form-line">
                                <label>Paid USD <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-paid">
                                </div>
                            </div>

                            <div class="add-client-form-line add-client-form-line-checkbox">
                                <label>
                                    <input type="checkbox" name="status" class="input-status">
                                    <p>Approved</p>
                                    <span></span>
                                </label>
                            </div>

                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Payment comment</label>
                                <div class="input-addclient-wrap">
                                    <textarea name="name" class="input-comment"> </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="add-new-form-submit-buttons col-12">
                            <div class="add-new-form-submit-buttons-wrap col-4">
                                <div class="add-new-form-submit-buttons-wrap-width">
                                    <a class="submit-form" >Submit <img src="/web/img/preloader.gif" style="display: none;"></a>
                                    <a href="#" class="reset-form">Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <script type="text/javascript">
    	$( ".select-type" ).change(function() {
			if($('.select-type option:selected').val() == 1){
				$('.select-source-block').show();
				$('.select-out_source-block').hide();
                $('.select-project-block').hide();
			}
			else if($('.select-type option:selected').val() == 2){
				$('.select-source-block').hide();
				$('.select-out_source-block').show();
                $('.select-project-block').show();
			}
			else{
				$('.select-source-block').hide();
				$('.select-out_source-block').hide();
                $('.select-project-block').hide();
			}
		});

    	$('.submit-form').click(function(){
            if(this.hasAttribute("disabled")){

            }
            else{
                //блок кнопки и прелоадер
                $(this).find('img').css('display','');
                $(this).attr('disabled', 'true');
                ///
        		if ($(".input-status").is(":checked")) {
    			    var input_status = 1;
    			}
    			else {
    			    var input_status = 0;
    			}

        		$.ajax({ 
    	            type: 'POST', 
    	            url: '/payments/createcosts', 
    	            data:{
    		            manager: 			$('.select-manager option:selected').val(), 
    	            	branch: 			$('.select-branch option:selected').val(), 
    	            	date_add: 			$('.input-date-add').val(), 
    	            	payment_status: 	$('.select-status option:selected').val(),
    	            	payment_type: 		$('.select-type option:selected').val(),
    	            	client_id: 			$('.select-client option:selected').val(),
    	            	//invoice: 			$('.file-invoice').val(),
    	            	//bankbill: 		$('.file-bankbill').val(),
    	            	paid_usd: 			$('.input-paid').val(), 
    	            	status: 			input_status,
    	            	comment: 			$('.input-comment').val(), 
    	            	paid_fact: 			$('.input-paid-fact').val(),
    	            	paid_currency_id: 	$('.select-currency option:selected').val(), 
    	            	////////
    	            	source: 			$('.select-source option:selected').val(),
    	            	out_source: 		$('.select-out_source option:selected').val(), 
                        project:            $('.select-project option:selected').val(), 
    	            	'<?= \yii::$app->request->csrfParam?>': '<?= \yii::$app->request->csrfToken ?>'
    		        },
    	            //dataType: 'json',
    	            success: function (data) { 
    	                console.log(data);
    	                if (data == 1) {
    	                	window.location.replace("/payments/costs");
    	                }
    	            }
    	        });
            }
    	});

        $('.mob-menu-Payments > a').addClass('active');

        $('.input-paid-fact').on('input', function(){
            if($('.select-currency').find('option:selected').attr('data-currency') > 0){
                calc = $('.input-paid-fact').val()/$('.select-currency').find('option:selected').attr('data-currency');
                $('.input-paid').val(calc.toFixed(2));
            }else{
                calc = $('.input-paid-fact').val();
                $('.input-paid').val(calc);
            }
        });

        $(".select-currency").change(function(){ 
            if($('.select-currency').find('option:selected').attr('data-currency') > 0){
                calc = $('.input-paid-fact').val()/$('.select-currency').find('option:selected').attr('data-currency');
                $('.input-paid').val(calc.toFixed(2));
            }else{
                calc = $('.input-paid-fact').val();
                $('.input-paid').val(calc);
            }
        });

        $( ".select-client" ).change(function() {
            select_client();
        });

        function select_client(){
            $.ajax({ 
                url: '/cargo/clientsearch/'+$('.select-client option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data != 0){
                        $(".select-manager select").val(data);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
            $.ajax({ 
                url: '/projects/projectselect/'+$('.select-client option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data != ''){
                        $('.select-project').remove();
                        $('.select-input-project').html(data);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
        }
        
        // $(document).keyup(function(e) {
        //     if(e.which == 13){ //enter press
        //         $('.submit-form').trigger('click');
        //     }
        //     if (e.key === "Escape") { 
        //         $('.reset-form').trigger('click');
        //     }
        // });
        $('.reset-form').click(function(){
            location.reload();
        });
    </script>
