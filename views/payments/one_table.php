<tr class="payments-row <?= ($payment->status == 1)? 'payments-row-approve' : '' ?>" data-payment_id="<?= (!empty($payment->payment_id)) ? $payment->payment_id : '' ?>">
    <!-- <td></td>
    <td></td> -->
    <td><?= (!empty($payment->payment_id)) ? 'PAY-'.$payment->payment_id : '' ?></td>
    <td><?= (!empty($payment->manager->username)) ? $payment->manager->username : '' ?></td>
    <td><?= (!empty($payment->date_payment)) ? date('d.m.Y', $payment->date_payment) : '' ?></td>
    <td><?= (!empty($payment->payment_type)) ? $payments_Obj->type($payment->payment_type) : '' ?></td>
    <td><?= (!empty($payment->source->name)) ? $payment->source->name : '' ?></td>
    <td><?= (!empty($payment->out_source_id)) ? $payments_Obj->outSource($payment->out_source_id) : '' ?></td>
    <td></td>
    <td></td>
    <td><?= (!empty($payment->cash_usd)) ? $payment->cash_usd : '' ?></td>
    <td><?= (!empty($payment->cash_fact)) ? $payment->cash_fact : '' ?></td>
    <td><?= (!empty($payment->currency->name)) ? $payment->currency->name : '' ?></td>
    
    <td class="table-description"><?= (!empty($payment->comment)) ? $payment->comment : '' ?></td>
</tr>