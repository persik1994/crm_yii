<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Client;
use app\models\CustomTable;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Balance Clients';
?>

    <?=$menu?>

    <style type="text/css">
        .table-bordered tbody{
          display:block;
          overflow:auto;
          height:200px;
          width:100%;
          overflow-x: hidden;
        }
        .table-bordered thead tr{
          display:block;
        }
    </style>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        Balance Clients * <?= Client::SearchShowBtn(1) ?>
                    </div>
                </div>

                <div class="table-wrap clent-wrap">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'rowOptions'=>function($model){
                            return ['class' => 'client-balance-row contextmenu-row', 'url-edit' => '/payments/editbalance/'.$model->client_id, 'url-delete' => ''];
                        },
                        'columns' => [
                            [
                                'attribute' => 'check',
                                'label' => '<input type="checkbox" class="checkbox-table-all" value="">',
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::widthTable(100)],
                                'contentOptions' => ['style' => CustomTable::widthTable(100)],
                                "format" => 'raw',
                                'value' => function ($data) {
                                    return '<input type="checkbox" class="checkbox-table" value="'.$data->client_id.'"> <i class="far fa-eye eye-click-view-row" title="View" data-key="'.$data->client_id.'"></i><a href="/payments/editbalance/'.$data->client_id.'" class="table-edit-btn"><i class="fas fa-pencil-alt"></i></a>'; 
                                },
                            ],
                            [
                                'attribute' => 'client_id',
                                'label' => Client::clientDropMenu(Client::attributeLabels()['client_id'], 'client_id', 'text', 'Write client id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::widthTable(100)],
                                'contentOptions' => ['style' => CustomTable::widthTable(100)],
                                'value' => function ($data) {
                                    return (!empty($data->client_id)) ? '88-'.$data->client_id : '';
                                },
                            ],
                            [
                                'attribute' => 'name',
                                'label' => Client::clientDropMenu(Client::attributeLabels()['name'], 'name', 'text', 'Write client id'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::widthTable(250)],
                                'contentOptions' => ['style' => CustomTable::widthTable(250)],
                                'value' => function ($data) {
                                    return (!empty($data->name)) ? $data->name : '';
                                },
                            ],
                            [
                                'attribute' => 'manager_id',
                                'label' => Client::clientDropMenu(Client::attributeLabels()['manager_id'], 'manager_id', 'select', 'Select Manager'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::widthTable(140)],
                                'contentOptions' => ['style' => CustomTable::widthTable(140)],
                                'value' => function ($data) {
                                    return $data->manager->username ?? '';
                                },
                            ],
                            [
                                'attribute' => 'balance',
                                'label' => Client::clientDropMenu(Client::attributeLabels()['balance'], 'balance', 'text', 'Write balance'),
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::widthTable(100)],
                                'contentOptions' => ['style' => CustomTable::widthTable(100)],
                                'value' => function ($data) {
                                    return (!empty($data->balance)) ? round($data->balance) : '0';
                                },
                            ],
                        ],
                    ]); ?>
                </div>

                <div class="detail-info-top-line">
                    <div class="detail-info-controls">
                        <a href="#" id="slide-position-left"><i class="fas fa-chevron-left"></i></a>
                        <a href="#" id="slide-position-right"><i class="fas fa-chevron-right"></i></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="" class="client-detail-info-url-edit"><i class="fas fa-pencil-alt"></i>Edit</a>
                            <a href="#" id="close-detail-info"><i class="fas fa-times"></i></a>
                        </div>
                    </div>
                </div>

                <div class="detail-info-wrap">

                    <div class="detail-info">

                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client code</div>
                                <div class="detail-info-list-column detail-info-id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Name</div>
                                <div class="detail-info-list-column detail-info-name"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Balance now $</div>
                                <div class="detail-info-list-column detail-info-balance"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Money for all time $</div>
                                <div class="detail-info-list-column detail-info-balance_total"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client all weight (kg.)</div>
                                <div class="detail-info-list-column detail-info-weight"></div>
                            </div>
                        </div>
                        <div class="detail-info-list-wrap client_money_all_block">
                            <label>Client Money Transfers</label>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Date</div>
                                <div class="detail-info-list-column column-grey">Paid $</div>
                                <div class="detail-info-list-column column-grey">Project</div>
                                <div class="detail-info-list-column column-grey">Balance</div>
                                <div class="detail-info-list-column column-grey">Comment</div>
                            </div>
                            <div class="client_money_all">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing summ_bottom">
                </div>
                <div class="col-6 summary">
                    <!-- <img src="/web/img/list-icon.png" alt="#"> <a href="#">Show Summary</a> -->
                </div>
            </div>
        </div>
    </section>

    <nav id="context-menu" class="context-menu">
        <ul class="context-menu__items">
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="View"><i class="fa fa-eye"></i> View</a>
          </li>
          <li class="context-menu__item">
            <a href="#" class="context-menu__link" data-action="Edit"><i class="fa fa-edit"></i> Edit</a>
          </li>
          <?php if(Yii::$app->user->identity->role >= 9){ ?>
              <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="Delete"><i class="fa fa-times"></i> Delete</a>
              </li>
          <?php } ?>
        </ul>
    </nav>

<script src="/web/js/contecst_menu.js"></script>
<script type="text/javascript">
    
    setInterval(function() {
        tableHeight();
    }, 500);

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        summ_height = window_height - ($('.all-cargos-title').height() + $('header').height() + $('.options').height() + 50);
        if(summ_height < 500){
            summ_height = 500;
        }
        $('.table-wrap').css({'max-height' : summ_height+'px'});
        $('.table-bordered').css({'max-height' : summ_height+'px'});
        $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
    }

    $('.mob-menu-Payments > a').addClass('active');

    $('.summ_bottom').html($('.summary').html());
    $('.summary').html('');


    $(document).on('keyup', '.dropdown-search-input', function (event) {
        if(event.keyCode==13) {
            runSearch($(this).attr("data-type"),$(this).val());
        }
    });

    $(document).on('click', '.search-menu-click', function () {
        runSearch($(this).attr("data-type"),$(this).attr("data-id"));
    });

    $(document).on('change', '#form-choose-date', function () {
        runSearch($(this).attr("data-type"),$(this).val());
    });

    function runSearch(type, value){
        $.ajax({ 
            type: 'POST', 
            url: '/clients/searchbalances?'+type+'='+value+'&sort=<?=Yii::$app->request->get('sort')?>', 
            success: function (data) { 
                $('.all-cargos-content').html(data);
            }
        });
    };

    $(".delete-post-url").click(function() {
        //var delete_text = confirm('Do you really want to delete client?');
        //if(delete_text) window.location.replace('/clients/delete/'+$(this).attr("data-id"));
    });

    $('.custom-table-btn').click(function() {
        var sListType = '';
        $('.custom-table-checked input').each(function () {
            if(this.checked == "1"){
                sListType += $(this).val() + ",";
            }
        });
        if(sListType != ''){
            $.ajax({ 
                type: 'GET', 
                url: '/clients/customtable?page=client&type='+sListType, 
                success: function (data) { 
                    location.reload();
                }
            });
        }
    });

    $(document).on('click', '.checkbox-table-all', function () {
        $('.checkbox-table').not(this).prop('checked', this.checked);
        allCargosSelectEdits(1);
    });

    $(document).on('click', '.checkbox-table', function () {
        if(this.checked == "1"){
            $('.payment-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
        }else{
            $('.payment-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
        }
        allCargosSelectEdits();
    });

    function allCargosSelectEdits(type = 0){
        var sList = '';
        ///////
        $('.checkbox-table').each(function () {
            if(this.checked == "1"){
                if(type == 1){
                    $('.payment-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
                }
                sList += $(this).val() + ",";
            }else if(type == 1){
                $('.payment-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
            }
        });
    }
</script>