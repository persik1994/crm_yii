<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = Yii::t('app', 'Edit Cost Payment');
?>

<?=$menu?>

	<section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">Edit Cost Payment</div>
            </div>

            <div class="add-client-form-wrap">
                <form>

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="add-client-form-line">
                                <label>Manager <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager selectpicker" name="manager" onblur="this.style.color = '#000'" <?=(Yii::$app->user->identity->role < 9) ? 'disabled' : ''?> data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option <?= ($manager->manager_id == $model->manager_id) ? 'selected' : '' ?> value="<?=$manager->manager_id?>"><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Date payment </label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date" class="input-date-add" value="<?= (!empty($model->date_payment)) ? date('d-M-Y',$model->date_payment) : '' ?>" autocomplete="off">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-4 offset-lg-1 col-md-12"> 

                            <div class="add-client-form-line">
                                <label>Fact paid currency <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-currency" name="manager" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($currencies as $currency) { ?>
                                            <option <?= ($currency->сurrency_id == $model->cash_fact_currency) ? 'selected' : '' ?> value="<?=$currency->сurrency_id?>" data-currency="<?=$currency->currency?>"><?=$currency->name?> - <?=$currency->currency?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="add-client-form-line">
                                <label>Fact paid <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-paid-fact" value="<?= (!empty($model->cash_fact)) ? $model->cash_fact : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Paid USD <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="0.00" class="input-paid" value="<?= (!empty($model->cash_usd)) ? $model->cash_usd : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line add-client-form-line-checkbox">
                                <label>
                                    <input type="checkbox" name="status" class="input-status" <?= ($model->status == 1) ? 'checked' : '' ?>> 
                                    <p>Approved</p>
                                    <span></span>
                                </label>
                            </div>

                            <div class="add-client-form-line input-addclient-wrap-textarea">
                                <label>Payment comment</label>
                                <div class="input-addclient-wrap">
                                    <textarea name="name" class="input-comment"><?= (!empty($model->comment)) ? $model->comment : '' ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="add-new-form-submit-buttons col-12">
                            <div class="add-new-form-submit-buttons-wrap col-4">
                                <div class="add-new-form-submit-buttons-wrap-width">
                                    <a class="submit-form" >Submit <img src="/web/img/preloader.gif" style="display: none;"></a>
                                    <a href="#" class="reset-form">Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        
        $( ".select-type" ).change(function() {
			type_select_check();
		});
			type_select_check();

		function type_select_check(){
			if($('.select-type option:selected').val() == 1){
				$('.select-source-block').show();
				$('.select-out_source-block').hide();
                $('.select-project-block').hide();
			}
			else if($('.select-type option:selected').val() == 2){
				$('.select-source-block').hide();
				$('.select-out_source-block').show();
                $('.select-project-block').show();
			}
			else{
				$('.select-source-block').hide();
				$('.select-out_source-block').hide();
                $('.select-project-block').hide();
			}
		}

        $('.submit-form').click(function(){
            if(this.hasAttribute("disabled")){

            }
            else{
                //блок кнопки и прелоадер
                $(this).find('img').css('display','');
                $(this).attr('disabled', 'true');
                ///
                if ($(".input-status").is(":checked")) {
                    var input_status = 1;
                }
                else {
                    var input_status = 0;
                }
                var formData = new FormData();
                formData.append('manager',          $('.select-manager option:selected').val());
                formData.append('branch',           $('.select-branch option:selected').val());
                formData.append('payment_status',   $('.select-status option:selected').val());
                formData.append('payment_type',     $('.select-type option:selected').val());
                formData.append('client_id',        $('.select-client option:selected').val());
                formData.append('paid_currency_id', $('.select-currency option:selected').val());
                formData.append('source',           $('.select-source option:selected').val());
                formData.append('out_source',       $('.select-out_source option:selected').val());
                formData.append('project',          $('.select-project option:selected').val());

                formData.append('date_add',           $('.input-date-add').val());
                formData.append('paid_usd',           $('.input-paid').val());
                formData.append('status',             input_status);
                formData.append('comment',            $('.input-comment').val());
                formData.append('paid_fact',          $('.input-paid-fact').val());
                formData.append('code',               $('.input-code').val());
                formData.append('weight',             $('.input-weight').val());
                formData.append('volume',             $('.input-volume').val());
                formData.append('cartons',            $('.input-cartons').val());
                formData.append('volume_weight',      $('.input-volume-weight').val());
                formData.append('inside',             $('.input-inside').val());
                formData.append('local_tracking',     $('.input-local-tracking').val());
                formData.append('local_price',        $('.input-local-price').val());
                formData.append('description',        $('.input-description').val());
                formData.append('delivery_tracking',  $('.input-delivery-tracking').val());
                // Attach file
                // formData.append('invoice', document.querySelector('#get-invoice-file').files[0]);
                // formData.append('bankbill', document.querySelector('#get-other-file').files[0]);
                // /////csrf
                formData.append('<?= \yii::$app->request->csrfParam?>', '<?= \yii::$app->request->csrfToken ?>');

                $.ajax({ 
                    url: '/payments/editcosts/<?= $model->payment_id ?>',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data) { 
                        if (data == 1) {
                            window.location.replace("/payments/costs");
                        }
                    }
                });
            }
        });

        $('.mob-menu-Payments > a').addClass('active');

        $('.input-paid-fact').on('input', function(){
            if($('.select-currency').find('option:selected').attr('data-currency') > 0){
                calc = $('.input-paid-fact').val()/$('.select-currency').find('option:selected').attr('data-currency');
                $('.input-paid').val(calc.toFixed(2));
            }else{
                calc = $('.input-paid-fact').val();
                $('.input-paid').val(calc);
            }
        });

        $(".select-currency").change(function(){ 
            if($('.select-currency').find('option:selected').attr('data-currency') > 0){
                calc = $('.input-paid-fact').val()/$('.select-currency').find('option:selected').attr('data-currency');
                $('.input-paid').val(calc.toFixed(2));
            }else{
                calc = $('.input-paid-fact').val();
                $('.input-paid').val(calc);
            }
        });

        $( ".select-client" ).change(function() {
            select_client();
        });
        select_client();

        function select_client(){
            $.ajax({ 
                url: '/cargo/clientsearch/'+$('.select-client option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data != 0){
                        $(".select-manager select").val(data);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
            $.ajax({ 
                url: '/projects/projectselect/'+$('.select-client option:selected').val(), 
                type: 'GET',
                data:{
                    payment_id: <?= (isset($model->payment_id)) ? $model->payment_id : 0 ?>, 
                },
                success: function (data) { 
                    if(data != ''){
                        $('.select-project').remove();
                        $('.select-input-project').html(data);
                    }
                }
            });
        }

        var invoice = new FileReader();
        var other = new FileReader();

        $('.close-preview-invoice').on("click", function() {
            $('#get-invoice-file').val('');
            $('.get-invoice-file .ava-name').text('');
            $('.get-invoice-file .ava-size').text('');
            $("#preview_invoice").attr("src", "");
            $('.get-invoice-file').css('height', '0');
            $('.close-preview-invoice').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-invoice-file").change(function(event) {

            $('.close-preview-invoice').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-invoice-file .ava-name').text(filename[fileLen]);

            invoice.onload = function() {
                $('.get-invoice-file').css('height', 'auto');
                var preview = $("#preview_invoice");
                preview.attr("src", invoice.result);
                $('.invoice-base64').val(invoice.result);
            }

            invoice.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-invoice-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });

        $('.close-preview-other').on("click", function() {
            $('#get-other-file').val('');
            $('.get-other-file .ava-name').text('');
            $('.get-other-file .ava-size').text('');
            $("#preview_other").attr("src", "");
            $('.get-other-file').css('height', '0');
            $('.close-preview-other').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-other-file").change(function(event) {

            $('.close-preview-other').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-other-file .ava-name').text(filename[fileLen]);

            other.onload = function() {
                $('.get-other-file').css('height', 'auto');
                var preview = $("#preview_other");
                preview.attr("src", other.result);
                $('.other-base64').val(other.result);
            }

            other.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-other-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });
        
        // $(document).keyup(function(e) {
        //     if(e.which == 13){ //enter press
        //         $('.submit-form').trigger('click');
        //     }
        //     if (e.key === "Escape") { 
        //         $('.reset-form').trigger('click');
        //     }
        // });
        $('.reset-form').click(function(){
            location.reload();
        });
    </script>
