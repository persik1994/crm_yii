<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Cargo;
use app\models\CustomTable;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Сargos';
?>

					<?= GridView::widget([
				        'dataProvider' => $dataProvider,
    					'showHeader'=> false,
    					'layout'=>"{items}",
    					'rowOptions'=>function($model){
					        return ['class' => 'cargo-row contextmenu-row', 'url-edit' => '/cargo/edit/'.$model->cargo_id, 'url-delete' => '/cargo/delete/'.$model->cargo_id];
					    },
				        'columns' => [
				            [
				            	'attribute' => 'check',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
					            "format" => 'raw',
					            'value' => function ($data) {
					                return '<input type="checkbox" class="checkbox-table" value="'.$data->cargo_id.'"> <i class="far fa-eye eye-click-view-row" title="View" data-key="'.$data->cargo_id.'"></i><a href="/cargo/edit/'.$data->cargo_id.'" class="table-edit-btn"><i class="fas fa-pencil-alt"></i></a>'; 
					            },
					        ],
				            [
				            	'attribute' => 'cargo_id',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','cargo_id')],
					            'value' => function ($data) {
					                return 'CC-'.$data->cargo_id; 
					            },
					        ],
					        [
                                'attribute' => 'photo',
                                'label' => false,
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','photos').CustomTable::widthTable(100).'text-align:center;'],
                                'format' => 'raw',
                                'value' => function ($data) {
                                	if(!empty($data->photos)){
										$photos_all = explode(",", $data->photos);
										$data->photos = '';
										foreach ($photos_all as $photo) {
											if(!empty($photo))
												$data->photos .= '<a href="'.$photo.'" target="_blank"><i class="fas fa-camera-retro"></i></a> ';
										}
									}
                                    return (!empty($data->photos)) ? $data->photos : ''; 
                                },
                            ],
				            [
				            	'attribute' => 'date_received',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','date_received').CustomTable::widthTable(120)],
					            'value' => function ($data) {
					                return (!empty($data->date_received)) ? date('d.m.Y',$data->date_received) : ''; 
					            },
					        ],
				            [
				            	'attribute' => 'client_id',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','client_id')],
					            'value' => function ($data) {
					                return (!empty($data->client_id)) ? '88-'.$data->client_id : '';
					            },
					        ],
				            [
				            	'attribute' => 'manager_id',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','manager_id').CustomTable::widthTable(140)],
					            'value' => function ($data) {
					                return $data->manager->username ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'create_manager_id',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','create_manager_id').CustomTable::widthTable(140)],
					            'value' => function ($data) {
					                return $data->managerCreate->username ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'warehouse_id',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','warehouse_id').CustomTable::widthTable(150)],
					            'value' => function ($data) {
					                return $data->warehouse->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'shipment_type_id',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','shipment_type_id').CustomTable::widthTable(120)],
					            'value' => function ($data) {
					                return $data->shipmentType->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'status_id',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','status_id').CustomTable::widthTable(115)],
					            'value' => function ($data) {
					                return $data->cargoStatus->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'weight',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => function ($data) {
					                return ['style' => CustomTable::rowVisible('cargo','index','weight'), 'class'=>'data-weight-'.$data->cargo_id]; 
					            },
					            'value' => function ($data) {
					                return $data->weight; 
					            },
					        ],
				            [
				            	'attribute' => 'volume',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => function ($data) {
					                return ['style' => CustomTable::rowVisible('cargo','index','volume'), 'class'=>'data-volume-'.$data->cargo_id]; 
					            },
					            'value' => function ($data) {
					                return $data->volume ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'volume_weight',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
					            'contentOptions' => function($model){
				                    return ['style' => (((!empty($model->weight)) && (!empty($model->volume)) && ($model->weight / $model->volume) < 167) ? 'background-color:red;'.CustomTable::rowVisible('cargo','index','volume_weight') : CustomTable::rowVisible('cargo','index','volume_weight')).CustomTable::widthTable(120), 'class'=>'data-volume_weight-'.$model->cargo_id];
				                },
					            'value' => function ($data) {
					                return $data->volume*167 ?? 0; 
					            },
					        ],
				            [
				            	'attribute' => 'cartons',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => function ($data) {
					                return ['style' => CustomTable::rowVisible('cargo','index','cartons'), 'class'=>'data-cartons-'.$data->cargo_id]; 
					            },
					            'value' => function ($data) {
					                return $data->cartons ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'count_inside',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','count_inside')],
					            'value' => function ($data) {
					                return $data->count_inside ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'type_id',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','type_id')],
					            'value' => function ($data) {
					                return $data->type->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'type_special_id',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','type_special_id')],
					            'value' => function ($data) {
					                return Cargo::special_type($data->type_special_id); 
					            },
					        ],
				            [
				            	'attribute' => 'project_id',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','project_id')],
					            'value' => function ($data) {
					                return (!empty($data->project_id)) ? 'PJ-'.$data->project_id : '';
					            },
					        ],
				            [
				            	'attribute' => 'shipment_id',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
					            'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','shipment_id').'overflow-x: auto;'.CustomTable::widthTable(150)],
					            'value' => function ($data) {
                                        return ((!empty($data->shipment_id)) ? 'SH-'.$data->shipment_id : '').((!empty($data->shipment->name)) ? '-'.$data->shipment->name : '');
					            },
					        ],
				            [
				            	'attribute' => 'local_delivery_price',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','local_delivery_price').CustomTable::widthTable(145)],
					            'value' => function ($data) {
					                return ($data->local_delivery_price ?? '0.00').' ¥'; 
					            },
					        ],
					        [
				            	'attribute' => 'Local tracking',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','local_track').CustomTable::widthTable(200)],
					            'value' => function ($data) {
					                return $data->local_track ?? ''; 
					            },
					        ],
					        [
				            	'attribute' => 'Cargo description',
					            'label' => false,
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','index','description').CustomTable::widthTable(600).'overflow-x: auto;'],
					            'value' => function ($data) {
					                return $data->description ?? ''; 
					            },
					        ],
				        ],
				    ]); ?>