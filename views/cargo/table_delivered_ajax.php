<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Cargo;
use app\models\Stock;
use app\models\CargoStatus;
use app\models\CustomTable;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
if(!empty($id)){
	$status = CargoStatus::findOne(['status_id'=>(int)$id]);
	if(isset($status)){
		$this->title = 'Cargo '.$status->name;
	}
}

?>
					
					<?php
						$model_filter = new Cargo;
						$count_all = Cargo::SearchQuery(3,(int)$id)->count();
					?>
					<?= GridView::widget([
				        'dataProvider' => $dataProvider,
    					'showHeader'=> false,
    					'layout'=>"{items}",
    					'rowOptions'=>function($model){
					        return ['class' => 'cargo-row contextmenu-row', 'url-edit' => '/cargo/edit/'.$model->cargo_id, 'url-delete' => '/cargo/delete/'.$model->cargo_id];
					    },
				        'beforeRow' => function ($cargo) use ($model_filter){
					        if($model_filter->row_shipment_type_id != $cargo->shipment_type_id || 
                        	$model_filter->row_shipment_id != $cargo->shipment_id || 
                        	$model_filter->row_status_id != $cargo->status_id){
					    		$html = '';
					        	///////////обнуляю сумму і вивожу////////
			                    if($model_filter->row_weight != 0 && 
			                    $model_filter->row_volume != 0 && 
			                    $model_filter->row_volume_weight != 0 && 
			                    $model_filter->row_cartons != 0){
			                    	$html .= CustomTable::rowCounts('cargo', 'delivered', $model_filter->row_weight, $model_filter->row_volume, $model_filter->row_volume_weight, $model_filter->row_cartons);
			                        $model_filter->row_weight = 0;
			                        $model_filter->row_volume = 0;
			                        $model_filter->row_volume_weight = 0;
			                        $model_filter->row_cartons = 0;
			                    }
			                    //////
					            $model_filter->row_weight += $cargo->weight;
						        $model_filter->row_volume += $cargo->volume;
						        $model_filter->row_volume_weight += ($cargo->volume*167);
						        $model_filter->row_cartons += $cargo->cartons;
			                    ///////оновлення змінних/////
			                    $model_filter->row_shipment_type_id = $cargo->shipment_type_id;
			                    $model_filter->row_shipment_id = $cargo->shipment_id;
			                    $model_filter->row_status_id = $cargo->status_id;
			                    //////////////////////////////
			                    $shipment_type = (!empty($cargo->shipmentType->name)) ? $cargo->shipmentType->name : '';
		                        $shipment = (!empty($cargo->shipment_id)) ? 'SH-'.$cargo->shipment_id : '';
		                        $status = (!empty($cargo->cargoStatus->name)) ? $cargo->cargoStatus->name : '';
			                    /////////// вставляємо рядок в таблицю//////
			                    $text = $shipment_type .' > '.$shipment .' > '.$status;
			                    ////////
			                    $model_filter->row_count++;
			                    $model_filter->row_status_count = 1;
			                    return $html.Yii::$app->controller->renderPartial('one_table_row', ['text'=>$text, 'row_count'=>$model_filter->row_count]);
			                }
			                ////////
			                $model_filter->row_weight += $cargo->weight;
				            $model_filter->row_volume += $cargo->volume;
				            $model_filter->row_volume_weight += ($cargo->volume*167);
				            $model_filter->row_cartons += $cargo->cartons;
					    },
					    'afterRow' => function($cargo, $key, $index) use ($model_filter, $count_all){
					    	if($count_all == ($index + 1)){
			                    if($model_filter->row_weight != 0 && 
			                    $model_filter->row_volume != 0 && 
			                    $model_filter->row_volume_weight != 0 && 
			                    $model_filter->row_cartons != 0){
			                    	$html = CustomTable::rowCounts('cargo', 'delivered', $model_filter->row_weight, $model_filter->row_volume, $model_filter->row_volume_weight, $model_filter->row_cartons);
			                        $model_filter->row_weight = 0;
			                        $model_filter->row_volume = 0;
			                        $model_filter->row_volume_weight = 0;
			                        $model_filter->row_cartons = 0;
							    	return $html;
			                    }
					        }
					    },
				        'columns' => [
				            [
				            	'attribute' => 'check',
					            'label' => '<input type="checkbox" class="checkbox-table-all" value="">',
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
					            "format" => 'raw',
					            'value' => function ($data) use ($model_filter){
					                return '<input type="checkbox" class="checkbox-table checkbox-table-select-rows-'.$model_filter->row_count.'" value="'.$data->cargo_id.'"> <i class="far fa-eye eye-click-view-row" title="View" data-key="'.$data->cargo_id.'"></i><a href="/cargo/edit/'.$data->cargo_id.'" class="table-edit-btn"><i class="fas fa-pencil-alt"></i></a>'; 
					            },
					        ],
				            [
				            	'attribute' => 'cargo_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['cargo_id'], 'cargo_id', 'text', 'Write CC'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','cargo_id')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','cargo_id')],
					            'value' => function ($data) {
					                return 'CC-'.$data->cargo_id; 
					            },
					        ],
					        [
                                'attribute' => 'photo',
                                'label' => 'Photo',
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','photos').CustomTable::widthTable(50)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','photos').CustomTable::widthTable(50).'text-align:center;'],
                                'format' => 'raw',
                                'value' => function ($data) {
                                	if(!empty($data->photos)){
										$photos_all = explode(",", $data->photos);
										$data->photos = '';
										foreach ($photos_all as $photo) {
											if(!empty($photo))
												$data->photos .= '<a href="'.$photo.'" target="_blank"><i class="fas fa-camera-retro"></i></a> ';
										}
									}
                                    return (!empty($data->photos)) ? $data->photos : '';
                                },
                            ],
				            [
				            	'attribute' => 'date_received',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['date_received'], 'date_received', 'date', 'Select date'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','date_received').CustomTable::widthTable(120)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','date_received').CustomTable::widthTable(120)],
					            'value' => function ($data) {
					                return (!empty($data->date_received)) ? date('d.m.Y',$data->date_received) : ''; 
					            },
					        ],
				            [
				            	'attribute' => 'client_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['client_id'], 'client_id', 'text', 'Write client id'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','client_id')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','client_id')],
					            'value' => function ($data) {
					                return (!empty($data->client_id)) ? '88-'.$data->client_id : '';
					            },
					        ],
				            [
				            	'attribute' => 'manager_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['manager_id'], 'manager_id', 'select', 'Select Manager'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','manager_id').CustomTable::widthTable(140)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','manager_id').CustomTable::widthTable(140)],
					            'value' => function ($data) {
					                return $data->manager->username ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'create_manager_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['create_manager_id'], 'create_manager_id', 'select', 'Select Manager'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','create_manager_id').CustomTable::widthTable(140)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','create_manager_id').CustomTable::widthTable(140)],
					            'value' => function ($data) {
					                return $data->managerCreate->username ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'warehouse_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['warehouse_id'], 'warehouse_id', 'select', 'Select checkpoint'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','warehouse_id').CustomTable::widthTable(150)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','warehouse_id').CustomTable::widthTable(150)],
					            'value' => function ($data) {
					                return $data->warehouse->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'shipment_type_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['shipment_type_id'], 'shipment_type_id', 'select', 'Select shipment type'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','shipment_type_id').CustomTable::widthTable(120)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','shipment_type_id').CustomTable::widthTable(120)],
					            'value' => function ($data) {
					                return $data->shipmentType->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'status_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['status_id'], 'status_id', 'select', 'Select status'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','status_id').CustomTable::widthTable(115)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','status_id').CustomTable::widthTable(115)],
					            'value' => function ($data) {
					                return $data->cargoStatus->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'weight',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['weight'], 'weight', 'text', 'Write weight'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','weight')],
				            	'contentOptions' => function ($data) {
					                return ['style' => CustomTable::rowVisible('cargo','delivered','weight'), 'class'=>'data-weight-'.$data->cargo_id]; 
					            },
					            'value' => function ($data) {
					                return $data->weight; 
					            },
					        ],
				            [
				            	'attribute' => 'volume',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['volume'], 'volume', 'text', 'Write volume'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','volume')],
				            	'contentOptions' => function ($data) {
					                return ['style' => CustomTable::rowVisible('cargo','delivered','volume'), 'class'=>'data-volume-'.$data->cargo_id]; 
					            },
					            'value' => function ($data) {
					                return $data->volume ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'volume_weight',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['volume_weight'], 'volume_weight', 'text', 'Write volume-weight'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','volume_weight').CustomTable::widthTable(120)],
				                'contentOptions' => function($model){
				                    return ['style' => (((!empty($model->weight)) && (!empty($model->volume)) && ($model->weight / $model->volume) < 167) ? 'background-color:red;'.CustomTable::rowVisible('cargo','delivered','volume_weight') : CustomTable::rowVisible('cargo','delivered','volume_weight')).CustomTable::widthTable(120), 'class'=>'data-volume_weight-'.$model->cargo_id];
				                },
					            'value' => function ($data) {
					                return $data->volume*167 ?? 0; 
					            },
					        ],
				            [
				            	'attribute' => 'cartons',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['cartons'], 'cartons', 'text', 'Write cartons'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','cartons')],
				            	'contentOptions' => function ($data) {
					                return ['style' => CustomTable::rowVisible('cargo','delivered','cartons'), 'class'=>'data-cartons-'.$data->cargo_id]; 
					            },
					            'value' => function ($data) {
					                return $data->cartons ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'count_inside',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['count_inside'], 'count_inside', 'text', 'Write qty inside'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','count_inside')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','count_inside')],
					            'value' => function ($data) {
					                return $data->count_inside ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'type_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['type_id'], 'type_id', 'select', 'Select type'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','type_id')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','type_id')],
					            'value' => function ($data) {
					                return $data->type->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'type_special_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['type_special_id'], 'type_special_id', 'select', 'Select special type'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','type_special_id')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','type_special_id')],
					            'value' => function ($data) {
					                return Cargo::special_type($data->type_special_id); 
					            },
					        ],
				            [
				            	'attribute' => 'project_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['project_id'], 'project_id', 'text', 'Write project id'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','project_id')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','project_id')],
					            'value' => function ($data) {
					                return (!empty($data->project_id)) ? 'PJ-'.$data->project_id : '';
					            },
					        ],
				            [
				            	'attribute' => 'shipment_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['shipment_id'], 'shipment_id', 'text', 'Write shipment id'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','shipment_id').CustomTable::widthTable(150)],
					            'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','shipment_id').'overflow-x: auto;'.CustomTable::widthTable(150)],
					            'value' => function ($data) {
                                        return ((!empty($data->shipment_id)) ? 'SH-'.$data->shipment_id : '').((!empty($data->shipment->name)) ? '-'.$data->shipment->name : '');
					            },
					        ],
				            [
				            	'attribute' => 'local_delivery_price',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['local_delivery_price'], 'local_delivery_price', 'text', 'Write price'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','local_delivery_price').CustomTable::widthTable(145)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','local_delivery_price').CustomTable::widthTable(145)],
					            'value' => function ($data) {
					                return ($data->local_delivery_price ?? '0.00').' ¥'; 
					            },
					        ],
					        [
				            	'attribute' => 'Local tracking',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['local_track'], 'local_track', 'text', 'Write tracking'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','local_track').CustomTable::widthTable(200)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','local_track').CustomTable::widthTable(200)],
					            'value' => function ($data) {
					                return $data->local_track ?? ''; 
					            },
					        ],
					        [
				            	'attribute' => 'Cargo description',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['description'], 'description', 'text', 'Write description'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','description').CustomTable::widthTable(600)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','delivered','description').CustomTable::widthTable(600).'overflow-x: auto;'],
					            'value' => function ($data) {
					                return $data->description ?? ''; 
					            },
					        ],
				        ],
				    ]); ?>