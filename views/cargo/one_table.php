<tr class="cargo-row" data-key="<?= (!empty($cargo->cargo_id)) ? $cargo->cargo_id : '' ?>">
    <td><input type="checkbox" class="checkbox-table" value="<?= (!empty($cargo->cargo_id)) ? $cargo->cargo_id : '' ?>"> <i class="far fa-eye eye-click-view-row" title="View" data-key="<?= (!empty($cargo->cargo_id)) ? $cargo->cargo_id : '' ?>"></i></td>
    <td><?= (!empty($cargo->cargo_id)) ? 'CC-'.$cargo->cargo_id : '' ?></td>
    <td><?= (!empty($cargo->date_received)) ? date('d.m.Y', $cargo->date_received) : '' ?></td>
    <td><?= (!empty($cargo->client_id)) ? '88-'.$cargo->client_id : '' ?></td>
    <td><?= (!empty($cargo->manager->username)) ? $cargo->manager->username : '' ?></td>
    <td><?= (!empty($cargo->project_id)) ? 'PRJ-'.$cargo->project_id : '' ?></td>
    <td><?= (!empty($cargo->shipment_id)) ? 'SH-'.$cargo->shipment_id : '' ?></td>
    <td><?= (!empty($cargo->shipmentType->name)) ? $cargo->shipmentType->name : '' ?></td>
    <td><?= (!empty($cargo->type->name)) ? $cargo->type->name : '' ?></td>
    <td><?= (!empty($cargo->cargoStatus->name)) ? $cargo->cargoStatus->name : '' ?></td>
    <td><?= (!empty($cargo->weight)) ? $cargo->weight : 0 ?></td>
    <td><?= (!empty($cargo->volume)) ? $cargo->volume : 0 ?></td>
    <td style="<?= (!empty($cargo->weight) && !empty($cargo->volume) && ($cargo->weight / $cargo->volume) < 167) ? 'background-color: red;' : '' ?>"><?= (!empty($cargo->volume)) ? $cargo->volume*167 : 0 ?></td>
    <td><?= (!empty($cargo->cartons)) ? $cargo->cartons : 0 ?></td>
    <td><?= (!empty($cargo->count_inside)) ? $cargo->count_inside : 0 ?></td>
    <td class="table-description"><?= (!empty($cargo->description)) ? $cargo->description : '' ?></td>
    <td><?= (!empty($cargo->local_track)) ? $cargo->local_track : '' ?></td>
</tr>