<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cargo */

$this->title = $model->cargo_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cargos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cargo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->cargo_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->cargo_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cargo_id',
            'shipment_id',
            'date_add',
            'date_received',
            'from_id',
            'to_id',
            'date_given_to_client',
            'delivery_time:datetime',
            'client_id',
            'manager_id',
            'project_id',
            'warehouse_id',
            'shipment_type_id',
            'status_id',
            'weight',
            'volume',
            'cartons',
            'volume_weight',
            'count_inside',
            'local_track:ntext',
            'local_delivery_price',
            'track:ntext',
            'type_id',
            'status',
            'description:ntext',
            'photos:ntext',
            'invoice:ntext',
            'file:ntext',
            'site',
        ],
    ]) ?>

</div>
