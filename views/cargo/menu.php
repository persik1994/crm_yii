<div class="options">

        <div class="choose-country d-sm-none d-md-none d-lg-none d-xl-none" id="country-click">
            <a href="#" class="choose-country-click">Choose Warehouse <span><i class="fas fa-caret-right"></i></span></a>
            <ul id="sm-countries">
                <?php if(Yii::$app->user->identity->id != 50){ ?>
                    <li><a href="/cargo/warehouse/40"><img src="/web/img/chine-flag.png" alt="">Beijing Warehouse</a></li>
                    <li><a href="/cargo/warehouse/39"><img src="/web/img/chine-flag.png" alt="">Shanghai Warehouse</a></li>
                    <li><a href="/cargo/warehouse/10"><img src="/web/img/chine-flag.png" alt="">Foshan1 Warehouse</a></li>
                    <?php if(Yii::$app->user->identity->role != 4 || Yii::$app->user->identity->id == 38) {?>
                      <li><a href="/cargo/warehouse/11"><img src="/web/img/chine-flag.png" alt="">Foshan2 Warehouse</a></li>
                    <?php } ?>
                    <li><a href="/cargo/warehouse/13"><img src="/web/img/hongkong-flag.png" alt="">Hong Kong Warehouse</a></li>
                    <li><a href="/cargo/warehouse/37"><img src="/web/img/usa-flag.png" alt="">USA Warehouse</a></li>
                    <li><a href="/cargo/warehouse/41"><img src="/web/img/poland-flag.png" alt="">Chelm Warehouse</a></li>
                    <li><a href="/cargo/warehouse/32"><img src="/web/img/ukraine-flag.png" alt="">Kiev Warehouse</a></li>
                    <li><a href="/cargo/warehouse/45"><img src="/web/img/ukraine-flag.png" alt="">Kiev Vidubychi Warehouse</a></li>
                <?php } ?>
                <?php if(Yii::$app->user->identity->id == 50){ ?>
                    <li><a href="/cargo/warehouse/45"><img src="/web/img/ukraine-flag.png" alt="">Kiev Vidubychi Warehouse</a></li>
                <?php } ?>
            </ul>
        </div>

        <div class="choose-country choose-info d-sm-none d-md-none d-lg-none d-xl-none" id="info-click">
            <a href="#" class="choose-country-click"><span><i class="fas fa-caret-left"></i></span>Cargos</a>
            <ul id="sm-info">
                <?php if(Yii::$app->user->identity->id != 50){ ?>
                    <li><a href="/cargo/index"><i class="fa fa-list" aria-hidden="true"></i>All Cargos</a></li>
                    <li><a href="/cargo/filter/5"><i class="fa fa-filter" aria-hidden="true"></i>Export process</a></li>
                    <li><a href="/cargo/filter/6"><i class="fa fa-filter" aria-hidden="true"></i>In transit</a></li>
                    <li><a href="/cargo/filter/7"><i class="fa fa-filter" aria-hidden="true"></i>Import process</a></li>
                    <li><a href="/cargo/delivered/11"><i class="fa fa-filter" aria-hidden="true"></i>Delivered</a></li>
                    <li><a href="/cargo/create" class=""><i class="fa fa-plus" aria-hidden="true"></i>Add Cargo</a></li>
                <?php } ?>
            </ul>
        </div>

        <ul id="countries">
            <?php if(Yii::$app->user->identity->id != 50){ ?>
            <li class="dropdown-menu-block" data-dropdown_class="dropdown-menu-china">
                <a class="dropdown-menu-china-btn"><img src="/web/img/chine-flag.png" alt="">China Mainland Warehouse</a>
                <div class="dropdown-menu dropdown-menu-china" x-placement="top-start" style="display: none;">
                  <!-- <a class="dropdown-item" href="/cargo/warehouse/35"><i class="fa fa-list" aria-hidden="true"></i>Shenzhen Warehouse</a>
                  <a class="dropdown-item" href="/cargo/warehouse/12"><i class="fa fa-list" aria-hidden="true"></i>Guangzhou Warehouse</a> -->
                  <a class="dropdown-item" href="/cargo/warehouse/40"><i class="fa fa-list" aria-hidden="true"></i>Beijing Warehouse</a>
                  <a class="dropdown-item" href="/cargo/warehouse/39"><i class="fa fa-list" aria-hidden="true"></i>Shanghai Warehouse</a>
                  <?php if(Yii::$app->user->identity->id != 52) {?>
                  <a class="dropdown-item" href="/cargo/warehouse/10"><i class="fa fa-list" aria-hidden="true"></i>Foshan1 Warehouse</a>
                  <?php } ?>
                  <?php if(Yii::$app->user->identity->role != 4 || Yii::$app->user->identity->id == 52 || Yii::$app->user->identity->id == 38) {?>
                    <a class="dropdown-item" href="/cargo/warehouse/11"><i class="fa fa-list" aria-hidden="true"></i>Foshan2 Warehouse</a>
                  <?php } ?>
                </div>
            </li>

            <li><a href="/cargo/warehouse/13"><img src="/web/img/hongkong-flag.png" alt="">Hong Kong Warehouse</a></li>
            <li><a href="/cargo/warehouse/37"><img src="/web/img/usa-flag.png" alt="">USA Warehouse</a></li>
            <li><a href="/cargo/warehouse/41"><img src="/web/img/poland-flag.png" alt="">Chelm Warehouse</a></li>
            <li class="dropdown-menu-block" data-dropdown_class="dropdown-menu-kiev">
                <a class="dropdown-menu-kiev-btn"><img src="/web/img/ukraine-flag.png" alt="">Kiev Warehouse</a>
                <div class="dropdown-menu dropdown-menu-kiev" x-placement="top-start" style="display: none;">
                  <a class="dropdown-item" href="/cargo/warehouse/32"><i class="fa fa-list" aria-hidden="true"></i>Kiev Warehouse</a>
                  <a class="dropdown-item" href="/cargo/warehouse/45"><i class="fa fa-list" aria-hidden="true"></i>Kiev Vidubychi Warehouse</a>
                </div>
            </li>
            <?php } ?>
            <?php if(Yii::$app->user->identity->id == 50){ ?>
                <li><a href="/cargo/warehouse/45"><img src="/web/img/ukraine-flag.png" alt="">Kiev Vidubychi Warehouse</a></li>
            <?php } ?>
        </ul>

        <ul id="cargo-info"><!-- 
            <li class="dropdown-menu-block" data-dropdown_class="dropdown-menu-cargos">
                <a class="dropdown-menu-cargos-btn"><i class="fa fa-list" aria-hidden="true"></i>Cargos</a>
                <div class="dropdown-menu dropdown-menu-cargos" x-placement="top-start" style="display: none;">
                </div>
            </li> -->
            <?php if(Yii::$app->user->identity->id != 50){ ?>
                <li><a href="/cargo/index"><i class="fa fa-list" aria-hidden="true"></i>All Cargos</a></li>
                <li><a href="/cargo/filter/5"><i class="fa fa-filter" aria-hidden="true"></i>Export process</a></li>
                <li><a href="/cargo/filter/6"><i class="fa fa-filter" aria-hidden="true"></i>In transit</a></li>
                <li><a href="/cargo/filter/7"><i class="fa fa-filter" aria-hidden="true"></i>Import process</a></li>
                <li><a href="/cargo/delivered/11"><i class="fa fa-filter" aria-hidden="true"></i>Delivered</a></li>
                <li><a href="/cargo/create" class="btn-add-client"><i class="fa fa-plus" aria-hidden="true"></i>Add Cargo</a></li>
            <?php } ?>
        </ul>
    </div>