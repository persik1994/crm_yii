<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Cargo;
use app\models\Stock;
use app\models\CustomTable;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
if(!empty($id)){
	$stock = Stock::findOne(['stock_id'=>(int)$id]);
	if(isset($stock)){
		$this->title = $stock->name;
	}
}

?>

    <?=$menu?>

    <style type="text/css">
    	.table-bordered tbody{
		  display:block;
		  overflow:auto;
		  height:200px;
		  width:100%;
		  overflow-x: hidden;
		}
		.table-bordered thead tr{
		  display:block;
		}
    </style>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        <?=$this->title?> * 
                        <a class="edit-packets-slect-cargos" data-toggle="modal" data-target="#modalPacketEdits" style="display: none;"><img src="/web/img/pencil.png" alt="">Edit select cargos</a> 
                        
                        <?= Cargo::SearchShowBtn(1, (int)$id) ?>
                    </div>
                    <!-- <div class="sorting">
                        <div class="sorting-title">search</div>
                        <div class="picked-sorting"><input type="text" class="search-filter-table"><a href="#"><img src="/web/img/cancel-icon.png" alt="#"></a></div>
                    </div> -->
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                    <div class="add-new">
                        <div class="dropdown dropdown-table export-xls-cargod-div" style="display: none;">
                        	<a class="export-xls-cargos" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                      <i class="fas fa-file-export"></i>Export select cargos
		                    </a>
		                    <div class="dropdown-menu dropdown-menu-table export-xls-cargos-checked" aria-labelledby="dropdownMenuLink">
							    <a class="export-xls-cargos-checked-btn"><i class="fas fa-file-excel"></i> Export to exel</a><Br>
		                    	<?php 
		                    		$allTypes = array('cargo_id','shipment_id','project_id','from_id',  'to_id','date_received','client_id','manager_id', 'warehouse_id', 'shipment_type_id','status_id',  'weight','volume','cartons','volume_weight', 'count_inside', 'local_track','local_delivery_price','track','type_id', 'description');
		                    		echo '<input type="checkbox" class="select-all-export" value="0">Select All<Br>';
		                    		foreach ($allTypes as $type) {
		                    			$session = Yii::$app->session;
		                    			if($session->has('cargo_export_type') && in_array($type, $session['cargo_export_type'])){
		                    				echo '<input class="select-export" type="checkbox" checked value="'.$type.'">'.Cargo::attributeLabels()[$type].'<Br>';
		                    			}else{
		                    				echo '<input class="select-export" type="checkbox" value="'.$type.'">'.Cargo::attributeLabels()[$type].'<Br>';
		                    			}
		                    		}
		                    	?>
		                    </div>
                        </div>
	                    <div class="dropdown dropdown-table custom-table-div" style="">
	                    	<a class="custom-table-menu" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			                  <i class="fas fa-cog"></i> Custom table
			                </a>
			                <div class="dropdown-menu dropdown-menu-table custom-table-checked" aria-labelledby="dropdownMenuLink">
							    <a class="custom-table-btn"><i class="fas fa-save"></i> Save setting</a><Br>
			                	<?= CustomTable::menuOptions('cargo', 'warehouse') ?>
			                </div>
	                    </div>
                    </div>
                </div>
				<div class="table-wrap">
					<?php
						$model_filter = new Cargo;
						$count_all = Cargo::SearchQuery(1,(int)$id)->count();
					?>
				    <?= GridView::widget([
				        'dataProvider' => $dataProvider,
				        'rowOptions'=>function($model){
					        return ['class' => 'cargo-row contextmenu-row', 'url-edit' => '/cargo/edit/'.$model->cargo_id, 'url-delete' => '/cargo/delete/'.$model->cargo_id];
					    },
					    'layout' => '{items}{summary}',
					    'beforeRow' => function ($cargo) use ($model_filter){
					        if($model_filter->row_shipment_type_id != $cargo->shipment_type_id || 
			                $model_filter->row_shipment_id != $cargo->shipment_id || 
			                $model_filter->row_status_id != $cargo->status_id){
					    		$html = '';
					        	///////////обнуляю сумму і вивожу////////
			                    if($model_filter->row_weight != 0 && 
			                    $model_filter->row_volume != 0 && 
			                    $model_filter->row_volume_weight != 0 && 
			                    $model_filter->row_cartons != 0){
			                        $html .= CustomTable::rowCounts('cargo', 'warehouse', $model_filter->row_weight, $model_filter->row_volume, $model_filter->row_volume_weight, $model_filter->row_cartons);
			                        $model_filter->row_weight = 0;
			                        $model_filter->row_volume = 0;
			                        $model_filter->row_volume_weight = 0;
			                        $model_filter->row_cartons = 0;
			                    }
			                    //////
					            $model_filter->row_weight += $cargo->weight;
						        $model_filter->row_volume += $cargo->volume;
						        $model_filter->row_volume_weight += ($cargo->volume*167);
						        $model_filter->row_cartons += $cargo->cartons;
			                    ///////оновлення змінних/////
			                    $model_filter->row_shipment_type_id = $cargo->shipment_type_id;
			                    $model_filter->row_shipment_id = $cargo->shipment_id;
			                    $model_filter->row_status_id = $cargo->status_id;
			                    //////////////////////////////
			                    $shipment_type = (!empty($cargo->shipmentType->name)) ? $cargo->shipmentType->name : '';
			                    $shipment = (!empty($cargo->shipment_id)) ? 'SH-'.$cargo->shipment_id : '';
			                    $status = (!empty($cargo->cargoStatus->name)) ? $cargo->cargoStatus->name : '';
			                    /////////// вставляємо рядок в таблицю//////
			                    $text = $shipment_type .' > '.$shipment .' > '.$status;
			                    ////////
			                    $model_filter->row_count++;
			                    $model_filter->row_status_count = 1;
			                    return $html.Yii::$app->controller->renderPartial('one_table_row', ['text'=>$text, 'row_count'=>$model_filter->row_count]);
			                }
			                ////////
			                $model_filter->row_weight += $cargo->weight;
				            $model_filter->row_volume += $cargo->volume;
				            $model_filter->row_volume_weight += ($cargo->volume*167);
				            $model_filter->row_cartons += $cargo->cartons;
					    },
					    'afterRow' => function($cargo, $key, $index) use ($model_filter, $count_all){
					    	if($count_all == ($index + 1)){
			                    if($model_filter->row_weight != 0 && 
			                    $model_filter->row_volume != 0 && 
			                    $model_filter->row_volume_weight != 0 && 
			                    $model_filter->row_cartons != 0){
			                    	$html = CustomTable::rowCounts('cargo', 'warehouse', $model_filter->row_weight, $model_filter->row_volume, $model_filter->row_volume_weight, $model_filter->row_cartons);
			                        $model_filter->row_weight = 0;
			                        $model_filter->row_volume = 0;
			                        $model_filter->row_volume_weight = 0;
			                        $model_filter->row_cartons = 0;
							    	return $html;
			                    }
					        }
					    },
				        'columns' => [
				            [
				            	'attribute' => 'check',
					            'label' => '<input type="checkbox" class="checkbox-table-all" value="">',
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
					            "format" => 'raw',
					            'value' => function ($data) use ($model_filter){
					                return '<input type="checkbox" class="checkbox-table checkbox-table-select-rows-'.$model_filter->row_count.'" value="'.$data->cargo_id.'"> <i class="far fa-eye eye-click-view-row" title="View" data-key="'.$data->cargo_id.'"></i><a href="/cargo/edit/'.$data->cargo_id.'" class="table-edit-btn"><i class="fas fa-pencil-alt"></i></a>'; 
					            },
					        ],
				            [
				            	'attribute' => 'cargo_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['cargo_id'], 'cargo_id', 'text', 'Write CC'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','cargo_id')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','cargo_id')],
					            'value' => function ($data) {
					                return 'CC-'.$data->cargo_id; 
					            },
					        ],
					        [
                                'attribute' => 'photo',
                                'label' => 'Photo',
                                'encodeLabel' => false,
                                'class' => 'yii\grid\DataColumn',
                                'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','photos').CustomTable::widthTable(50)],
                                'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','photos').CustomTable::widthTable(50).'text-align:center;'],
                                'format' => 'raw',
                                'value' => function ($data) {
                                	if(!empty($data->photos)){
										$photos_all = explode(",", $data->photos);
										$data->photos = '';
										foreach ($photos_all as $photo) {
											if(!empty($photo))
												$data->photos .= '<a href="'.$photo.'" target="_blank"><i class="fas fa-camera-retro"></i></a> ';
										}
									}
                                    return (!empty($data->photos)) ? $data->photos : '';
                                },
                            ],
				            [
				            	'attribute' => 'date_received',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['date_received'], 'date_received', 'date', 'Select date'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','date_received').CustomTable::widthTable(120)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','date_received').CustomTable::widthTable(120)],
					            'value' => function ($data) {
					                return (!empty($data->date_received)) ? date('d.m.Y',$data->date_received) : ''; 
					            },
					        ],
				            [
				            	'attribute' => 'client_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['client_id'], 'client_id', 'text', 'Write client id'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','client_id')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','client_id')],
					            'value' => function ($data) {
					                return (!empty($data->client_id)) ? '88-'.$data->client_id : '';
					            },
					        ],
				            [
				            	'attribute' => 'manager_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['manager_id'], 'manager_id', 'select', 'Select Manager'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','manager_id').CustomTable::widthTable(140)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','manager_id').CustomTable::widthTable(140)],
					            'value' => function ($data) {
					                return $data->manager->username ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'create_manager_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['create_manager_id'], 'create_manager_id', 'select', 'Select Manager'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','create_manager_id').CustomTable::widthTable(140)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','create_manager_id').CustomTable::widthTable(140)],
					            'value' => function ($data) {
					                return $data->managerCreate->username ?? '';
					            },
					        ],
				            [
				            	'attribute' => 'warehouse_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['warehouse_id'], 'warehouse_id', 'select', 'Select checkpoint'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','warehouse_id').CustomTable::widthTable(150)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','warehouse_id').CustomTable::widthTable(150)],
					            'value' => function ($data) {
					                return $data->warehouse->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'shipment_type_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['shipment_type_id'], 'shipment_type_id', 'select', 'Select shipment type'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','shipment_type_id').CustomTable::widthTable(120)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','shipment_type_id').CustomTable::widthTable(120)],
					            'value' => function ($data) {
					                return $data->shipmentType->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'status_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['status_id'], 'status_id', 'select', 'Select status'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','status_id').CustomTable::widthTable(115)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','status_id').CustomTable::widthTable(115)],
					            'value' => function ($data) {
					                return $data->cargoStatus->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'weight',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['weight'], 'weight', 'text', 'Write weight'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','weight')],
				            	'contentOptions' => function ($data) {
					                return ['style' => CustomTable::rowVisible('cargo','warehouse','weight'), 'class'=>'data-weight-'.$data->cargo_id]; 
					            },
					            'value' => function ($data) {
					                return $data->weight; 
					            },
					        ],
				            [
				            	'attribute' => 'volume',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['volume'], 'volume', 'text', 'Write volume'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','volume')],
				            	'contentOptions' => function ($data) {
					                return ['style' => CustomTable::rowVisible('cargo','warehouse','volume'), 'class'=>'data-volume-'.$data->cargo_id]; 
					            },
					            'value' => function ($data) {
					                return $data->volume ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'volume_weight',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['volume_weight'], 'volume_weight', 'text', 'Write volume-weight'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','volume_weight').CustomTable::widthTable(120)],
				                'contentOptions' => function($model){
				                    return ['style' => (((!empty($model->weight)) && (!empty($model->volume)) && ($model->weight / $model->volume) < 167) ? 'background-color:red;'.CustomTable::rowVisible('cargo','warehouse','volume_weight') : CustomTable::rowVisible('cargo','warehouse','volume_weight')).CustomTable::widthTable(120), 'class'=>'data-volume_weight-'.$model->cargo_id];
				                },
					            'value' => function ($data) {
					                return $data->volume*167 ?? 0; 
					            },
					        ],
				            [
				            	'attribute' => 'cartons',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['cartons'], 'cartons', 'text', 'Write cartons'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','cartons')],
				            	'contentOptions' => function ($data) {
					                return ['style' => CustomTable::rowVisible('cargo','warehouse','cartons'), 'class'=>'data-cartons-'.$data->cargo_id]; 
					            },
					            'value' => function ($data) {
					                return $data->cartons ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'count_inside',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['count_inside'], 'count_inside', 'text', 'Write qty inside'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','count_inside')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','count_inside')],
					            'value' => function ($data) {
					                return $data->count_inside ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'type_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['type_id'], 'type_id', 'select', 'Select type'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','type_id')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','type_id')],
					            'value' => function ($data) {
					                return $data->type->name ?? ''; 
					            },
					        ],
				            [
				            	'attribute' => 'type_special_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['type_special_id'], 'type_special_id', 'select', 'Select special type'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','type_special_id')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','type_special_id')],
					            'value' => function ($data) {
					                return Cargo::special_type($data->type_special_id); 
					            },
					        ],
				            [
				            	'attribute' => 'project_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['project_id'], 'project_id', 'text', 'Write project id'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','project_id')],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','project_id')],
					            'value' => function ($data) {
					                return (!empty($data->project_id)) ? 'PJ-'.$data->project_id : '';
					            },
					        ],
				            [
				            	'attribute' => 'shipment_id',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['shipment_id'], 'shipment_id', 'text', 'Write shipment id'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','shipment_id').CustomTable::widthTable(150)],
					            'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','shipment_id').'overflow-x: auto;'.CustomTable::widthTable(150)],
					            'value' => function ($data) {
                                        return ((!empty($data->shipment_id)) ? 'SH-'.$data->shipment_id : '').((!empty($data->shipment->name)) ? '-'.$data->shipment->name : '');
					            },
					        ],
				            [
				            	'attribute' => 'local_delivery_price',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['local_delivery_price'], 'local_delivery_price', 'text', 'Write price'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','local_delivery_price').CustomTable::widthTable(145)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','local_delivery_price').CustomTable::widthTable(145)],
					            'value' => function ($data) {
					                return ($data->local_delivery_price ?? '0.00').' ¥'; 
					            },
					        ],
					        [
				            	'attribute' => 'Local tracking',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['local_track'], 'local_track', 'text', 'Write tracking'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','local_track').CustomTable::widthTable(200)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','local_track').CustomTable::widthTable(200)],
					            'value' => function ($data) {
					                return $data->local_track ?? ''; 
					            },
					        ],
					        [
				            	'attribute' => 'Cargo description',
					            'label' => Cargo::cargoDropMenu(Cargo::attributeLabels()['description'], 'description', 'text', 'Write description'),
					            'encodeLabel' => false,
					            'class' => 'yii\grid\DataColumn',
				            	'headerOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','description').CustomTable::widthTable(600)],
				            	'contentOptions' => ['style' => CustomTable::rowVisible('cargo','warehouse','description').CustomTable::widthTable(600).'overflow-x: auto;'],
					            'value' => function ($data) {
					                return $data->description ?? ''; 
					            },
					        ],
				        ],
				    ]); ?>
				</div>
				
				<div class="detail-info-top-line">
                    <div class="detail-info-controls">
                        <a href="#" id="slide-position-left"><i class="fas fa-chevron-left"></i></a>
                        <a href="#" id="slide-position-right"><i class="fas fa-chevron-right"></i></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="" class="cargo-detail-info-url-edit"><i class="fas fa-pencil-alt"></i>Edit</a>
                            <a data-id="" class="delete-post-url" style="display: <?=(Yii::$app->user->identity->role >= 9)?';':'none;'?>"><i class="far fa-trash-alt"></i>Delete</a>
                            <a href="#" id="close-detail-info"><i class="fas fa-times"></i></a>
                        </div>
                    </div>
                </div>

                <div class="detail-info-wrap">
                    <div class="detail-info">
                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">From</div>
                                <div class="detail-info-list-column detail-info-from"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">To</div>
                                <div class="detail-info-list-column detail-info-to"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo date add</div>
                                <div class="detail-info-list-column detail-info-date_add"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo date received</div>
                                <div class="detail-info-list-column detail-info-date_received"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo given to client</div>
                                <div class="detail-info-list-column detail-info-given_to_client"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo delivery time</div>
                                <div class="detail-info-list-column detail-info-delivery_time"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo weight</div>
                                <div class="detail-info-list-column detail-info-weight"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo volume</div>
                                <div class="detail-info-list-column detail-info-volume"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo cartons</div>
                                <div class="detail-info-list-column detail-info-cartons"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo volume weight</div>
                                <div class="detail-info-list-column detail-info-volume_weight"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Qty inside</div>
                                <div class="detail-info-list-column detail-info-inside"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo description</div>
                                <div class="detail-info-list-column detail-info-description"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo type</div>
                                <div class="detail-info-list-column detail-info-type"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Special cargo</div>
                                <div class="detail-info-list-column detail-info-special"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project</div>
                                <div class="detail-info-list-column detail-info-project"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client</div>
                                <div class="detail-info-list-column detail-info-client_id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment</div>
                                <div class="detail-info-list-column detail-info-shipment"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo code</div>
                                <div class="detail-info-list-column detail-info-cargo_id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Local Tracking</div>
                                <div class="detail-info-list-column detail-info-local_tracking"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Local delivery price</div>
                                <div class="detail-info-list-column detail-info-local_delivery_price"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Checkpoint</div>
                                <div class="detail-info-list-column detail-info-warehouse"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo shipment type</div>
                                <div class="detail-info-list-column detail-info-shipment_type"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo status</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo photo</div>
                                <div class="detail-info-list-column detail-info-photo"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo invoice</div>
                                <div class="detail-info-list-column detail-info-invoice"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Other files</div>
                                <div class="detail-info-list-column detail-info-files"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing summ_bottom">
                </div>
                <div class="col-6 summary-selected">
                    <span class="summ-weight"></span>
                    <span class="summ-volume"></span>
                    <span class="summ-volume_weight"></span>
                    <span class="summ-cartons"></span>
                </div>
            </div> 
        </div>
    </section>
    <nav id="context-menu" class="context-menu">
	    <ul class="context-menu__items">
	      <li class="context-menu__item">
	        <a href="#" class="context-menu__link" data-action="View"><i class="fa fa-eye"></i> View</a>
	      </li>
	      <li class="context-menu__item">
	        <a href="#" class="context-menu__link" data-action="Edit"><i class="fa fa-edit"></i> Edit</a>
	      </li>
	      <?php if(Yii::$app->user->identity->role >= 9){ ?>
		      <li class="context-menu__item">
		        <a href="#" class="context-menu__link" data-action="Delete"><i class="fa fa-times"></i> Delete</a>
		      </li>
		  <?php } ?>
	    </ul>
	</nav>

<script src="/web/js/contecst_menu.js"></script>

<!-- Modal -->
<div class="modal fade" id="modalPacketEdits" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit select cargos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?=$FormEditMany?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel edits</button>
        <button type="button" class="btn btn-primary submit-form">Edit cargos</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

    // setInterval(function() {
    //     tableHeight();
    // }, 500);

    // tableHeight();

    // function tableHeight(){
    //     window_height = $(window).height();
    //     summ_height = window_height - ($('.all-cargos-title').height() + $('.all-cargos').height() + $('.options').height() + 70);
    //     if(summ_height >= 600){
    //         $('.table-wrap').css({'max-height' : summ_height+'px'});
    //         $('.table-bordered').css({'max-height' : summ_height+'px'});
    //         $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
    //     }
    //     else if(summ_height < 600){
    //         summ_height = 600;
    //         $('.table-wrap').css({'max-height' : summ_height+'px'});
    //         $('.table-bordered').css({'max-height' : summ_height+'px'});
    //         $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
    //     }
    // }
    setInterval(function() {
        tableHeight();
    }, 500);

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        summ_height = window_height - ($('.all-cargos-title').height() + $('header').height() + $('.options').height() + 50);
        if(summ_height < 400){
        	summ_height = 400;
        }
        $('.table-wrap').css({'max-height' : summ_height+'px'});
        $('.table-bordered').css({'max-height' : summ_height+'px'});
        $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
    }

    $('.mob-menu-Cargo > a').addClass('active');

    $('.summ_bottom').html($('.summary').html());
    $('.summary').html('');


	$(document).on('keyup', '.dropdown-search-input', function (event) {
        if(event.keyCode==13) {
			runSearch($(this).attr("data-type"),$(this).val());
		}
    });

	$(document).on('click', '.search-menu-click', function () {
        runSearch($(this).attr("data-type"),$(this).attr("data-id"));
    });

	$(document).on('change', '#form-choose-date', function () {
        runSearch($(this).attr("data-type"),$(this).val());
    });

    function runSearch(type, value){
            $.ajax({ 
                type: 'POST', 
                url: '/cargo/searchwarehouse/<?=$id?>?'+type+'='+value+'&sort=<?=Yii::$app->request->get('sort')?>', 
                success: function (data) { 
                    $('.all-cargos-content').html(data);
                }
            });
    };

    $(document).on('click', '.checkbox-table-all', function () {
        $('.checkbox-table').not(this).prop('checked', this.checked);
    	allCargosSelectEdits(1);
    });

    $(document).on('click', '.checkbox-table', function () {
    	if(this.checked == "1"){
			$('.cargo-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
		}else{
			$('.cargo-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
		}
        allCargosSelectEdits();
    });

    $(document).on('click', '.checkbox-table-select-rows', function () {
	    $('.checkbox-table-select-rows-'+$(this).attr('data-group')).not(this).prop('checked', this.checked);
	    allCargosSelectEdits(1);
    });

	function allCargosSelectEdits(type = 0){
		var sList = '';
		//////
		var allWeight = 0;
		var allVolume = 0;
		var allVolume_Weight = 0;
		var allCartons = 0;
		///////
		$('.checkbox-table').each(function () {
			if(this.checked == "1"){
				if(type == 1){
					$('.cargo-row[data-key="'+$(this).val()+'"]').addClass('contextmenu-row-hover');
				}
		    	sList += $(this).val() + ",";
		    	////////
		    	allWeight        += parseFloat($('.data-weight-'+$(this).val()).text());
		    	allVolume        += parseFloat($('.data-volume-'+$(this).val()).text());
		    	allVolume_Weight += parseFloat($('.data-volume_weight-'+$(this).val()).text());
		    	allCartons       += parseFloat($('.data-cartons-'+$(this).val()).text());
		    }else if(type == 1){
				$('.cargo-row[data-key="'+$(this).val()+'"]').removeClass('contextmenu-row-hover');
		    }
		});
		/////
		$('.summ-weight').text('Weight = '+allWeight.toFixed(2)+';');
		$('.summ-volume').text('Volume = '+allVolume.toFixed(2)+';');
		$('.summ-volume_weight').text('Volume Weight = '+allVolume_Weight.toFixed(2)+';');
		$('.summ-cartons').text('Cartons = '+allCartons+';');
		/////
		if(sList.trim() == ''){
			$('.edit-packets-slect-cargos').css('display', 'none');
			$('.export-xls-cargod-div').css('display', 'none');
			$('.hidden-select-cargos-all').val(sList);
		}else{
			$('.edit-packets-slect-cargos').css('display', 'inline-table');
			$('.export-xls-cargod-div').css('display', 'inline-table');
			$('.hidden-select-cargos-all').val(sList);
		}
	}

	$('.export-xls-cargos-checked-btn').click(function() {
		var sListType = '';
		$('.export-xls-cargos-checked input').each(function () {
			if(this.checked == "1" && $(this).val() != 0){
		    	sListType += $(this).val() + ",";
		    }
		});
		window.open('<?=Yii::$app->getUrlManager()->getBaseUrl()?>/cargo/exportxls?cargos='+$('.hidden-select-cargos-all').val()+'&type='+sListType, '_blank').focus();
	});

	$('.custom-table-btn').click(function() {
		var sListType = '';
		$('.custom-table-checked input').each(function () {
			if(this.checked == "1"){
		    	sListType += $(this).val() + ",";
		    }
		});
		if(sListType != ''){
			$.ajax({ 
                type: 'GET', 
                url: '/cargo/customtable?page=warehouse&type='+sListType, 
                success: function (data) { 
                    location.reload();
                }
            });
		}
	});

	$(".delete-post-url").click(function() {
		var delete_text = confirm('Do you really want to delete cargo?');
		if(delete_text) window.location.replace('/cargo/delete/'+$(this).attr("data-id"));
	});

	$(document).on('click', '.btn-search-filter-show span', function () {
		if($(this).parent().text().includes('Track = ') > 0) {
			var text = $(this).parent().text();
			$(this).parent().find('span').text('');
			$(this).parent().prepend('<input type="text" data-type="'+$(this).parent().attr('data-type')+'">');
		}
	});

	$(document).on('keydown', '.btn-search-filter-show input', function (e) {
	  if(e.keyCode === 13) {
	  	$.ajax({ 
            type: 'GET', 
            url: '/cargo/setfilter?type='+$(this).attr('data-type')+'&value='+$(this).val(), 
            success: function (data) { 
                location.reload();
            }
        });
	  }
	});

	$('.select-all-export').click(function(){
		$('.select-export').not(this).prop('checked', this.checked);
	});

	//////////прокрутка в низ таблици
	jQuery(function($) {
		var page = 2;
		var status_ajax_loader = 0;
	    $('.table-bordered tbody').on('scroll', function() {
			if(status_ajax_loader == 0){
		        if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
					$('.table-bordered tbody').append('<div class="loader-ajax"><div class="loader-ajax-block"></div><span> Loading...</span></div>');
					status_ajax_loader = 1;
		        	//summ = parseInt($('.summ_bottom b').text());
		        	$.ajax({ 
			            type: 'GET', 
			            url: '/cargo/warehouseajax/<?=$id?>?sort=<?=(!empty($_GET["sort"]))?>&page='+page, 
			            success: function (data) {
			            	if(data != ''){
			            		$('.table-bordered tbody').append(data);
			            		if((page * 100) > $('.summ_bottom b:eq(1)').text()){
			            			summ = $('.summ_bottom b:eq(1)').text();
			            			status_ajax_loader = 1;
			            		}else{
									summ = page * 100;
									status_ajax_loader = 0;
			            		}
			            		$('.summ_bottom b:eq(0)').text('1-'+summ);
			            		$(".loader-ajax").remove();
			            		page++;
			            	}else{
				            	$(".loader-ajax").remove();
				            	status_ajax_loader = 1;
			            	}
			            }
			        });
		        }
		    }
	    })
	});
</script>