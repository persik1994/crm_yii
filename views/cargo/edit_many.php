<div class="add-client-form-line" style="display: none;">
    <label>Cargo date add</label>
    <div class="input-addclient-wrap">
        <input type="text" placeholder="Choose Date" id="form-choose-date-3" class="input-date-add">
        <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
    </div>
</div>

<div class="add-client-form-line" style="display: none;">
    <label>Cargo date received</label>
    <div class="input-addclient-wrap">
        <input type="text" placeholder="Choose Date" id="form-choose-date-1" class="input-date-received">
        <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
    </div>
</div>

<div class="add-client-form-line" style="display: none;">
    <label>Cargo given to client</label>
    <div class="input-addclient-wrap">
        <input type="text" placeholder="Choose Date" id="form-choose-date-2" class="input-date-given">
        <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
    </div>
</div>

<div class="add-client-form-line" style="display: none;">
    <label>Client code</label>
        <select class="select-client selectpicker" name="manager" data-live-search="true">
            <option value="" selected class="placeholder-select">-Select-</option>
            <?php foreach ($clients as $client) { ?>
                <option value="<?=$client->client_id?>">88-<?=$client->client_id?></option>
            <?php } ?>
        </select>
</div>

<div class="add-client-form-line" style="display: none;">
    <label>Manager</label>
    <div class="input-addclient-wrap select-input">
        <select class="select-manager selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true">
            <option value="" selected class="placeholder-select">-Select-</option>
            <?php foreach ($managers as $manager) { ?>
                <option value="<?=$manager->manager_id?>"><?=$manager->username?> - <?=$manager->name?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="add-client-form-line">
    <label>Shipment</label>
    <div class="input-addclient-wrap select-input">
        <select class="select-shipment selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true">
            <option value="" selected class="placeholder-select">-Select-</option>
            <?php foreach ($shipments as $shipment) { ?>
            	<option value="<?=$shipment->shipment_id?>">SH-<?=$shipment->shipment_id?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="add-client-form-line" style="">
    <label>Manager shipment</label>
    <div class="input-addclient-wrap">
        <a class="manager_shipment"><span></span></a>
    </div>
</div>

<div class="add-client-form-line">
    <label>Project</label>
    <div class="input-addclient-wrap select-input">
        <select class="select-project selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true">
            <option value="" selected class="placeholder-select">-Select-</option>
            <?php foreach ($projects as $project) { ?>
                <option value="<?=$project->project_id?>">PJ-<?=$project->project_id?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="add-client-form-line">
    <label>From</label>
    <div class="input-addclient-wrap select-input">
        <select class="select-from" name="manager" onblur="this.style.color = '#000'">
            <option value="" selected class="placeholder-select">-Select-</option>
            <?php foreach ($countries as $country) { ?>
            	<option value="<?=$country->country_id?>"><?=$country->name?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="add-client-form-line">
    <label>To</label>
    <div class="input-addclient-wrap select-input">
        <select class="select-to" name="manager" onblur="this.style.color = '#000'">
            <option value="" selected class="placeholder-select">-Select-</option>
            <?php foreach ($countries as $country) { ?>
            	<option value="<?=$country->country_id?>"><?=$country->name?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="add-client-form-line">
    <label>Checkpoint</label>
    <div class="input-addclient-wrap select-input">
        <select class="select-stock selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true">
            <option value="" selected class="placeholder-select">-Select-</option>
            <?php foreach ($stocks as $stock) { ?>
            	<option value="<?=$stock->stock_id?>"><?=$stock->name?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="add-client-form-line" style="<?=(Yii::$app->user->identity->role == 8) ? 'display:none;' : '' ?>" >
    <label>Cargo shipment type</label>
    <div class="input-addclient-wrap select-input">
        <select class="select-shipment_type" name="manager" onblur="this.style.color = '#000'">
            <option value="" selected class="placeholder-select">-Select-</option>
            <?php foreach ($shipment_types as $shipment_type) { ?>
            	<option value="<?=$shipment_type->type_id?>"><?=$shipment_type->name?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="add-client-form-line">
    <label>Cargo status</label>
    <div class="input-addclient-wrap select-input">
        <select class="select-status" name="manager" onblur="this.style.color = '#000'">
            <option value="" selected class="placeholder-select">-Select-</option>
            <?php foreach ($statuses as $status) { ?>
            	<option value="<?=$status->status_id?>"><?=$status->name?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="add-client-form-line" style="<?=(Yii::$app->user->identity->role == 8) ? 'display:none;' : '' ?>" >
    <label>Cargo type</label>
    <div class="input-addclient-wrap select-input">
        <select class="select-type" name="manager" onblur="this.style.color = '#000'">
            <option value="" selected class="placeholder-select">-Select-</option>
            <?php foreach ($types as $type) { ?>
            	<option value="<?=$type->type_id?>"><?=$type->name?></option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="add-client-form-line" style="display: none;">
    <label>Cargo description</label>
    <div class="input-addclient-wrap">
        <input type="text" class="input-description"> 
    </div>
</div>

<div class="add-client-form-line" style="display: none;">
    <label>Local Tracking</label>
    <div class="input-addclient-wrap">
        <input type="text" class="input-local-tracking">
    </div>
</div>

<input type="hidden" class="hidden-select-cargos-all" value="">

<script type="text/javascript">
    $( ".select-client" ).change(function() {
        $.ajax({ 
            url: '/cargo/clientsearch/'+$('.select-client option:selected').val(), 
            type: 'GET',
            success: function (data) { 
                if(data != 0){
                    $(".select-manager").val(data);
                }
                console.log(data);
            }
        })
    });

    $(document).on('click', '.submit-form', function () {
        var formData = new FormData();
        formData.append('cargos',           $('.hidden-select-cargos-all').val());

        formData.append('date_add',           $('.input-date-add').val());
        formData.append('date_received',      $('.input-date-received').val());
        formData.append('date_given',         $('.input-date-given').val());

        formData.append('client',             $('.select-client option:selected').val());
        formData.append('manager',            $('.select-manager option:selected').val());
        formData.append('shipment',           $('.select-shipment option:selected').val());
        formData.append('project',            $('.select-project option:selected').val());
        formData.append('from',               $('.select-from option:selected').val());
        formData.append('to',                 $('.select-to option:selected').val());
        formData.append('stock',              $('.select-stock option:selected').val());
        formData.append('shipment_type',      $('.select-shipment_type option:selected').val());
        formData.append('status',             $('.select-status option:selected').val());
        formData.append('type',               $('.select-type option:selected').val());

        formData.append('description',        $('.input-description').val());
        formData.append('local_tracking',     $('.input-local-tracking').val());

        $.ajax({ 
            url: '/cargo/editmany',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) { 
                console.log(data);
                if (data == 1) {
                    location.reload();
                }
                else{
                    alert('Ошибка при изменении!');
                    location.reload();
                }
            }
        })
    });

    $(document).on('change', '.select-shipment', function(){
        $.ajax({ 
            url: '/shipments/whatmanager/'+$('.select-shipment option:selected').val(),
            type: 'GET',
            data: {},
            success: function (data) { 
                $('.manager_shipment span').text(data);
            }
        })
    });

    $('.selectpicker').selectpicker('refresh');
</script>