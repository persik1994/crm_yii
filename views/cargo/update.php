<?php

use yii\helpers\Html;
use app\models\Cargo;


/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = 'Edit Сargo';
?>

    <?=$menu?>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">
                <div class="add-client-title">Edit Cargo</div>
            </div>

            <div class="add-client-form-wrap update-cargo-form">
                <form>

                    <div class="top-part row no-fw-row">

                        <div class="col-lg-4 col-md-12">
                            <div class="bottom-part-title">
                                About cargo
                            </div>
                            <div class="add-client-form-line">
                                <label>From <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-from" name="select-from" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option <?= ($country->country_id == $model->from_id) ? 'selected' : '' ?> value="<?=$country->country_id?>"><?=$country->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>To <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-to" name="select-to" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option <?= ($country->country_id == $model->to_id) ? 'selected' : '' ?> value="<?=$country->country_id?>"><?=$country->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <!-- <div class="add-client-form-line">
                                <label>Cargo date add <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date" class="input-date-add" value="<?= (!empty($model->date_add)) ? date('d-M-Y',$model->date_add) : '' ?>" autocomplete="off" name="date-add">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div> -->
                            <div class="add-client-form-line">
                                <label>Cargo date received <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date-1" class="input-date-received" value="<?= (!empty($model->date_received)) ? date('d-M-Y',$model->date_received) : '' ?>" autocomplete="off" name="date-received">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo given to client</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="Choose Date" id="form-choose-date-2" class="input-date-given" value="<?= (!empty($model->date_given_to_client)) ? date('d-M-Y',$model->date_given_to_client) : '' ?>" autocomplete="off">
                                    <span class="input-icon"><i class="far fa-calendar-alt"></i></span>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo delivery time</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" disabled="disabled" class="input-time" value="<?= (isset($model->delivery_time)) ? $model->delivery_time : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo weight <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-weight" value="<?= (!empty($model->weight)) ? $model->weight : '' ?>" name="weight" <?= (Yii::$app->user->identity->role == 8) ? 'disabled' : '' ?> > 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo volume <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-volume" value="<?= (!empty($model->volume)) ? $model->volume : '' ?>" name="volume" <?= (Yii::$app->user->identity->role == 8) ? 'disabled' : '' ?> >
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo cartons <span>*</span></label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-cartons" value="<?= (!empty($model->cartons)) ? $model->cartons : '' ?>" name="cartons" <?= (Yii::$app->user->identity->role == 8) ? 'disabled' : '' ?> >
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo volume weight</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" disabled="disabled" class="input-volume-weight" value="<?= (!empty($model->volume_weight)) ? $model->volume_weight : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Qty inside</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-inside" value="<?= (!empty($model->count_inside)) ? $model->count_inside : '' ?>" name="qty-inside" <?= (Yii::$app->user->identity->role == 8) ? 'disabled' : '' ?> >
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo description</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-description" value="<?= (!empty($model->description)) ? $model->description : '' ?>"> 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo type</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-type" name="manager" onblur="this.style.color = '#000'" <?= (Yii::$app->user->identity->role == 8) ? 'disabled' : '' ?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($types as $type) { ?>
                                            <option <?= ($type->type_id == $model->type_id) ? 'selected' : '' ?> value="<?=$type->type_id?>"><?=$type->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Special cargo</label>
                                <div class="input-addclient-wrap select-input radio-btn-cargo-block">
                                    <?php 
                                        $special_type = Cargo::special_types();
                                        if(isset($special_type)){
                                            foreach ($special_type as $type_key => $type_name) { ?>
                                                <div class="form-check">
                                                  <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="radio-special-cargo" value="<?=$type_key?>" <?=($type_key == $model->type_special_id) ? 'checked' : ''?> <?= (Yii::$app->user->identity->role == 8) ? 'disabled' : '' ?>><?=$type_name?>
                                                  </label>
                                                </div>
                                            <?php }
                                        }
                                    ?>
                                </div>
                            </div>

                            <div class="bottom-part-title">
                                Files cargo
                            </div>

                            <div class="add-client-form-line add-client-form-line-file">
                                <label>Cargo photos</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                    <input type="file" placeholder="" class="file" id='upload-images' multiple="true">
                                    <textarea class="photo-base64" style="display: none;"></textarea>
                                </div>
                            </div>
                            <link href="/web/css/jquery.uploadPreviewer.css" rel="stylesheet" />
                            <script src="/web/js/jquery.uploadPreviewer.js"></script>
                            <script type="text/javascript">
                              $(document).ready(function() {
                                myUploadInput = $("#upload-images").uploadPreviewer();
                              });
                            </script>

                            <?php if(empty($model->photos)){ ?>
                                <div class="gen-img load-file"><img src="" id='preview_avatar' alt="">
                                    <span class="ava-name"></span>
                                    <span class="ava-size"></span>
                                    <span class="close-preview"><i class="fas fa-times"></i></span>
                                </div>
                            <?php } else { 
                                $photos_all = explode(",", $model->photos);
                                foreach ($photos_all as $photo) {
                                    if(!empty($photo)){ ?>
                                        <div class="gen-img load-file" style="height: auto;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhE..." id="preview_avatar" alt=""><i class="far fa-file"></i>
                                            <span class="ava-name"><a href="<?=$photo?>" target="_blank"><?=str_replace('/uploads/image/', '', $photo)?></a></span>
                                            <span class="ava-size"></span>
                                            <span class="close-preview" style="display: inline;"><a href="/cargo/filedelete?type=photos&cargo=<?=$model->cargo_id?>&link=<?=$photo?>"><i class="fas fa-times"></i></a></span>
                                        </div>
                                    <?php }
                                }
                            } ?>
                            <hr>
                            <div class="add-client-form-line add-client-form-line-file">
                                <label>Cargo invoice</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <input type="file" placeholder="" class="file" id="get-invoice-file">
                                    <textarea class="invoice-base64" style="display: none;"></textarea>
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                </div>
                            </div>
                            <?php if(empty($model->invoice)){ ?>
                                <div class="get-invoice-file load-file"><img src="" id='preview_invoice' alt="">
                                    <span class="ava-name"></span>
                                    <span class="ava-size"></span>
                                    <span class="close-preview-invoice"><i class="fas fa-times"></i></span>
                                </div>
                            <?php } else { ?>
                                <div class="get-invoice-file load-file" style="height: auto;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhE..." id="preview_invoice" alt=""><i class="far fa-file"></i>
                                    <span class="ava-name"><a href="<?=$model->invoice?>" target="_blank"><?=str_replace('/uploads/files/', '', $model->invoice)?></a></span>
                                    <span class="ava-size"></span>
                                    <span class="close-preview-invoice" style="display: inline;"><a href="/cargo/filedelete?type=invoice&cargo=<?=$model->cargo_id?>"><i class="fas fa-times"></i></a></span>
                                </div>
                            <?php } ?>
                            <hr>
                            <div class="add-client-form-line add-client-form-line-file">
                                <label>Other files</label>
                                <div class="input-addclient-wrap border-span">
                                    <input type="" placeholder="Select File">
                                    <input type="file" placeholder="" class="file" id="get-other-file">
                                    <textarea class="other-base64" style="display: none;"></textarea>
                                    <span class="input-icon"><i class="fas fa-upload"></i></span>
                                </div>
                            </div>
                            <?php if(empty($model->file)){ ?>
                                <div class="get-other-file load-file"><img src="" id='preview_other' alt="">
                                    <span class="ava-name"></span>
                                    <span class="ava-size"></span>
                                    <span class="close-preview-other"><i class="fas fa-times"></i></span>
                                </div>
                            <?php } else { ?>
                                <div class="get-other-file load-file" style="height: auto;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhE..." id="preview_other" alt=""><i class="far fa-file"></i>
                                    <span class="ava-name"><a href="<?=$model->file?>" target="_blank"><?=str_replace('/uploads/files/', '', $model->file)?></a></span>
                                    <span class="ava-size"></span>
                                    <span class="close-preview-other" style="display: inline;"><a href="/cargo/filedelete?type=file&cargo=<?=$model->cargo_id?>"><i class="fas fa-times"></i></a></span>
                                </div>
                            <?php } ?>
                            <hr>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="bottom-part-title">
                                Options cargo
                            </div>

                            <div class="add-client-form-line">
                                <label>Manager</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-manager selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true" <?= (Yii::$app->user->identity->role == 8) ? 'disabled' : '' ?>> 
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($managers as $manager) { ?>
                                            <option <?= ($manager->manager_id == $model->manager_id) ? 'selected' : '' ?> value="<?=$manager->manager_id?>"><?=$manager->username?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Project</label>
                                <div class="input-addclient-wrap select-input select-input-project">
                                    <select class="select-project selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?= ($model->project_id > 0) ? '<option selected value="'.$model->project_id.'">PJ-'.$model->project_id.'</option>' : '' ?>
                                        <?php foreach ($projects as $project) { ?>
                                            <option <?= ($project->project_id == $model->project_id) ? 'selected' : '' ?> value="<?=$project->project_id?>">PJ-<?=$project->project_id?> <?=$project->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Client</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-client selectpicker" name="manager" data-live-search="true" <?= (Yii::$app->user->identity->role == 8) ? 'disabled' : '' ?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?= ($model->client_id > 0) ? '<option selected value="'.$model->client_id.'">88-'.$model->client_id.'</option>' : '' ?>
                                        <?php foreach ($clients as $client) { ?>
                                            <option <?= ($client->client_id == $model->client_id) ? 'selected' : '' ?> value="<?=$client->client_id?>">88-<?=$client->client_id?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Shipment</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-shipment selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($shipments as $shipment) { ?>
                                            <option <?= ($shipment->shipment_id == $model->shipment_id) ? 'selected' : '' ?> value="<?=$shipment->shipment_id?>">SH-<?=$shipment->shipment_id?>-<?=$shipment->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="bottom-part-title">
                                Tracking information
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo code (CC)</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" disabled="disabled" class="input-code" value="<?= (!empty($model->cargo_id)) ? 'CC-'.$model->cargo_id : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Local Tracking</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-local-tracking" value="<?= (!empty($model->local_track)) ? $model->local_track : '' ?>">
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Local delivery price</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-local-price" value="<?= (!empty($model->local_delivery_price)) ? $model->local_delivery_price : '' ?>" name="local-price"> 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Local Additional Expenses</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" class="input-local-expenses" name="local-expenses" value="<?= (!empty($model->local_expenses)) ? $model->local_expenses : '' ?>" placeholder="Comment additional expenses"> 
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Checkpoint</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-stock selectpicker" name="manager" onblur="this.style.color = '#000'" data-live-search="true">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($stocks as $stock) { ?>
                                            <option <?= ($stock->stock_id == $model->warehouse_id) ? 'selected' : '' ?> value="<?=$stock->stock_id?>"><?=$stock->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo shipment type</label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-shipment_type" name="manager" onblur="this.style.color = '#000'" <?= (Yii::$app->user->identity->role == 8) ? 'disabled' : '' ?>>
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($shipment_types as $shipment_type) { ?>
                                            <option <?= ($shipment_type->type_id == $model->shipment_type_id) ? 'selected' : '' ?> value="<?=$shipment_type->type_id?>"><?=$shipment_type->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo status <span>*</span></label>
                                <div class="input-addclient-wrap select-input">
                                    <select class="select-status" name="status" onblur="this.style.color = '#000'">
                                        <option value="" selected class="placeholder-select">-Select-</option>
                                        <?php foreach ($statuses as $status) { ?>
                                            <option <?= ($status->status_id == $model->status_id) ? 'selected' : '' ?> value="<?=$status->status_id?>"><?=$status->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="add-client-form-line">
                                <label>Cargo QR-CODE <a href="#" onclick="PrintIframe(); return false;">(print)</a></label>
                                <div class="input-addclient-wrap">
                                    <img src="/uploads/qr-code/QR-cargo-<?=$model->cargo_id?>.png"> 
                                    <iframe style="display: none;" name="frame_print_img" src="" class="frame_print_img">
                                    </iframe>
                                </div>
                                <script type="text/javascript">
                                    var $iframe = $('.frame_print_img');
                                    $iframe.ready(function() {
                                        $iframe.contents().find("body").append('<img src="/uploads/qr-code/QR-cargo-<?=$model->cargo_id?>.png">');
                                        $iframe.contents().find("body").append('<p style="font-size: 20px;text-transform: uppercase;font-weight: bold;">Cargo code - <?=$model->cargo_id?></p>');
                                    });
                                    function PrintIframe(){
                                        frames["frame_print_img"].focus();
                                        frames["frame_print_img"].print();
                                    }
                                </script>
                            </div>
                            
                            <div class="bottom-part-title">
                                Local Delivery Ukraine
                            </div>

                            <div class="add-client-form-line">
                                <label>Tracking</label>
                                <div class="input-addclient-wrap">
                                    <input type="text" placeholder="" class="delivery-tracking" value="<?= (!empty($model->track)) ? $model->track : '' ?>"> 
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="bottom-part row no-fw-row">
                        <div class="add-new-form-submit-buttons col-12">
                            <div class="add-new-form-submit-buttons-wrap col-4">
                                <div class="add-new-form-submit-buttons-wrap-width">
                                    <button  class="submit-form">Submit <img src="/web/img/preloader.gif" style="display: none;"></button>
                                    <a href="#" class="reset-form">Reset</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        //RULES FORM
        $(".update-cargo-form form").validate({
          // Specify validation rules
          rules: {
            "select-from": {
                required: true
            },
            "select-to": {
                required: true
            },
            // 'date-add': {
            //     required: true
            // },
            'date-received': {
                required: true
            },

            "weight": {
                required: true,
                number: true
            },
            'volume': {
                required: true,
                number: true
            },
            'cartons': {
                required: true,
                digits: true
            },
            'qty-inside': {
                digits: true
            },
            'local-price': {
                number: true
            },

            'status': {
                required: true
            },
          },
          // Specify validation error messages
          messages: {
            "select-from": {
                required: "Please select",
            },
            "select-to": {
                required: "Please select",
            },
            // "date-add": {
            //     required: "Please select date",
            // },
            "date-received": {
                required: "Please select date",
            },

            "weight": {
                required: "Please enter weight",
                number: "Please replace , by .",
            },
            "volume": {
                required: "Please enter volume",
                number: "Please replace , by .",
            },
            "cartons": {
                required: "Please enter cartons",
                digits: "Please enter an integer",
            },
            "qty-inside": {
                digits: "Please enter an integer",
            },
            "local-price": {
                number: "Please replace , by .",
            },

            "status": {
                required: "Please select status",
            }
          },
          // Make sure the form is submitted to the destination defined
          // in the "action" attribute of the form when valid
          submitHandler: function(form) {
            submitForm();
          }
        });
    
    </script>

    <script type="text/javascript">
        function submitForm(){
                //блок кнопки и прелоадер
                $('.submit-form').find('img').css('display','');
                $('.submit-form').attr('disabled', 'true');
                ///
                var formData = new FormData();
                formData.append('shipment', $('.select-shipment option:selected').val());
                formData.append('from', $('.select-from option:selected').val());
                formData.append('to',            $('.select-to option:selected').val());
                formData.append('client',        $('.select-client option:selected').val());
                formData.append('manager',       $('.select-manager option:selected').val());
                formData.append('project',       $('.select-project option:selected').val());
                formData.append('stock',         $('.select-stock option:selected').val());
                formData.append('shipment_type', $('.select-shipment_type option:selected').val());
                formData.append('status',        $('.select-status option:selected').val());
                formData.append('type',          $('.select-type option:selected').val());
                //////radiobtn////
                formData.append('type_special', $('input[name=radio-special-cargo]:checked').val());
                /////////////////

                // formData.append('date_add',           $('.input-date-add').val());
                formData.append('date_received',      $('.input-date-received').val());
                formData.append('date_given',         $('.input-date-given').val());
                // formData.append('date_add',           $('.input-date-add').val());
                formData.append('time',               $('.input-time').val());
                formData.append('code',               $('.input-code').val());
                formData.append('weight',             $('.input-weight').val());
                formData.append('volume',             $('.input-volume').val());
                formData.append('cartons',            $('.input-cartons').val());
                formData.append('volume_weight',      $('.input-volume-weight').val());
                formData.append('inside',             $('.input-inside').val());
                formData.append('local_tracking',     $('.input-local-tracking').val());
                formData.append('local_price',        $('.input-local-price').val());
                formData.append('local_expenses',     $('.input-local-expenses').val());
                formData.append('description',        $('.input-description').val());
                formData.append('delivery_tracking',  $('.delivery-tracking').val());
                // Attach file
                //formData.append('photos', document.querySelector('#get-img').files[0]); 
                formData.append('invoice', document.querySelector('#get-invoice-file').files[0]);
                formData.append('file', document.querySelector('#get-other-file').files[0]);

                var photosAll = document.querySelector('#upload-images').files;
                for (var photosCount = 0; photosCount < photosAll.length; photosCount++) {
                    formData.append('photos[]', photosAll[photosCount]);
                }

                $.ajax({ 
                    url: '/cargo/edit/<?= $model->cargo_id ?>', 
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    //dataType: 'json',
                    success: function (data) { 
                        console.log(data);
                        if (data == 1) {
                            window.location.replace('<?=Yii::$app->request->referrer?>');
                        }
                    }
                });
        }

        $( ".select-client" ).change(function() {
            select_client();
        });
        select_client();

        function select_client(){
            $.ajax({ 
                url: '/cargo/clientsearch/'+$('.select-client option:selected').val(), 
                type: 'GET',
                success: function (data) { 
                    if(data != 0){
                        $(".select-manager").val(data);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
            $.ajax({ 
                url: '/projects/projectselect/'+$('.select-client option:selected').val(), 
                type: 'GET',
                data:{
                    cargo_id: <?= (isset($model->cargo_id)) ? $model->cargo_id : 0 ?>, 
                },
                success: function (data) { 
                    if(data != ''){
                        $('.select-project').remove();
                        $('.select-input-project').html(data);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
        }

        $('.mob-menu-Cargo > a').addClass('active');
        
        var invoice = new FileReader();
        var other = new FileReader();

        $('.close-preview-invoice').on("click", function() {
            $('#get-invoice-file').val('');
            $('.get-invoice-file .ava-name').text('');
            $('.get-invoice-file .ava-size').text('');
            $("#preview_invoice").attr("src", "");
            $('.get-invoice-file').css('height', '0');
            $('.close-preview-invoice').hide();
            $('.add-client-form-line-file-mt').css('margin-top', '0px');
        });

        $("#get-invoice-file").change(function(event) {

            $('.close-preview-invoice').show();
            var filename = $(this).val().split("\\");
            var fileLen = filename.length - 1;
            $('.get-invoice-file .ava-name').text(filename[fileLen]);

            invoice.onload = function() {
                $('.get-invoice-file').css('height', 'auto');
                var preview = $("#preview_invoice");
                preview.attr("src", invoice.result);
                $('.invoice-base64').val(invoice.result);
            }

            invoice.readAsDataURL(event.target.files[0]);
            var size = event.target.files[0].size / 1000;

            $('.get-invoice-file .ava-size').text(size.toFixed(2) + ' KB');
            $('.add-client-form-line-file-mt').css('margin-top', '0px');

        });

        // $('.close-preview-other').on("click", function() {
        //     $('#get-other-file').val('');
        //     $('.get-other-file .ava-name').text('');
        //     $('.get-other-file .ava-size').text('');
        //     $("#preview_other").attr("src", "");
        //     $('.get-other-file').css('height', '0');
        //     $('.close-preview-other').hide();
        //     $('.add-client-form-line-file-mt').css('margin-top', '0px');
        // });

        // $("#get-other-file").change(function(event) {

        //     $('.close-preview-other').show();
        //     var filename = $(this).val().split("\\");
        //     var fileLen = filename.length - 1;
        //     $('.get-other-file .ava-name').text(filename[fileLen]);

        //     other.onload = function() {
        //         $('.get-other-file').css('height', 'auto');
        //         var preview = $("#preview_other");
        //         preview.attr("src", other.result);
        //         $('.other-base64').val(other.result);
        //     }

        //     other.readAsDataURL(event.target.files[0]);
        //     var size = event.target.files[0].size / 1000;

        //     $('.get-other-file .ava-size').text(size.toFixed(2) + ' KB');
        //     $('.add-client-form-line-file-mt').css('margin-top', '0px');

        // });

        $('.input-volume').change(function(){
            $('.input-volume-weight').val($(this).val() * 167).toFixed(2);
        });
        
        // $(document).keyup(function(e) {
        //     if(e.which == 13){ //enter press
        //         $('.submit-form').trigger('click');
        //     }
        //     if (e.key === "Escape") { 
        //         $('.reset-form').trigger('click');
        //     }
        // });
        $('.reset-form').click(function(){
            location.reload();
        });
    </script>
