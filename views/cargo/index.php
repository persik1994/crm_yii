<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title ?? 'Cargos';
//$this->params['breadcrumbs'][] = $this->title;
?>

    <?=$menu?>

    <style type="text/css">
        .table-bordered tbody{
          display:block;
          overflow:auto;
          height:200px;
          width:100%;
          overflow-x: hidden;
        }
        .table-bordered thead tr{
          display:block;
        }
    </style>

    <section class="all-cargos-content">
        <div class="container-fluid">
            <div class="row no-fw-row">

                <div class="col-sm-6">
                    <div class="all-cargos-title">
                        <?=$title ?? 'Cargos'?> *
                    </div>
                </div>

                <div class="col-sm-6 all-cargos-buttons">
                    <div class="add-new">
                    </div>
                </div>

                <div class="table-wrap">
                    <table class="table table-striped table-bordered table-resizable custom-tab">
                        <thead>
                                <tr>
                                    <!-- <th class="low-w"><i class="far fa-eye"></i></th> -->
                                    <th class="low-w"><input type="checkbox" class="checkbox-table" value=""></th>
                                    <th>Cargo code</th>
                                    <th>Cargo Date received</th>
                                    <th>Client code</th>
                                    <th>Manager</th>
                                    <th>Project</th>
                                    <th>Shipment number</th>
                                    <th>Cargo shipment type</th>
                                    <th>Cargo type</th>
                                    <th>Cargo status</th>
                                    <th>Cargo weight</th>
                                    <th>Cargo volume</th>
                                    <th>Cargo volume weight</th>
                                    <th>Cargo cartons</th>
                                    <th>Qty inside</th>
                                    <th>Cargo description</th>
                                    <th>Local Tracking</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?= $all_table_rows ?>
                            </tbody>
                    </table>
                </div>

                <div class="detail-info-top-line">
                    <div class="detail-info-controls">
                        <a href="#" id="slide-position-left"><i class="fas fa-chevron-left"></i></a>
                        <a href="#" id="slide-position-right"><i class="fas fa-chevron-right"></i></a>
                    </div>

                    <div class="detail-info-right-controls">
                        <div class="detail-info-right-controls-button">
                            <a href="" class="cargo-detail-info-url-edit"><i class="fas fa-pencil-alt"></i>Edit</a>
                            <a data-id="" class="delete-post-url" style="display: <?=(Yii::$app->user->identity->role >= 9)?';':'none;'?>"><i class="far fa-trash-alt"></i>Delete</a>
                            <a href="#" id="close-detail-info"><i class="fas fa-times"></i></a>
                        </div>
                    </div>
                </div>

                <div class="detail-info-wrap">

                    <div class="detail-info">

                        <div class="detail-info-list-wrap">
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Manager</div>
                                <div class="detail-info-list-column detail-info-manager"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo weight</div>
                                <div class="detail-info-list-column detail-info-weight"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo volume</div>
                                <div class="detail-info-list-column detail-info-volume"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo cartons</div>
                                <div class="detail-info-list-column detail-info-cartons"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Qty inside</div>
                                <div class="detail-info-list-column detail-info-inside"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo description</div>
                                <div class="detail-info-list-column detail-info-description"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo date received</div>
                                <div class="detail-info-list-column detail-info-date_received"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo given to client</div>
                                <div class="detail-info-list-column detail-info-given_to_client"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo delivery time</div>
                                <div class="detail-info-list-column detail-info-delivery_time"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo shipment type</div>
                                <div class="detail-info-list-column detail-info-shipment_type"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo status</div>
                                <div class="detail-info-list-column detail-info-status"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo photo</div>
                                <div class="detail-info-list-column type-img detail-info-photo"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo invoice</div>
                                <div class="detail-info-list-column detail-info-invoice"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Other files</div>
                                <div class="detail-info-list-column detail-info-files"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo code</div>
                                <div class="detail-info-list-column detail-info-cargo_id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo warehouse</div>
                                <div class="detail-info-list-column detail-info-warehouse"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Local Tracking</div>
                                <div class="detail-info-list-column detail-info-local_tracking"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Local delivery price</div>
                                <div class="detail-info-list-column detail-info-local_delivery_price"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo volume weight</div>
                                <div class="detail-info-list-column detail-info-volume_weight"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Client code</div>
                                <div class="detail-info-list-column detail-info-client_id"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Shipment</div>
                                <div class="detail-info-list-column detail-info-shipment"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Cargo type</div>
                                <div class="detail-info-list-column detail-info-type"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">From</div>
                                <div class="detail-info-list-column detail-info-from"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">To</div>
                                <div class="detail-info-list-column detail-info-to"></div>
                            </div>
                            <div class="detail-info-list-row">
                                <div class="detail-info-list-column column-grey">Project</div>
                                <div class="detail-info-list-column detail-info-project"></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="row no-fw-row bt-row">
                <div class="col-6 showing">
                    Showing <?=$cargosCount?> of <?=$cargosCount?>
                </div>
                <div class="col-6 summary">
                    <!-- <img src="/web/img/list-icon.png" alt="#"> <a href="#">Show Summary</a> -->
                </div>
            </div>
        </div>
    </section>

<script type="text/javascript">
    setInterval(function() {
        tableHeight();
    }, 500);

    tableHeight();

    function tableHeight(){
        window_height = $(window).height();
        summ_height = window_height - ($('.all-cargos-title').height() + $('.all-cargos').height() + $('.options').height() + 70);
        if(summ_height >= 600){
            $('.table-wrap').css({'max-height' : summ_height+'px'});
            $('.table-bordered').css({'max-height' : summ_height+'px'});
            $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
        }
        else if(summ_height < 600){
            summ_height = 600;
            $('.table-wrap').css({'max-height' : summ_height+'px'});
            $('.table-bordered').css({'max-height' : summ_height+'px'});
            $('.table-bordered tbody').css({'height' : (summ_height-100)+'px'});
        }
    }

    $('.mob-menu-Cargo > a').addClass('active');

    $('.summ_bottom').html($('.summary').html());
    $('.summary').html('');

    $(".delete-post-url").click(function() {
        var delete_text = confirm('Do you really want to delete cargo?');
        if(delete_text) window.location.replace('/cargo/delete/'+$(this).attr("data-id"));
    });
</script>
