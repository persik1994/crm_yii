<?php
namespace app\helpers;
 
class HelperStr
{
    /**
 * ���������� ��������� ������
 *
 * @param int ����� ������
 * @param string [string,int,symbol] ��������� � ���� ����� �������� ������
 * @return string
 */
public static function randomText()
{
    //�������� ���������
    $args_ar = func_get_args();
    $new_arr = array();
 
    //���������� ����� ������
    $length = $args_ar[0];
    unset($args_ar[0]);
 
    if(!sizeof($args_ar))
    {
        $args_ar = array("string","int","symbol");
    }
 
    $arr['string'] = array(
         'a','b','c','d','e','f',
         'g','h','i','j','k','l',
         'm','n','o','p','r','s',
         't','u','v','x','y','z',
         'A','B','C','D','E','F',
         'G','H','I','J','K','L',
         'M','N','O','P','R','S',
         'T','U','V','X','Y','Z');
 
    $arr['int'] = array(
         '1','2','3','4','5','6',
         '7','8','9','0');
 
    $arr['symbol'] = array(
         '.','$','[',']','!','@',
         '*', '+','-','{','}');
 
    //������� ������ �� ���� ��������
    foreach($args_ar as $type)
    {
        if(isset($arr[$type]))
        {
            $new_arr = array_merge($new_arr,$arr[$type]);
        }
    }
 
    // ���������� ������
    $str = "";
    for($i = 0; $i < $length; $i++)
    {
        // ��������� ��������� ������ �������
        $index = rand(0, count($new_arr) - 1);
        $str .= $new_arr[$index];
    }
    return $str;
}
}